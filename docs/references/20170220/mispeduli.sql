-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 19, 2017 at 03:00 PM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mispeduli`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `active` enum('0','1') DEFAULT '1' COMMENT '0=inactive , 1=active',
  `pilar_id` int(11) unsigned NOT NULL,
  `organization_type_id` smallint(5) unsigned NOT NULL,
  `organization_id` int(11) unsigned NOT NULL,
  `activity_type_id` smallint(5) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `activity_name` text,
  `male_direct_beneficiaries` int(11) unsigned DEFAULT NULL,
  `female_direct_beneficiaries` int(11) unsigned DEFAULT NULL,
  `trans_direct_beneficiaries` int(11) unsigned DEFAULT NULL,
  `male_indirect_beneficiaries` int(11) unsigned DEFAULT NULL,
  `female_indirect_beneficiaries` int(11) unsigned DEFAULT NULL,
  `trans_indirect_beneficiaries` int(11) unsigned DEFAULT NULL,
  `male_government_official_beneficiaries` int(11) unsigned DEFAULT NULL,
  `female_government_official_beneficiaries` int(11) unsigned DEFAULT NULL,
  `trans_government_official_beneficiaries` int(11) unsigned DEFAULT NULL,
  `province_id` int(11) unsigned NOT NULL,
  `kabupaten_kota_id` int(11) unsigned NOT NULL,
  `activity_address` text,
  `result_description` text,
  `conversation` text,
  `outcome_codes` varchar(255) DEFAULT NULL COMMENT 'coma separated',
  `output_codes` varchar(255) DEFAULT NULL COMMENT 'coma separated',
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_organization_id` (`organization_id`),
  KEY `fk_activity_pilar_id` (`pilar_id`),
  KEY `fk_activity_province_id` (`province_id`),
  KEY `fk_activity_kabupaten_kota_id` (`kabupaten_kota_id`),
  KEY `fk_activity_type_id` (`activity_type_id`),
  KEY `fk_activity_organization_type_id` (`organization_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=119 ;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `description`, `active`, `pilar_id`, `organization_type_id`, `organization_id`, `activity_type_id`, `start_date`, `end_date`, `activity_name`, `male_direct_beneficiaries`, `female_direct_beneficiaries`, `trans_direct_beneficiaries`, `male_indirect_beneficiaries`, `female_indirect_beneficiaries`, `trans_indirect_beneficiaries`, `male_government_official_beneficiaries`, `female_government_official_beneficiaries`, `trans_government_official_beneficiaries`, `province_id`, `kabupaten_kota_id`, `activity_address`, `result_description`, `conversation`, `outcome_codes`, `output_codes`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, NULL, '1', 1, 2, 11, 8, '2015-03-13', '2015-03-13', 'Rapat Staf Pelaksana Program', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 1, '', 'Rapat staf untuk menyiapkan agenda - agenda pelaksanaan program, terutama untuk mengidentifikasi assesor yang akan melakukan assesment dalam program ini. setelah melakukan identifikasi, rapat memutuskan masing - masing ; \n1. Sdr. Junus Djefri Ukru (Koordinator Jaringan Baileo Maluku).\n2. Sdr. Rony Siwabessy (Fasilitator Jaringan Baileo Maluku) \n3. Lussy Peilouw (Aktivis perempuan yang selama ini dekat dengan suku Nuaulu.', '', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:11:58', 1, '2017-02-08 19:11:58'),
(2, NULL, '1', 1, 2, 11, 8, '2015-04-10', '2015-04-10', 'Diskusi Pehaman Program', 0, 0, 0, 5, 3, 0, 0, 0, 0, 10, 1, 'Kantor AMAN Maluku', 'Diskusi pemahaman program, dilakukan dengan melibatkan staf kantor AMAN dengan para assessor yang akan  melakukan assesment. \nHasil dari pertemuan ini adalah semua staf program memahami isi program dan hasil - hasil yang harus dicapai oleh program.', '', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:11:58', 1, '2017-02-08 19:11:58'),
(3, NULL, '1', 1, 2, 11, 1, '2015-04-17', '2015-04-17', 'Diskusi Alat  Assesment', 0, 0, 0, 3, 2, 0, 0, 0, 0, 10, 1, '', 'AMAN Wilayah Maluku dan para assessor bersepakat untuk diskusi bersama pada tanggal 17 April 2015, melakukan rapat awal untuk menjelaskan tujuan dan out put yang ingin dicapai dari proses assessment yang dilakukan. Selanjutnya masing – masing assessor bresepakat untuk membuat kerangka assessment secara tertulis untuk selanjutnya akan  didiskusikan lagi pada tanggal 01 Mei 2015.', '', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:11:58', 1, '2017-02-08 19:11:58'),
(4, NULL, '1', 1, 2, 11, 1, '2015-05-01', '2015-05-01', 'Diskusi Alat Assasment (Lanjutan)', 0, 0, 0, 3, 2, 0, 0, 0, 0, 10, 1, '', 'Pada tanggal 1 Mei 2015, para assessor kembali berkumpul dan masing – masing mempresentasikan alatnya yang akan dipakai untuk assessment. Dari hasil diskusi tersebut kemudian dilakukan penyatuan terhadap pandangan ketiga assessor. Untuk menyatukan ke – tiga pandangan tersebut maka diberikan tanggungjawab kepada Lussy Peilouw yang akan menyatukannya dan memastikan point – point yang terkait dengan perempuan pada suku Nuaulu.', '', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:11:58', 1, '2017-02-08 19:11:58'),
(5, NULL, '1', 1, 2, 11, 1, '2015-05-12', '2015-05-12', 'Finalisasi Alat Assesment', 0, 0, 0, 3, 2, 0, 0, 0, 0, 10, 1, '', 'Pada tanggal 12 Mei 2015, dilakukan finalisasi alat assessment yang akan digunakan oleh assessor selama proses assessment dikomunitas masing- masing. hasil dari pertemuan ini adalah adanya dokumen yang dijadikan sebagai acuan dalam melakukan assasment.', '', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:11:59', 1, '2017-02-08 19:11:59'),
(6, NULL, '1', 1, 2, 11, 1, '2015-05-17', '2015-05-23', 'Assasment Program di Suku Nuaulu', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', 'Proses assessment diawali dengan proses perekrutan tim assessor,dan memutuskan bersama pengalaman dan kapasitas masing – masing ; \n1.	Lussy Peilouw, S.Pi, MA\n2.	Drs. Junus Djefri Ukru \n3.	Rony Siwabessy, S.Pi \nSelanjutnya AMAN Wilayah Maluku dan para assessor bersepakat untuk diskusi bersama pada tanggal 17 April 2015, melakukan rapat awal untuk menjelaskan tujuan dan out put yang ingin dicapai dari proses assessment yang dilakukan. Selanjutnya masing – masing assessor membuat kerangka assessment secara tertulis untuk didiskusikan lagi pada tanggal 1 Mei 2015. \nPada tanggal 12 Mei 2015, dilakukan finalisasi alat assessment yang akan digunakan oleh assessor selama proses assessment dikounitas masing- masing. \nAsesment akan dilaksanakan di Negeri Administrasi Nuanea, Negeri Administrasi Hatuhenu, kampung Rohua, Kampung Hahuwalang/Latan, Kampung Bonara, Kampung Rahua Waimanesi, Kampung Tawanewane dan kampung Simalouw. Serta pengumpulan bahan – bahan hukum pada Biro Hukum Pemerintah Kabupaten Maluku Tengah. \nAsesment, akan berlangsung dari tanggal  17 s/d 23 Mei  2014. Para assessor berangkat dari Ambon pada tanggal 16 Mei 2013 dan kembali pada tanggal 23 Mei 2013. \n\nPembagian assessor, dengan kampung – kampung yang akan di assessment adalah ; \n\nNo	Nama Assesor 	Lokasi Assesment\n1.	Lussy Peilouw, S.Pi, MA  	Kampung Rohua, Kampung Bonara, Kampung Hahuwalan/Latan dan Biro Hukum Pemerintah Kabupaten Maluku Tengah. \n2. 	Rony Siwaabessy, S.Pi 	Negeri Administrasi Hatuhenu, Kampung Rouhua Waimanesi dan Kampung Simalouw. \n3.	Drs. Junus Djefri Ukru	Negeri Administrasi Nuanea dan Kampung Tawanewane.\n\nDalam assessment, para assessor bertemu langsung dengan raja negeri administrasi, kepala – kepala kampung, tokoh adat, tokoh perempuan dan pemuda. \nAlat assesemnt terlampir dalam laporan narasi ini, sedangkan laporan hasil assessment akan diserahkan kemudian.', '', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:11:59', 1, '2017-02-08 19:11:59'),
(7, NULL, '1', 1, 2, 11, 2, '2015-05-21', '2015-05-23', 'Pendampingan Penulisan Kertas Posisi  Tahap I', 9, 4, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Negeri Nuanea', 'Pendampingan untuk penulisan kertas posisi pada tahap pertama dilakukan di Negeri Nuanea dengan melibatkan anak - anak muda yang ada di negeri Nuanea. Dalam pendampingan pandampingan awal ini, lebih banyak mendiskusikan apa itu hak sosial dasar, dan bagaimana tanggungjawab negara dalam memenuhi hak - hak tersebut. Hal ini bertujuan untuk membantu peserta dalam mengidentifikasi hak - hak dasar yang selama ini dipenuhi oleh negara maupun yang mereka penuhi sendiri dan hak - hak dasar yang sama sekali tidak dipenuhi oleh negara. Pertemuan berlangsung selama kurang lebih 4 jam, 30 menit dan berakhir dengan tugas kepada masing - masing peserta mengidentifikasi masalah pemenuhan hak sosial dasar selama ini untuk nantinya akan dijelaskan pada  pendampingan tahap ke - II. \nTelah disepakati bahwa pendampingan tahap II akan dilakukan pada tanggal 30 Mei 2015 s/d 2 Juni 2015.  \nSetelah selesai pertemuan, tim pendamping dari AMAN Maluku melakukan kunjungan ke Kampung Rohua dan Bonara yang aktivitas mereka sangat dekat dengan negeri Sepa yang memiliki kepentingan untuk tidak ingin melepaskan suku Nuaulu berdiri sendiri sebagai negeri yang otonomom. Dari diskusi tergambar bahwa negeri Sepa telah mengetahui bahwa ada upaya dari Negeri Nuanea dan kampung - kampung suku Nuaulu yang berada di dalam pemerintahan negeri Sepa untuk melepaskan diri dan membentuk pemerintahan adat yang otonom.', 'yasirsani(2015-07-06 05:55:50) says:', 'oc.lbl11, oc.lbl8', 'op.lbl30a, op.lbl22a', 1, '2017-02-08 19:11:59', 1, '2017-02-08 19:11:59'),
(8, NULL, '1', 1, 2, 11, 8, '2015-05-22', '2015-05-22', 'Pertemuan Denga Wakil Bupati', 3, 2, 0, 2, 1, 0, 1, 0, 0, 10, 2, '', 'Pertemuan dilakukan di ruangan kerja Wakil Bupati. Pertemuan di hadiri oleh Utusan suku Nuaulu berjumlah 5 orang, AMAN Wilayah Maluku 2 orang dan 1 orang Dewan AMAN Nasional asal Region Maluku. \nDalam pertemuan Ketua AMAN Wilayah Maluku menjelaskan program yang akan berlangsung selama 18 bulan kedepan dan di responst baik oleh pemerintah kabupaten Maluku dalam hal ini Bupati Maluku Tengah, dengan komitmen untuk membangun kerja sama kedepan, untuk kemajuan masyarakat suku Nuaulu.', '', 'oc.lbl11', 'op.lbl30a', 1, '2017-02-08 19:12:00', 1, '2017-02-08 19:12:00'),
(9, NULL, '1', 1, 2, 11, 2, '2015-05-30', '2015-06-01', 'Pendampingan Penulisan Kertas Posisi Tahap II', 15, 8, 0, 2, 0, 0, 0, 0, 0, 10, 2, 'Negeri Nuanea', 'Pendampingan Tahap II peserta mendiskusikan lebih detail permasalahan hak sosial dasar yang dialami selama ini. proses ini dimulai dengan penjelasan dari masing - masing kampung. \nPada saat pendampingan tahap II, kamu juga melakukan pengambilan titik kordinat setiap komunitas suku Nuaulu di wilayah Seram Selatan, dengan menggunakan GPS.\nPengambilan titik kordinat suku Nuaulu dengan GPS hanya dengan 1 orang peserta yang mengantar, karena ada 3 titik yang harus di ambil di dekat Negeri Sepa yang saat ini masih menguasai Suku Nuaulu, hal tersebut disepakati agar tidak menarik perhatian.', 'amanmaluku(2015-08-01 11:12:22) says: \nyasirsani(2015-07-06 05:56:37) says: dalam pengambilan titi GPS itu apakah dilakukan bersama peserta pelatihan?', 'oc.lbl5, oc.lbl11', 'op.lbl11a, op.lbl30a', 1, '2017-02-08 19:12:00', 1, '2017-02-08 19:12:00'),
(10, NULL, '1', 1, 2, 11, 2, '2015-06-19', '2015-06-21', 'Pendampingan Penulisan Kertas Posisi Tahap III', 5, 4, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Negeri Nuanea', 'Yang terlibat pada pendampingan III dari AMAN wilayah Maluku adalah Yohanes . Y. Balubun dan Charles .B. Litaay. Pada pendampingan ini, kami tidak lagi mengumpulkan anak – anak muda yang terlibat pada saat pendampingan pertama dan kedua. Karena pada saat pendampingan III, hasil dari pendampingan I dan pendampingan II didiskusikan dengan orang – orang tua dan itu dilakukan secara terpisah, karena kami tidak ingin mengganggu aktivitas ekonomi maupuan adat yang mereka lakukan. Pada saat pendampingan III,  sementara dilakukan pembangunan rumah adat marga Pia sehingga mengalami sedikit keterlambatan karena semua tokoh adat yang akan dilibatkan sementara terlibat dalam pembangunan rumah adat marga Pia.', '', 'oc.lbl11, oc.lbl8', 'op.lbl30a, op.lbl22a', 1, '2017-02-08 19:12:00', 1, '2017-02-08 19:12:00'),
(11, NULL, '1', 1, 2, 11, 5, '2015-06-30', '2015-07-01', 'Seminar dan lokakarya', 29, 8, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', 'Dilakukan pada tanggal 30 Juni s/d 1 Juli 2015, bertempat Hotel Lelemuku – Kota Masohi. Kami melaksanakan Seminar dan Lokakarya Penguatan Kapasitas Masyarakat Adat Suku Nuaulu. \nSeminar dan Lokakarya ini, difasilitasi oleh ; (1) Lussy Peilouw dan (2) Yohanes .Y. Balubun. Sedangkan yang menjadi narasumber adalah ; \n1.	Bpk. O. Lawalata, SH, M.Hum; Dosen pada Fakultas Hukum Universtias Pattimura. \n2.	Ny. H. Karapesina, S.Pt, Kepala Sub Bidang DIKPORA, BUDPAR, Kesehatan, Agama, Kesejahteraan Sosial dan Pemberdayaan Masyarakat pada BAPEDA Kabupaten Maluku Tengah.  \n3.	Lusia  Peilouw, S.Pi, MA  dari Aktivis Perempuan dan Pemerhati Masyarakat Suku Nuaulu. \n4.	Yohanes .Y. Balubun, SH dari AMAN Wilayah Maluku.\nSeminar dan lokakarya dihadiri oleh 37 orang peserta yang berasal dari ;\n1.	Negeri Administrasi Nuanea. \n2.	Negeri Administrasi Hatuhenu. \n3.	Kampung Rohua \n4.	Kampung Hahuwalan / Latan.\n5.	Kampung Bonara\n6.	Kampung Simalouw \n7.	Kampung Tawanewane\n8.	Kmapung Rohua Waimanese.\nSeminar dan lokakarya dengan materi – materi yang diberikan merupakan hal baru untuk masyarakat adat suku Nuaulu sehingga sebagian besar waktu dihabiskan untuk berdiskusi dengan ke – empat narasumber.\nPara narasumber dengan materinya masing – masing yaitu ; \n1.	Yohanes .Y. Balubun,SH, dengan judul ; AMAN dan Gerakan Advokasi Masyarakat Adat saat ini.\n2.	Lusia Peilouw, S.Pi, MA, dengan judul ; Suku Nuaulu dari catatan Roy Ellen.\n3.	Bpk. O. Lawalata, SH, MH, dengan judul ; Masyarakat Adat Dalam Tata Hukum Internasional, Nasional dan Daerah. \n4.	Ketua BAPEDA Kab. Maluku Tengah, dengan judul ; Kebijakan Pemerintah Kabupaten Maluku Tengah dalam Rangka Perlindungan dan Penghormatan Hak – Hak Masyarakat Adat di Kabupaten Maluku Tengah.', '', 'oc.lbl11, oc.lbl8', 'op.lbl30a, op.lbl22a', 1, '2017-02-08 19:12:01', 1, '2017-02-08 19:12:01'),
(12, NULL, '1', 1, 2, 11, 2, '2015-07-10', '2015-07-10', 'Hearing Dengan Bupati dan DPRD Kab. Maluku Tengah', 17, 10, 0, 1, 1, 0, 0, 0, 0, 10, 1, 'Masohi - Kabupaten Maluku Tengah', 'Direncanakan tanggal 26 Juni berdasarkan jadwal yang telah kami kirimkan , namun ketika rapat tanggal 24 Juni dengan Tim Assesor maka disepakati bahwa bukan kertas posisi yang harus disampaikan kepada Pemerintah Kabupaten Maluku Tengah, namun Kertas Kebijakan (Police Breff) yang harus disampaikan ke Pemerintah Kabupaten Maluku Tengah. Sebab dari hasil assessment ternyata sumber permasalahan yang menyebabkan terjadinya eksklusi social yang selama ini dialami oleh masyarakat adat suku Nuaulu adalah Kebijakan Pemerintah Kabupaten Maluku Tengah. \nDengan demikian disepakati bahwa Hiering terhadap kertas kebijakan akan dilaksanakan setelah Seminar dan Lokakarya selesai dilaksanakan, karena pada saat lokakarya akan dibahas rancangan kertas kebijakan yang akan disampaikan ke Pemerintah Kabupaten Maluku Tengah. \nSetelah seminar dan lokakarya berakhir, maka disepakati dalam Semiloka yang merupakan RTL adalah menyerahkan Kertas Kebijakan pada tanggal 10 Juli 2015, dengan menghadirkan utusan dari setiap kampung yang terdiri dari 3 orang, yaitu 1 orang utusan tokoh adat, 1 orang utusan perempuan dan 1 orang utusan pemuda. \nTernyata pada tanggal 10 Juli 2015, ketika kami hadir dengan 27 orang peserta dari lokasi program untuk melakukan hearing, terjadi miskomunikasi antara para pejabat di tingkat Kabupaten, sehingga surat yang kami masukan masih berada di dalam ruangan Bupati. \nKami menunggu dari jam 09.00  Wit s/d 14.00 Wit, namun tidak kepasatian akhirnya kami memutuskan untuk bertemu dengan Sekertaris Daerah, tetapi Sekertaris Daerah juga tidak berani untuk melakukn dialog dengna masyarakat suku Nuaulu, dengan berbagai alasannya. \nKami kemudian meninggalkan kantor Bupati dan melakukan rapat singkat dan memutuskan bahwa akan dilakukan pertemuan ulang.', '', 'oc.lbl11, oc.lbl8', 'op.lbl30a, op.lbl22a', 1, '2017-02-08 19:12:01', 1, '2017-02-08 19:12:01'),
(13, NULL, '1', 1, 2, 11, 6, '2015-09-21', '2015-09-24', 'Pelatihan CO', 7, 9, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', 'Proses pelatihan berlangsung  selama 4 hari dan berjalan secara partisipasi dengan melibatkan semua peserta, dengan materi yang diberikan meliputi ; \nPada hari Pertama, peserta terlibat dalama materi – materi ; \n1. Memahami potret masyarakat adat, pada komunitas adat suku Nuaulu. \n2. Memahami posisi masyarakat adat dalam konteks kepentingn negara dan pasar/bisnis. \n3. Serta memahami kebijakan nasional dan internasional yang mengatur tentang masyarakat adat. \n4. Mengenal AMAN, terkait dengan Perjuangan, Prinsip – Prinsip dan Nilai.\n5. Memahami FPIC sebagai salah gerakan politik AMAN.\n6. Memahami pengorganisasian masyarakat adat dan mengorganisir masyarakat adat. \nProses pelatihan sangat partisipasi, dan untuk memahami materi - materi diatas peserta dibawa dalam permainan -permainan yang sederhana.\nPelatihan difasilitasi oleh Drs. Junus Djefri Ukru dan Ronny .S. Siwabessy, S.Pi. \nDari evaluasi dengan fasilitator, ternyata peserta yang ada anak - anak muda Nuaulu memiliki kemampuan yang baik untuk menjadi seorang CO, serta mereka sudah cukup berani untuk tampil dan mulai memfasilitasi diskusi - diskusi selama proses pelatihan. \nMenjadi catatan penting untuk kegiatan selama 4 hari ini adalah ; CO (Community Organizer) atau Pengorganisasian Masyarakat Tidak Hanya Dipelajari, Tetapi Harus Dilakukan dan Dipraktekan Berulang – Ulang.', '', 'oc.lbl11, oc.lbl5', 'op.lbl30a, op.lbl11a', 1, '2017-02-08 19:12:01', 1, '2017-02-08 19:12:01'),
(14, NULL, '1', 1, 2, 11, 6, '2015-09-28', '2015-09-30', 'Pelatihan Perancangan Peraturan Negeri', 21, 15, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', 'Pelatihan Perancangan Peraturan Negeri, dilaksanakan di Negeri Nuanea, dengan melibatkan sebagian masyarakat negeri Nuanea. Hal ini dimaksudkan karena Peraturan Negeri yang akan dihasilkan dari pendampingan di tahun kedua adalah Peraturan Negeri Nuanea, yang di dalamnya melindungi kepentingan suku Nuaulu. \nPelatihan difasilitasi oleh Clife Kissya dan Junus Djefri Ukru, serta Narasumber yang hadir dalam pertemuan adalah Jemmy Pieters, SH, MH untuk menjelaskan terkait dengan dasar hukum dan teknik penyusunan Peraturan Negeri, serta Lussy Peilouw untuk menjelaskan terkait dengan posisi perempuan Nuaulu yang ada di Nuanea dalam perannya terhadap penyusunan peraturan negeri, terutama peraturan negeri yang akan berdampak langsung ke aktivitas perempuan Nuaulu. \nDalam pelatihan perancangan peraturan negeri, sangat terlihat adanya perubahan yang begitu baik dari peserta, terutama peserta perempuan yang sudah mulai memahami posisi mereka dalam wilayah sosial dalam pemenuhan hak - hak sosial dasar dan posisi mereka dalam wilayah adat yang masih harus didiskusikan lebih dalam.', '', 'oc.lbl5, oc.lbl11', 'op.lbl11a, op.lbl30b, op.lbl30a', 1, '2017-02-08 19:12:02', 1, '2017-02-08 19:12:02'),
(15, NULL, '1', 1, 2, 11, 2, '2015-11-19', '2015-11-23', 'Pendampingan Penulisan Peraturan Negeri Tahap I', 6, 1, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Kampung Rohua dan Negeri Nuanea', 'Pendampingan penyusunan peraturan negeri pada  pertama dilakukan dengan berdiskusi bersama tokoh tokoh adat pada kampung Rohua dan Negeri Nuanea. \nPendampingan dilakukan lebih cepat dari jadwal karena pada saat itu Petuanan Negeri Nuaulu terancam dari aktivitas PT. Bintang Lima Makmur. Sehingga selain pendampingan untuk penyiapan peraturan negeri, tetapi juga untuk mendiskusikan langkah- langkah advokasi untuk menyelamatkan hutan adat mereka.', '', 'oc.lbl5, oc.lbl11, oc.lbl8', 'op.lbl11a, op.lbl30a, op.lbl22a', 1, '2017-02-08 19:12:02', 1, '2017-02-08 19:12:02'),
(16, NULL, '1', 1, 2, 11, 6, '2015-12-01', '2015-12-03', 'Pelatihan Analisa Sosial', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', 'Pelatihan Analisa Sosial dilaksanakan pada tanggal 1 – 3 Desember 2015, di Hotel Lelemuku, Kota Masohi. Dalam pelatihan ini, 21 orang peserta dilatih oleh 2 orang trainer yaitu Drs. Junus Djefry Ukru dan Rony Siwabessy, SPi.  \nPeserta difasilitasi untuk belajar tentang 3 alat bantu (tools) Analisa Sosial yaitu:\n1.	Sketsa Desa\n2.	Alur (kecenderungan) Perubahan\n3.	Pohon Masalah – Pohon Tujuan\n3 alat sederhana ini digunakan untuk melakukan analisa terhadap masalah-masalah sosial yang terjadi di kampungnya. \nPenyampaian materi dilakukan secara bertahan dimulai dari penjelasan apa yang dimaksudkan dengan analisa, apa saja masalah sosial, mengapa masalah-masalah sosial dalam masyarakat perlu dilakukan analisa dan bagaimana melakukan analisa itu. Seteah peserta memahami hal-hal umum itu, fasilitator meyampaikan macam-macam alat analisa, dimana untuk pelatihan yang berlangsung selama 3 hari peserta hanya akan diperkenalkan dengan 3 alat bantu analisa. Selanjutnya fasilitator menjelaskan cara kerja setiap alat bantu. Untuk memastikan bahwa peserta memahami bagaimana merea dapat menggunakan setiap alat, dilakukan simulasi yang dirangkai dengan praktek langsung melakukan analisa terhadap 3 issu e utama masyarakat Nuaulu. Metode yang digunakan adalah ceramah - diskusi, simulasi tool dan praktek analisa.', '', 'oc.lbl11', 'op.lbl30a, op.lbl30b', 1, '2017-02-08 19:12:02', 1, '2017-02-08 19:12:02'),
(17, NULL, '1', 1, 2, 11, 2, '2015-12-04', '2015-12-08', 'Pendampingan Penyusunan  Peraturan Negeri Tahap II', 0, 0, 0, 11, 0, 0, 0, 0, 0, 10, 2, 'Kampung Bonara', 'Pendampingan pada tahap kedua dilakukan dengan berdiskusi point - point yang harus dimasukan dalam peraturan negeri dengan tokoh - tokoh adat di kampung Bonara- Negeri Sepa. \nDari diskusi dan pendampingan yang dilakukan memutuskan beberapa point antara lain ; \n1. Perlindungan terhadap hutan suku Naulu yang saat ini terancam dengan aktivitas PT. Bintang Lima Makmur. \n2. Pengelolaan SDA suku Nualu untuk kepentingan masyarakat adat suku Nuaulu. \n3. Pengelolaan SDA harus sesuai adat istiadat di Suku Nuaulu. \n4. Pembahasan harus melibatkan utusan - utusan marga. \n\ndalam pendampingan ini juga membahas dan memutuskan langkah - langkah advokasi perlindungan hutan adat yang saat ini terancam dengan aktivitas PT. Bintang Lima Makmur.', '', 'oc.lbl5', 'op.lbl11a, op.lbl13a', 1, '2017-02-08 19:12:03', 1, '2017-02-08 19:12:03'),
(18, NULL, '1', 1, 2, 11, 2, '2015-12-26', '2015-12-30', 'Pendampingan Penyusunan Peraturan Negeri Tahap III', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', '', '', 'oc.lbl5', 'op.lbl11a', 1, '2017-02-08 19:12:03', 1, '2017-02-08 19:12:03'),
(19, NULL, '1', 1, 2, 11, 2, '2016-01-25', '2016-01-29', 'Pendampingan Penyusunan Dokumen Pendaftaran MA Suku Nuaulu Tahap I', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', '', '', 'oc.lbl8, oc.lbl11', 'op.lbl22a, op.lbl30b', 1, '2017-02-08 19:12:03', 1, '2017-02-08 19:12:03'),
(20, NULL, '1', 1, 2, 11, 2, '2016-02-08', '2016-02-08', 'FGD Tokoh Adat Terkait Penulisan Peraturan Negeri', 15, 1, 0, 1, 1, 0, 0, 0, 0, 10, 2, '', 'FGD yang dilakukan pada tanggal 8 Pebruari 2016, diikuti oleh peserta suku Nuaulu dari Negeri Nua Nea, Kampung Hahuwalan / Latan, Kampung Bonara dan Kampung Rohua. Sedangkan keterwakilan kampung yang lain yaitu kampung Simalouw, Rohua Waimanesi sudah disampaikan tetapi mereka berhalangan untuk hadir. \nDari diskusi tersebut disepakati bahwa Saniri danPemerintah Negeri Nua Nea, dapat menyusun Peraturan Negeri yang mengatur wilayah petuanan Nuaulu. dan disepkati pula dalam Peraturan Negeri harus dirumuskan bahwa Petuanan Negeri Nuanea adalah merupakan wilayah adat milik marga - marga dalam suku Nuaulu.', '', 'oc.lbl5, oc.lbl11', 'op.lbl11a, op.lbl30b', 1, '2017-02-08 19:12:04', 1, '2017-02-08 19:12:04'),
(21, NULL, '1', 1, 2, 11, 2, '2016-03-25', '2016-03-29', 'Pendampingan Penyusunan Peraturan Negeri Tahap VIII', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', '', '', 'oc.lbl5, oc.lbl11, oc.lbl8', 'op.lbl11a, op.lbl30a, op.lbl22a, op.lbl13a', 1, '2017-02-08 19:12:04', 1, '2017-02-08 19:12:04'),
(22, NULL, '1', 1, 2, 11, 2, '2016-07-13', '2016-07-17', 'Pendampingan Penyusunan Peraturan Negeri Tahap XI', 7, 1, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Maluku Tengah', '13 Juli bertemu dengan Sekertaris Neg Nua Nea yang  melaporkan  kejadian tanggal 7 Juni (penahanan 2 org karyawan PT BLM) dan kemudian pertemuan dengan Kapolsek dan Camat Amahai di kantor kecamatan dan serta menyerahkan rekaman pembicaraan dalam pertemuan yang bersifat intimidasi kepada masyarakat suku Nuaulu untuk nantinya akan di teruskan ke Komnas HAM kantor Perwakilan Maluku\n14 Juli ke Dusun Rohua tetapi kepala Dusun tidak berada di tempat dan hanya bertemu dengan Ket Pemuda  Bpk Wata Peirisa.\n15  Juli   melakukan koordinasi dengan kader AMAN  yang tersebar di Suku Nuaulu ,untuk mengetahui kondisi jalan dan situasi keamanan.\nTanggal 16 Juli bertemu dengan Bpk Kepala Suku ,tetapi karena beliau sementara menerima tamu makanya kami ke Rumah anaknya Aharena dan kemudian berdiskusi  bersama Rena dengan suaminya Patty Nahatue, kami menanyakan perkembangan Tim kecil yang dibentuk untuk menyusun peraturan Negeri  ,menurutnya mereka akan melakukan pertemuan untuk membahas kembali  ttg peraturan negeri, konsentrasi mereka untuk penyusunan ini terpecah karena beberapa waktu lalu terjadi masalah antara marga Leapary dan Perusahaan  PT Bintang Lima Makmur,\nvakum dan tidak melakukan pendampingan  sehingga ada beberapa hal yang timbul karena kekosongan tersebut tetapi untungnya masyarakat Nuaulu tanggap terhadap kondisi yang terjadi dan menolak tawaran bantuan tersebut,pembicaraan kami hari itu tentang Program Peduli yang sudah akan selesai pada bulan Agustus ini untuk itu kami mohon kerjasama semua masyarakat yang ada  baik untuk Peraturan negeri maupun penyusunan dokumen untuk duduk sama-sama dan menyelesaikan tanggung jawab ini.\n17 Juli : ke Sepa dan melihat lokasi Perusahaan Bintang Lima Makmur dan mengambil gambar spanduk  peringatan dari kepolisian kepada masyarakat dalam hal ini masyarakat Nuaulu kemudian kembali ke penginapan.', '', 'oc.lbl5, oc.lbl8, oc.lbl11', 'op.lbl11a, op.lbl22a, op.lbl30b', 1, '2017-02-08 19:12:04', 1, '2017-02-08 19:12:04'),
(23, NULL, '1', 1, 2, 11, 2, '2016-07-14', '2016-07-18', 'Pendampingan draft kedua dokumen sejarah', 9, 1, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Dusun Bunara dan Negeri Nuanea', '- Identikasi situasi terkini di komunitas-komunitas dampingan. Terlihat bahwa pada saat program ini vakum beberapa waktu lamanya, masyarakat mengira bahwa program tidak dilanjutkan lagi pasca kepergian almarhum Yanes Balubun. Oleh karenanya pada hari pertama dan kedua dipakai untuk mendiskusikan lagi pelaksanaan program, mengingatkan masyarakat kembali akan tujuan dan output dari program yang harus dicapai bersama. Hari pertama (14 Juli), begitu tiba dari Ambon langsung menuju Bunara untuk berdiskusi Hari kedua di Nuanea. Ini pun sulit sekali bertemu dengan pemerintah negeri karena semua sibuk di kebun dan hutan. BAru dapat bertemu mereka pada sore dan malam hari.\n\n- Selanjutnya di hari ketiga (16 Juli) pergi ke Rouhua. Sayangnya tidak dapat berbicara menyangkut kelanjutan program karena kepala dusun tidak berada di tempat.\n\n- Hari keempat, meminta waktu dari semua pihak pertemuan dengan pemerintah negeri, sore - tengah malam.\n\n- Hari kelima jalan belik ke Ambon.\n\n- Disepakati bahwa pendokumentasian akan dipusatkan di Nuanea. Leadernya diambil alih langsung oleh bapak Raja.\n\n- Rabu (tanggal 20 Juli) akan diadakan pertemuan di kalangan pemerintah negeri dan pemuka adat untuk memulai penuturan sejarah', '', 'oc.lbl11, oc.lbl8', 'op.lbl30b, op.lbl22a', 1, '2017-02-08 19:12:05', 1, '2017-02-08 19:12:05'),
(24, NULL, '1', 1, 2, 11, 2, '2016-07-22', '2016-07-26', 'Diskusi TIm penyusun', 6, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Nuanea', 'Pendampingan tidak maksimal karena kesibukan masyarakat dengan pekerjaan di hujan dan kebun.\nDi periode ini kami melakukan kegiatan-kegiatan lapangan bertepatan dengan musim timr yang membuat kita semua tertantang untuk mengatur jadwal dengan baik', '', NULL, NULL, 1, '2017-02-08 19:12:05', 1, '2017-02-08 19:12:05'),
(25, NULL, '1', 1, 2, 11, 2, '2016-07-29', '2016-08-02', 'Pendampingan dokumentasi 6 marga', 6, 6, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Negeri Nuanea', 'Jika berpatokan pada MIS (rencana kegiatan), pada pendampingan ketiga ini direncanakan akan tuntas dengan pendokumentasian 6 marga. Namun, belum dapat dijalankan oleh karena kesibukan para pemimpin. \n\nkami hanya bisa menemui staf negeri pada malam hari.\n\nDua kali dicoba untuk melakukan pertemuan di malam hari, tidak bisa jalan oleh karena cuaca dan keperluan rumah tangga,\n\nDi minggu ini, kunjungan sangat terhalang oleh cuaca yang buruk dengan hujan yang tak henti-hentinya. PAda hari Sabtu kami tidak bisa memasuki Negeri Nuanea oleh karena banjir. Baru pada hari minggu dan Senin. Ini pun di siang hari kami tidak bisa bertemu dengan pejabat-pejabat negeri yang semuanya berada (bekerja) di hutan, sementara sekretaris negeri berada di Kota Ambon untuk sesuatu urusan penting.\n\nMengisi kekosongan dan gap antara rencana kegiatan dengan implementasinya, kami melakukan pendekatan dengan pemerintah daerah di Masohi (ibukota kabupaen) yaitu dengan wakil bupati, staff dinas sosial  dan kepala bappeda. Memastikan mereka tidak lupa dengan program dan masih ada komitmen yang harus diimplementasikan ke depannya, di pertengahan hingga akhir Agustus.\n\nbelajar dari pengalaman, pendekatan dengan pemda mesti dilakukan setiap kali ada kesempatan, bukan hanya nantinya ketika akan dilakukan kegiatan yang melibatkan mereka secara langsung.', '', 'oc.lbl11, oc.lbl8', 'op.lbl30a, op.lbl22a', 1, '2017-02-08 19:12:05', 1, '2017-02-08 19:12:05'),
(26, NULL, '1', 1, 2, 11, 2, '2016-08-06', '2016-08-11', 'Pendampingan dokumentasi (umum)', 16, 3, 0, 0, 0, 0, 0, 0, 0, 10, 2, 'Nua Nea', 'Pada pendapingan kali ini, terlihat ada progress; \ndimana sudah didapat daftar kampung lama tiap marga yang merupakan bagian terpenting dan titik awal dari sejarah dan mulai terdokumentasikannya sejarah marga Matoke. Rincian persoalan dan perjalanan adalah :\n6 Juli: Setelah tiba di Nuanea, diksusi dengan Raja dan staf pemerintah negeri. 7 Juli : FGD dengn Rena berdasarkan hasil pertemuan malam sebelumnya. \n8 Juli: Diskusi kampung di Bonara untuk marga-marga di sana\n9 Juli : Penuturan sejarah Matoke (elaborasi kampung lama)\n10 Juli: Pertajam sejarah matoke \n11 Juli : Kembali ke Ambon', '', 'oc.lbl11', 'op.lbl30a, op.lbl30b', 1, '2017-02-08 19:12:06', 1, '2017-02-08 19:12:06'),
(27, NULL, '1', 1, 2, 11, 2, '2016-08-16', '2016-08-21', 'Pendampingan - finalisasi dokumen sejarah', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', '', '', 'oc.lbl8', 'op.lbl22a', 1, '2017-02-08 19:12:06', 1, '2017-02-08 19:12:06'),
(28, NULL, '1', 1, 2, 11, 2, '2016-08-24', '2016-08-29', 'pendampingan penyusunan dokumen sejarah', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', '', '', NULL, NULL, 1, '2017-02-08 19:12:07', 1, '2017-02-08 19:12:07'),
(29, NULL, '1', 1, 2, 11, 2, '2016-09-01', '2016-09-05', 'penyelesaian dokumen sejarah', 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 2, '', '', '', NULL, NULL, 1, '2017-02-08 19:12:07', 1, '2017-02-08 19:12:07'),
(30, NULL, '1', 1, 2, 12, 5, '2015-02-05', '2015-02-07', 'Assesment kebutuhan masyarakat adat Talang Mamak dan penyusunan strategi pencapaian', 25, 7, 0, 10, 6, 0, 0, 0, 0, 11, 3, '', 'Kegiatan Assesment ini merupakan kegiatan awal yang menggali semua kebutuhan masyarakat Talang Mamak Langkah Lama dan Talang Mamak Langkah baru, dari hasil assesment diperoleh kesimpulan bahwa Talang Mamak langkah lama membutuhkan pelayanan dasar seperti pendidikan, Kesehatan dan Ekonomi.\ndisamping pengakuan akan hak-hak masyarakat adat Talang Mamak juga penting untuk membuat regulasi yang mengaturnya terutama syarat minimaldanya PERDA Kabupaten Indragiri Hulu tentang pengakuan hak-hak masyarakat adat Talang Mamak.', 'amanriau(2015-07-08 01:21:40) says: pada kontek ini kami sudah berkordinasi bersama beberapa kepala desa untuk menginventaris wilayah adat yang tersisa berupa hutan dan tanah melalui pemetaan partisipatif, untuk PERDA dan PERBUP langkah ini belum banyak progres yang kami lakukan karena dari beberapa pertemuan hanya membahas masalah regulasi ditingkat dasar dulu.\nyasirsani(2015-07-06 05:54:13) says: proses perda apakah tidak bisa dimulai dari perbup dahulu?', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:12:07', 1, '2017-02-08 19:12:07'),
(31, NULL, '1', 1, 2, 12, 8, '2015-03-01', '2015-09-30', 'Kunjungan monitoring dan evaluasi ke lapangan dilakukan 3 kali selama proyek berjalan. Laporan MIS dilakukan setiap bulan memastikan proyek berjalan sesuai perencanaan', 10, 5, 0, 5, 5, 0, 2, 0, 0, 11, 3, 'Talang Mamak', 'Monev kelapangan dilakukan hampir setiap ada kegiatan yang melibatkan direktur dan Koordinator Program.\nhasilnya adalah perlu peningkatan kapasitas sumberdaya manusia terutama pendamping lapangan dan dibutuhkan durasi program yang cukup panjang.', '', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:12:07', 1, '2017-02-08 19:12:07'),
(32, NULL, '1', 1, 2, 12, 1, '2015-04-01', '2015-08-31', 'Pengumpulan dan pendokumentasian hukum adat, wilayah adat dan tata kelola adat Talang Mamak', 40, 20, 0, 1000, 1000, 0, 5, 5, 0, 11, 3, 'Talang Mamak', 'kegiatan ini dilakukan dengan metode diskusi dan wawancara mendalam dengan tokoh adat dan mengumpulkan data-data dokumen dari berbagai referensi baik bersumber dari batin maupun pustaka.\nkegiatan ini berlangsung sepanjang waktu ketika ada pertemuan-pertemuan adat dan kegiatan gawai (pesta adat).\nhasilnya adalah telah terkumpulnya beberapa dokumentasi hukum adat, kearifan lokal dan sejarah asal-usul Talang Mamak.', '', 'oc.lbl8', 'op.lbl19b', 1, '2017-02-08 19:12:08', 1, '2017-02-08 19:12:08'),
(33, NULL, '1', 1, 2, 12, 6, '2015-04-20', '2015-07-31', 'Budidaya tanaman obat dan tanaman penghidupan (pandan) air, bagi perempuan adat Talang Mamak.', 5, 25, 0, 5, 10, 0, 2, 0, 0, 11, 3, 'Talang Mamak', 'Pembentukan kelompok perempuan adat, dan memberikan penyuluhan kepada perempuan adat tentang pentingnya produsen produk terutama bahan baku pembuatan peralatan dan kebutuhan sehari-hari, seperti pandan dsb.\nhasilnya telah teridentifikasinya tanaman obat dan tanaman penghidupan serta terbentknya kelompok perempuan adat yang akan melakukan kegiatan budidaya.', '', 'oc.lbl5', 'op.lbl11a', 1, '2017-02-08 19:12:09', 1, '2017-02-08 19:12:09'),
(34, NULL, '1', 1, 2, 12, 2, '2015-04-30', '2015-07-31', 'Dialog kebijakan tentang Pengakuan Masyarakat Hukum Adat antara pemangku adat dengan kepala desa di tiga desa (kebatinan)', 15, 6, 0, 10, 10, 0, 5, 5, 0, 11, 3, 'Anak Talang', 'telah dilakukan beberapa kali dialog dan diskusi bersama kepala desa anak talang dengan membangun kesepahaman tentang pentingnya regulasi dalam penangan ruang hidup masyarakat.\nhasilnya; telah disepakati bahwa pembuatan draf PERDES akan dilakukan oleh Tim Peduli bersama masyarakat', '', 'oc.lbl8', 'op.lbl20a', 1, '2017-02-08 19:12:09', 1, '2017-02-08 19:12:09'),
(35, NULL, '1', 1, 2, 12, 8, '2015-05-01', '2015-08-31', 'Pendampingan masyarakat adat Talang Mamak dalam mengenal kembali, mempelajari budaya leluhur Talang Mamak (bercurai/petatah petitih) dan mempublikasikan budaya dan kearifan masyarakat tersebut melalui website Talang Mamak', 35, 20, 0, 10, 5, 0, 3, 0, 0, 11, 3, 'Anak Talang dan Ampang Delapan', 'diskusi awal Memberikan pemahaman kepada tokoh adat tentang perlunya menurunkan petatah dan petitih adat kepada anak cucu agar tidak hilang ditelan perkembangan zaman; hasilnya; disetiap kegiatan acara adat atau perkumpulan kebiasaan bercurai (berpetatah-petitih) sudah mulai tumbuh kembali.', '', 'oc.lbl8', 'op.lbl19b', 1, '2017-02-08 19:12:10', 1, '2017-02-08 19:12:10'),
(36, NULL, '1', 1, 2, 12, 1, '2015-05-02', '2015-07-16', 'Diskusi untuk memberikan pemahaman kepada masyarakat Talang Mamak Langkah Lama terkait administrasi pendaftaran penduduk (penulisan agama)', 10, 5, 0, 10, 5, 0, 2, 2, 0, 11, 3, 'Talang Mamak', 'pada tahap awal diawali dengan memberikan pemahaman kepada tokoh adat atau Batin. Dampaknya adalah telah terjadi pertemuan Batin Talang Parit (Irasan) dengan dinas pendudukan catatan sipil (Disdukcapil) membahas tentang administrasi kependudukan seperti akta nikah, akta lahir,KTP dsb.\nHasilnya ; Disdukcapil Kabupaten Indragiri Hulu mengakomodir permintaan Batin adat dengan syarat harus membuat dokumentasi melalui naskah tertulis tentang sejarah asal usul Talang Mamak.', '', 'oc.lbl10', 'op.lbl26a', 1, '2017-02-08 19:12:10', 1, '2017-02-08 19:12:10'),
(37, NULL, '1', 1, 2, 12, 1, '2015-05-02', '2015-07-31', 'Pendampingan masyarakat untuk memperoleh hak-hak dasar seperti pendidikan layak, kesehatan(sanitasi) dan infratruktur yang baik', 10, 5, 0, 1000, 1000, 0, 5, 2, 0, 11, 3, 'Komunitas Talang Mamak', 'Diskusi ini dibangun untuk memberikan kesepahaman antar para pemuka adat Talang Mamak(Batin); hasilnya adalah telah terbangun kesepakatan dan pemahaman bersama tentang pentingnya hak dasar dalam menata kehidupan di Talang Mamak.', '', 'oc.lbl2, oc.lbl10, oc.lbl3', 'op.lbl1c, op.lbl26a, op.lbl3a', 1, '2017-02-08 19:12:11', 1, '2017-02-08 19:12:11'),
(38, NULL, '1', 1, 2, 12, 6, '2015-06-13', '2015-06-13', 'Pelatihan kader pemuda penggerak di tingkat komunitas (pengorganisasian dan community organiser)', 20, 5, 0, 20, 3, 0, 2, 0, 0, 11, 3, 'Pekanbaru-Riau', 'KADER PEDULI; kegiatan ini mendidik pemuda Talang Mamak untuk bergerak dan melakukan perubahan di Talang Mamak, membaca potensi wilayah adalah penekanan yang paling utama.\nkemudian para pemuda juga dituntut untuk mengawal dan menjaga keutuhan wilayah adat Talang Mamak.\nhasilnya yaitu; terbentuknya kader penggerak di Talang Mamak dan dijadikan sebagai Kelompok Kerja (POKJA) atau pemuda adat yang membantu dalam perjuangan Talang Mamak kedepan.', 'yasirsani(2015-07-06 05:53:08) says:', 'oc.lbl8', 'op.lbl22a', 1, '2017-02-08 19:12:11', 1, '2017-02-08 19:12:11'),
(39, NULL, '1', 1, 2, 12, 6, '2015-06-14', '2015-06-14', 'Pembuatan website Talang Mamak', 2, 1, 0, 1000, 1000, 0, 0, 0, 0, 11, 3, 'Pekanbaru-Riau', 'Pembuatan website;  desain dan pembelian domain, adalah salah satu bagian dari pembuatan website yang berguna sebagai wadah pusat informasi yang berkaitan dengan kegiatan dan aktivitas di Talang Mamak, penyebaran informasi melalui media situs sangat penting mengingat pengguna internet dan basis internet sebakin luas di Indonesia. acara ini dilakukan sekaligus dengan lounching website walaupun masih banyak pembenahan-pembenahan yang dilakukan.\nhasilnya adalah telah dibuatnya website talangmamak.com', 'amanriau(2015-06-30 01:48:41) says: nurman(30-06-2015 00:40) says:\nterima kasih mas sani koreksinya, kemarin saya agak kesulitan menentukan ambang batas atas untuk penerima manfaat tidak langsung karena masyarakat Talang Mamak dari data nonspasial lebih dari 1000 jiwa tetapi dikolom yang tersedia hanya 1000 saja.\ncoba cek yang saya kareksi mas sani, jika ada kekurangan jangan sungkan untuk menanyakan.\nyasirsani(2015-06-29 12:54:26) says: Terkait dengan kegiatan website, maka penerima manfaat sebetulnya tidak mencapai 1000 orang. Kegiatan pembuatan website ini hanya dilakukan oleh 1-2 orang dan hanya menjadi penerima manfaat tidak langsung.  Pemanfaat langsung adalah komunitas talang mamak yang tidak terlibat dalam kegiatan pembuatan website. Mohon klarifikasinya? terima kasih', 'oc.lbl3', 'op.lbl3b', 1, '2017-02-08 19:12:11', 1, '2017-02-08 19:12:11'),
(40, NULL, '1', 1, 2, 12, 6, '2015-06-14', '2015-06-14', 'Pelatihan mengelola website Talang Mamak', 20, 11, 0, 1000, 1000, 0, 2, 0, 0, 11, 3, 'Pekanbaru - Riau', 'Pengelolaan Website; kegiatan ini disandingkan dengan pemanfaatan media sosial sebagai alat perjuangan, tingginya pengguna medsos di Indonesia dimanfaatkan sebagai alat untuk perubahan terutama tentang perjuangan masyarakat adat Talang Mamak yang dulu tidak bisa terekspos saat ini dalam hitungan detik sudah bisa diketahui oleh pihak luar.\nhasil dari kader yang dilatih saat ini sudah melakukan kampanye di lapangan termasuk memberikan data, informasi untuk diupload dalam website.', 'amanriau(2015-07-08 01:18:05) says: terima kasih pak sani koreksinya\nyasirsani(2015-07-06 05:52:36) says: tolong dicek terkait dengan data, seharusnya pemafaat langsung tersebut adalah peserta pelatihan. sehingga apakah betul peserta pelatihan berjumlah 1000 orang?', 'oc.lbl8', 'op.lbl19b', 1, '2017-02-08 19:12:12', 1, '2017-02-08 19:12:12'),
(41, NULL, '1', 1, 2, 12, 2, '2015-07-18', '2015-12-22', 'Gelar acara resident art dan publikasi kegiatan Talang Mamak melalui berbagai media', 35, 5, 0, 50, 20, 0, 0, 0, 0, 11, 4, 'Pekanbaru', '- menampilkan beberapa foto komunitas adat Talang mamak dalam ekslusi\n- film-film pendek juga menceritakan bahwa keterpurukan masyarakat adat Talang Mamak khususnya.\n- penonton dan audiensi dapat melihat secara gamlang keadaan masyarakat adat yang masih dalam tekanan.', '', 'oc.lbl10', 'op.lbl27a', 1, '2017-02-08 19:12:12', 1, '2017-02-08 19:12:12'),
(42, NULL, '1', 1, 2, 12, 2, '2015-09-07', '2015-09-07', 'Dialog / hearing ke DPRD Kab. Inhu, mendorong pengakuan masyarakat hukum adat Talang Mamak', 8, 0, 0, 29, 10, 0, 0, 0, 0, 11, 3, 'Kantor DPRD Kab. INHU', 'INHU,7 September 2015. pertemuan dilakukan dikantor DPRD Kab. INHU di ruangan Komisi C yang dihadiri oleh Ketua Komisi C Raja Irwan Toni,SE. sekaligus bersama tokoh adat Talang Mamak dan tim Program Peduli.\nhasil pertemuan menyimpulkan bahwa DPRD INHU melalui komisi C akan berjuang besama-sama masyarakat dalam memperjuangkan regulasi pengakuan hak-hak masyarakat adat Talang Mamak melalui inisiatif PERDA.', '', 'oc.lbl10', 'op.lbl26a', 1, '2017-02-08 19:12:12', 1, '2017-02-08 19:12:12'),
(43, NULL, '1', 1, 2, 12, 8, '2015-09-21', '2015-09-23', 'Kunjungan monitoring dan evaluasi ke lapangan dilakukan 3 kali selama proyek berjalan (Bulan September 2015. Laporan MIS dilakukan setiap bulan memastikan proyek berjalan sesuai perencanaan', 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 3, '', '', '', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:12:12', 1, '2017-02-08 19:12:12'),
(44, NULL, '1', 1, 2, 12, 1, '2015-12-15', '2015-12-17', 'Training Of Change', 10, 1, 0, 0, 0, 0, 0, 0, 0, 11, 4, 'Kota Pekanbaru', 'Dari Hasil diskusi selama 2 (dua) hari,mereview kembali seluruh program dan kegiatan yang dilaksanakan, ada bebrapa rekomendasi yang dapat dijadikan sebagai perbaikan untuk langkah selanjutnya diantaranya adalah perbaikan SDM dalam pengelolaan Program terutama pendamping Lapangan dan staf Finance.\nkomunikasi efektif dalam menjalankan program harus lebih inten.', '', 'oc.lbl3', 'op.lbl3b', 1, '2017-02-08 19:12:13', 1, '2017-02-08 19:12:13'),
(45, NULL, '1', 1, 2, 12, 1, '2015-12-27', '2015-12-29', 'diskusi tanah', 10, 6, 0, 3, 1, 0, 4, 2, 0, 11, 4, '', 'blum ada ksepakatan', 'kemitraanjakarta(2015-12-17 03:23:36) says: \namananaktalang(2015-12-17 03:22:12) says: krna glau\nkemitraanjakarta(2015-12-17 03:20:09) says: kenapa tdk setuju.jgn lupa 5w1h', 'oc.lbl5', 'op.lbl11a', 1, '2017-02-08 19:12:13', 1, '2017-02-08 19:12:13'),
(46, NULL, '1', 1, 2, 12, 8, '2016-01-06', '2016-01-08', 'rapat desa', 10, 5, 0, 0, 0, 0, 3, 4, 0, 11, 3, '', 'sebeum ada rapat ini', 'kemitraanjakarta(2015-12-17 03:07:21) says: \namananaktalang(2015-12-17 03:02:56) says: \nkemitraanjakarta(2015-12-17 03:02:43) says: bagus, tapi berapa nilai anggaran desa utk talang mamak?', 'oc.lbl8', 'op.lbl20a', 1, '2017-02-08 19:12:13', 1, '2017-02-08 19:12:13'),
(47, NULL, '1', 1, 2, 12, 7, '2016-02-26', '2016-02-29', 'Fasilitasi Gawai  dalam rangka menumbuh kembangkan nilai-nilai budaya dan konsolidasi Talang Mamak', 100, 70, 0, 300, 200, 0, 20, 20, 0, 11, 3, 'Komunitas Adat Talang Mamak (Ds. Aur cina)', '-	Menyatukan tokoh adat Talang Mamak dalam helatan gawai adat sebagai salah satu perayaan yang bercampur dengan advokasi.\n-	Penyerahan Peta wilayah adat di 15 komunitas adat Talang Mamak seluas ±195.000 Ha.\n-	Menyuarakan ke pihak luar bahwa Talang Mamak ada dan wajib diperhatikan Pemerintah dalam bentuk pengakuan yang legal.\n-	Mengingatkan kembali keseluruh para batin adat atau pemangku adat atas maklumat gawai gedang tahun 2013.\n-	Menagih RESOLUSI terhadap pemerintah hasil dari gawai gedang tahun 2013', 'kemitraanjakarta(2016-06-06 05:43:07) says: - Menyerahkan peta wilayah adat itu kepada siapa, dan siapa saja yang terlibat?\n\n- Maklumat Gawai sebelumnya itu apa isin ya?\n-  Resolusi yang dimaksud itu seperti apa isinya?\n\nMohon dilengkapi dengan lebih detil.', 'oc.lbl10', 'op.lbl27a', 1, '2017-02-08 19:12:14', 1, '2017-02-08 19:12:14'),
(48, NULL, '1', 1, 2, 12, 1, '2016-03-04', '2016-04-04', 'FGD I; Fasilitasi pembentukan Team para pihak untuk pengawalan dan percepatan PERDA Kekhususan Talang Mamak.', 22, 5, 0, 10, 10, 0, 0, 0, 0, 11, 4, 'Kantor WALHI Riau', 'membangun kesepahaman bersama, tentang perjuangan masyarakat adat dan hutan Riau, mengkasji konflik', 'kemitraanjakarta(2016-06-06 05:40:51) says: Bentuk kesepahamannya apa? Apakah dalam bentuk lisan atau tertulis. Pihak mana saja yang sepaham? Mohon dijelaskan', 'oc.lbl10', 'op.lbl27a', 1, '2017-02-08 19:12:14', 1, '2017-02-08 19:12:14'),
(49, NULL, '1', 1, 2, 12, 1, '2016-03-08', '2016-03-08', 'FGD II; Fasilitasi pembentukan Team para pihak untuk pengawalan dan percepatan PERDA Kekhususan Talang Mamak.', 20, 5, 0, 10, 5, 0, 0, 0, 0, 11, 4, 'Kantor AMAN Riau', 'Membuat nama Koalisi, merencanakan aksi-aksi dan kerja-kerja bersama di Riau.', 'kemitraanjakarta(2016-06-06 05:39:56) says: rangkuman hasil, belum ada penjelasan detilnya. Nama Koalisinya apa, aksi-aksi yang dimaksud apa, kerja-kerja  bersama yang dimaksud apa? Mohon penjelasan lebih rinci', 'oc.lbl10', 'op.lbl27a', 1, '2017-02-08 19:12:14', 1, '2017-02-08 19:12:14'),
(50, NULL, '1', 1, 2, 12, 6, '2016-03-14', '2016-03-16', 'In house training; Pelatihan Manajemen Proyek.', 20, 100, 0, 20, 10, 0, 5, 0, 0, 11, 3, 'Komunitas Adat Talang Mamak', '-	Training juga mensosialisasikan program peduli kepada masyarakat adat baik Talang Mamak maupun yang ada disekitarnya baik transmigrasi maupun para pendatang.\n-	Melatih CO lapang untuk lebih focus pada tujuan program peduli.', 'kemitraanjakarta(2016-06-06 05:38:28) says: Disinipun yang dilaporkan sebagai rangkuman hasil adalah justru kegiatan itu sendiri. Mestinya yang ditulis di kolom tersebut adalah apa yang dihasikan dari pelatihan?...\nTolong diperbaiki', 'oc.lbl3', 'op.lbl3b', 1, '2017-02-08 19:12:14', 1, '2017-02-08 19:12:14'),
(51, NULL, '1', 1, 2, 12, 1, '2016-03-17', '2016-03-17', 'Fasilitasi pembentukan Team para pihak untuk pengawalan dan percepatan PERDA Kekhususan Talang Mamak.', 25, 10, 0, 25, 10, 0, 5, 2, 0, 11, 4, 'LAM Riau', '-	Telah terbentu KOALISI ANTAR Masyarakat adat dan NGO di Riau yang dinamai dengan (Koalisi Masyarakat Adat dan Hutan Riau) KOMA TAN RIAU.\n-	Untuk mengawal Masyarakat adat di Riau dan perebutan regulasi ditingkat provinsi Riau dan mendorong PERDA pengakuan masyarakat adat di tingkat Kabupaten.', 'kemitraanjakarta(2016-06-06 05:37:14) says:', 'oc.lbl8', 'op.lbl22a', 1, '2017-02-08 19:12:15', 1, '2017-02-08 19:12:15'),
(52, NULL, '1', 1, 2, 12, 6, '2016-03-18', '2016-03-18', 'Pelatihan hukum kritis bagi BATIN  adat Talang Mamak dan mendorong terbentuknya lembaga payung Talang Mamak, guna menyongsong implementasi UU Desa nomor 06 tahun 2014 tentang Desa adat.', 25, 10, 0, 20, 10, 0, 5, 5, 0, 11, 3, 'Komunitas Adat Talang Mamak', '-	Diikuti oleh 10 batin-batin adat dan tokoh adat Talang mamak, dari 15 komunitas adat yang mengalami konflik wilayah adat. \n-	Diajarkan oleh fasilitatpor bagaimana menghadapi konflik, menumbuhkan kepercayaan diri para batin adat untuk berbicara lantang dan menantang.\n- hasil dari kegiatan in adalah para tokoh dan batin adat dapat menjelaskan secara gamlang keterkaiatan antara hukum positif dan konflik-konflik yang mereka alami\n- 18 batin adat telah memahami secara baik dengan melakukan beberapa simulasi, bagaimana mereka bertindak dan harus melakukan jika tersandung kasusu hukum.', 'amanriau(2016-08-04 11:32:21) says: \nkemitraanjakarta(2016-06-06 05:36:55) says: Kebanyakan rangkuman hasil yang dituliskan disini bukan hasil, tetapi  malah kegiatan. \n\nApalagi jika kegiatannya adalah training, mestinya yang dihasilkan adalah meningkatnya kapasitas peserta, yaitu perubahan pada pengetahuan dan skill peserta training...', 'oc.lbl8', 'op.lbl22a', 1, '2017-02-08 19:12:15', 1, '2017-02-08 19:12:15'),
(53, NULL, '1', 1, 2, 12, 6, '2016-03-20', '2016-03-25', 'Budidaya tanaman obat dan tanaman penghidupan (pandan) air, bagi perempuan adat Talang Mamak.', 15, 25, 0, 20, 25, 0, 2, 2, 0, 11, 3, 'Ds. Anak Talang, Ds. Durian Cacar, Ds. Talang Parit', '-	Terbentuknya kelompok ibu-ibu masyarakat adat guna berdiskusi bersama dan memikirkan bersama keberlanjutan penghidupan dimasyarakat adat terutama menyangkut dengan ketahanan pangan rumah tangga.\n- Pengetahuan ibu-ibu dikelompok dampingan akan pentingnya pengelolaan ekonomi rumah tangga dengan baik semakin meningkat\n- nilai jual terhadap produk akan mempengaruhi pendapatan ibu-ibu diampang delapan sebanyak 15 orang sehingga pemasaran juga menjadi fokus dalam diskusi.', 'amanriau(2016-08-04 11:29:27) says: \nkemitraanjakarta(2016-06-06 05:34:22) says: Kalau bentuk keegiatannya adalah training, mestinya yang dihasilkan juga adalah kapasitas peserta (berapa orang?) training meningkat, dari tidak tahu menjadi tahu. Mohon lebih didetilkan', 'oc.lbl5', 'op.lbl12a', 1, '2017-02-08 19:12:15', 1, '2017-02-08 19:12:15'),
(54, NULL, '1', 1, 2, 12, 1, '2016-03-22', '2016-03-26', 'Pengumpulan dan pendokumentasian hukum adat, wilayah adat dan tata kelola adat Talang Mamak', 20, 15, 0, 25, 20, 0, 5, 5, 0, 11, 3, 'Ds. Anak Talang, Ds. Durian Cacar, Ds. Durian Parit', '-	Data sejarah dibeberapa komunitas sudah terkumpul dan tahap validasi\n-	Masyarakat Talang Mamak (batin adat) menginginkan sejarah adat secara keseluruhan Talang Mamak tidak mau hanya daerah dampingan\n-	Sejarah 3 (tiga) desa sudah hampir 70%', 'kemitraanjakarta(2016-06-06 05:30:37) says:', 'oc.lbl8', 'op.lbl19b', 1, '2017-02-08 19:12:16', 1, '2017-02-08 19:12:16'),
(55, NULL, '1', 1, 2, 12, 1, '2016-03-27', '2016-03-27', 'Fasilitasi Diskusi Kebijakan tentang Pengakuan masyarakat Hukum adat antara pemangku adat (Batin) dengan Pemerintahan (Kepala Desa), guna merumuskan tata kelola Desa menurut UU 06 tahun 2014 tentang Desa dan Desa Adat', 20, 10, 0, 10, 10, 0, 5, 5, 0, 11, 3, 'Ds. Anak Talang, Ds. Durian Cacar,', '-	Legalitas wilayah adat menjadi topic yang paling diprioritaskan. Terutama untuk terbitnya peraturan desa guna menghadang perusakan terhaddap wilayah adat.\n-	Ikut berpartisipasi dalam pembangunan desa.\n-	Merumuskan masuknya kebutuhan masyarakat adat dalam pengelolaan pemerintah desa terutama yang menyangkut masalah budaya.(seperti adanya alokasi dana ADD masuk dalam pembiayaan)', 'amanriau(2016-08-04 11:25:32) says: \nkemitraanjakarta(2016-06-06 05:30:12) says: Untuk Point terakhir di rangkuman, kurang detil, sistem seperti apa yang dimaksud? apakah program masyarakat adat masuk dalam RPJMDEsa atau masyarakat Adat terlibat dalam proses penyusunan RPJMDes', 'oc.lbl11', 'op.lbl29a', 1, '2017-02-08 19:12:16', 1, '2017-02-08 19:12:16'),
(56, NULL, '1', 1, 2, 12, 8, '2016-03-30', '2016-03-31', 'Kunjungan monitoring dan evaluasi ke lapangan dilakukan 3 kali selama tahun II. Laporan MIS dilakukan setiap bulan memastikan proyek berjalan sesuai perencanaan', 10, 10, 0, 10, 10, 0, 5, 2, 0, 11, 3, 'Ds. Anak Talang, Ds. Durian cacar, Ds. Talang Parit', '- kegiatan berjalan namun, ada beberapa kendala terutama pendamping lapangan yang tidak berjalan.\n- intervensi ke pemerintah daerah masih lemah', 'amanriau(2016-08-04 11:17:29) says: \nkemitraanjakarta(2016-06-06 05:25:54) says: bang Norman, tampaknya yang dirimu isikan di kolom Rangkuman Hasil itu bukan hasil tetapi adalah kegiatannya. Seharusnya yang dilaporkan adalah apa hasil dari Monev yang dilakukan oleh AMAN Riau.', 'oc.lbl3', 'op.lbl3b', 1, '2017-02-08 19:12:16', 1, '2017-02-08 19:12:16');
INSERT INTO `activities` (`id`, `description`, `active`, `pilar_id`, `organization_type_id`, `organization_id`, `activity_type_id`, `start_date`, `end_date`, `activity_name`, `male_direct_beneficiaries`, `female_direct_beneficiaries`, `trans_direct_beneficiaries`, `male_indirect_beneficiaries`, `female_indirect_beneficiaries`, `trans_indirect_beneficiaries`, `male_government_official_beneficiaries`, `female_government_official_beneficiaries`, `trans_government_official_beneficiaries`, `province_id`, `kabupaten_kota_id`, `activity_address`, `result_description`, `conversation`, `outcome_codes`, `output_codes`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(57, NULL, '1', 1, 2, 12, 1, '2016-05-14', '2016-05-16', 'Kompilasi sejarah adat Talang mamak', 5, 5, 0, 10, 10, 0, 0, 0, 0, 11, 3, 'Komunitas Anak Talang', '- Sejarah adat Talang mamak sangat komplek.\n- memulai dengan babakan\n- masing-masing suku memiliki cerita selalu beruara pada tata tatanan Talang Mamak yang berasal dari pagaruyung', 'kemitraanjakarta(2016-08-04 12:44:31) says: Kalau kompilasi, hasilnya tentu berupa dokumen dong? berapa banyak datanya? Tulisannya berapa banyak, terdiri dari apa saja?  foto, rekaman atau video?  kalau yang dilaporkan sebagairangkuman hasil seperti yang tertulis diatas, itu bukan hasil... mohon diperbaiki.', 'oc.lbl8', 'op.lbl22a', 1, '2017-02-08 19:12:16', 1, '2017-02-08 19:12:16'),
(58, NULL, '1', 1, 2, 12, 6, '2016-05-20', '2016-05-23', 'Pelatihan hukum kritis bagi BATIN  adat Talang Mamak dan mendorong terbentuknya lembaga payung Talang Mamak, guna menyongsong implementasi UU Desa nomor 06 tahun 2014 tentang Desa adat.', 18, 2, 0, 20, 10, 0, 2, 2, 0, 11, 4, 'Rumah AMAN Riau', '- Tokoh adat atau Batin adat mengetahui secara baik tentang hukum positif\n- elaborasi antara hukum adat dan hukum negara\n- tokoh adat diharapkan tidak lagi gagap bila berhadapan dengan hukum', 'kemitraanjakarta(2016-08-04 12:41:33) says: Point terakhir kok masih harapan, bukannya hasil?', 'oc.lbl10', 'op.lbl27a', 1, '2017-02-08 19:12:17', 1, '2017-02-08 19:12:17'),
(59, NULL, '1', 1, 2, 12, 4, '2016-06-06', '2016-06-09', 'Pembuatan Pustaka Adat', 10, 5, 0, 20, 10, 0, 0, 0, 0, 11, 3, 'Desa Talang Parit', '- pustaka dibuat oleh CO lapang yang livein di desa Talang Parit\n- pustaka ini hanya awal saja yang nanti akan kita komunikasikan bersama dinas pendidikan,', 'kemitraanjakarta(2016-08-04 12:40:22) says: Buku untuk pusataka nya darimana? \nBerapa pustaka yang dibuat, dimana? \nBerapa buku yang tersedia? \nMohon lebih detil...', 'oc.lbl2', 'op.lbl1c', 1, '2017-02-08 19:12:17', 1, '2017-02-08 19:12:17'),
(60, NULL, '1', 1, 2, 12, 7, '2016-06-20', '2016-06-24', 'Lomba MTQ, Tahfizd, Tartil, azan surau Nurul hikmah, pada cucu perpatih', 30, 20, 0, 20, 20, 0, 5, 2, 0, 11, 3, 'Desa Talang Mulya (cucu perpatih)', '- cucu perpatih yang tersebar dari anak talang sudah memiliki kepercayaan diri untuk bersama-sama masyarakat trans dalam mengadaan kegiatan bersama\n-cucu perpatih ini tereklusif ditengah-tengah masyarakat trans, dan ini merupakan masyarakat adat yang diberi jatah trans dari Anak Talang', 'kemitraanjakarta(2016-08-04 12:36:34) says: Pada Point terakhir, itu rasanya kok bukan hasil ya? karena masih terekslusi. Seharusnya dengan kegiatan ini, mereka menjadi inklusif dan warga trans menerima mereka.., mohon diperjelas', 'oc.lbl8', 'op.lbl19b', 1, '2017-02-08 19:12:17', 1, '2017-02-08 19:12:17'),
(61, NULL, '1', 1, 2, 12, 5, '2016-07-17', '2016-07-19', 'Pelatihan paralegal dan mengkaji UU 06 tahun 2014 tentang Desa dan Desa adat guna sinkronisasi ditingkat komunitas dalam membantu penyelesaian konflik wilayah adat.', 15, 2, 0, 25, 5, 0, 0, 0, 0, 11, 4, 'Pekanbaru', '- Para tokoh adat dan Pemuda adat sudah memahami tentang penanganan kasus-kasus konflik yang terjadi diwilayah adat Talang Mamak\n- memahmi secara baik pembuatan kronologis kasus dan pengelolaan kasus.\n- pengawalan-kasus-kasus terutama dalam pengaduan', 'kemitraanjakarta(2016-08-04 12:31:25) says: Dari pelatihan workshop ini, apakah para peserta sudah memiiki rencana kerja, dan kasus apa yang mereka akan selesaikan pasca pelatihan?', 'oc.lbl5, oc.lbl10', 'op.lbl11a, op.lbl27a', 1, '2017-02-08 19:12:18', 1, '2017-02-08 19:12:18'),
(62, NULL, '1', 1, 2, 12, 5, '2016-07-20', '2016-07-20', 'workshop multipihak peduli talang mamak yg diikuti oleh komunitas adat, Pemerintah Daerah (SKPD terkait), NGO, ORMAS, DPRD.', 100, 30, 0, 180, 50, 0, 30, 10, 0, 11, 4, 'Kota Pekanbaru', '- workshop ini mendedah regulasi payung ditingkat Provinsi\npintu masuknya adalah PERDA no. 10 Tahun 2015 tentang tanah ulayat dan pemanfaatnya\n- RTL nya adalah mendesak Pemerintah Riau yaitu pak gubernur untuk membat PERGUB yang berkaiatan dengan implemntasinya di masyarakat adat', 'kemitraanjakarta(2016-08-04 12:34:44) says: Bisakah dijelaskan agak lengkap, multi pihak yang hadir dalam workshop ini siapa saja? Apakah DPRD juga hadir? SKPD mana yang mewakili pemerintah?  dan mohon jelaskan peran masing-masing dalam mengawal lahirnya pergub', 'oc.lbl10', 'op.lbl27a', 1, '2017-02-08 19:12:18', 1, '2017-02-08 19:12:18'),
(63, NULL, '1', 1, 1, 9, 8, '2014-10-09', '2014-10-11', 'Roundtable Discussion on Social Inclusion', 8, 3, 0, 7, 3, 0, 0, 0, 0, 26, 17, 'null', 'pke', 'taf(2015-01-07 09:02:12) says:', 'oc.lbl7, oc.lbl8, oc.lbl11', 'op.lbl18a, op.lbl21b, op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:13:56', 1, '2017-02-08 19:13:56'),
(64, NULL, '1', 1, 1, 9, 6, '2014-10-24', '2014-10-26', 'Clinic Proposal', 16, 2, 0, 14, 6, 0, 0, 0, 0, 26, 17, '', 'proposal 9 mitra siap maju dalam forum PAC', 'tafkirik(2015-05-19 10:53:08) says:', 'oc.lbl7, oc.lbl11', 'op.lbl18a, op.lbl30a, op.lbl30b', 1, '2017-02-08 19:13:56', 1, '2017-02-08 19:13:56'),
(65, NULL, '1', 1, 1, 9, 8, '2014-10-26', '2014-10-29', 'Proposal Appraisal Committe I', 16, 2, 0, 10, 8, 0, 0, 0, 0, 26, 17, '', 'oke', 'taf(2015-01-07 09:02:09) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:13:57', 1, '2017-02-08 19:13:57'),
(66, NULL, '1', 1, 1, 9, 6, '2014-11-26', '2014-12-30', 'Project Finance Management', 0, 9, 0, 6, 3, 0, 0, 0, 0, 26, 17, '', 'oke', 'taf(2015-01-07 09:02:03) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:13:58', 1, '2017-02-08 19:13:58'),
(67, NULL, '1', 1, 1, 9, 8, '2015-01-09', '2015-01-12', 'Clinic Proposal II', 14, 4, 0, 7, 5, 0, 0, 0, 0, 26, 17, '', '8 proposal lolos mengikuti PAC', 'tafkirik(2015-05-19 10:52:55) says:', 'oc.lbl11, oc.lbl7', 'op.lbl30a, op.lbl30b, op.lbl18a', 1, '2017-02-08 19:13:59', 1, '2017-02-08 19:13:59'),
(68, NULL, '1', 1, 1, 9, 8, '2015-01-12', '2015-01-14', 'Proposal Appraisal Committe II', 12, 4, 0, 13, 9, 0, 0, 0, 0, 26, 17, '', '7 proposal lolos mengikuti workshop TOC', 'tafkirik(2015-05-19 10:52:09) says:', 'oc.lbl5, oc.lbl3, oc.lbl7, oc.lbl8, oc.lbl9, oc.lbl11', 'op.lbl11a, op.lbl3a, op.lbl18a, op.lbl19b, op.lbl23a, op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:00', 1, '2017-02-08 19:14:00'),
(69, NULL, '1', 1, 1, 9, 3, '2015-02-14', '2015-02-15', 'Pelatihan Kewirausahaan Sosial', 0, 0, 0, 1, 1, 0, 0, 0, 0, 26, 10, '', 'Kegiatan Kewirausahawan Sosial bagi Non Government Organization digunakan merupakan upaya untuk memperkuat NGO dalam mencari pendanaan yang bersumber dari kemampuan organisasi itu sendiri.  Maksud dari kegiatan pelatihan ini adalah agar organisasi memiliki kemampuan untuk tidak tergantung pada donor, dan menjalin hubungan strategis dengan mitra maupun komunitas  yang didampingi menjadikan hubungan kerjasama melalui konsep kewirausahawan social sangat diperlukan.  Capaian dari kegiatan pelatihan ini adalah Kemitraan mulai mengembangkan model bisnis unit dalam meningkatkan kemampuan organisasi.  Sedangkan bagi mitra dan komunitas dikembangkan metode bisnis canvas sebagai alat dalam menyusun usaha.', 'tafkirik(2015-06-30 01:58:45) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:01', 1, '2017-02-08 19:14:01'),
(70, NULL, '1', 1, 1, 9, 6, '2015-03-09', '2015-03-11', 'Project Financial Management Training II', 5, 12, 0, 5, 5, 0, 0, 0, 0, 26, 17, '', '17 orang staf keuangan telah dilatih manajemen dan SOP keuangan', 'tafkirik(2015-05-19 10:52:26) says:', 'oc.lbl8', 'op.lbl20a', 1, '2017-02-08 19:14:01', 1, '2017-02-08 19:14:01'),
(71, NULL, '1', 1, 1, 9, 6, '2015-03-16', '2015-03-23', 'Pelatihan Fasilitator Program Peduli', 37, 10, 0, 8, 2, 0, 1, 0, 0, 5, 19, '', '47 orang fasilitator dari 15 mitra, 1 mitra lokal dan 1 perangkat desa setempat telah dilatih', 'tafkirik(2015-05-19 10:52:41) says:', 'oc.lbl5, oc.lbl7, oc.lbl8, oc.lbl11', 'op.lbl11a, op.lbl10a, op.lbl18a, op.lbl21b, op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:02', 1, '2017-02-08 19:14:02'),
(72, NULL, '1', 1, 1, 9, 3, '2015-03-31', '2015-04-01', 'Rakornas Kementerian Desa PDT Transmigrasi', 0, 0, 0, 1, 1, 0, 25, 12, 0, 26, 17, '', 'Rapat koordinasi tersebut, yang mengundang Gubernur, Bupati, dan Kepala Dinas Provinsi dan Kabupaten merupakan momentum dari Kementerian Desa, PDT, dan Transmigrasi untuk menyampaikan beberapa kebijakan terkait desa.  Kebijakan-kebijakan tersebut tertuang dalam Permendesa, PDT, dan Transmigrasi maupun panduan-panduan yang dikeluarkan oleh kementerian tersebut.  Dalam rapat koordinasi yang berjalan selama dua hari Kemitraan memberikan masukan terkait dengan kondisi yang ada di masyrakat adat maupun terpencil serta kebutuhan yang sangat dirasakan sehingga perlu adanya pendampingan dan kebutuhan khusus yang diperlukan. Capaian dari pertemuan ini adalah, tersedianya informasi dan kebijakan terkait pendampingan desa serta berbagai program kerja yang akan dijalankan oleh Kementerian Desa, PDT, dan Transmigrasi', 'tafkirik(2015-06-30 01:59:17) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:03', 1, '2017-02-08 19:14:03'),
(73, NULL, '1', 1, 1, 9, 3, '2015-04-16', '2015-04-16', 'FGD tentang BUMDes', 18, 14, 0, 5, 2, 0, 2, 2, 0, 26, 17, '', 'BUMDes perlu dirancang dan dipersiapkan untuk 5 fungsi:\n~ Pendidikan dan pendampingan\n~ Layanan keuangan \n~ Pengembangan usaha produktif dan pemasaran\n~ Penerapan teknologi tepat guna dan pelestarian lingkungan\n~ Pengembangan lokal spesifik dan ekonomi kreatif\n\nPasal 8 Permendagri no 4 tahun 2015 tentang Badan Usaha Milik Desa dapat membentuk unit usaha berbadan hukum PT dsb, namun tidak disebutkan badan hukum BUMDes.Karena itu perlu diperkuat dengan Surat Edaran tentang Koperasi  sebagai badan hukum yang paling sesuai untuk BUMDes.', 'tafkirik(2015-06-30 02:00:05) says:', 'oc.lbl5, oc.lbl7', 'op.lbl13a, op.lbl13c, op.lbl18a', 1, '2017-02-08 19:14:04', 1, '2017-02-08 19:14:04'),
(74, NULL, '1', 1, 1, 9, 5, '2015-05-05', '2015-05-05', 'FGD Arah Kebijakan Pengakuan Masyarakat Adat di Era Jokowi', 0, 0, 0, 14, 7, 0, 4, 2, 0, 26, 17, '', 'Hasil dari kegiatan ini antara lain:\n- Teridentifikasinya berbagai permasalahan yang dihadapi masyarakat adat dan peta\nkomunitas adat yang terekslusi\n- Terbukanya peluang sinergi dengan Pemangku Kepentingan yang selama ini relatif tertutup seperti Kemensos;\n- Adanya rencana tindaklanjut diskusi untuk mempersiapkan rancangan regulasi yang dapat\nmenjadi masukan bagi para pihak yang berkepentingan;', 'tafnovi(2015-10-16 02:52:02) says:', 'oc.lbl11, oc.lbl10, oc.lbl5, oc.lbl8, oc.lbl7, oc.lbl9', 'op.lbl30a, op.lbl30b, op.lbl28a, op.lbl27a, op.lbl29a, op.lbl11a, op.lbl21b, op.lbl18a, op.lbl23a', 1, '2017-02-08 19:14:05', 1, '2017-02-08 19:14:05'),
(75, NULL, '1', 1, 1, 9, 3, '2015-05-15', '2015-05-17', 'Festival Kopi Rakyat', 7, 3, 0, 0, 0, 0, 6, 2, 0, 19, 20, '', 'Festival Kopi Rakyat yang dilaksanakan di Ciamis merupakan upaya dari komunitas yang selama diabaikan sebagai pegiat kopi Arabica dan selalu mendapat kendala dengan perhutani terkait lahan yang dikelola sebagai perkebunan kopi.  Untuk itu, festival kopi rakyat merupakan upaya dari komunitas yang memiliki kebun kopi untuk menumbuhkan budaya memiliki kopi sebagai produk local kebanggaan desa.  Festival tersebut disamping menampilkan berbagai narasumber, juga menampilkan bentuk kerajinan, dan kesenian yang asli dank has ciamis.  Festival tersebut juga didukung dari para pegiat desa sehingga dilakukan berbagai kelas-kelas FGD seperti: kelas kewirausahawan social, kelas terkait hutan dan pelestarian SDA, kelas terkait jurnalistik. Festival juga menampilkan praktek memilih biji kopi dan cara membut kopi seduh yang memiliki cita rasa berbeda.  Capaian dari kegiatan ini adalah Kemitraan terlibat dalam kolaborasi para pegiat desa dan komunitas desa yang memiliki perjuangan terkait dengan pengelolaan hutan, maupun hasil kopi untuk dapat dipasarkan dan bersaing untuk bisa distributor gerai-gerai kopi yang ada diberbagai kota besar.', 'tafkirik(2015-06-30 02:00:45) says:', 'oc.lbl5, oc.lbl8', 'op.lbl13a, op.lbl11a, op.lbl12a, op.lbl22a', 1, '2017-02-08 19:14:06', 1, '2017-02-08 19:14:06'),
(76, NULL, '1', 1, 1, 9, 4, '2015-05-18', '2015-05-21', 'Pelatihan Perencanaan dan Penganggaran Desa, Program Pedampingan Peduli', 0, 0, 0, 1, 0, 0, 0, 0, 0, 7, 12, '', 'Pelatihan yang dilaksanakan oleh Inisiatif berupaya membedah berbagai regulasi terkait dengan peraturan menteri terkait pelaksanaan UU Desa.  Dalam pelatihan tersebut membahas secara teknis bagaimana desa mulai dari perencanaan sampai dengan pelaporan.  Jika dilihat dari operasionalisasi UU Desa maka kewenangan menjadi sangat penting untuk bisa menjalankan 4 fungsi yang melekat di desa, yaitu pemerintahan desa, pembinaan lembaga kemasyarakat desa, pembangunan desa, dan pemberdayaan masyarakat.  Dari keempat fungsi tersebut maka turunan dari UU Desa ternyata banyak yang tidak sinkron dan bahkan menyulitkan desa terutama secara adminitrasi.  Hal ini yang menjadikan ide desa sebagai fungsi subsidiaritas dan recognisi akhirnya terjebak dalam fungsi adminitrasi yang sebetulnya masuk dalam peran pemerintah desa semata. Capaian dari kegiatan ini adalah bagaimana peran Kemitraan untuk menjembatani hubungan antar kemenertian sehingga dapat menghasilkan kebijakan yang sinkron dan mempermudah bagi desa.  Sedangkan bagi mitra dan komunitas yang ada daerah maka materi-materi pelatihan menjadikan referensi dalam melakukan fungsi pendampingan baik bagi desa maupun komunitas.', 'tafnovi(2015-07-02 09:52:16) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:07', 1, '2017-02-08 19:14:07'),
(77, NULL, '1', 1, 1, 9, 4, '2015-05-18', '2015-05-21', 'Pelatihan Perencanaan dan Penganggaran Desa, Program Pedampingan Peduli', 0, 0, 0, 1, 0, 0, 0, 0, 0, 7, 12, '', 'Pelatihan yang dilaksanakan oleh Inisiatif berupaya membedah berbagai regulasi terkait dengan peraturan menteri terkait pelaksanaan UU Desa.  Dalam pelatihan tersebut membahas secara teknis bagaimana desa mulai dari perencanaan sampai dengan pelaporan.  Jika dilihat dari operasionalisasi UU Desa maka kewenangan menjadi sangat penting untuk bisa menjalankan 4 fungsi yang melekat di desa, yaitu pemerintahan desa, pembinaan lembaga kemasyarakat desa, pembangunan desa, dan pemberdayaan masyarakat.  Dari keempat fungsi tersebut maka turunan dari UU Desa ternyata banyak yang tidak sinkron dan bahkan menyulitkan desa terutama secara adminitrasi.  Hal ini yang menjadikan ide desa sebagai fungsi subsidiaritas dan recognisi akhirnya terjebak dalam fungsi adminitrasi yang sebetulnya masuk dalam peran pemerintah desa semata. Capaian dari kegiatan ini adalah bagaimana peran Kemitraan untuk menjembatani hubungan antar kemenertian sehingga dapat menghasilkan kebijakan yang sinkron dan mempermudah bagi desa.  Sedangkan bagi mitra dan komunitas yang ada daerah maka materi-materi pelatihan menjadikan referensi dalam melakukan fungsi pendampingan baik bagi desa maupun komunitas.', 'tafkirik(2015-06-30 02:01:24) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:08', 1, '2017-02-08 19:14:08'),
(78, NULL, '1', 1, 1, 9, 3, '2015-05-25', '2015-05-30', 'Pembekalan Kader Pelopor Revolusi Mental oleh Kemendagri', 0, 0, 0, 6, 1, 0, 50, 20, 0, 19, 21, '', 'Pemekalan dilaksanakan selama 1 minggu untuk lebih menamakan nilai-nilai Pancasila, UUD 1945, dan Bhineka Tunggal Ika pada 2.000 orang praja IPDN yang akan lulus tahun 2015.  Kemitraan terlibat daam kegiatan ini, dismaping untuk memberikan warna terkait poa pengajaran, pelatihan, dan pengasuhan tetapi juga mendorong agar masalah-masalah terkait ketidakadilan serta eksklusi social masuk dalam materi pembekalan. Hal ini menjadi sangat menarik ketika berbagai materi terkait dengan Pancasila, UUD 1945, dan Bhineka Tunggal Ika dikatikan dengan kondisi bangsa Indonesia yang masih belum menghargai pluralism, masih terjadi ketidakadilan dalam pembangunan, maupun masih terjadi stigma terhadap berbagai komunitas masyarakat yang ada.  Capaian yang didapat adalah bahwa pembekalan ini sangat strategis bagi permasalahan inclusi social yang ada di negeri ini.  Dengan adanya materi dan penjelasan melalui pembekalan bagi para praja IPDN yang akan disebar diberbagai daerah maka terkait inklusi social bukan menjadi hal baru.  Untuk itu, melalui pembekalan ini Kemitraan juga membantu bagi Pemerintah dalam menjelasakan aan iklusi social dikaitkan dengan nilai-nilai yang ada di Pancasila, UUD 1945, dan Bhineka Tunggal Ika.', 'tafkirik(2015-06-30 02:02:44) says:', 'oc.lbl11', 'op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:09', 1, '2017-02-08 19:14:09'),
(79, NULL, '1', 1, 1, 9, 3, '2015-06-08', '2015-06-10', 'Rapat Kerja Badan Kerjasama Antar Desa', 0, 0, 0, 1, 0, 0, 154, 35, 0, 19, 22, '', 'Rapat kerjasama Antar Desa yang dijalankan oeh beberapa BKAD yang terbentuk oleh PNPM Perdesaan merupaan upaya untuk mengaktualisasikan berbagai hal yang selama ini dikerjasamakan secara keprojeckan menjadi bersifat legal dan sustainbel.  Dalam proses pertemuan yang digelar di Bogor terjadi beberapa kesepakatan yang menjadi tantangan BKAD ke depan.  Tantangan tersebut menyangkut struktur organisasi dan kelembagaan yang dibentuk  serta bagaimana operasionalisasi organisasi.  Capaian dari kegiatan ini adalah bagaimana memberikan pemahaman pada para elit desa maupun antar desa terkait tujuan dari beroganisasi maupun perhatian terhadap komunitas terutama yang tereklusi di daerahnya.', 'tafkirik(2015-06-30 02:03:15) says:', 'oc.lbl5, oc.lbl8', 'op.lbl13a, op.lbl20a', 1, '2017-02-08 19:14:10', 1, '2017-02-08 19:14:10'),
(80, NULL, '1', 1, 1, 9, 3, '2015-06-10', '2015-06-10', 'Dialog dengan Ditjen PPMD, Kementerian Desa, PDT, dan Transmigrasi', 0, 0, 0, 2, 2, 0, 6, 2, 0, 26, 10, '', 'Dialog dengan Dirjen PPMD membahas berbagai kegiatan yang dijalankan oleh Ditjen PPMD, dan saat ini yang menjadi konsentrasi adalah bagaimana penempatan pendamping untuk penyelesaian PNPM Perdesaan.  Dalam penjelasan Dirjen PPMD disebutkan bahawa Kementerian Desa, PDT, dan Transmigrasi tidak mungkin berjalan sendiri tanpa keterlibatan dari kalangan organisasi masyarakat sipil.  Untuk itu, kemitraan menjadi alat perekat dalam melakukan koordinasi dan kerjasama dalam pelaksanaan berbagai upaya untuk membangun desa. Capain dari kegiatan ini adalah adanya kegiatan-kegiatan yang bisa dikerjasamakan dengan Ditjen PPMD, baik dalam bentuk dukungan dalam fasilitasi kegiatan yang sudah disiapakan ditjen PPMD, maupun kegiatan-kegiatan yang diharapakan bisa direplikasi dan menjadi acuan dalam penyusunan kebijakan.', 'tafkirik(2015-06-30 02:03:41) says:', 'oc.lbl7, oc.lbl8, oc.lbl11', 'op.lbl18a, op.lbl20a, op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:11', 1, '2017-02-08 19:14:11'),
(81, NULL, '1', 1, 1, 9, 3, '2015-06-17', '2015-06-19', 'Penyusunan FAQ Desa dan Penyelesaian PNPM', 0, 0, 0, 2, 1, 0, 5, 3, 0, 26, 17, '', 'Penyusunan FAQ menjadi prioritas Kemitraan ketika dilibatkan oleh Kementerian Desa, PDT, dan Transmigrasi terkait banyaknya pertnayaan dari daerah terhadap pelaksanaan UU Desa.  Pertanyaan tersebut  yang kemudian akan disusun dan menjadi FAQ terkait implementasi UU Desa. Kegiatan yang berjalan selama dua hari tersebut, disamping membahas FAQ juga membahas pengakhiran PNPM terutama Perdesaan.  Dalam konsep pengakhiran lebih banyak dibahas strategi yang akan dijalankan serta panduan yang akan dijalankan oleh para pendamping yang akan ditempatkan setelah proses pembiayaan dari kementerian sudah selesai dilakukan. Capaian dari kegiatan ini adalah Kemitraan berperan dalam pertanyaan-pertanyaan terkait desa adat, komunitas masyarakat adat, maupun persoalan yang dihadapi desa terkait dengan belum sinkronnya pemahaman baik oleh Pemda kabupaten, organisasi masyarakat sipil, dan desa.', 'tafkirik(2015-06-30 02:04:07) says:', 'oc.lbl5, oc.lbl8', 'op.lbl13a, op.lbl20a, op.lbl11a', 1, '2017-02-08 19:14:12', 1, '2017-02-08 19:14:12'),
(82, NULL, '1', 1, 1, 9, 3, '2015-07-01', '2015-09-30', 'Engagement dengan Kementerian Desa', 0, 0, 0, 6, 2, 0, 4, 2, 0, 26, 17, '', 'Kemitraan memandang implementasi UU no 6 2014 tentang Desa adalah salahsatu peluang untuk mendorong inklusi sosial di Indonesia. Karena itu Kemitraan melakukan komunikasi intensif dengan Kementerian Desa yang merupakan aktor utama. Kerja keras periode Juli-September 2015 membuahkan hasil menggembirakan sekaligus menantang komitmen serius Kemitraan. Hasil dari komunikasi ini yaitu: Kemitraan telah menandatangani MoU dengan Kementerian Desa tentang sinergi dan dukungan dalam implementasi UU Desa.\nSalahsatu tindaklanjut dari MoU ini adalah tim Kemitraan diminta menjadi National Master of Trainer untuk para fasilitator pemberdayaan masyarakat yang diadakan Kemendes.', 'tafnovi(2015-10-16 02:55:51) says:', 'oc.lbl11, oc.lbl5, oc.lbl7, oc.lbl10', 'op.lbl30a, op.lbl30b, op.lbl28a, op.lbl13a, op.lbl18a, op.lbl27a', 1, '2017-02-08 19:14:13', 1, '2017-02-08 19:14:13'),
(83, NULL, '1', 1, 1, 9, 3, '2015-07-01', '2015-09-30', 'Engagement dengan Kementerian Dalam Negeri', 0, 0, 0, 2, 1, 0, 5, 1, 0, 26, 10, '', 'Kemitraan memandang implementasi UU no 6 2014 tentang Desa adalah salahsatu peluang untuk mendorong inklusi sosial di Indonesia. Karena itu Kemitraan melakukan komunikasi intensif dengan Kementerian Dalam Negeri yang merupakan aktor utama. Kerja keras periode Juli-September 2015 membuahkan hasil menggembirakan sekaligus menantang komitmen serius Kemitraan. Hasil dari komunikasi ini yaitu Kemitraan diminta terlibat dalam penyusunan Draft RPP Partisipasi Masyarakat, Ditjen Bangda, Kemendagri.\nRPP ini sangat penting dalam mendorong proses pengakuan bagi kelompok tereksklusi yang tidak bisa berpartisipasi dalam proses pembangunan di desa karena stigma oleh mayoritas.', 'tafnovi(2015-10-16 02:51:39) says:', 'oc.lbl5, oc.lbl10, oc.lbl11', 'op.lbl13a, op.lbl11a, op.lbl27a, op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:14', 1, '2017-02-08 19:14:14'),
(84, NULL, '1', 1, 1, 9, 5, '2015-07-02', '2015-07-08', 'Penyusunan Modul dan Bahan Ajar Pendampingan Desa', 0, 0, 0, 12, 4, 0, 4, 0, 0, 26, 17, '', 'Kegiatan ini dilakukan untuk menyiapkan berbagai kebutuhan peningkatan kapasitas para pendamping desa setelah turunnya dana desa. hasil lokakarya ini antara lain:\n- terpetakannya kebutuhan fasilitator desa di berbagai tingkatan\n- Tersusunnya kurikulum, modul dan bahan ajar pelatihan fasilitator di tiap tingkatan\n- Inklusi sosial masuk menjadi materi pokok dalam modul untuk pendamping di tiap tingkatan', 'tafnovi(2015-10-16 02:52:23) says:', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:15', 1, '2017-02-08 19:14:15'),
(85, NULL, '1', 1, 1, 9, 1, '2015-07-22', '2015-07-26', 'Kajian masyarakat adat SAD & proses inklusi sosial', 12, 9, 0, 4, 2, 0, 4, 2, 0, 16, 23, '', 'Komunitas SAD yang hidup disepanjang perlintasan Bungo & Merangin mengalami eksklusi sosial yang cukup kompleks. Baik dari stigma yang diberikan oleh masyarakat setempat, ketiadaan dalam mengakses layanan dasar, maupun terisolasi atau tersingkir dari hutan sebagai ruang hidupnya. \n\nBeberapa komunitas SAD di Bungo & Merangin mulai menetap dengan disediakannya pemukiman oleh dinas sosial pemda setempat. Namun pemda belum memberikan solusi atas kepastian ruang hidup dan penghidupan SAD. Misalnya kawasan hutan yang digunakan untuk berburu atau mengambil hasil hutan lainnya. Kondisi ini menyebabkan komunitas SAD tergantung pada bantuan yang disediakan oleh pemda dalam memenuhi kebutuhan hidupnya. \n\nWalaupun demikian, proses interaksi sosial dengan warga disekitarnya (transmigran) mulai membaik. Hal ini ditandai dengan dilibatkannya komunitas SAD dalam kegiatan yang dilakukan oleh warga. Misalnya dalam kegiatan hajatan pernikahan.', 'tafnovi(2015-10-16 02:52:45) says:', 'oc.lbl11', 'op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:15', 1, '2017-02-08 19:14:15'),
(86, NULL, '1', 1, 1, 9, 8, '2015-08-04', '2015-08-07', 'Rapat Koordinasi CSO, Pemda dan Pemerintah Pusat', 0, 0, 0, 27, 27, 0, 10, 7, 0, 26, 17, '', 'Salah satu hal yang menjadi kunci penting dalam mengatasi keterekslusian masyarakat adat dan lokal terpencil adalah peran pemerintah daerah dan masyarakat sekitar. Keterlibatan Pemerintah dan Pemerintah Daerah menjadi faktor pendorong pengakuan dan perlindungan, serta penghapusan stigma yang dihadapi komunitas tereksklusi. Di sisi lain, keterlibatan masyarakat sekitar menjadi prasyarat untuk agar penerimaan dan keberbauran komunitas yang terstigma dengan yang menstigma bisa dilakukan. Dari kegiatan ini, setidaknya ada 3 hal yang dihasilkan kegiatan ini yaitu:\n•	Adanya kesepahaman antara pemerintah, EO dan CSO tentang kebijakan inklusi sosial dan implementasi di lapangan \n•	Adanya pembelajaran implementasi program dan rencana kerja tindak lanjut (RKTL) \n•	Adanya kesepakatan strategi penajaman inklusi sosial dan pembagian peran para pihak.', 'tafnovi(2015-10-16 02:53:21) says:', 'oc.lbl7, oc.lbl11, oc.lbl10', 'op.lbl18a, op.lbl30a, op.lbl30b, op.lbl26a', 1, '2017-02-08 19:14:16', 1, '2017-02-08 19:14:16'),
(87, NULL, '1', 1, 1, 9, 1, '2015-08-10', '2015-08-13', 'Kajian masyarakat adat Sawang & proses inklusi sosial', 4, 2, 0, 4, 1, 0, 1, 0, 0, 28, 24, '', 'Suku Sawang terancam berada  diambang kepunahan. Hal ini ditandai dengan hilanganya  identitas sebagai suku dalam hal kebiasaan melaut (tidak dimilikinya alat produksi seperti sampan), minimnya penguasaan bahasa asli oleh anak-anak Suku Sawang, serta terancamnya keberlanjutan tradisi adat yang hanya dipahami atau dikuasi oleh segelintir orang, serta jumlah populasinya yang terus menurun. \nSelain itu, walaupun telah ada upaya dari pemerintah daerah untuk menguatkan identitas Suku Sawang sebagai bagian dari  budaya Belitung Timur. Namun upaya tersebut hanya sebatas memanfaatkan budaya Suku Sawang, belum ada upaya yang secara khusus dilakukan dalam memberdayakan Suku Sawang dalam berbagai aspek kehidupan (pendidikan, kesehatan, partisipasi pembangunan) yang membuatnya tidak merasa rendah diri dibandingkan dengan suku lainnya.', 'tafnovi(2015-10-16 02:54:04) says:', 'oc.lbl11', 'op.lbl30b, op.lbl30a, op.lbl28a', 1, '2017-02-08 19:14:17', 1, '2017-02-08 19:14:17'),
(88, NULL, '1', 1, 1, 9, 1, '2015-09-11', '2015-09-15', 'Kajian masyarakat adat Mentawai & proses inklusi sosial', 3, 1, 0, 4, 2, 0, 1, 0, 0, 29, 25, '', 'Komunitas Masyarakat Adat Mentawai yang hidup di sepanjang DAS Sila Onain merupakan kelompok yang mengalami eksklusi secara geografis dan penyediaan layanan dasar. Kehidupan masyarakata adat  Mentawai yang menggantungkan hidupnya dari sumberdaya alam hutan juga terancam karena hutan yang digunakan sebagai sumber penghidupannya telah mengalami kersukan akibat konsesi HPH. \n\nUpaya untuk meningkatkan posisi tawar masyarakat adat Mentawai diupayakan melalui ruang yang disediakan oleh UU Desa dalam membentuk desa atau desa adat yang memberikan pengakuan atas tradisi dan pengelolaan sumberdaya alam.', 'tafnovi(2015-10-16 02:41:24) says: Apakah ada rencana tindak lanjut dari kegiatan diskusi ini?', 'oc.lbl5, oc.lbl11', 'op.lbl11a, op.lbl30a, op.lbl30b, op.lbl28a', 1, '2017-02-08 19:14:19', 1, '2017-02-08 19:14:19'),
(89, NULL, '1', 1, 1, 9, 5, '2015-09-14', '2015-09-18', 'Forum Mitra Payung Program Peduli', 0, 0, 0, 25, 15, 1, 5, 4, 0, 27, 15, '', 'Sebagaimana telah disepakati, Mitra Payung Program Peduli akan bertemu secara berkala dalam satu forum. Tujuannya adalah untuk saling tukar pembelajaran baik seputar program maupun hal-hal berkenaan dengan yang bersifat administratif, tukar gagasan untuk memaksimalkan kinerja Program Peduli termasuk di dalamnya persoalan membangun jaringan dan advokasi bersama. Forum ini juga merupakan wahana yang tepat untuk secara berkala menujukan sampai di mana Program Peduli secara utuh kepada semua pegiat Peduli. \nSecara umum hasil dari forum ini adalah: \n•	Forum menjadi wadah pertemuan reguler bagi para mitra payung Program Peduli untuk berbagi dan belajar – baik tentang hasil maupun tantangan yang dihadapi di lapangan\n•	Meningkatnya pemahaman mitra payung tentang MIS, strategi program terkait implementasi UU Desa dan pengelolaan keuangan program\n•	Meningkatnya pengetahuan tentang isu disabilitas dalam program, terutama tentang Desa Inklusi', 'tafnovi(2015-10-16 02:42:56) says:', 'oc.lbl11, oc.lbl10', 'op.lbl28a, op.lbl30b, op.lbl30a, op.lbl27a', 1, '2017-02-08 19:14:20', 1, '2017-02-08 19:14:20'),
(90, NULL, '1', 1, 1, 9, 5, '2015-09-25', '2015-09-27', 'Konsultasi Publik Raperda Masyarakat Adat Kasepuhan dalam acara Riung Sabaki dan Seren Tahun di Kasepuhan Cipta Gelar', 80, 20, 0, 10, 5, 0, 7, 0, 0, 14, 26, '', 'Kemitraan memfasilitasi Forum Konsultasi Publik dengan masyarakat Kasepuhan khususnya untuk mendapatkan masukan mengenai isi Perda, menyiapkan masyarakat Kasepuhan dalam menyongsong lahirnya Perda,  serta mendiskusikan kesepakatan dan komitmen implementasi Perda melalui forum konsultasi dengan masyarakat Kasepuhan. \nBeberapa hasil konsultasi publik yang dilaksanakan di Kasepuhan Ciptagela antara lain sbb:\n-	Masyarakat Kasepuhan bersepakat akan terus memperjuangkan agar Perda Kasepuhan segera ditetapkan, dan apabila sudah ditetapkan siap menjaga kelestarian hutan adat;\n-	Masyarakat siap menyampaikan aspirasi langsung kepada pemerintah (kabupaten dan propinsi) maupun pemerintah pusat, dan bila diperlukan siap melakukan penggalangan dana melalui pengumpulan beras 1 liter per kepala keluarga sebagai bentuk partisipasi komitmen perjuangan seluruh warga kasepuhan;\n-	Warga kasepuhan yang beraktivitas mengelola SDA di wilayah hukum adat siap bertanggungjawab untuk selalu menjalankan aturan-aturan adat yang berwawasan pelestarian alam dan ramah lingkungan;\n-	Bahwa isi Perda dan penjelasannya telah disusun berdasarkan hasil musyawarah adat di masing-masing kasepuhan yang dibuktikan dengan penyampaian saran dan masukan dalam bentuk tertulis atau tidak tertulis;\n-	Akan memperkuat aturan adat yang berlaku di masing-maisng kasepuhan untuk semua sektor kehidupan sebagai bentuk nyata pelestarian budaya yang ada;\n-	Merekomendasikan sistem pemilihan kepala desa di desa-desa yang didalamnya terdapat masyarakat kasepuhan, untuk ditunjuk oleh Pupuhu Kasepuhan melalui hukum adat (sistem pemilihan kepala desa dikembalikan ke sistem adat);\n-	SABAKI harus mengagendakan program peningkatan pemberdayaan peran perempuan dan pemuda SABAKI;\n-	Pupuhu Kasepuhan mempercayakan kepada pengurus SABAKI untuk memperjuangkan hak-hak masyarakat adat kasepuhan sesuai dengan mandat yang diberikan pada saat Riuangan adat kasepuhan sesuai dengan mandat.', 'tafnovi(2015-10-16 02:50:06) says:', 'oc.lbl5, oc.lbl11, oc.lbl10, oc.lbl8', 'op.lbl13a, op.lbl28a, op.lbl30b, op.lbl30a, op.lbl27a, op.lbl20a, op.lbl22a, op.lbl11a', 1, '2017-02-08 19:14:21', 1, '2017-02-08 19:14:21'),
(91, NULL, '1', 1, 1, 9, 2, '2015-09-25', '2015-09-30', 'Journalist Visit “Ayo ke Pipikoro”', 0, 0, 0, 10, 4, 0, 3, 0, 0, 6, 5, '', 'Kegiatan ini adalah upaya memfasilitasi para jurnalis dan pelaku media untuk ‘mengalami’ kehidupan, merasakan pergulatan masyarakat adat Topo Uma dan Kulawi Uma  yang terstigma dan terekslusi, juga menemukan potensi yang mereka miliki sekaligus melakukan liputan yang dalam dan koprehensif.  Informasi dan berita yang dipublikasikan wartawan diharapkan dapat membuka mata public serta pemerintah agar lebih peduli dan berpihak kepada masyarakat adat. Ada 3 capaian dari kegiatan ini yaitu:\n•	Masing-masing media mendapatkan informasi dan data yang cukup  mengenai ekslusi sosial serta perubahan yang terjadi,  baik dari warga di desa-desa yang dikunjungi\n•	Seluruh wartawan yang melakukan liputan, tidak hanya peka, tetapi juga menjadi lebih peduli serta sensitive terhadap isu inklusi yang dialami masyarakat, setelah mengalami langsung kehidupan warga desa. Ini tercermin dari  cerita lisan pengalaman mereka atau dapat dilihat dari angel tulisan dalam pemberitaan mereka di media masing-masing\n•	Hingga tanggal 13 Oktober 2015, telah terpublikasi 17 berita (5 di Koran Kompas, 2 di The Jakarta Post, 9 di portal berita Detik, 1 di Koran tempo, 1 di portal berita Mongabay', 'tafnovi(2015-10-16 02:45:47) says:', 'oc.lbl5, oc.lbl11', 'op.lbl13a, op.lbl30a, op.lbl30b, op.lbl28a, op.lbl11a', 1, '2017-02-08 19:14:22', 1, '2017-02-08 19:14:22'),
(92, NULL, '1', 1, 1, 9, 3, '2015-09-27', '2015-12-30', 'MoU Kemitraan dan SCF dengan Kabupaten Bulukumba', 0, 0, 0, 5, 5, 0, 25, 5, 0, 17, 27, '', '* MoU mencakup sejumlah kecamatan dalam DAS Bejawan\n\n* Akan segera dibentuk Pokja dan Bupati meminta BAPEDA Bulukumba menjadi leading sektor untuk program terkait\n\n* Roadmap program terkait MoU akan diterjemahkan ke dalam program kerja SKPD terkait', '', 'oc.lbl2, oc.lbl5, oc.lbl11, oc.lbl10, oc.lbl3, oc.lbl8', 'op.lbl1b, op.lbl1c, op.lbl1a, op.lbl13a, op.lbl30a, op.lbl30b, op.lbl28a, op.lbl27a, op.lbl11a, op.lbl3a, op.lbl2b, op.lbl26a, op.lbl2a, op.lbl19b', 1, '2017-02-08 19:14:23', 1, '2017-02-08 19:14:23'),
(93, NULL, '1', 1, 1, 9, 8, '2015-09-29', '2015-09-29', 'RTL pertemuan Mitra Payung dengan Komnas HAM', 0, 0, 0, 9, 4, 0, 1, 0, 0, 26, 17, '', 'Kegiatan ini merupakan RTL forum mitra payung di Yogyakarta terkait dukungan EO untuk penyusunan laporan tahunan Komnas HAM desk kelompok minoritas sekaligus sebagai media advokasi. beberapa hasil kesepakatan:\n- Komnas HAM sepakat dengan dukungan para EO untuk menyediakan berbagai data pelanggaran HAM terkait pilar masing-masing\n- RTL dari pertemuan ini adalah FGD bulan November yang akan diadakan Komnas HAM dimana para EO diminta mengirimkan nama peserta FGD dan berbagai hal yang dibutuhkan', 'tafnovi(2015-10-16 02:51:13) says:', 'oc.lbl11, oc.lbl9, oc.lbl7, oc.lbl8', 'op.lbl30a, op.lbl30b, op.lbl28a, op.lbl24b, op.lbl18a, op.lbl21b', 1, '2017-02-08 19:14:24', 1, '2017-02-08 19:14:24'),
(94, NULL, '1', 1, 1, 9, 8, '2015-10-01', '2015-12-31', 'Review dokumen mitra dan amandemen kontrak', 0, 0, 0, 6, 2, 0, 0, 0, 0, 26, 17, '', 'Program Peduli fase 2 tahun pertama berakhir September 2015 bagi mitra CSO. Untuk itu mitra CSO mengajukan dokumen usulan kegiatan tahun kedua tanpa menghentikan kegiatan dilapangan. Ada 4 mitra CSO yang perlu melakukan pembaharuan hasil TOC. Karena berdekatan dengan libur akhir tahun baru 2 mitra yang berhasil melaksanakan TOC dalam periode ini. Walau proses review berlangsung intensif, amandemen kontrak 14 mitra berhasil dilaksanakan.', 'tafnovi(2016-03-09 11:42:32) says:', 'oc.lbl2, oc.lbl5, oc.lbl3, oc.lbl4, oc.lbl11', 'op.lbl1b, op.lbl1c, op.lbl13a, op.lbl4c, op.lbl11a, op.lbl2b, op.lbl2c, op.lbl9a, op.lbl7a, op.lbl10a, op.lbl30a, op.lbl5a', 1, '2017-02-08 19:14:25', 1, '2017-02-08 19:14:25'),
(95, NULL, '1', 1, 1, 9, 5, '2015-10-05', '2015-10-08', 'Workshop Berbagi Pembelajaran dari Lapangan Program Pendampingan Peduli Desa 2015', 0, 0, 0, 20, 10, 0, 4, 3, 0, 5, 14, '', 'Selain sebagai ruang koordinasi dan konsolidasi antar pihak, workshop ini diharapkan dapat menghasilkan catatan pengetahuan dan pengalaman sekaligus praktik-praktik baik yang menjadi pembelajaran bagi pelaku program.\nDari workshop ini ada beberapa hasil penting yaitu:\n•	Catatan elaborasi pengetahuan dan pengalaman lapangan selama pendampingan peduli desa berjalan. \n•	Informasi praktik baik di lapangan sebagai sumber pengetahuan dan pembelajaran bersama, serta bahan dokumentasi untuk advokasi di tingkat nasional. \n•	Rumusan rencana strategi ke depan.', 'tafnovi(2015-10-16 02:39:10) says:', 'oc.lbl11, oc.lbl10, oc.lbl8, oc.lbl5', 'op.lbl30a, op.lbl30b, op.lbl28a, op.lbl27a, op.lbl20a, op.lbl11a', 1, '2017-02-08 19:14:26', 1, '2017-02-08 19:14:26'),
(96, NULL, '1', 1, 1, 9, 5, '2015-10-23', '2015-10-26', 'FGD dan Festival Budaya kasepuhan', 40, 25, 0, 10, 8, 0, 6, 0, 0, 14, 26, '', 'Kegiatan ini merupakan tindaklanjut Konsultasi Publik Raperda Masyarakat Adat Kasepuhan dalam acara Riung Sabaki dan Seren Tahun di Kasepuhan Cipta Gelar pada bulan September 2015. Selain untuk mengaktualisasikan budaya kasepuhan untuk generasi muda maupun ke pihak luar, kegiatan ini adalah upaya mendorong pengesahan Ranperda pengakuan masyarakat Adat Kasepuhan oleh DPRD Lebak bulan November 2015. Dari kegiatan ini pengetahuan pengelolaan keanekaragaman hayati yang dilakukan masyarakat adat Kasepuhan selama ratusan tahun tersosialisasi ke kalangan luas. Selain itu kegiatan ini juga menghasilkan beberapa rekomendasi penting terkait pengesahan Ranperda dan rencana tindak lanjutnya.', 'tafnovi(2016-03-09 11:43:02) says:', 'oc.lbl11, oc.lbl10, oc.lbl5', 'op.lbl28a, op.lbl30a, op.lbl30b, op.lbl27a, op.lbl11a', 1, '2017-02-08 19:14:27', 1, '2017-02-08 19:14:27'),
(97, NULL, '1', 1, 1, 9, 5, '2015-11-02', '2015-11-02', 'Penyusunan RPP Partisipasi Masyarakat di Ditjen Bangda Kemendagri', 0, 0, 0, 16, 5, 0, 13, 5, 0, 26, 17, '', 'Kegiatan ini membahas RPP turunan UU 23 tahun 2014 tentang Pemda, dalam pertemuan dibahas tentang keterlibatan masyarakat terutama NGO untuk dapat mendorong percepatan program pemerintah maupun pemerintah daerah melalui pengalokasian anggaran untuk NGO.  Partisipasi tersebut menjadi diskusi menarik karena perspektif tentang pendaaan bagi organisasi masyarakat.', 'tafnovi(2016-03-09 11:42:06) says:', 'oc.lbl11, oc.lbl8', 'op.lbl30a, op.lbl20a, op.lbl30b', 1, '2017-02-08 19:14:28', 1, '2017-02-08 19:14:28'),
(98, NULL, '1', 1, 1, 9, 8, '2015-11-03', '2015-11-05', 'Studi Awal Implementasi UU Desa bagi Masyarakat Adat', 30, 25, 0, 12, 8, 0, 6, 2, 0, 14, 26, '', 'Kegiatan ini dilakukan World Bank bersama Kemitraan untuk melihat implementasi UU Desa terhadap masyarakat adat. masyarakat adat kasepuhan adalah yang pertama dikunjungi dari rencana 5 masy adat. dari hasil FGD dan wawancara terhadap masy dan perangkat desa, implementasi UU desa memberikan dampak positif terutama dari sisi pendanaan pembangunan desa. untuk sisi akuntabilitas dan transparansi,pemerintah desa dan warga cirompang cukup baik.hal ini ditunjukkan dari hasil audit BPKP yang tidak menemukan pelanggaran.', 'tafnovi(2016-03-09 11:41:40) says:', 'oc.lbl11', 'op.lbl30b', 1, '2017-02-08 19:14:29', 1, '2017-02-08 19:14:29'),
(99, NULL, '1', 1, 1, 9, 5, '2015-11-09', '2015-11-10', 'Lokakarya Masyarakat Adat dan Eksploitasi SDA P2K-LIPI', 0, 0, 0, 18, 8, 0, 6, 4, 0, 26, 17, '', 'Untuk meningkatkan kapasitas, jaringan dan peluang sinergi terkait persoalan masyarakat adat, Kemitraan juga mengikuti beberapa forum diskusi yang diadakan lembaga strategis seperti LIPI. Dalam periode ini, Kemitraan diundang sebagai peserta aktif dari forum diskusi masyarakat adat . Catatan dari kegiatan ini:\n•	Terpetanya persoalan masyarakat adat yang sangat kompleks serta keterkaitan dengan aktor ditingkat lokal, nasional dan internasional\n•	Persoalan Hak Milik adalah persoalan besar dan fundamental yg belum selesai, sekaligus warisan pendiri bangsa yg terabaikan. Fundamental karena pada akhirnya menyangkut sistem ekonomi Indonesia yg juga sampai hari ini tidak selesai. Karena tidak selesai, menyisakan terus konflik sosial horisontal dan vertikal yg pelik sehingga menimbulkan kegamangan hub antara negara, korporasi dan masy adat\n•	Terbukanya peluang berkolaborasi', 'tafnovi(2016-03-09 11:41:08) says:', 'oc.lbl5, oc.lbl11', 'op.lbl11a, op.lbl29a, op.lbl30a', 1, '2017-02-08 19:14:30', 1, '2017-02-08 19:14:30'),
(100, NULL, '1', 1, 1, 9, 4, '2015-11-11', '2015-11-14', 'TOC AMAIR', 3, 2, 0, 5, 2, 0, 2, 0, 0, 28, 24, '', '-Terpetakannya kondisi terkini beserta aktornya\n-Teridentifikasinya persoalan implementasi proyek dilapangan\n-Adanya kesepakatan capaian prioritas', 'tafnovi(2016-03-09 11:40:21) says:', 'oc.lbl2, oc.lbl5, oc.lbl3, oc.lbl11', 'op.lbl1b, op.lbl1c, op.lbl13a, op.lbl2b, op.lbl4c, op.lbl2c, op.lbl12a, op.lbl30a', 1, '2017-02-08 19:14:31', 1, '2017-02-08 19:14:31'),
(101, NULL, '1', 1, 1, 9, 6, '2015-11-15', '2015-11-21', 'Pelatihan Peningkatan Kapasitas Fasilitator dalam Mempromosikan & Mendokumentasikan Inklusi Sosial', 5, 0, 0, 20, 9, 0, 15, 12, 0, 5, 14, '', 'Selain sebagai ajang konsolidasi dan refleksi, kegiatan yang dibuka PLT Bupati Kebumen ini dilaksanakan sebagai upaya peningkatan mitra CSO dalam mendokumentasikan,mempromosikan dan mendorong inklusi sosial dalam berbagai aspek kehidupan masyarakat. Kegiatan ini juga dilakukan karena salah satu tantangan yang dihadapi dalam pelaksanaan Program Peduli adalah kesulitan mitra kerja dalam menangkap dan mendiskripsikan perubahan-perubahan yang terjadi di masyarakat sebagai akibat dari intervensi yang dilakukan dalam konteks Implementasi UU desa. Salahsatu hasil penting dari kegiatan ini selain menguatnya hubungan dengan pemerintah Kebumen adalah 6 film pendek karya fasilitator yang diminta PLT Bupati diunggah di 6 website SID', 'tafnovi(2016-03-09 11:39:42) says:', 'oc.lbl11, oc.lbl2, oc.lbl7, oc.lbl8, oc.lbl5', 'op.lbl30a, op.lbl30b, op.lbl1c, op.lbl18a, op.lbl21b, op.lbl11a', 1, '2017-02-08 19:14:32', 1, '2017-02-08 19:14:32'),
(102, NULL, '1', 1, 1, 9, 4, '2015-11-15', '2015-11-15', 'Pertemuan Program Peduli dengan Deputy Kemenko PMK', 0, 0, 0, 15, 10, 0, 3, 2, 0, 26, 10, '', 'Pemaparan program peduli yang ada di 6 pilar kepada Deputy PMK', 'tafnovi(2016-03-09 11:40:04) says:', 'oc.lbl11', 'op.lbl30a', 1, '2017-02-08 19:14:33', 1, '2017-02-08 19:14:33'),
(103, NULL, '1', 1, 1, 9, 5, '2015-11-20', '2015-11-20', 'Rakornas Pemangku Kepentingan Desa I Dirjen PMD Kemendes', 0, 0, 0, 19, 12, 0, 6, 0, 0, 26, 17, '', 'Kegiatan ini adalah upaya Kemendes untuk mencari masukan para pemangku kepentingan dari masyarakat sipl, akademisi setelah setahun pelaksanaan UU Desa. dalam pertemuan pertama ini lebih banyak melakukan brainstorming dan pemetaan isu-isu penting dalam setahun implementasi UU Desa.', 'tafnovi(2016-01-15 12:23:56) says:', 'oc.lbl7', 'op.lbl17a, op.lbl18a', 1, '2017-02-08 19:14:33', 1, '2017-02-08 19:14:33'),
(104, NULL, '1', 1, 1, 9, 5, '2015-12-02', '2015-12-04', 'Rakornas Pemangku Kepentingan Desa II Dirjen PMD Kemendes', 0, 0, 0, 40, 25, 0, 6, 0, 0, 26, 17, '', 'sebagai tindaklanjut kegiatan sebelumnya, pada tahap kedua Rakornas ini peserta dibagi dalam 8 kelompok sesuai isu yang sudah dipetakan dalam pertemuan pertama.Hasil dari rakornas ini adalah Konsensus Nasional Forum Pemangku Kepentingan Desa”Desa Membangun Indonesia” yang berisi 9 butir konsensus. Ke-9 butir ini akan disampaikan dalam forum Rembug Nasional', 'tafnovi(2016-01-15 12:22:33) says: Bisa diberikan keterangan pengantar secara singkat dalam bagian rangkuman hasil: "ini adalah acara Kemendes....peserta adalah dari NGOs, K/L (Pemerintah dll)....".', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:34', 1, '2017-02-08 19:14:34'),
(105, NULL, '1', 1, 1, 9, 5, '2015-12-03', '2015-12-03', 'Semiloka Identifikasi Upaya-Upaya Penghormatan, Perlindungan, dan Pemenuhan Hak-Hak Kelompok Minoritas', 6, 2, 0, 15, 10, 0, 4, 2, 0, 26, 17, '', 'Kegiatan ini merupakan tindak lanjut dari dua Diskusi Kelompok Terfokus sebelumnya. Dari kegiatan ini ada beberapa catatan penting yaitu:\n•	Terpetanya kondisi pemenuhan atas hak-hak kelompok minoritas terutama hak-hak tertentu yang selama ini dianggap sebagai prioritas (core) tanpa menganggap bahwa hak-hak lainnya tidak penting\n•	Terumuskannya rancangan besar untuk menggambarkan bagaimana pemenuhan hak oleh kelompok minoritas\n•	Tersusunnya bahan dasar bagi pembuatan Initial Report Pelapor Khusus Kelompok Minoritas Komnas HAM', 'tafnovi(2016-01-15 12:17:57) says:', 'oc.lbl11, oc.lbl7', 'op.lbl28a, op.lbl18a', 1, '2017-02-08 19:14:35', 1, '2017-02-08 19:14:35'),
(106, NULL, '1', 1, 1, 9, 4, '2015-12-10', '2015-12-13', 'TOC YTB', 11, 6, 0, 5, 4, 0, 1, 0, 0, 8, 28, '', '- terpetanya kondisi terkini\n- perlunya advokasi multipihak\n- pengorganisasian masy. perlu ditingkatkan\n- fokus program dirubah dari isu kesehatan kepada advokasi kebijakan', 'tafnovi(2016-03-09 11:39:19) says:', 'oc.lbl2, oc.lbl5, oc.lbl3', 'op.lbl1b, op.lbl1c, op.lbl13a, op.lbl4c, op.lbl11a', 1, '2017-02-08 19:14:36', 1, '2017-02-08 19:14:36'),
(107, NULL, '1', 1, 1, 9, 5, '2015-12-11', '2015-12-12', 'Workshop Ketahanan Masyarakat', 0, 0, 0, 15, 25, 0, 10, 15, 0, 5, 29, '', 'Workshop Ketahanan Masyarakat membahas tentang memfungsikan paralegal dalam proses  advokasi bagi masyarakat yang mengalami ketidakadilan hukum, baik di desa maupun kabupaten. Dalam workshop ini dibahas tentang bagaimana memperkuat paralegal dalam kegaiatan yang mendorong terjadinya inklusi sosial melalui proses advokasi dari paralegal kepada komunitas yang termarginalkan/tereksklusi.', 'tafnovi(2016-01-15 12:12:47) says: Ini persis apa yang akan kita diskusikan untuk tahun ini, dalam Peduli. Ada rencana konkrit tindak lanjut untuk mitra-mitra CSO Kemitraan terkait community paralegal ini?', 'oc.lbl4, oc.lbl9', 'op.lbl6a, op.lbl24a', 1, '2017-02-08 19:14:37', 1, '2017-02-08 19:14:37'),
(108, NULL, '1', 1, 1, 9, 5, '2015-12-14', '2015-12-16', 'Rembug Desa Dirjen PMD Kemendes', 0, 0, 0, 60, 10, 0, 20, 4, 0, 26, 10, '', 'kegiatan Rembug Desa mensosialisasikan Konsensus Nasional Forum Pemangku Kepentingan Desa”Desa Membangun Indonesia” yang berisi 9 butir konsensus.', 'tafnovi(2016-06-09 10:27:10) says: \nkemitraanjakarta(2016-06-01 01:48:32) says: \ntafnovi(2016-01-15 11:25:57) says: Bisa tuliskan 9 butir konsensus tersebut? Info penting ini bisa kita share ke seluruh mitra kita. Terutama yang bekerja dengan pendekatan strategi desa.', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:38', 1, '2017-02-08 19:14:38'),
(109, NULL, '1', 1, 1, 9, 4, '2015-12-15', '2015-12-18', 'TOC AMAN Riau', 5, 0, 0, 7, 1, 0, 0, 0, 0, 11, 4, '', '-Terpetakannya kondisi terkini beserta aktornya\n-Teridentifikasinya persoalan implementasi proyek dilapangan\n-Adanya kesepakatan capaian prioritas', 'tafnovi(2016-03-09 11:39:02) says:', 'oc.lbl2, oc.lbl5, oc.lbl3, oc.lbl4, oc.lbl11', 'op.lbl1b, op.lbl1c, op.lbl13a, op.lbl4c, op.lbl11a, op.lbl7a, op.lbl9a, op.lbl30a, op.lbl30b', 1, '2017-02-08 19:14:38', 1, '2017-02-08 19:14:38'),
(110, NULL, '1', 1, 1, 9, 8, '2015-12-19', '2015-12-20', 'Bakti SOsial dan Jelajah Desa', 0, 0, 0, 40, 0, 0, 40, 0, 0, 7, 30, '', 'Dalam rangka melihat pelaksanaan Dana Desa di lokasi-lokasi terpencil, maka kementerian Desa, PDT dan Transmigrasi mengundang Kemitraan untuk berpartisipasi dalam kegiatan bakti sosial di daerah Tengger, lereng gunung Bromo.', 'tafnovi(2016-01-14 05:07:44) says: Bisa dijelaskan sedikit, apakah persisnya peran partisipasi Kemitraan di acara Baksos ini? Jumlah ''beneficiary  tidak langsung adalah 40; apakah yang dimaksud pejabat pemerintah 40? Atau total peserta memang 80? Tidak ada peserta perempuan terlibat?', 'oc.lbl7', 'op.lbl18a', 1, '2017-02-08 19:14:40', 1, '2017-02-08 19:14:40'),
(111, NULL, '1', 1, 1, 9, 8, '2015-12-21', '2015-12-21', 'Pertemuan Awal dengan Kementerian ATR', 0, 0, 0, 2, 0, 0, 1, 1, 0, 26, 10, '', 'Kementerian ATR merupakan salahsatu pemangku kepentingan strategis yang banyak terkait dengan persoalan yang dihadapi masyarakat adat selama ini. Kemitraan berupaya “mencari jalan” untuk menyelesaikan persoalan ini. Pertemuan awal dilakukan di gedung Kementerian ATR dengan hasil yaitu:\n•	Terbukanya komunikasi awal dengan Kementerian ATR\n•	Teridentifikasinya program Kementerian ATR dan Direktorat yang terkait dengan permasalahan yang dihadapi pemanfaat program Peduli \n•	Rencana pertemuan tindaklanjut untuk pembahasan lebih detil rencana sinergi Kemitraan dengan Kementerian ATR', 'tafnovi(2016-01-14 11:53:43) says: - Siapa/direktorat apa di ATR?\n- Apa program ATR yang diidentifikasi sejalan dengan permasalahan yang dihadapi masyarakat adat?\n- Suddah ada rencana lebih detail untuk bersinergi? Pertemuan selanjutnya kapan, atau bentuknya bagaimana?', 'oc.lbl11, oc.lbl5', 'op.lbl30a, op.lbl13a', 1, '2017-02-08 19:14:40', 1, '2017-02-08 19:14:40'),
(112, NULL, '1', 1, 1, 9, 5, '2016-03-22', '2016-03-22', 'Workshop Regional Barat', 4, 4, 0, 6, 7, 0, 14, 0, 0, 26, 17, 'Hotel Fave Blok M Jakarta', 'Pertemuan Regional Barat ini pada dasarnya bertujuan untuk mempertemukan pemerintah daerah dan pemerintah pusat terkait enam isu utama yang berlaku di masyarakat adat yaitu permasalahan konflik SDA, kehutanan, agraria, perumahan rakyat, pendidikan, kesehatan dan sosial.\n\nKementerian yang diundang telah memaparkan berbagai macam program pada tingkat nasional yang dapat diakses oleh pemerintah daerah di lokasi Program Peduli. Terjadi dialog yang menarik sebab ternyata banyak permasalahan di daerah yang terjadi akibat peraturan yang tidak konsisten.\n\nBerdasar hasil dialog tersebut masing-masing mitra yang hadir membuat rencana tindak lanjut. Hal ini menjadi semacam perjanjian kerjasama antara Pemda dan mitra Peduli di daerah untuk menyelesaikan agenda tertentu berdasar kesepakatan dan kebutuhan di daerah', 'tafnovi(2016-06-09 10:25:19) says:', NULL, NULL, 1, '2017-02-08 19:14:41', 1, '2017-02-08 19:14:41');
INSERT INTO `activities` (`id`, `description`, `active`, `pilar_id`, `organization_type_id`, `organization_id`, `activity_type_id`, `start_date`, `end_date`, `activity_name`, `male_direct_beneficiaries`, `female_direct_beneficiaries`, `trans_direct_beneficiaries`, `male_indirect_beneficiaries`, `female_indirect_beneficiaries`, `trans_indirect_beneficiaries`, `male_government_official_beneficiaries`, `female_government_official_beneficiaries`, `trans_government_official_beneficiaries`, `province_id`, `kabupaten_kota_id`, `activity_address`, `result_description`, `conversation`, `outcome_codes`, `output_codes`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(113, NULL, '1', 1, 1, 9, 5, '2016-04-05', '2016-04-05', 'Workshop Program Peduli Wilayah Timur', 12, 5, 0, 6, 5, 0, 13, 8, 0, 17, 8, '', '- Mitra  Peduli  segera merevisi  rumusan  Rencana  aksi  yang  telah disesuaikan \ndengan kesepakatan bersama pemda;\n-  Mitra  Peduli  melaksanakan  rencana  aksi  sebagaimana \nyang  telah  disepakati \ndalam forum di wilayah masing-masing;\n- Kemitraan  secara  berkala  melakukan  pemantauan  terhadap  implementasi \nrencana aksi di daerah;\n- Kemitraan  segera  menyusun  advokasi  tingkat  nasional  sebagai  follow  up \nRakornas dan Regional Meeting.', 'tafnovi(2016-06-09 10:26:23) says: \nwidyaanggraini(2016-06-02 11:21:00) says: Mbak novi, sudah diperbaiki untuk data penerima manfaat. Untuk RTL mencakup ketiga aspek tersebut mbak sesuai yang ada dalam laporan yang saya kirim lewat dropbox. Secara umum RTL MPM pelayanan publik bidang pendidikan, SCF fokus di ekonomi, legalitas dokumen dan festival budaya untuk penerimaan sosial, YCMM mendorong inklusi dengan kebijakan daerah, Karsa dengan BUMDES dan E-governance, SAMANTA dengan adminduk satu atap, KBCF penguatan kelembagaan desa, AMAN Maluku : penguatan kelembagaan adat.\ntafnovi(2016-06-01 02:59:16) says: Rencana Aksi yang dimaksud, dalam hal apa? mohon lengkapi. Yaitu untuk meningkatkan pengakuan, akses layanan dasar bagi masyarakat adat? Atau lainnya?\nJumlah Penerima manfaat tidak langsung yang hadir, kosong? EO, CSO, TAF, merupakan penerima manfaat tidak langsung. Jumlah untuk pejabat pemerintah, sudah benar? Termasuk dari K/L yang hadir maupun staf Pemda (Bappeda, Wabup, dll). Mohon dicek kembali dan lengkapi.', 'oc.lbl2, oc.lbl8, oc.lbl5, oc.lbl3, oc.lbl11, oc.lbl10, oc.lbl9', 'op.lbl1c, op.lbl20a, op.lbl11a, op.lbl3a, op.lbl30b, op.lbl27a, op.lbl21b, op.lbl23a', 1, '2017-02-08 19:14:42', 1, '2017-02-08 19:14:42'),
(114, NULL, '1', 1, 1, 9, 2, '2016-04-22', '2016-04-27', 'Jurnalist Visit II-Sumba Timur', 600, 500, 0, 4, 2, 0, 3, 1, 0, 8, 31, 'Desa Meurumba dan Lung Anai', 'LOKASI I: Desa Meurumba, Sumba Timur, Kecamatan Kahaungi Eti, Kabupaten Sumba, NTT\nTanggal kegiatan: 22-26 Maret 2016\n\n- Meningkatnya pengetahuan dan juga pengalaman pelaku media (wartawan) mengenai Program Peduli dan isu inklusi sosial di Sumba Timur. Baik wartawan lokal maupun Nasional\n\n- Terkomunikasikannya berbagai persoalan masyarakat adat Sumba Timur (Meurumba) yang merupakan Desa dampingan Program Peduli  ke publik, termasuk kepada para pemangku kepentingan di level pusat maupun daerah.  Publikasi ini telah memicu gerakan wartawan Katolik mengumpulkan donasi untuk pembangunan Gereja dan pendidikan anak-anak Meorumba. Selain itu juga teah menginspirasi penyanyi Mirielle dan Anthony untuk berkomitmen menyumbangkan hasil penjualan album lagu-lagu mereka untuk pendidikan anak-anak di Sumba timur dan Sumba). \n\n- Terpublikasinya setidaknya 8  media cetak dan online dengan publikasi 12 berita. Yaitu 3 berita di Tribun Kupang (online) dan 3 Kupang Post (cetak),  1 berita di Portal Antara yang didistribusikan ke news babe (online), Desa Merdeka (online), Kaskus (online), Kosmo (online media, Malaysia) MNC TV (tv dan portal online) dan di-broadcast  ke lebih dari 100 sosial media (twitter dan FB) media masing-masing. Selain itu juga dipublikasi di portal media internal Program Peduli dan Peduli Adat serta dibroadcast ke sejumlah sosial media.\n\n- Terjalinnya hubungan yang berkelanjutan antara Aparat Desa, Masyarakat Adat,  CSO/Mitra lokal dengan jurnalist lokal maupun nasional. \n- Terbukanya akses informasi masyarakat adat yang selama ini tidak memiliki akses media. Dan wartawan juga menjadi memiliki akses langsung ke sumber utama berita.', 'tafnovi(2016-06-09 10:27:50) says: \nkemitraanjakarta(2016-06-03 04:01:09) says: Hi, Mbak Novi.\n- Berikut sudah saya buat revisi sesuai masukan mbak Novi. Baik terkait detil lokasi maupun waktu masing-masing kegiatan mulai dan selesai.\n\n- Terkait memisahkan laporan, saya tidak bisa memasukan laporan secara terpisah, karena utuk membuat rencana kegiatannya taggalnya sudah selesai. Jadi saya gabungkan di dalam laporan ini.\n\n- Terkait waktu kegiatan bukan satu bulan. Pada awalnya hendak dilaksanakan pararel di lokasi yang berbeda, yaitu Lung Anai dan Meurumba. Tapi karena terjadi perubahan schedule Mitra, kami menyesuaikan dengan agenda mereka, khususnya di Desantara di Lung Anai dan sudah saya rinci di dalam laporan ini.\n\nDemikain mbak. Thanks\n\ntafnovi(2016-06-01 03:24:53) says: - Ini dua kegiatan sejenis di lokasi yang berbeda kan? Sebaiknya laporan kegiatannya dipisahkan. Kalau pun disatukan, tolong lengkapi untuk Lokasi Provinsi, Lokasi Kabupaten.\n- Tanggal kegiatan dimulai dan selesai, betul, 1 bulan kegiatan? Mohon cek kembali.\n- Terpublikasikannya 30 berita dan photo ini - melalui media sosial saja, atau termasuk media cetak?', NULL, NULL, 1, '2017-02-08 19:14:43', 1, '2017-02-08 19:14:43'),
(115, NULL, '1', 1, 1, 9, 1, '2016-05-12', '2016-05-12', 'FGD Masyarakat Adat', 0, 0, 0, 5, 12, 0, 2, 1, 0, 26, 17, '', '- Masyarakat adat saat ini telah banyak mendapatkan akses informasi melalui media elektronik dan sosial. Di bidang politik, partipisasi politik masih rendah malah hanya menjadi objek untuk kampanye politik saja. \n- Sudah banyak perubahan yang terjadi di tengah masyarakat adat. Faktor pendorong dari luar yang mendorong perubahan, antara lain adalah penetapan status hutan lindung. Sementara, faktor internal, meliputi migrasi yang menyebar, melemahnya sistem adat, migrasi pemuda (terkait pendidikan yang mereka miliki).\n- Globalisasi berdampak pada sistem perladangan menjadi berubah, pertanian bergeser ke pertanian intensif, serta akses terhadap sumber daya hutan terbatas. \n\n- Adat tidak dapat dilihat berdiri sendiri tapi harus dilihat relasinya dengan Negara dan pasar pada era sekarang ketika era globalisasi sangat kuat. Harus ada dialog adat dalam konteks global di mana konteks global seringkali memberikan nilai-nilai identitas, dll.\n\n- Strategi yang saat ini dilakukan, meliputi survival menjadi petani kecil, apresiasi budaya, pengaduan kasus ke Pemda dan pusat. Strategi dalam konteks globalisasi, meliputi membangun gerakan internasional untuk memperkuat identitas, kelestarian, ekosistem, dll. Strategi-strategi tersebut harus dikaitkan dengan gerakan-gerakan yang ada sekarang, seperti organisasi-organisasi indigenous people di tingkat internasional. Konteksnya sudah konteks global sehingga harus diangkat strategi global; memperuat basis produksi; memperjuangkan pengakuan secara legal; memelihara sistem ekonomi kolektif di dalam komunitas (bagaimana roda produksi masyarakat adat bisa dipelihara secara kolektif).', 'tafnovi(2016-06-01 03:37:14) says: - Jumlah peserta pertemuan dari pejabat daerah, sudah benar? Mohon dicek kembali. Seingat kami ada lebih dari 3 orang? Kemendikbud, LIPI, Kemensos? \n- Apa rencana tindak lanjut dari kegiatan ini? mohon diperjelas. Dalam TOR disebutkan akan ada rumusan rekomendasi bagi K/L terkait. Kapan rencananya draft rumusan rekomendasi ini disampaikan? Juga termasuk hasil pendataan/mapping masyarakat adat peduli?', NULL, NULL, 1, '2017-02-08 19:14:44', 1, '2017-02-08 19:14:44'),
(116, NULL, '1', 1, 1, 9, 5, '2016-05-26', '2016-05-26', 'Worshop Peduli Desa', 2, 0, 0, 7, 0, 0, 12, 7, 0, 12, 11, '', '- Pemerintahan desa memerlukan peningkatan kapasitas terkait dengan perpajakan, kemampuan teknis dalam menysusun desaign dan RAB, serta dalam menyusun dokumen penatuasahaan desa.\n- pemerintah desa menanyakan kewenangan desa yang belum diatur dengan jelas dalam perbup.  untuk peningakatan kapasitas desa hendaknya diimbangi peningkatan kapasitas aparat desa.\n- agar Peduli Desa juga menjangkau lokai-lokasi lain yang bukan lokasi peduli, minimla dalam 1 kecamatan ada 2 desa yang terlibat dalam kegiatan peningkatan kapasitas desa.', 'tafnovi(2016-06-01 03:44:19) says: Catatan untuk ke depannya (berhubungan dengan masukan dari Pak Sujana), ketika meningkatkan kapasitas pemerintah desa, pemerintah kabupaten, jangan lupa perspektif inklusinya, perhatian terhadap kebutuhan masyarakat marjinal.\n- Peserta, laki-laki semua?', 'oc.lbl11, oc.lbl7', 'op.lbl30a, op.lbl17a, op.lbl30b', 1, '2017-02-08 19:14:45', 1, '2017-02-08 19:14:45'),
(117, NULL, '1', 1, 1, 9, 2, '2016-08-06', '2016-08-07', 'Festival Budaya Masy Adat', 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 32, '', '', 'tafnovi(2016-06-01 03:46:18) says: Mungkin yang dimaksud, Festival Budaya Long Anai yang 21 Mei? Mohon lengkapi ya. Thanks\ntafnovi(2016-06-01 03:45:28) says: Mohon lengkapi laporannya.', 'oc.lbl8, oc.lbl7, oc.lbl11', 'op.lbl20a, op.lbl17a, op.lbl21b, op.lbl30a', 1, '2017-02-08 19:14:46', 1, '2017-02-08 19:14:46'),
(118, NULL, '1', 1, 1, 9, 7, '2016-08-31', '2016-08-31', 'Peduli Day', 0, 0, 0, 0, 0, 0, 0, 0, 0, 26, 10, 'Pusat Perfilman Haji Usmar Ismail', 'Peduli Day merupakan kegiatan puncak Program Peduli di Kemitraan. Kegiatan ini dimaksudkan sebagai wadah bagi para mitra untuk mempresentasikan capaian program Peduli selama satu tahun terakhir kepada khalayak ramai. presentasi hasil ini mengambil beragam bentuk seperti pembuatan booth berisi hasil karya warga dampingan, foto-foto adat serta berbagai alat komunikasi seperti flyer berisi informasi program.\n\nSecara keseluruhan program Peduli memberikan ruang untuk terjadinya dialog antar komunitas, dengan pemerintah dan masyarakat umum. sehingga Peduli Day kali ini memiliki beberapa rangkaian acara seperti talkshow, pameran foto dan atraksi kesenian. Satu sesi yang tidak diaksanakan adalah orasi budaya yang sedianya akan disampaikan oleh Tamrin Tamagola. beliau meninggalkan lokasi sebelum waktunya sehingga jadwal orasi budaya dibatalkan. \n\nKekurangan dari kegiatan ini adalah minimnya promosi keluar Peduli Day sehingga jumlah masyarakat umum yang hadir terbatas. kemudian terdapat permasalahan koordinasi dengan kemenko PMK yang menyebabkan Deputi Bidang Koordinasi Pemberdayaan Masyarakat, Desa dan Kawasan batal hadir. \n\nSecara keseluruhan mitra dan publik yang hadir menikmati rangkaian kegiatan Peduli Day. Beragam kesenian dari daerah menjadi kekuatan kegiatan ini dan partisipan sangat terhibur oleh keanekaragaman seni dan hasil karya masyarakat adat yang ditampilkan dalam booth mitra. Peduli Day juga meluncurkan dua buku yang merupakan hasil dokumentasi capaian Program Peduli berbentuk foto essay dan satu buku studi mengenai masyarakat adat dan permasalahan eksklusi yang dialami mereka.', 'tafnovi(2016-10-11 09:42:13) says: Mohon isi jumlah peserta yang hadir, baik itu penerima manfaat langsung, penerima manfaat tidak langsung maupun pejabat pemerintah.\n\nBisa dijelaskan secara singkat, output dari kegiatan ini? Apakah ada tindak lanjutnya?', 'oc.lbl8, oc.lbl11, oc.lbl10', 'op.lbl21b, op.lbl28a, op.lbl30a, op.lbl27a', 1, '2017-02-08 19:14:47', 1, '2017-02-08 19:14:47');

-- --------------------------------------------------------

--
-- Table structure for table `activity_outputs`
--

CREATE TABLE IF NOT EXISTS `activity_outputs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) unsigned NOT NULL,
  `output_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_output_activity_id` (`activity_id`),
  KEY `fk_activity_output_output_id` (`output_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=300 ;

--
-- Dumping data for table `activity_outputs`
--

INSERT INTO `activity_outputs` (`id`, `activity_id`, `output_id`) VALUES
(1, 1, 55),
(2, 2, 55),
(3, 3, 55),
(4, 4, 55),
(5, 5, 55),
(6, 6, 55),
(7, 7, 54),
(8, 7, 45),
(9, 8, 54),
(10, 9, 20),
(11, 9, 54),
(12, 10, 54),
(13, 10, 45),
(14, 11, 54),
(15, 11, 45),
(16, 12, 54),
(17, 12, 45),
(18, 13, 54),
(19, 13, 20),
(20, 14, 20),
(21, 14, 55),
(22, 14, 54),
(23, 15, 20),
(24, 15, 54),
(25, 15, 45),
(26, 16, 54),
(27, 16, 55),
(28, 17, 20),
(29, 17, 22),
(30, 18, 20),
(31, 19, 45),
(32, 19, 55),
(33, 20, 20),
(34, 20, 55),
(35, 21, 20),
(36, 21, 54),
(37, 21, 45),
(38, 21, 22),
(39, 22, 20),
(40, 22, 45),
(41, 22, 55),
(42, 23, 55),
(43, 23, 45),
(44, 25, 54),
(45, 25, 45),
(46, 26, 54),
(47, 26, 55),
(48, 27, 45),
(49, 30, 39),
(50, 31, 39),
(51, 32, 41),
(52, 33, 20),
(53, 34, 42),
(54, 35, 41),
(55, 36, 50),
(56, 37, 3),
(57, 37, 50),
(58, 37, 8),
(59, 38, 45),
(60, 39, 9),
(61, 40, 41),
(62, 41, 51),
(63, 42, 50),
(64, 43, 39),
(65, 44, 9),
(66, 45, 20),
(67, 46, 42),
(68, 47, 51),
(69, 48, 51),
(70, 49, 51),
(71, 50, 9),
(72, 51, 45),
(73, 52, 45),
(74, 53, 21),
(75, 54, 41),
(76, 55, 53),
(77, 56, 9),
(78, 57, 45),
(79, 58, 51),
(80, 59, 3),
(81, 60, 41),
(82, 61, 20),
(83, 61, 51),
(84, 62, 51),
(85, 63, 39),
(86, 63, 44),
(87, 63, 54),
(88, 63, 55),
(89, 63, 52),
(90, 64, 39),
(91, 64, 54),
(92, 64, 55),
(93, 65, 39),
(94, 66, 39),
(95, 67, 54),
(96, 67, 55),
(97, 67, 39),
(98, 68, 20),
(99, 68, 8),
(100, 68, 39),
(101, 68, 41),
(102, 68, 46),
(103, 68, 54),
(104, 68, 55),
(105, 68, 52),
(106, 69, 39),
(107, 70, 42),
(108, 71, 20),
(109, 71, 18),
(110, 71, 39),
(111, 71, 44),
(112, 71, 54),
(113, 71, 55),
(114, 71, 52),
(115, 72, 39),
(116, 73, 22),
(117, 73, 24),
(118, 73, 39),
(119, 74, 54),
(120, 74, 55),
(121, 74, 52),
(122, 74, 51),
(123, 74, 53),
(124, 74, 20),
(125, 74, 44),
(126, 74, 39),
(127, 74, 46),
(128, 75, 22),
(129, 75, 20),
(130, 75, 21),
(131, 75, 45),
(132, 76, 39),
(133, 77, 39),
(134, 78, 54),
(135, 78, 55),
(136, 78, 52),
(137, 79, 22),
(138, 79, 42),
(139, 80, 39),
(140, 80, 42),
(141, 80, 54),
(142, 80, 55),
(143, 80, 52),
(144, 81, 22),
(145, 81, 42),
(146, 81, 20),
(147, 82, 54),
(148, 82, 55),
(149, 82, 52),
(150, 82, 22),
(151, 82, 39),
(152, 82, 51),
(153, 83, 22),
(154, 83, 20),
(155, 83, 51),
(156, 83, 54),
(157, 83, 55),
(158, 83, 52),
(159, 84, 39),
(160, 85, 54),
(161, 85, 55),
(162, 85, 52),
(163, 86, 39),
(164, 86, 54),
(165, 86, 55),
(166, 86, 50),
(167, 87, 55),
(168, 87, 54),
(169, 87, 52),
(170, 88, 20),
(171, 88, 54),
(172, 88, 55),
(173, 88, 52),
(174, 89, 52),
(175, 89, 55),
(176, 89, 54),
(177, 89, 51),
(178, 90, 22),
(179, 90, 52),
(180, 90, 55),
(181, 90, 54),
(182, 90, 51),
(183, 90, 42),
(184, 90, 45),
(185, 90, 20),
(186, 91, 22),
(187, 91, 54),
(188, 91, 55),
(189, 91, 52),
(190, 91, 20),
(191, 92, 2),
(192, 92, 3),
(193, 92, 1),
(194, 92, 22),
(195, 92, 54),
(196, 92, 55),
(197, 92, 52),
(198, 92, 51),
(199, 92, 20),
(200, 92, 8),
(201, 92, 5),
(202, 92, 50),
(203, 92, 4),
(204, 92, 41),
(205, 93, 54),
(206, 93, 55),
(207, 93, 52),
(208, 93, 48),
(209, 93, 39),
(210, 93, 44),
(211, 94, 2),
(212, 94, 3),
(213, 94, 22),
(214, 94, 12),
(215, 94, 20),
(216, 94, 5),
(217, 94, 6),
(218, 94, 17),
(219, 94, 15),
(220, 94, 18),
(221, 94, 54),
(222, 94, 13),
(223, 95, 54),
(224, 95, 55),
(225, 95, 52),
(226, 95, 51),
(227, 95, 42),
(228, 95, 20),
(229, 96, 52),
(230, 96, 54),
(231, 96, 55),
(232, 96, 51),
(233, 96, 20),
(234, 97, 54),
(235, 97, 42),
(236, 97, 55),
(237, 98, 55),
(238, 99, 20),
(239, 99, 53),
(240, 99, 54),
(241, 100, 2),
(242, 100, 3),
(243, 100, 22),
(244, 100, 5),
(245, 100, 12),
(246, 100, 6),
(247, 100, 21),
(248, 100, 54),
(249, 101, 54),
(250, 101, 55),
(251, 101, 3),
(252, 101, 39),
(253, 101, 44),
(254, 101, 20),
(255, 102, 54),
(256, 103, 38),
(257, 103, 39),
(258, 104, 39),
(259, 105, 52),
(260, 105, 39),
(261, 106, 2),
(262, 106, 3),
(263, 106, 22),
(264, 106, 12),
(265, 106, 20),
(266, 107, 14),
(267, 107, 47),
(268, 108, 39),
(269, 109, 2),
(270, 109, 3),
(271, 109, 22),
(272, 109, 12),
(273, 109, 20),
(274, 109, 15),
(275, 109, 17),
(276, 109, 54),
(277, 109, 55),
(278, 110, 39),
(279, 111, 54),
(280, 111, 22),
(281, 113, 3),
(282, 113, 42),
(283, 113, 20),
(284, 113, 8),
(285, 113, 55),
(286, 113, 51),
(287, 113, 44),
(288, 113, 46),
(289, 116, 54),
(290, 116, 38),
(291, 116, 55),
(292, 117, 42),
(293, 117, 38),
(294, 117, 44),
(295, 117, 54),
(296, 118, 44),
(297, 118, 52),
(298, 118, 54),
(299, 118, 51);

-- --------------------------------------------------------

--
-- Table structure for table `activity_types`
--

CREATE TABLE IF NOT EXISTS `activity_types` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `activity_types`
--

INSERT INTO `activity_types` (`id`, `name`) VALUES
(1, 'Diskusi'),
(2, 'Kampanye dan Advokasi'),
(3, 'Kolaborasi Eksternal'),
(4, 'Kolaborasi Internal'),
(5, 'Lokakarya (workshop)'),
(6, 'Pelatihan (training)'),
(7, 'Perayaan'),
(8, 'Rapat'),
(9, 'Seminar');

-- --------------------------------------------------------

--
-- Table structure for table `domain_perubahan`
--

CREATE TABLE IF NOT EXISTS `domain_perubahan` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_domain_perubahan_created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `domain_perubahan`
--

INSERT INTO `domain_perubahan` (`id`, `name`, `created_on`, `created_by`) VALUES
(1, 'Individu', '2017-02-17 00:00:00', 1),
(2, 'Keluarga', '2017-02-17 00:00:00', 1),
(3, 'Komunitas', '2017-02-17 00:00:00', 1),
(4, 'Organisasi', '2017-02-17 00:00:00', 1),
(5, 'Negara', '2017-02-17 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `display_order` tinyint(4) unsigned NOT NULL,
  `subject` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `content` mediumtext,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fkemails_created_by_user_id` (`created_by`),
  KEY `fkemails_updated_by_user_id` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `display_order`, `subject`, `description`, `content`, `updated_on`, `updated_by`, `created_on`, `created_by`) VALUES
(1, 0, 'Forgot password', 'Email sent when visitor submit forgot password form ', 'Dear {full_name} ,<br />\n<br />\nSomeone has requested to reset password.<br />\n<br />\nIf you not request it, then please ignore this email.<br />\n<br />\nPlease click {reset_url} to reset your password.<br />\n<br />\nThis link will only valid for 1 hour.<br />\n<br />\n<br />\n<br />\nRegards<br />\n<br />\n<br />\nAdmin ', '2017-01-01 16:44:36', 1, '2017-01-31 16:44:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_logs`
--

CREATE TABLE IF NOT EXISTS `email_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0=unsent, 1=sent',
  `message` mediumtext NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `intermediate_outputs`
--

CREATE TABLE IF NOT EXISTS `intermediate_outputs` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `outcome_id` smallint(5) unsigned NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_intermediate_outputs_id` (`outcome_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `intermediate_outputs`
--

INSERT INTO `intermediate_outputs` (`id`, `outcome_id`, `code`, `name`) VALUES
(1, 1, 'op.lbl1', 'Kesadaran penerima manfaat tentang layanan pendidikan meningkat'),
(2, 1, 'op.lbl2', 'Dukungan penerima manfaat pada layanan pendidikan'),
(3, 2, 'op.lbl3', 'Kesadaran penerima manfaat terhadap pelayanan kesehatan meningkat'),
(4, 2, 'op.lbl4', 'Dukungan penerima manfaat pada pelayanan kesehatan'),
(5, 3, 'op.lbl5', 'Kesadaran penerima manfaat terhadap akses kepemilikan KTP'),
(6, 3, 'op.lbl6', 'Dukungan akses pada resolusi sengketa dan layanan peradilan pidana\r\n'),
(7, 3, 'op.lbl7', 'Dukungan untuk mendapatkan akte kelahiran'),
(8, 3, 'op.lbl8', 'Dukungan untuk mengesahkan pernikahan'),
(9, 3, 'op.lbl9', 'Dukungan untuk mendapatkan KTP'),
(10, 4, 'op.lbl10', 'Kesadaran penerima manfaat terhadap mata pencaharian meningkat'),
(11, 4, 'op.lbl11', 'Kesadaran penerima manfaat tentang hak dan penanganan sumber daya alam, termasuk tanah, meningkat'),
(12, 4, 'op.lbl12', 'Dukungan terhadap penerima manfaat pada mata pencaharian'),
(13, 4, 'op.lbl13', 'Dukungan mengamankan akses pada sumber daya alam dan tempat tinggal'),
(14, 5, 'op.lbl14', 'Kesadaran penerima manfaat atas hak mendapatkan pelayanan sosial meningkat'),
(15, 5, 'op.lbl15', 'Dukungan kepada penerima manfaat kepada layanan sosial'),
(16, 6, 'op.lbl16', 'Dukungan dialog antara kelompok penerima manfaat dan keluarganya'),
(17, 6, 'op.lbl17', 'Dukungan dialog antara kelompok penerima manfaat dan kelompok masyarakat lain'),
(18, 6, 'op.lbl18', 'Dukungan dialog antara kelompok penerima manfaat dengan pejabat publik'),
(19, 7, 'op.lbl19', 'Dukungan dan pengakuan atas identitas'),
(20, 7, 'op.lbl20', 'Dukungan pada kegiatan masyarakat yang inklusif'),
(21, 7, 'op.lbl21', 'Kesadaran penerima manfaat atas hak suara dan tanggung jawab politik meningkat'),
(22, 7, 'op.lbl22', 'Pelatihan dan dukungan untuk advokasi'),
(23, 8, 'op.lbl23', 'Dukungan untuk deteksi konflik dan mediasi'),
(24, 8, 'op.lbl24', 'Dukungan untuk mendokumentasikan pelanggaran HAM berat dan insiden kekerasan berbasis kelompok'),
(25, 8, 'op.lbl25', 'Dukungan untuk mengurangi pekerjaan eksploitatif'),
(26, 9, 'op.lbl26', 'Akses yang luas pada program layanan masyarakat yang dibuat oleh pemerintah'),
(27, 9, 'op.lbl27', 'Kesadaran pejabat publik terhadap kelompok penerima manfaat meningkat'),
(28, 10, 'op.lbl28', 'Dukungan untuk mengurangi pengucilan sosial disampaikan pada masyarakat umum'),
(29, 10, 'op.lbl29', 'Peraturan daerah dan kebijakan pemerintah pusat lainnya memuat nilai - nilai inklusi sosial'),
(30, 10, 'op.lbl30', 'Kesadaran penerima manfaat dan masyarakat luas pada permasalahan inklusi sosial meningkat');

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten_kota`
--

CREATE TABLE IF NOT EXISTS `kabupaten_kota` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `province_id` int(11) unsigned DEFAULT NULL COMMENT 'fk to province(province_id)',
  `description` varchar(255) DEFAULT NULL,
  `active` enum('0','1') DEFAULT '1',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_kabupaten_province_id` (`province_id`),
  KEY `fk_kabupaten_kota_created_by_user_id` (`created_by`),
  KEY `fk_kabupaten_kota_updated_by_user_id` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `kabupaten_kota`
--

INSERT INTO `kabupaten_kota` (`id`, `name`, `province_id`, `description`, `active`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'Kota Ambon', 10, NULL, '1', '2017-02-08 18:49:58', NULL, '2017-02-08 18:49:58', NULL),
(2, 'Maluku Tengah', 10, NULL, '1', '2017-02-08 18:50:00', NULL, '2017-02-08 18:50:00', NULL),
(3, 'Indragiri Hulu', 11, NULL, '1', '2017-02-08 19:02:44', NULL, '2017-02-08 19:02:44', NULL),
(4, 'Kota Pekanbaru', 11, NULL, '1', '2017-02-08 19:02:49', NULL, '2017-02-08 19:02:49', NULL),
(5, 'Sigi', 6, NULL, '1', '2017-02-08 19:12:18', NULL, '2017-02-08 19:12:18', NULL),
(6, 'Donggala', 6, NULL, '1', '2017-02-08 19:12:21', NULL, '2017-02-08 19:12:21', NULL),
(7, 'Kota Palu', 6, NULL, '1', '2017-02-08 19:12:22', NULL, '2017-02-08 19:12:22', NULL),
(8, 'Kota Makassar', 17, NULL, '1', '2017-02-08 19:12:23', NULL, '2017-02-08 19:12:23', NULL),
(9, 'Kota Denpasar', 25, NULL, '1', '2017-02-08 19:12:24', NULL, '2017-02-08 19:12:24', NULL),
(10, 'Kodya Jakarta Pusat', 26, NULL, '1', '2017-02-08 19:12:25', NULL, '2017-02-08 19:12:25', NULL),
(11, 'Kutai Barat', 12, NULL, '1', '2017-02-08 19:12:42', NULL, '2017-02-08 19:12:42', NULL),
(12, 'Kota Surabaya', 7, NULL, '1', '2017-02-08 19:12:46', NULL, '2017-02-08 19:12:46', NULL),
(13, 'Kota Banjar Baru', 22, NULL, '1', '2017-02-08 19:12:49', NULL, '2017-02-08 19:12:49', NULL),
(14, 'Kebumen', 5, NULL, '1', '2017-02-08 19:12:57', NULL, '2017-02-08 19:12:57', NULL),
(15, 'Kota Yogyakarta', 27, NULL, '1', '2017-02-08 19:13:21', NULL, '2017-02-08 19:13:21', NULL),
(16, 'Kota Samarinda', 12, NULL, '1', '2017-02-08 19:13:35', NULL, '2017-02-08 19:13:35', NULL),
(17, 'Kodya Jakarta Selatan', 26, NULL, '1', '2017-02-08 19:13:47', NULL, '2017-02-08 19:13:47', NULL),
(18, 'Kota Balikpapan', 12, NULL, '1', '2017-02-08 19:13:47', NULL, '2017-02-08 19:13:47', NULL),
(19, 'Banyumas', 5, NULL, '1', '2017-02-08 19:14:02', NULL, '2017-02-08 19:14:02', NULL),
(20, 'Ciamis', 19, NULL, '1', '2017-02-08 19:14:06', NULL, '2017-02-08 19:14:06', NULL),
(21, 'Bandung', 19, NULL, '1', '2017-02-08 19:14:09', NULL, '2017-02-08 19:14:09', NULL),
(22, 'Bogor', 19, NULL, '1', '2017-02-08 19:14:10', NULL, '2017-02-08 19:14:10', NULL),
(23, 'Merangin', 16, NULL, '1', '2017-02-08 19:14:15', NULL, '2017-02-08 19:14:15', NULL),
(24, 'Belitung Timur', 28, NULL, '1', '2017-02-08 19:14:17', NULL, '2017-02-08 19:14:17', NULL),
(25, 'Kepulauan Mentawai', 29, NULL, '1', '2017-02-08 19:14:19', NULL, '2017-02-08 19:14:19', NULL),
(26, 'Lebak', 14, NULL, '1', '2017-02-08 19:14:21', NULL, '2017-02-08 19:14:21', NULL),
(27, 'Bulukumba', 17, NULL, '1', '2017-02-08 19:14:23', NULL, '2017-02-08 19:14:23', NULL),
(28, 'Kota Kupang', 8, NULL, '1', '2017-02-08 19:14:36', NULL, '2017-02-08 19:14:36', NULL),
(29, 'Semarang', 5, NULL, '1', '2017-02-08 19:14:37', NULL, '2017-02-08 19:14:37', NULL),
(30, 'Probolinggo', 7, NULL, '1', '2017-02-08 19:14:40', NULL, '2017-02-08 19:14:40', NULL),
(31, 'Sumba Timur', 8, NULL, '1', '2017-02-08 19:14:43', NULL, '2017-02-08 19:14:43', NULL),
(32, 'Kutai Kartanegara', 12, NULL, '1', '2017-02-08 19:14:46', NULL, '2017-02-08 19:14:46', NULL),
(33, 'Sorong', 13, NULL, '1', '2017-02-08 19:15:08', NULL, '2017-02-08 19:15:08', NULL),
(34, 'Kota Tangerang', 14, NULL, '1', '2017-02-08 19:15:29', NULL, '2017-02-08 19:15:29', NULL),
(35, 'Bekasi', 19, NULL, '1', '2017-02-08 19:15:30', NULL, '2017-02-08 19:15:30', NULL),
(36, 'Kodya Jakarta Timur', 26, NULL, '1', '2017-02-08 19:15:51', NULL, '2017-02-08 19:15:51', NULL),
(37, 'Kodya Jakarta Utara', 26, NULL, '1', '2017-02-08 19:16:10', NULL, '2017-02-08 19:16:10', NULL),
(38, 'Dharmasraya', 29, NULL, '1', '2017-02-08 19:16:22', NULL, '2017-02-08 19:16:22', NULL),
(39, 'Kota Jambi', 16, NULL, '1', '2017-02-08 19:16:26', NULL, '2017-02-08 19:16:26', NULL),
(40, 'Bungo', 16, NULL, '1', '2017-02-08 19:16:27', NULL, '2017-02-08 19:16:27', NULL),
(41, 'Lombok Timur', 15, NULL, '1', '2017-02-08 19:18:43', NULL, '2017-02-08 19:18:43', NULL),
(42, 'Lombok Tengah', 15, NULL, '1', '2017-02-08 19:19:26', NULL, '2017-02-08 19:19:26', NULL),
(43, 'Kota Mataram', 15, NULL, '1', '2017-02-08 19:19:50', NULL, '2017-02-08 19:19:50', NULL),
(44, 'Lombok Barat', 15, NULL, '1', '2017-02-08 19:20:59', NULL, '2017-02-08 19:20:59', NULL),
(45, 'Barru', 17, NULL, '1', '2017-02-08 19:21:08', NULL, '2017-02-08 19:21:08', NULL),
(46, 'Kepulauan Talaud', 30, NULL, '1', '2017-02-08 19:21:53', NULL, '2017-02-08 19:21:53', NULL),
(47, 'Kota Tomohon', 30, NULL, '1', '2017-02-08 19:21:54', NULL, '2017-02-08 19:21:54', NULL),
(48, 'Kupang', 8, NULL, '1', '2017-02-08 19:22:25', NULL, '2017-02-08 19:22:25', NULL),
(49, 'Timor Tengah Selatan', 8, NULL, '1', '2017-02-08 19:22:51', NULL, '2017-02-08 19:22:51', NULL),
(50, 'Kota Palembang', 31, NULL, '1', '2017-02-08 19:23:23', NULL, '2017-02-08 19:23:23', NULL),
(51, 'Tangerang', 14, NULL, '1', '2017-02-08 19:23:27', NULL, '2017-02-08 19:23:27', NULL),
(52, 'Malang', 7, NULL, '1', '2017-02-08 19:23:35', NULL, '2017-02-08 19:23:35', NULL),
(53, 'Kota Banjarmasin', 22, NULL, '1', '2017-02-08 19:23:36', NULL, '2017-02-08 19:23:36', NULL),
(54, 'Sleman', 27, NULL, '1', '2017-02-08 19:23:37', NULL, '2017-02-08 19:23:37', NULL),
(55, 'Jember', 7, NULL, '1', '2017-02-08 19:23:40', NULL, '2017-02-08 19:23:40', NULL),
(56, 'Kudus', 5, NULL, '1', '2017-02-08 19:23:42', NULL, '2017-02-08 19:23:42', NULL),
(57, 'Sumba Barat Daya', 8, NULL, '1', '2017-02-08 19:24:00', NULL, '2017-02-08 19:24:00', NULL),
(58, 'Sambas', 23, NULL, '1', '2017-02-08 19:24:00', NULL, '2017-02-08 19:24:00', NULL),
(59, 'Sumba Barat', 8, NULL, '1', '2017-02-08 19:24:05', NULL, '2017-02-08 19:24:05', NULL),
(60, 'Kulon Progo', 27, NULL, '1', '2017-02-08 19:24:08', NULL, '2017-02-08 19:24:08', NULL),
(61, 'Indramayu', 19, NULL, '1', '2017-02-08 19:24:16', NULL, '2017-02-08 19:24:16', NULL),
(62, 'Blitar', 7, NULL, '1', '2017-02-08 19:24:24', NULL, '2017-02-08 19:24:24', NULL),
(63, 'Sikka', 8, NULL, '1', '2017-02-08 19:24:25', NULL, '2017-02-08 19:24:25', NULL),
(64, 'Kota Blitar', 7, NULL, '1', '2017-02-08 19:24:32', NULL, '2017-02-08 19:24:32', NULL),
(65, 'Lampung Timur', 2, NULL, '1', '2017-02-08 19:24:38', NULL, '2017-02-08 19:24:38', NULL),
(66, 'Garut', 19, NULL, '1', '2017-02-08 19:24:38', NULL, '2017-02-08 19:24:38', NULL),
(67, 'Lombok Utara', 15, NULL, '1', '2017-02-08 19:25:14', NULL, '2017-02-08 19:25:14', NULL),
(68, 'Labuhanbatu Utara', 18, NULL, '1', '2017-02-08 19:25:23', NULL, '2017-02-08 19:25:23', NULL),
(69, 'Klaten', 5, NULL, '1', '2017-02-08 19:25:33', NULL, '2017-02-08 19:25:33', NULL),
(70, 'Sukoharjo', 5, NULL, '1', '2017-02-08 19:25:34', NULL, '2017-02-08 19:25:34', NULL),
(71, 'Bantul', 27, NULL, '1', '2017-02-08 19:30:19', NULL, '2017-02-08 19:30:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_histories`
--

CREATE TABLE IF NOT EXISTS `login_histories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `login_histories`
--

INSERT INTO `login_histories` (`id`, `ip_address`, `user_agent`, `login`, `time`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486008967),
(2, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486020981),
(3, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486021698),
(4, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486023009),
(5, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486033440),
(6, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486042308),
(7, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486091218),
(8, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486094959),
(9, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486102234),
(10, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486126631),
(11, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486375026),
(12, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486433252),
(13, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486455370),
(14, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486536803),
(15, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486548841),
(16, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486551013),
(17, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486554549),
(18, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486562154),
(19, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486696973),
(20, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486703858),
(21, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1486724052),
(22, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487058231),
(23, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487065244),
(24, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487076585),
(25, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487086065),
(26, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487297696),
(27, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487301492),
(28, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487323733),
(29, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487388788),
(30, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 FirePHP/4Chrome', 'admin', 1487487845);

-- --------------------------------------------------------

--
-- Table structure for table `msc`
--

CREATE TABLE IF NOT EXISTS `msc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pilar_id` int(11) unsigned NOT NULL,
  `organization_id` int(11) unsigned NOT NULL,
  `msc_title` text NOT NULL,
  `province_id` int(11) unsigned DEFAULT NULL,
  `kabupaten_kota_id` int(11) unsigned DEFAULT NULL,
  `change_level` tinyint(1) unsigned NOT NULL,
  `related_goals` text NOT NULL,
  `date` date NOT NULL,
  `conversation` text,
  `created_on` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_msc_pilar_id` (`pilar_id`),
  KEY `fk_msc_province_id` (`province_id`),
  KEY `fk_msc_kabupaten_kota_id` (`kabupaten_kota_id`),
  KEY `fk_msc_created_by` (`created_by`),
  KEY `fk_msc_updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `msc_domain`
--

CREATE TABLE IF NOT EXISTS `msc_domain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msc_id` int(11) unsigned NOT NULL,
  `domain_id` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_msc_domain_msc_id` (`msc_id`),
  KEY `fk_msc_domain_domain_id` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `msc_outcomes`
--

CREATE TABLE IF NOT EXISTS `msc_outcomes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `msc_id` int(11) unsigned NOT NULL,
  `outcome_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_msc_outcomes_msc_id` (`msc_id`),
  KEY `fk_msc_outcomes_outcome_id` (`outcome_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `narrative`
--

CREATE TABLE IF NOT EXISTS `narrative` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pilar_id` int(11) unsigned NOT NULL,
  `organization_id` int(11) unsigned NOT NULL,
  `date` date NOT NULL,
  `narrative_title` varchar(255) NOT NULL,
  `activity_goal` text NOT NULL,
  `challenge` text NOT NULL,
  `learning` text,
  `conversation` int(11) DEFAULT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_narrative_pilar_id` (`pilar_id`),
  KEY `fk_msc_created_by` (`created_by`),
  KEY `fk_msc_updated_by` (`updated_by`),
  KEY `fk_msc_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `objectives`
--

CREATE TABLE IF NOT EXISTS `objectives` (
  `id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `objectives`
--

INSERT INTO `objectives` (`id`, `code`, `name`) VALUES
(1, 'obj1', 'Increased Access to Public Services and Social Assistance'),
(2, 'obj2', 'Increased Empowerment and Social Acceptance'),
(3, 'obj3', 'Improved Policy on Social Inclusion');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organization_type_id` smallint(5) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `province_id` int(11) unsigned DEFAULT NULL,
  `province_id_2` int(11) unsigned DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `person_in_charge` varchar(50) DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_organization_type_id` (`organization_type_id`),
  KEY `fk_organization_parent_id` (`parent_id`),
  KEY `fk_organization_province_id` (`province_id`),
  KEY `fk_organization_province_id_2` (`province_id_2`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `organization_type_id`, `parent_id`, `name`, `address`, `province_id`, `province_id_2`, `phone`, `person_in_charge`, `active`) VALUES
(1, 1, NULL, 'IKA', NULL, NULL, NULL, NULL, NULL, '1'),
(2, 2, 1, 'Ikatan Keluarga Orang Hilang (IKOHI) Jakarta', 'Jl. Gugus Depan No. 2 Matraman, Jakarta', 1, 2, '021-8502226', 'Mugiyanto', '1'),
(3, 2, 1, 'KIPER (Kiprah Perempuan)/FOPERHAM', 'Jl. Rejowinangun No. 1A Kotagede, Yogyakarta', 3, NULL, '0819 0372 7775', 'Pipit Ambarmirah', '1'),
(4, 2, 1, 'Relawan untuk Kemanusiaan (RPuK)', 'Jl. Sudirman VI No. 19 Geuceu Iniem.Banda Aceh  23239', 4, NULL, '0651-7551659, Fax : 0651-41315', 'Maida Syfrina/Azriana', '1'),
(5, 2, 1, 'Sekretariat Bersama 65 (Sekber 65)', 'Tegalrejo RT 01/ RW 04 Jebres Surakarta', 5, NULL, '0819 2508 04/0817 2851 854', 'Didik Dyah Suci Rahayu/Winarso1', '1'),
(6, 2, 1, 'Solidaritas Korban Pelanggaran HAM (SKP HAM) Donggala', 'Jl. Basuki Rahmat I Lorong Posarara I No. 101 H, Kel. Birobuli Kec. Palu Selatan, Sulawesi Tengah', 6, NULL, '0814 4114 6684', 'Nurlaela Lamasitudju', '1'),
(7, 2, 1, 'The Post Institute', 'Jl. Muradi No. 01 Kota Blitar 66133 Jawa Timur', 7, NULL, '0342-814484', 'Mawan Mahyuddin', '1'),
(8, 2, 1, 'Perhimpunan Bantuan Hukum Nusa Tenggara (PBH NUSRA)', 'Jl. Jendral Sudirman, Kel. waioti, Kec. Alok Timur, Maumere-Sikka-NTT﻿', 8, NULL, '081338168846 / 082143765366 (Piter Embu Gusi)', 'Fransesko Bero (Direktur), Piter Embu Gusi (Manage', '1'),
(9, 1, NULL, 'Kemitraan', NULL, NULL, NULL, NULL, NULL, '1'),
(10, 2, 9, 'Air Mata Air (AMA)', 'Jl. Penasin No. 2 RT 10 RW 05, Desa Mengkubang, Kec. Damar Kab. Belitung Timur', 9, NULL, '81271212703', 'Wahyu Epan Yudhistira', '1'),
(11, 2, 9, 'Aman Maluku', 'Jln. Karang Panjang No. 18, RT. 002 / RW.03, Kel. Karang Panjang, Kec. Sirimau – Kota Ambon.', 10, NULL, '0911-342492 atau 081343184672', 'Yohanes .Y. Balubun', '1'),
(12, 2, 9, 'Aman Riau', 'PERUMAHAN DAMAI LANGGENG \nJl.Soekarno Hatta Blok. A No. 61 Kel. Sidomulyo Barat Kec.Tampan – Kota Pekanbaru', 11, NULL, '81365559610', 'EFRIANTO', '1'),
(13, 2, 9, 'Desantara', 'Komplek Depok Lama Alam Permai Blok K nomor 3, Depok, Jawa Barat', 12, NULL, '021-77201121\n021-77210247', 'Mokh Sobirin', '1'),
(14, 2, 9, 'KARSA (Lingkar Pembaruan Pedesaan dan Agraria)  INSTITUTE', 'Jl. Kijang 8 No. 7 Kel. BiroBuli Selatan Kec. Palu Selatan Sulawesi Tengah 94121', 6, NULL, '0451-482049', 'Rahmat Saleh', '1'),
(15, 2, 9, 'KBCF', 'Jl. Anggrek Merpati I No. 60 Komplek Batu Alam Permai, Samarinda. 75124 Kalimantan Timur,', 12, NULL, 'T/F. 0541-7773762', 'Saparuddin Senny', '1'),
(16, 2, 9, 'MPM Muhamadiyah', 'Jl. Kh. Ahmad Dahlan No. 01, Malawele, Aimas, Kabupaten Sorong, Papua Barat 98418', 13, NULL, '8124846822', 'Anang Triyoso', '1'),
(17, 2, 9, 'PPSW Jakarta', 'Jl. Pangkalan Jati VI Gg. Anggrek RT. 05 RW. 05 No. 6 Kel. Cipinang Melayu , Kec. Makasar  Jakarta Timur 13620', 14, NULL, 'Telp : 021-86608338\nFax  : 021-86603883', 'Tri Endang Sulistyowati', '1'),
(18, 2, 9, 'Rimbawan Muda Indonesia (RMI)', 'Bogor Baru Blok C 1 No. 12 A Tegallega Bogor Tengah 16127', 14, NULL, '0251-8311097, 8320253', 'Nia Ramdhaniati', '1'),
(19, 2, 9, 'Samanta', 'Jl. Garut No. B 29 BTN Taman Indah Mataram', 15, 8, '0370-648257', 'Dwi Sudarsono', '1'),
(20, 2, 9, 'SSS Pundi Sumatera', 'Jl M Yamin No. 6 Simpang Pulai Kel. Payo Lebar Kec. Jelutung Kota Jambi 36135', 16, NULL, '0741-670862', 'Mahendra Taher', '1'),
(21, 2, 9, 'Sulawesi Community Foundation (SCF)', 'Jl. DR J Leimena No. 87 Tello Baru Makassar Sulawesi Selatan', 17, NULL, '0411-449563', 'Anton Sanjaya', '1'),
(22, 2, 9, 'Yayasan Citra Mandiri Mentawai (YCMM)', 'Jl. Kampung Nias I No. 21 A. Kel. Parak Rumbio, Kecamatan Padang Selatan, Padang, Sumatera Barat', 18, NULL, 'Telp/fax:0751-35528', 'Roberta Sarogdok', '1'),
(23, 2, 9, 'Yayasan Tanpa Batas (YTB) Kupang', 'Jl. Percetakan Cendana No 1 Kel. Fontein Belakang BRI Kupang NTT', 8, NULL, '0380-821397', 'Deni Sailana', '1'),
(24, 1, NULL, 'Lakpesdam NU', NULL, NULL, NULL, NULL, NULL, '1'),
(25, 2, 24, 'PC Lakpesdam NU Bulukumba', 'Jl. DR. Sam Ratulangi. Kompleks BTN Tiara. Blok C. 9 No. 9. Kelurahan Caile, Kecamatan Ujung Bulu, Kab. Bulukumba, Sulawesi Selatan', 17, NULL, '085277386592, 081342118169', 'Abdul Samad/Abd A''la', '1'),
(26, 2, 24, 'PC Lakpesdam NU Tasikmalaya', 'Gedung PCNU Jln. Dr. Sukardjo No. 47 Tasikmalaya', 19, NULL, '0813 2192 2923', 'Fauz Noor', '1'),
(27, 2, 24, 'Pengurus Cabang Lakpesdam NU Bima', 'Sekretariat Lakpesdam Bima, Jl. Sukun Karara, SMU Al-Hidayah Kota Bima NTB', 15, NULL, '0852 3976 3500', 'Asrul Raman', '1'),
(28, 2, 24, 'Pengurus Cabang Lakpesdam NU Cilacap', 'JL Masjid I/36 Cilacap 53223, telp (0282)521141', 5, NULL, '0852 2775 7777', 'Syaiful Mustain', '1'),
(29, 2, 24, 'Pengurus Cabang Lakpesdam NU Cimahi', 'Sekretariat NU, Jl. Raya Cilember No.27 Blk. 283 Kota Cimahi Telp. 022-665 3454', 19, NULL, '0815 712 8595', 'A. Diana Handayani', '1'),
(30, 2, 24, 'Pengurus Cabang Lakpesdam NU Indramayu', 'Gedung PCNU, Jln.  Gatot Subroto No. 9 Indramayu, Jawa Barat', 19, NULL, '0812 2340 017', 'Iin Rohimin', '1'),
(31, 2, 24, 'Pengurus Cabang Lakpesdam NU Mataram', 'Jl. Bambu Runcing No.25 b. RT.02 Lingkungan Pejeruk Desa Kelurahan Pejeruk Desa Kecamatan Ampenan Kota Mataram-NTB', 15, NULL, '0819 1702 6119/0819 0697 4412', 'Jayadi/B. Ely Mahmudah', '1'),
(32, 2, 24, 'Pengurus Cabang Lakpesdam NU Sampang', 'Jl. Diponegoro No.51 Banyuanyar Sampang 69216, Kabupaten Sampang', 7, NULL, '0821 3956 5499', 'Faisol Romdhoni', '1'),
(33, 2, 24, 'Pengurus Cabang Lakpesdam NU Sampit', 'Gedung NU Lantai Dasar Ruang 5 Jl. S. Parman No.08/62 Sampit 74323', 20, NULL, '0852 4905 3369/0813 4901 7732', 'Edi Sucipto/Hadi Utomo', '1'),
(34, 2, 24, 'Pengurus Cabang Lakpesdam NU Jepara', 'Gedung NU Lantai 3, JL. Pemuda No. 51, Jepara', 5, NULL, '08522 546 4755 ', 'Lukman Khakim', '1'),
(35, 2, 24, 'Pengurus Wilayah Lakpesdam NU Aceh', 'JL Tn. Keuramat Lr. Bakti No.12\nDsn. Seroja Lamteuumen Timur\nBanda Aceh 23236', 4, NULL, '0813 6012 4109', 'Marini DC', '1'),
(36, 1, NULL, 'LPKP', NULL, NULL, NULL, NULL, NULL, '1'),
(37, 2, 36, 'Lembaga Perlindungan Anak (LPA) Tulungagung', 'Jl. Oerip Sumohardjo No. 4A Kepatihan Tulungagung', 7, NULL, '8123403224', 'Winny Isnaini', '1'),
(38, 2, 36, 'Yayasan Pengkajian dan Pemberdayaan Masyarakat (YKPM)', 'Jl. Hetasning Baru Perumahan Taman Yasmin Indah Blok.A/I No.6 Makassar', 17, NULL, '0411- 864201 , 0811 441129', 'Mulyadi Proayitno', '1'),
(39, 2, 36, 'Yayasan Prakarsa Swadaya Masyarakat (YPSM)', 'Jl. Masjid Al Akbar Timur No. 9 Surabaya', 7, NULL, '085259351103, Telp 0331-483748, telefax: 0318274891', 'Rizki Nurhaini', '1'),
(40, 2, 61, 'Yayasan Sosial Donders (YSD)', 'Jl.Katedral Weetebula  87254, Sumba Barat Daya, Nusa Tenggara Timur', 8, NULL, '0852 3999 5 999/0812 4602 9999/081 339 306 277/081 353 899 555', 'P. Mikhael Molan Keraf, C.SsR/Imelda Sulis Setiawa', '1'),
(41, 2, 36, 'Yayasan Tunas Alam Indonesia (SANTAI)', 'Jl. Energi Gang Arwana No.5 Karang Panas Ampenan, NTB 83114', 15, NULL, '081803654297, 0370 – 649264/649264', 'Suharti, SE.,MM', '1'),
(42, 2, 36, 'Samitra Abhaya Kelompok Perempuan Pro Demokrasi (SA KPPD)', 'Jalan Ngagel Madya VI No 2 Surabaya', 7, NULL, '83 2315 5262/ 08984053588, Telefax. 031 – 541 4091', 'Rosana Yuditia Ripi', '1'),
(43, 2, 36, 'Yayasan Hotline Surabaya', 'Jl. Indrapura No. 17, Surabaya - 60176 - Jawa Timur', 7, NULL, '031-356 6232', '', '1'),
(44, 1, NULL, 'PKBI', NULL, NULL, NULL, NULL, NULL, '1'),
(45, 2, 44, 'PKBI Bengkulu', 'Jl. Kapuas Raya No.15, Bengkulu 39001', 21, NULL, '08127809272 / (0736) 25260', 'Ir.Harmuda', '1'),
(46, 2, 44, 'PKBI Daerah Aceh', 'Jl.T.Nyak Arif No.229, Lamgugop Banda Aceh 23114', 4, NULL, '081973852272 / (0651) 7551235', 'Achmawaty Achmad', '1'),
(47, 2, 44, 'PKBI DKI Jakarta', 'Jl.Pisangan Baru Timur No.2 Rt.004/09 Kel.Pisangan Baru, Kec.Mantraman Jatinegara - Jakarta Timur 13110', 1, NULL, '081297695336/ (012)8520371/8591001', 'Bonita Merlina', '1'),
(48, 2, 44, 'PKBI Kalimantan Selatan', 'Jl.Brigjen H.Hasan Basry No. 24A Banjarmasin 70125', 22, NULL, '08125180582/ (0511) 302853', 'Hapniah', '1'),
(49, 2, 44, 'PKBI Kalimantan Tengah', 'Jl.Putri Junjung Buih No.38A Palangkaraya 73113', 20, NULL, '081349705823/(0536) 38290', 'Drs.Mirhan', '1'),
(50, 2, 44, 'PKBI Sulawesi Selatan', 'Jln.Landak Baru No.55 Ujung Pandang 90135', 17, NULL, '0411-871051, 08114611176', 'A.Iskandar Harun,ST', '1'),
(51, 2, 44, 'PKBI Sumatera selatan', 'Jl.Kol.H.Burlian/Mahkamah Militer Km.6, Palembang', 18, NULL, '08127101659/ (0711) 420786', 'Amirul Husni,SH', '1'),
(52, 2, 44, 'Sanggar Waria Remaja (SWARA)', 'Jl. Pisangan lama III, No.16 RT.007/008, Kel. Pisangan Timur, Kec.Pulo Gadung - Jaktim 13230', 1, NULL, '081293710834/ (021) 29617586', 'Fina', '1'),
(53, 1, NULL, 'Samin', NULL, NULL, NULL, NULL, NULL, '1'),
(54, 2, 53, 'Children Crisis Center', 'Jl. Rajabasa 2 Blok D no 21 Perumnas Way Halim, Kec Way Halim Bandar Lampung 35141', 2, NULL, '8127215872', 'Syafruddin', '1'),
(55, 2, 53, 'Konfederasi Anti Pemiskinan (KAP)  Indonesia', 'Jl. Rana No. 1,  Bandung 40261', 19, NULL, '08161411438, 08818189630, Telpon/Fax 022 - 4214913', 'Bambang Yuli Sundayana', '1'),
(56, 2, 53, 'Lembaga Pengembangan Masyarakat Pedesaan  (Gapemasda)', 'Jl. Pembangunan, Dusun Sebenua No. 33, Desa Lubuk Dagang, Kec. Sambas, Kab. Sambas, Kal-Bar. 79400.', 23, NULL, '81352666566', 'Syafrudin, S.Sos', '1'),
(57, 2, 53, 'Yayasan Bahtera', 'Jl. Cijerah Gg. Al-Hidayah No. 40 Kota Bandung.', 19, NULL, '08122339928, 022-6001601,  022-7501711', 'A Tamami Zain', '1'),
(58, 2, 53, 'Yayasan KKSP', 'Jln. Stella III No. 88, Medan, Sumatra Utara', 18, NULL, '81370212644', 'Jaelani', '1'),
(59, 2, 53, 'Yayasan Solidaritas Masyarakat Anak (SEMAK)', 'Jl. Wiranta no 70 Bandung 40121', 19, NULL, '085624772446, 022-7279133', 'Sarlistyarso', '1'),
(60, 2, 53, 'Pusaka Indonesia', 'Jl. Kenanga Sari No. 20 Setia Budi Kel. Tanjung Sari Kec. Medan Selayang - 20132', 18, NULL, '60 - 8223252, Fax: 061 - 8223252', 'Fatwa Fadillah', '1'),
(61, 1, NULL, 'Satunama', NULL, NULL, NULL, NULL, NULL, '1'),
(62, 2, 61, 'Aliansi Sumut Bersatu (ASB)', 'Jl. Jamin Ginting, Gg. Bangun Sangka Manuk No 3 Medan Tuntungan', 18, NULL, '0812 6593 680', 'Veriyanto Sitohang', '1'),
(63, 2, 61, 'Lembaga Studi Sosial dan Agama (eLSA) Semarang', 'Perum Bukit Walisongo Permai, Jl. Sunan Ampel No 11, Kel. Tambakaji, Kec. Ngalian, Semarang, 50185', 5, NULL, '0813 2577 3057', 'Tedi Kholiludin', '1'),
(64, 2, 61, 'Solidaritas Masyarakat untuk Transparansi (SOMASI) NTB', 'Jl Pariwisata No. 41 Mataram 83121 Nusa Tenggara Barat', 15, NULL, '0819 1730 4479, Telp. 0370-627556, Fax. 0370-621373', 'Lalu Ahyar Supriadi', '1'),
(65, 2, 61, 'Yayasan Waliati (Yasalti) NTT', 'Jl. Gajah Mada RT 012/RW 03 Hambala, 87112, Waingapu, Sumba Timur, NTT', 8, NULL, '0812 379 7570', 'Ninu Rambu W. Lodang, S.Sos', '1'),
(66, 2, 61, 'LKiS', 'JL Parangtritis Km 4,4, No.1, Salakan Baru, Yogyakarta', 3, NULL, '(0274) 387194', '', '1'),
(67, 1, NULL, 'TAF (Disabilitas)', NULL, NULL, NULL, NULL, NULL, '1'),
(68, 2, 67, 'YASMIB (Yayasan Swadaya Mitra Bangsa)', 'Jalan Tamalate V No. 48, Makassar, Sulawesi Selatan 90222, Indonesia', 17, NULL, '0411 845158, 081241366679\n081524123135\n0428-22546', 'Rosniaty Aziz', '1'),
(69, 2, 67, 'Yayasan Bahtera', 'Jl. A. Yani no 145, Rt 10/Rw 04, Kelurahan Mahiti, Kec. Kota Waikabubak.', 8, NULL, '82144411081', 'Martha Rambu Bangi', '1'),
(70, 2, 67, 'Sentra Advokasi Perempuan, Difabel dan Anak (SAPDA)', 'Komplek BNI No. 25 Patangpuluhan\nWirobrajan,Yogyakarta, 55252', 7, NULL, '+62274384066\nFax: +62274384066', 'Nurul Saadah', '1'),
(71, 2, 67, 'Karitas Indonesia Keuskupan Agung Semarang (Karina Kas)', 'Gedung BELA RASA, Jl. Panuluh 377 A, Pringwulung Condongcatur, Depok, Sleman - Yogyakarta - 55283', 5, NULL, '0274-552126', '', '1'),
(72, 2, 67, 'SIGAB (Sasana Integrasi dan Advokasi Difabel )', 'Jl. Wonosari Km 8 Berbah, Sleman, Yogyakarta, Indonesia,', 24, NULL, '82325236996', '', '1'),
(73, 2, 67, 'Pattiro', 'Jl. Intan No. 81 Cilandak Barat - Jakarta Selatan - 12430', 13, 15, '+62 21 7591 5498, +62 21 7591 5546', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `organization_types`
--

CREATE TABLE IF NOT EXISTS `organization_types` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(3) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `organization_types`
--

INSERT INTO `organization_types` (`id`, `name`, `desc`) VALUES
(1, 'EO', 'Executing Organization / Mitra Payung'),
(2, 'CSO', 'Civil Society Organization / Mitra Pelaksana');

-- --------------------------------------------------------

--
-- Table structure for table `outcomes`
--

CREATE TABLE IF NOT EXISTS `outcomes` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `objective_id` tinyint(1) unsigned NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_outcomes_objective_id` (`objective_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `outcomes`
--

INSERT INTO `outcomes` (`id`, `objective_id`, `code`, `name`) VALUES
(1, 1, 'oc.lbl2', 'Peningkatan akses ke layanan pendidikan'),
(2, 1, 'oc.lbl3', 'Peningkatan akses pelayanan kesehatan'),
(3, 1, 'oc.lbl4', 'Peningkatan akses ke layanan peradilan'),
(4, 1, 'oc.lbl5', 'Peningkatan akses terhadap peluang mata pencaharian'),
(5, 1, 'oc.lbl6', 'Peningkatan akses ke pelayanan sosial'),
(6, 2, 'oc.lbl7', 'Peningkatan pengakuan dan penerimaan sosial dari kelompok-kelompok penerima manfaat'),
(7, 2, 'oc.lbl8', 'Peningkatan partisipasi masyarakat\r\n'),
(8, 2, 'oc.lbl9', 'Peningkatan perlindungan terhadap kekerasan dan eksploitasi'),
(9, 3, 'oc.lbl10', 'Kebijakan pemerintah yang responsif terhadap kebutuhan kelompok penerima manfaat'),
(10, 3, 'oc.lbl11', 'Informasi inklusi sosial tersedia lebih banyak kepada pembuat kebijakan');

-- --------------------------------------------------------

--
-- Table structure for table `outputs`
--

CREATE TABLE IF NOT EXISTS `outputs` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `intermediate_ouput_id` smallint(5) unsigned NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_outputs_id` (`intermediate_ouput_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `outputs`
--

INSERT INTO `outputs` (`id`, `intermediate_ouput_id`, `code`, `name`) VALUES
(1, 1, 'op.lbl1a', 'Penyebaran informasi tentang  BOS'),
(2, 1, 'op.lbl1b', 'Penyebaran informasi tentang program Kejar Paket A, B dan C'),
(3, 1, 'op.lbl1c', 'Penyebaran informasi tentang pendidikan formal'),
(4, 2, 'op.lbl2a', 'Bantuan dalam mendaftar ke institusi pendidikan formal'),
(5, 2, 'op.lbl2b', 'Bantuan  dalam mengakses Kejar Paket A, B dan C'),
(6, 2, 'op.lbl2c', 'Memfasilitasi kebutuhan pendidikan non formal (ketrampilan)'),
(7, 2, 'op.lbl2d', 'Bantuan  dalam mengakses BOS'),
(8, 3, 'op.lbl3a', 'Penyebaran informasi pelayanan kesehatan'),
(9, 3, 'op.lbl3b', 'Penyebaran informasi pelayanan psiko-sosial'),
(10, 4, 'op.lbl4a', 'Bantuan dalam  mengakses Pusat Kesehatan Masyarakat (Puskesmas) dan rumah sakit'),
(11, 4, 'op.lbl4b', 'Bantuan dalam mengakses Pos Layanan Terpadu (Posyandu)'),
(12, 4, 'op.lbl4c', 'Bantuan yang diberikan dalam memperoleh kartu sehat (BPJS)'),
(13, 5, 'op.lbl5a', 'Penyebaran informasi tentang pendaftaran KTP'),
(14, 6, 'op.lbl6a', 'Bantuan dalam mengakses proses penyelesaian sengketa dan layanan peradilan pidana'),
(15, 7, 'op.lbl7a', 'Bantuan dalam mendapatkan akte kelahiran'),
(16, 8, 'op.lbl8a', 'Bantuan dalam mendapatkan sertifikat pernikahan'),
(17, 9, 'op.lbl9a', 'Bantuan dalam mendapatkan KTP'),
(18, 10, 'op.lbl10a', 'Penyebaran informasi/penyuluhan tentang bantuan mata pencaharian'),
(19, 10, 'op.lbl10b', 'Penyebaran informasi tentang ketersediaan lapangan/lowongan pekerjaan'),
(20, 11, 'op.lbl11a', 'Penyebaran informasi pada hak atas tanah atau sumber daya alam'),
(21, 12, 'op.lbl12a', 'Bantuan dalam memenuhi kebutuhan akan mata pencaharian'),
(22, 13, 'op.lbl13a', 'Bantuan dalam mengamankan tanah atau sumber daya alam'),
(23, 13, 'op.lbl13b', 'Bantuan dalam memperoleh pekerjaan'),
(24, 13, 'op.lbl13c', 'Bantuan yang diberikan dalam perolehan kredit usaha atau kredit pribadi'),
(25, 14, 'op.lbl14a', 'Penyebaran informasi tentang PKH'),
(26, 14, 'op.lbl14b', 'Penyebaran informasi tentang pembuatan Surat Keterangan Tidak Mampu/Miskin (SKTM)'),
(27, 14, 'op.lbl14c', 'Penyebaran informasi tentang pembuatan Surat Keterangan Catatan Kepolisian (SKCK)'),
(28, 14, 'op.lbl14d', 'Penyebaran informasi tentang Raskin'),
(29, 14, 'op.lbl14e', 'Penyebaran informasi tentang akses listrik (PLN) dan air bersih (PDAM)'),
(30, 14, 'op.lbl14f', 'Penyebaran informasi tentang BLT'),
(31, 15, 'op.lbl15a', 'Bantuan yang diberikan dalam mengakses PKH'),
(32, 15, 'op.lbl15b', 'Bantuan yang diberikan dalam mengakses Surat Keterangan Tidak Mampu/Miskin'),
(33, 15, 'op.lbl15c', 'Bantuan yang diberikan dalam mengakses Surat Keterangan Catatan Kepolisian (SKCK)'),
(34, 15, 'op.lbl15d', 'Bantuan yang diberikan dalam mengakses Raskin'),
(35, 15, 'op.lbl15e', 'Bantuan akses terhadap listrik (PLN) dan air bersih (PDAM)'),
(36, 15, 'op.lbl15f', 'Bantuan yang diberikan dalam mengakses BLT'),
(37, 16, 'op.lbl16a', 'Memfasilitasi pertemuan dan proses rekonsilasi dengan keluarga'),
(38, 17, 'op.lbl17a', 'Memfasilitasi pelaksanaan acara yang melibatkan kelompok penerima dan kelompok lainnya'),
(39, 18, 'op.lbl18a', 'Memfasilitasi penyusunan rencana kerja kelompok eksternal untuk membantu kelompok penerima manfaat'),
(40, 19, 'op.lbl19a', 'Memfasilitasi kegiatan yang melibatkan Lembaga Perlindungan Saksi dan Korban (LPSK)'),
(41, 19, 'op.lbl19b', 'Memfasilitasi kegiatan sosial bersifat umum yang melibatkan  kelompok penerima manfaat'),
(42, 20, 'op.lbl20a', 'Partisipasi dalam pertemuan perencanaan pembangunan desa/masyarakat'),
(43, 21, 'op.lbl21a', 'Penyebaran informasi tentang Pemilu'),
(44, 21, 'op.lbl21b', 'Penyebaran informasi tentang advokasi'),
(45, 22, 'op.lbl22a', 'Bantuan dalam membentuk kelompok solidaritas berbasis identitas'),
(46, 23, 'op.lbl23a', 'Penyebaran informasi tentang layanan mediasi'),
(47, 24, 'op.lbl24a', 'Pembentukan layanan mediasi, konflik dan peringatan dini '),
(48, 24, 'op.lbl24b', 'Bantuan mendokumentasikan pelanggaran HAM'),
(49, 25, 'op.lbl25a', 'Penyebaran informasi tentang cara untuk meninggalkan pekerjaan yang eksploitatif'),
(50, 26, 'op.lbl26a', 'Bantuan dalam memperoleh program layanan masyarakat'),
(51, 27, 'op.lbl27a', 'Memfasilitasi mennyelenggaraan acara yang melibatkan pejabat publik'),
(52, 28, 'op.lbl28a', 'Informasi tentang pengucilan sosial diserahkan ke domain publik'),
(53, 29, 'op.lbl29a', 'Dibatalkan/direvisi-nya peraturan daerah dan kebijakan pemerintah pusat yang diskriminatif'),
(54, 30, 'op.lbl30a', 'Penyebaran informasi masalah inklusi sosial'),
(55, 30, 'op.lbl30b', 'Memfasilitasi pengidentifikasian masalah inklusi sosial');

-- --------------------------------------------------------

--
-- Table structure for table `pilar`
--

CREATE TABLE IF NOT EXISTS `pilar` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` enum('0','1') DEFAULT '1' COMMENT '0=inactive , 1=active',
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pilar_created_by_user_id` (`created_by`),
  KEY `fk_pilar_updated_by_user_id` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `pilar`
--

INSERT INTO `pilar` (`id`, `name`, `description`, `active`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'Remote and indigenous people', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00'),
(2, 'Transgender', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00'),
(3, 'People with Disability', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00'),
(4, 'Discriminated religious minorities & tradisional belief', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00'),
(5, 'Victims of Gross Human Rights Violations', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00'),
(6, 'Vulnerable youth and children', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00'),
(7, 'Peduli Desa', NULL, '1', 1, '2017-02-02 00:00:00', 1, '2017-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` enum('0','1') DEFAULT '1' COMMENT '0=inactive , 1=active',
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_provinces_created_by_user_id` (`created_by`),
  KEY `fk_provinces_updated_by_user_id` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `description`, `active`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'DKI Jakarta', NULL, '1', NULL, '2017-02-08 18:48:47', NULL, '2017-02-08 18:48:47'),
(2, 'Lampung', NULL, '1', NULL, '2017-02-08 18:48:47', NULL, '2017-02-08 18:48:47'),
(3, 'DI Yogyakarta', NULL, '1', NULL, '2017-02-08 18:48:48', NULL, '2017-02-08 18:48:48'),
(4, 'Nanggroe Aceh Darussalam', NULL, '1', NULL, '2017-02-08 18:48:48', NULL, '2017-02-08 18:48:48'),
(5, 'Jawa Tengah', NULL, '1', NULL, '2017-02-08 18:48:48', NULL, '2017-02-08 18:48:48'),
(6, 'Sulawesi Tengah', NULL, '1', NULL, '2017-02-08 18:48:48', NULL, '2017-02-08 18:48:48'),
(7, 'Jawa Timur', NULL, '1', NULL, '2017-02-08 18:48:49', NULL, '2017-02-08 18:48:49'),
(8, 'Nusa Tenggara Timur', NULL, '1', NULL, '2017-02-08 18:48:49', NULL, '2017-02-08 18:48:49'),
(9, 'Bangka Belitung', NULL, '1', NULL, '2017-02-08 18:48:49', NULL, '2017-02-08 18:48:49'),
(10, 'Maluku', NULL, '1', NULL, '2017-02-08 18:48:49', NULL, '2017-02-08 18:48:49'),
(11, 'Riau', NULL, '1', NULL, '2017-02-08 18:48:49', NULL, '2017-02-08 18:48:49'),
(12, 'Kalimantan Timur', NULL, '1', NULL, '2017-02-08 18:48:49', NULL, '2017-02-08 18:48:49'),
(13, 'Papua Barat', NULL, '1', NULL, '2017-02-08 18:48:50', NULL, '2017-02-08 18:48:50'),
(14, 'Banten', NULL, '1', NULL, '2017-02-08 18:48:50', NULL, '2017-02-08 18:48:50'),
(15, 'Nusa Tenggara Barat', NULL, '1', NULL, '2017-02-08 18:48:50', NULL, '2017-02-08 18:48:50'),
(16, 'Jambi', NULL, '1', NULL, '2017-02-08 18:48:50', NULL, '2017-02-08 18:48:50'),
(17, 'Sulawesi Selatan', NULL, '1', NULL, '2017-02-08 18:48:50', NULL, '2017-02-08 18:48:50'),
(18, 'Sumatera Utara', NULL, '1', NULL, '2017-02-08 18:48:51', NULL, '2017-02-08 18:48:51'),
(19, 'Jawa Barat', NULL, '1', NULL, '2017-02-08 18:48:51', NULL, '2017-02-08 18:48:51'),
(20, 'Kalimantan Tengah', NULL, '1', NULL, '2017-02-08 18:48:52', NULL, '2017-02-08 18:48:52'),
(21, 'Bengkulu', NULL, '1', NULL, '2017-02-08 18:48:52', NULL, '2017-02-08 18:48:52'),
(22, 'Kalimantan Selatan', NULL, '1', NULL, '2017-02-08 18:48:53', NULL, '2017-02-08 18:48:53'),
(23, 'Kalimantan Barat', NULL, '1', NULL, '2017-02-08 18:48:53', NULL, '2017-02-08 18:48:53'),
(24, 'Yogyakarta', NULL, '1', NULL, '2017-02-08 18:48:55', NULL, '2017-02-08 18:48:55'),
(25, 'Bali', NULL, '1', NULL, '2017-02-08 19:12:24', NULL, '2017-02-08 19:12:24'),
(26, 'Daerah Khusus Ibukota Jakarta', NULL, '1', NULL, '2017-02-08 19:12:25', NULL, '2017-02-08 19:12:25'),
(27, 'Daerah Istimewa Yogyakarta', NULL, '1', NULL, '2017-02-08 19:13:21', NULL, '2017-02-08 19:13:21'),
(28, 'Kepulauan Bangka Belitung', NULL, '1', NULL, '2017-02-08 19:14:17', NULL, '2017-02-08 19:14:17'),
(29, 'Sumatera Barat', NULL, '1', NULL, '2017-02-08 19:14:18', NULL, '2017-02-08 19:14:18'),
(30, 'Sulawesi Utara', NULL, '1', NULL, '2017-02-08 19:21:52', NULL, '2017-02-08 19:21:52'),
(31, 'Sumatera Selatan', NULL, '1', NULL, '2017-02-08 19:23:23', NULL, '2017-02-08 19:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `email2` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `full_name` varchar(150) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `updated_on` int(11) unsigned NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `is_locked` enum('0','1') NOT NULL COMMENT '''0'' = not locked, ''1'' = locked',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `email2`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `full_name`, `first_name`, `last_name`, `institution`, `phone`, `mobile`, `updated_by`, `updated_on`, `created_by`, `is_locked`) VALUES
(1, '127.0.0.1', 'admin', '$2y$08$JTqom5hmlB.ouk0UuCP8z.EWjUoOkDtgKbYvexLjgRpqNjdMFcCHW', '', 'dedy.adhiewirawan@gmail.com', NULL, '', 'TmwJLFb.howAhoDmUfl8Oue5494f41ab165d5f93', 1486002490, NULL, 1268889823, 1487487845, 1, NULL, 'Admin', 'istrator', 'ADMIN', '0', NULL, 0, 0, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `fk_activity_kabupaten_kota_id` FOREIGN KEY (`kabupaten_kota_id`) REFERENCES `kabupaten_kota` (`id`),
  ADD CONSTRAINT `fk_activity_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
  ADD CONSTRAINT `fk_activity_organization_type_id` FOREIGN KEY (`organization_type_id`) REFERENCES `organization_types` (`id`),
  ADD CONSTRAINT `fk_activity_pilar_id` FOREIGN KEY (`pilar_id`) REFERENCES `pilar` (`id`),
  ADD CONSTRAINT `fk_activity_province_id` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `fk_activity_type_id` FOREIGN KEY (`activity_type_id`) REFERENCES `activity_types` (`id`);

--
-- Constraints for table `activity_outputs`
--
ALTER TABLE `activity_outputs`
  ADD CONSTRAINT `fk_activity_output_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`),
  ADD CONSTRAINT `fk_activity_output_output_id` FOREIGN KEY (`output_id`) REFERENCES `outputs` (`id`);

--
-- Constraints for table `domain_perubahan`
--
ALTER TABLE `domain_perubahan`
  ADD CONSTRAINT `fk_domain_perubahan_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `emails`
--
ALTER TABLE `emails`
  ADD CONSTRAINT `fkemails_created_by_user_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fkemails_updated_by_user_id` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fkemail_logs_created_by_user_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fkemail_log_updated_by_user_id` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `intermediate_outputs`
--
ALTER TABLE `intermediate_outputs`
  ADD CONSTRAINT `fk_intermediate_outputs_id` FOREIGN KEY (`outcome_id`) REFERENCES `outcomes` (`id`);

--
-- Constraints for table `kabupaten_kota`
--
ALTER TABLE `kabupaten_kota`
  ADD CONSTRAINT `fk_kabupaten_kota_created_by_user_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_kabupaten_kota_updated_by_user_id` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_kabupaten_province_id` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

--
-- Constraints for table `msc`
--
ALTER TABLE `msc`
  ADD CONSTRAINT `fk_msc_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_msc_kabupaten_kota_id` FOREIGN KEY (`kabupaten_kota_id`) REFERENCES `kabupaten_kota` (`id`),
  ADD CONSTRAINT `fk_msc_pilar_id` FOREIGN KEY (`pilar_id`) REFERENCES `pilar` (`id`),
  ADD CONSTRAINT `fk_msc_province_id` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `fk_msc_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `msc_domain`
--
ALTER TABLE `msc_domain`
  ADD CONSTRAINT `fk_msc_domain_domain_id` FOREIGN KEY (`domain_id`) REFERENCES `domain_perubahan` (`id`),
  ADD CONSTRAINT `fk_msc_domain_msc_id` FOREIGN KEY (`msc_id`) REFERENCES `msc` (`id`);

--
-- Constraints for table `msc_outcomes`
--
ALTER TABLE `msc_outcomes`
  ADD CONSTRAINT `fk_msc_outcomes_msc_id` FOREIGN KEY (`msc_id`) REFERENCES `msc` (`id`),
  ADD CONSTRAINT `fk_msc_outcomes_outcome_id` FOREIGN KEY (`outcome_id`) REFERENCES `outcomes` (`id`);

--
-- Constraints for table `narrative`
--
ALTER TABLE `narrative`
  ADD CONSTRAINT `fk_narrative_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_narrative_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
  ADD CONSTRAINT `fk_narrative_pilar_id` FOREIGN KEY (`pilar_id`) REFERENCES `pilar` (`id`),
  ADD CONSTRAINT `fk_narrative_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `fk_organization_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `organizations` (`id`),
  ADD CONSTRAINT `fk_organization_province_id` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `fk_organization_province_id_2` FOREIGN KEY (`province_id_2`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `fk_organization_type_id` FOREIGN KEY (`organization_type_id`) REFERENCES `organization_types` (`id`);

--
-- Constraints for table `outcomes`
--
ALTER TABLE `outcomes`
  ADD CONSTRAINT `fk_outcomes_objective_id` FOREIGN KEY (`objective_id`) REFERENCES `objectives` (`id`);

--
-- Constraints for table `outputs`
--
ALTER TABLE `outputs`
  ADD CONSTRAINT `fk_outputs_id` FOREIGN KEY (`intermediate_ouput_id`) REFERENCES `intermediate_outputs` (`id`);

--
-- Constraints for table `pilar`
--
ALTER TABLE `pilar`
  ADD CONSTRAINT `fk_pilar_created_by_user_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_pilar_updated_by_user_id` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `fk_provinces_created_by_user_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_provinces_updated_by_user_id` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
