<?php

/*
* Input : yyyy/mm/dd
* return : yyyy-mm-dd
*/
function dbdate($date,$delimiter="/"){
    list($year,$month,$date) = explode($delimiter,$date);
    
    return $year.'-'.$month.'-'.$date;
}

/*
* Input : yyyy-mm-dd
* return : dd/mm/yyyy
*/
function iddate($date,$delimiter="-"){
     list($year,$month,$date) = explode($delimiter,$date);
    
    return $date."/".$month."/".$year;
}

function date_indo($date){

$datefmt = new DateTime($date);
return $datefmt->format('d/m/Y');

}

function dategte($date){
	$date1 = new DateTime("now");
	$date2 = new DateTime($date);
	
	if($date2 >= $date1)
		return true;
	else
		return false;

}

function datelt($date){
	$date1 = new DateTime("now");
	$date2 = new DateTime($date);
	
	if($date2 < $date1)
		return true;
	else
		return false;
}

/*function datediff($date1,$date2){
	$datea = new DateTime($date1);
	$dateb = new DateTime($date2);
	
	$interval = $datea->diff($dateb);
	return $interval->format('%a');
}*/

