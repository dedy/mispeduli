 <link href='<?=assets_url()?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
 <link href='<?=assets_url()?>css/dataTables.bootstrap.css' rel='stylesheet'>

   <script>
	$(document).ready(function(){
		//datatable
	    $('.datatable').dataTable({
	        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12 center-block'p>>",
	        "sPaginationType": "bootstrap",
	        "oLanguage": {
	            "sLengthMenu": "_MENU_ records per page"
	        }
	         ,"bLengthChange": false
	       	 //,"order": [[ 5, "desc" ]]
	       	 //,"aaSorting": [[ 5, "desc" ]] // Sort by fift column descending
			 //,"orderSequence": [ "desc" ], "targets": [ 3 ] 
	    });
	});
</script>

    <div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url('Dashboard')?>">Home</a>
        </li>
        <li>
            <a href="#"><?=$title?></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title?></h2>
            </div>
            <div class="box-content row">
	            <div class="col-lg-12">
		            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
					    <thead>
					    	<tr>
					    		<?
					    		if($type == 2) { ?>	
					       			<th>EO</th>
					       			<th>CSO</th>
					       			<th><?=lang('laddress')?></th>
					       			<th><?=lang('lphone')?></th>
					       			<th><?=lang('lpic')?></th>
					       			<th><?=lang('ltarget_group')?></th>
					       			<th><?=lang('ltotal_target_group')?></th>
					       			<th><?=lang('lpilar')?></th>
					       			<th><?=lang('lactive')?></th>
					       		<?}else{?>		
						       		<th><?=lang('lname')?></th>
						       	<?}?>
						    </tr>

					    </thead>
					    <tbody>
					    
					    <?php
					    foreach($datas as $data) {
						?>
					    <tr>
					        <td><?=$data->parent_name?></td>
					        <td><?=$data->organization_name?></td>
					        <td><?=$data->organization_address?></td>
					        <td><?=$data->organization_phone?></td>
					        <td><?=$data->organization_pic?></td>
					        <td><?=$data->organization_target_group?></td>
					        <td><?=$data->organization_total_target_group?></td>
					        <td><?=$data->pilar_name?></td>
					        <td><?if($data->organization_active == '1') echo lang('lyes');else echo lang('lno');?></td>
					    </tr>
					    <?}?>


					    </tbody>
				    </table>
	            </div>
        	</div>
    	</div>
  	</div>
</div>

<!-- library for making tables responsive -->
<script src="<?=assets_url()?>bower_components/responsive-tables/responsive-tables.js"></script>

<!-- data table plugin -->
<script src='<?=assets_url()?>js/jquery.dataTables.min.js'></script>
<script src='<?=assets_url()?>js/dataTables.bootstrap.js'></script>
