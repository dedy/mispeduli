<?php
class Organization_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function data_exists_by_name($name,$type='',$address="",$province_id=''){
		$this->db->where('name',$name);
		if($type) {
			$this->db->where('organization_type_id',$type);
		}
		
		if($address){
			$this->db->where('address',$address);
		}

		if($province_id){
			$this->db->where('province_id',$province_id);
		}
		
	    $query = $this->db->get('organizations');
	    if ($query->num_rows() > 0){
	    	//get the id 
	    	foreach ($query->result() as $row)
			{
			    return $row->id;
			}
	    }
	    else{
	        return false;
	    }
  	}

  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){

  		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
  			$this->db->where("organizations.organization_type_id",$searchval['organization_type_id']);
  		}
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("organizations.name as name");
			$this->db->select("organizations.id as id");
			$this->db->select("a.name as parent_name");
			$this->db->select("organization_types.name as type_name");
			$this->db->select("organizations.id as organization_id");
			$this->db->select("organizations.organization_type_id as organization_type_id");
			$this->db->select("organizations.province_id as organization_province_id");
			$this->db->select("organizations.name as organization_name");
			$this->db->select("organizations.name2 as organization_name2");
			$this->db->select("organizations.address as organization_address");
			$this->db->select("organizations.phone as organization_phone");
			$this->db->select("organizations.person_in_charge as organization_pic");
			$this->db->select("organizations.active as organization_active");
			$this->db->select("organizations.total_target_group as organization_total_target_group");
			$this->db->select("organizations.target_group as organization_target_group");
			$this->db->select("organizations.active as organization_active");
			$this->db->select("b.name as pilar_name");

			$this->db->join("organization_types","organization_types.id = organizations.organization_type_id");
			$this->db->join("organizations a","a.id = organizations.parent_id",'left outer');
			$this->db->join("pilar b","b.id = organizations.pilar_id",'left outer');
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by('organization_active','desc');
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('organizations');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}


  function get_data_by_id($id){
    	$data = $this->searchSimple(0,1,'id','asc',array("id"=>$id));
    	return $data;
    }



	function searchSimple($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){

		if(isset($searchval['id']) && $searchval['id'] ){
    		$this->db->where("id",$searchval['id']);
    	}
    	
  		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
  			$this->db->where("organizations.organization_type_id",$searchval['organization_type_id']);
  		}

  		if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
  			$this->db->where("organizations.pilar_id",$searchval['pilar_id']);
  		}
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("organizations.name as organization_name");
			$this->db->select("organizations.id as organization_id");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('organizations');
		
		$query = $this->db->get();
		
 		return $query->result();
    }

	function get_org_type_id($org_type){
        $org_type = trim($org_type);
        $orgs = array(
            array("value"=>"CSO","id"=>2)
            ,array("value"=>"EO","id"=>1)
            ,array("value"=>"TAF","id"=>3)
        );

        foreach($orgs as $org){
            if(strtoupper($org_type) == $org['value']){
                return $org['id'];
            }
        }

        return '0';
    }

    function set_organizations(){
        $organizations = $this->search(0,1,'organization_name','asc');

        if(count($organizations)){
            foreach($organizations as $organization){
                array_push($this->datas,
                    array(
                        "id"     =>  $organization->organization_id
                        ,"name"  =>  $organization->organization_name
                        ,"name2"  =>  $organization->organization_name2
                        ,"province_id"  =>  $organization->organization_province_id
                        ,"type_id" =>  $organization->organization_type_id
                         )
                );
            }
        }   
    }


    function get_organization_id($organization_name,$organization_type_id,$province_id=0){
    	$organization_name = strtoupper(trim($organization_name));
        $total = count($this->datas);
              
        $organization_id =  0;
        $is_found = false;
        //check in array
        if($total){
            echo "check organization in array ".$organization_name.";";

  			if($organization_type_id == 1){
               //untuk EO ga perlu check province
  				foreach($this->datas as $value){
  					$value['name'] = strtoupper(trim($value['name']));
	                if( $value['name'] == $organization_name){
	                    $organization_id = $value['id'];
	                    $is_found = true;
	                    break;
	                }
            	}

            }else{
            
				foreach($this->datas as $value){
					//if($value['province_id'] == $province_id) {
	  					$value['name'] = strtoupper(trim($value['name']));
	  					$value['name2'] = strtoupper(trim($value['name2']));

		                if($value['name'] == $organization_name) {
							$organization_id = $value['id'];
		                    $is_found = true;
		                    break;
		                }else
		                if($value['name2'] == $organization_name) {
		                    $organization_id = $value['id'];
		                    $is_found = true;
		                    break;

		                }
	              //  }
            	}
            }
        }else{
            $is_found = false;
        }

        return $organization_id;
    }
	
}