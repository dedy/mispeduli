<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Organization extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
		$this->load->model('organization_model');
	}
	
	function index(){
    	$this->lists(1);
	}
	
	function eo(){
    	$this->lists(1);
	}

	function cso(){
    	$this->lists(2);
	}

   	
  	function lists($type=1){
   		$this->load->model('organization_model');
   		$datas = $this->organization_model->search(0,1,'parent_name,organization_name','asc',array('organization_type_id'=>$type));
   		//echo $this->db->last_query();
   		$this->viewparams['title'] = ($type == 1) ? "EO (Executing Organization)":"CSO (Civil Society Organization)";
   		$this->viewparams['type'] = $type;
   		$this->viewparams['datas'] = $datas;

   		if($type == 1){
   			parent::viewpage("vlist_eo");
		}else{
			parent::viewpage("vlist_cso");
		}
	}

	function get_by_pilar($pilar_id=0,$organization_type_id=0){

		$data = $this->organization_model->searchSimple(0,0,'organization_id','asc',array("pilar_id"=>$pilar_id,"organization_type_id"=>$organization_type_id));

		echo json_encode($data);
	}
	

}

/* End of file dashboard.php */
/* Location: ./apps/modules/dashboard/controllers/dashboard.php */
