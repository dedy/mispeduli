<?php
class Msc_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   	function insert_outcome($data){
		$this->db->insert("msc_outcomes", $data); 
		return $this->db->insert_id();
    }

    function insert_domain($data){
		$this->db->insert("msc_domain", $data); 
		return $this->db->insert_id();
    }

  	
  	function insert($data){
		$this->db->insert("msc", $data); 
		return $this->db->insert_id();
    }
    
 
    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
    	if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
    		$this->db->where("msc.pilar_id",$searchval['pilar_id']);
    	}

    	if(isset($searchval['province_id']) && $searchval['province_id']){
    		$this->db->where("msc.province_id",$searchval['province_id']);
    	}
	
	    if(isset($searchval['msc_title']) && $searchval['msc_title']){
		    $this->db->like("msc_title",$searchval['msc_title']);
	    }

		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
		    $this->db->where("organization_types.id",$searchval['organization_type_id']);
	    }

		if(isset($searchval['organization_id']) && $searchval['organization_id']){
    		$this->db->where("organizations.id",$searchval['organization_id']);
    	}

		if(isset($searchval['domain_id']) && $searchval['domain_id']){
    		$this->db->where("domain_perubahan.id",$searchval['domain_id']);
    	}
	    
      	if(isset($searchval['organization_name']) && $searchval['organization_name']){
    		$this->db->like("organizations.name",$searchval['organization_name']);
    	}

    	if(isset($searchval['date_from']) && $searchval['date_from']){
    		$this->db->where("date >=",$searchval['date_from']);
    	}

    	if(isset($searchval['year']) && $searchval['year']){
    		$this->db->where("EXTRACT(YEAR FROM date) = '".$searchval['year']."' ");
    	}

    	if(isset($searchval['date_to']) && $searchval['date_to']){
    		$this->db->where("date <=",$searchval['date_to']);
    	}

    	if(isset($searchval['keyword']) && $searchval['keyword']){
    		$qq = "(msc_title like '%".$searchval['keyword']."%'";
    		$qq .= " OR related_goals like '%".$searchval['keyword']."%'";
    		$qq .= " OR conversation like '%".$searchval['keyword']."%') ";
    		$this->db->where($qq);
    	}

		if($this->is_count){
			$this->db->select("count(msc.id) as total");
		}{
			$this->db->select("msc.*");
			$this->db->select("msc.id as msc_id");
			$this->db->select("pilar.name as pilar_name");
			$this->db->select("organizations.name as organization_name");
			$this->db->select("organization_types.name as organization_type_name");
			$this->db->select("provinces.name as province_name");
			$this->db->select("kabupaten_kota.name as kabupaten_kota_name");
            $this->db->select("DATE_FORMAT(date ,'%d/%m/%Y') as date_fmt",false);
			$this->db->select("domain_perubahan.name as domain_name");
		
		    /*$change_level_records =  $this->config->item('change_level');
		    for($li=1;$li<=$change_level_records;$li++) {
			    $this->db->select("(CASE WHEN msc.change_level = ".$li." THEN 1 ELSE '' END) as change_level_".$li);
		    }*/
		    
			$this->db->join("pilar","pilar.id = msc.pilar_id");
			$this->db->join("provinces","provinces.id = msc.province_id","left outer");
			$this->db->join("kabupaten_kota","kabupaten_kota.id = msc.kabupaten_kota_id","left outer"); //hack for left join no data in kab table
		}
		
		$this->db->join("organizations","organizations.id = msc.organization_id");
		$this->db->join("organization_types","organization_types.id = organizations.organization_type_id");
		$this->db->join("domain_perubahan","domain_perubahan.id = msc.domain_id","left outer"); //hack for left join no data in kab table
    
    
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
			$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('msc');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
	
	function search_group_by_pilar($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
		if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
			$this->db->where("m.pilar_id",$searchval['pilar_id']);
		}
		if(isset($searchval['domain_id']) && $searchval['domain_id']){
			$this->db->where("m.domain_id",$searchval['domain_id']);
		}
		
		if(isset($searchval['date_from']) && $searchval['date_from']){
			$this->db->where("m.date >=",$searchval['date_from']);
		}
		
		if(isset($searchval['date_to']) && $searchval['date_to']){
			$this->db->where("m.date <=",$searchval['date_to']);
		}
		
		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
			$this->db->where("o.organization_type_id",$searchval['organization_type_id']);
		}
		
		if(isset($searchval['organization_id']) && $searchval['organization_id']){
			$this->db->where("m.organization_id",$searchval['organization_id']);
		}
		
		if($this->is_count){
			$this->db->select("count(m.id) as total");
		}{
			$this->db->select("m.id as msc_id");
			$this->db->select("count(m.id) as total_msc");
			$this->db->select("p.name as pilar_name");
			//$this->db->select("dp.name as domain_name");
			
			$query = $this->db->query("SELECT id,name FROM domain_perubahan ORDER BY name asc");
			$result = $query->result();
			if($result) {
				foreach($result as $key => $res) {
					$qq = "(	SELECT count(mm.id) FROM msc as mm ";
					$qq .= "		LEFT OUTER JOIN `organizations` `o1` ON `o1`.`id` = `mm`.`organization_id` ";
					$qq .= "		WHERE mm.pilar_id = m.pilar_id AND mm.domain_id = '" .$res->id."' ";
					if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
						$qq .= "		AND	`o1`.`organization_type_id` = '".$searchval['organization_type_id']."' ";
					}
					
					if(isset($searchval['organization_id']) && $searchval['organization_id']){
						$qq .= "		AND	`mm`.`organization_id` = '".$searchval['organization_id']."' ";
					}
			
					$qq .= "	) AS ".url_title($res->name,'underscore',true);

					$this->db->select($qq);
				}
			}
			
			$query = $this->db->query("SELECT id,code FROM outcomes ORDER BY name asc");
			$result = $query->result();
			if($result) {
				foreach($result as $key => $res) {
					$this->db->select("(SELECT 
										count(mm.pilar_id) 
									FROM 
										msc_outcomes mo 
									INNER JOIN 
										msc as mm ON mm.id = mo.msc_id
									WHERE 
										mm.pilar_id = m.pilar_id AND mo.outcome_id = " .$res->id." 
										AND	mm.organization_id = m.organization_id
										) AS ".url_title($res->code,'underscore',true)
					);
				}
			}
			
		}
		//join all table
		$this->db->join("pilar as p","p.id = m.pilar_id");
		//$this->db->join("domain_perubahan as dp","dp.id = m.domain_id","left outer"); //hack for left join no data in kab table

		$this->db->join("organizations o","o.id = m.organization_id","left outer"); //hack for left join no data in kab table
		//$this->db->join("organization_types as ot","ot.id = o.organization_type_id","left outer"); //hack for left join no data in kab table

    	$this->db->group_by('m.pilar_id');
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
			
			if($sidx)
				$this->db->order_by($sidx,$sord);
		}
    	
		$this->db->from('msc as m');
		$query = $this->db->get();
		return $query->result();
	}
    
    public function msc_outcome_num_rows($searchval=array()) {
	    if(isset($searchval['msc_id']) && $searchval['msc_id']){
		    $this->db->where("msc_id",$searchval['msc_id']);
	    }
	
	    if(isset($searchval['outcome_id']) && $searchval['outcome_id']){
		    $this->db->where("outcome_id",$searchval['outcome_id']);
	    }
	
	    $this->db->from('msc_outcomes');
	    $query = $this->db->get();
	    return $query->num_rows();
    }
	
	public function outcome(){
		$this->db->select("outcomes.name as outcome_name");
		$this->db->from('outcomes');
		$query = $this->db->get();
		$records =  $query->result();
		
	}
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
	/* countSearchUser */
	function countSearchGroupByPilar($searchval=array()){
		$this->is_count = true;
		$res = $this->search_group_by_pilar(0,0,'','',$searchval);
		return $res[0]->total;
	}
	
}