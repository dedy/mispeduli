<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>

<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo assets_url(); ?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>


<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>

<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>

<script type="text/javascript">
	jQuery().ready(function (){
		 $("#organization_spinner").hide();
           $("select#pilar_id").change(function(){
           var organization_type_id = $("#organization_type_id").val();
           populate_organization($(this).val(),organization_type_id);
        });

        $("select#organization_type_id").change(function(){
           var pilar_id = $("#pilar_id").val();
            populate_organization(pilar_id,$(this).val());
        });

         populate_organization(0,0);
		
		$("#_date_from").click(function(){
			$("#date_from").val("");
		});
		
		$("#_date_to").click(function(){
			$("#date_to").val("");
		});
		
		$('.datepicker').datepicker(
			{
				changeMonth: true
				,changeYear: true
				,yearRange: '<?=config_item('year_start')?>:' + new Date().getFullYear()
				,beforeShow: function (textbox, instance) {
				var txtBoxOffset = $(this).offset();
				var top = txtBoxOffset.top;
				var left = txtBoxOffset.left;
				var textBoxHeight = $(this).outerHeight();
				setTimeout(function () {
					instance.dpDiv.css({
						top: top-$("#ui-datepicker-div").outerHeight(),
						left: left
					});
				}, 0);
			}
				,dateFormat: "yy-mm-dd"
			}
		);
		
		//chosen - improves select
		$('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect:true});
		$("#submit-btn").click(function(e){
			e.preventDefault();
			$("#submit_spinner").show();
			gridReload();
			return false;
		});
		
		$('#export-btn').click(function(e){
			//$('form#search-frm').submit();
			//return;
			e.preventDefault();
			var param = getParam();
			
			$.confirm({
				title:"Export MSC Report",
				text: "<?=lang('lexport_confirm')?>",
				confirmButton: "Ok",
				cancelButton: "Cancel",
				confirm: function(button) {
					$.post("<?=base_url()?>msc/do_msc_report_export/?"+param,
						function(data) {
							if(!data.is_error){
								window.location.href= data.file_url;
							}else{
								var rv = '<div class="alert alert-danger">'+data.message+'</div>';
								$('#show_message').html(rv);
								$('#show_message').slideDown('normal');
							}
							
							
							
						},"json"
					);
				},
				cancel: function(button) {
					// alert("You cancelled.");
				}
			});
			
			
		});
		
		
		jQuery("#list1").jqGrid({
			url:'<?=site_url('msc/reportLoadDataGrid')?>',      //another controller function for generating data
			mtype : "post",             //Ajax request type. It also could be GET
			datatype: "json",            //supported formats XML, JSON or Arrray
			colNames:[
				'No',
				<?php foreach($column as $xcol):?>
				'<?=$xcol?>',
				<?php endforeach?>
			],
			colModel:[
				{name:'no',index:'no', width:25, align:"right",sortable:false},
				<?php
				$i=0;
				foreach($field as $xfield):?>
				{name:'<?=$xfield?>',index:'<?=$xfield?>',align:'<?=$align[$i]?>',stype:'text',width:<?=$width[$i]?>},
				<?php 
				$i++;
				endforeach?>
			],
			rowNum:<?=$rowNum?>,
			<?php if(isset($rowList) && $rowList){?>
			rowList:[<?=$rowList?>],
			<?php }?>
			width: <?=$rowWidth?>,
			height: <?=$rowHeight?>,
			pager: '#pager1',
			viewrecords: true,
			sortname: 'name',
			caption:"<?=$title_page?>",
			footerrow: true,
			userDataOnFooter: true, // show custom data from JSON response to the footer - the Grand Total
			sortname: 'date',
			toppager: true,
			shrinkToFit:false,
			 hoverrows:false,
			  loadComplete: function(data) {
				var $self = $(this);
				<?php foreach($field as $xfield):?>
				<?php if($xfield!='pilar_name'):?>
				sum = $self.jqGrid("getCol", "<?=$xfield?>", false, "sum");
				$self.jqGrid("footerData", "set", {invdate: "Total:", <?=$xfield?>: sum});
				<?php endif?>
				<?php endforeach?>
				
			}
		}).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		
		jQuery("#list1").jqGrid('setGroupHeaders', {
			useColSpanStyle: true,
			groupHeaders:[
				{startColumnName: '<?=$domain['label']?>', numberOfColumns: <?=$domain['total']?>, titleText: '<?=lang('ldomain')?>'},
				{startColumnName: '<?=$outcome['label']?>', numberOfColumns: <?=$outcome['total']?>, titleText: '<?=lang('loutcome')?>'}
			]
		});
		
		
		
	});

	  function populate_organization(pilar_id,organization_type_id){
        $("#organization_spinner").show();
        $.getJSON("<?=site_url('organization/get_by_pilar')?>/"+pilar_id+'/'+organization_type_id,
            function(j){
              var options = '<option value=""></option>';
      
              for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].organization_id + '">' + j[i].organization_name+ '</option>';
              }
              $("select#organization_id").html(options).trigger('chosen:updated');
              $("#organization_spinner").hide();
            }
        );
    }

	function getParam(){
		var pilar_id = (jQuery("#pilar_id option:selected").val())?jQuery("#pilar_id option:selected").val():'0';
		var domain_id = (jQuery("#domain_id option:selected").val())?jQuery("#domain_id option:selected").val():'0';
		var organization_id =(jQuery("#organization_id").val())?jQuery("#organization_id").val():0;
        var organization_type_id =(jQuery("#organization_type_id").val())?jQuery("#organization_type_id").val():0;
	   
		var date_from =(jQuery("#date_from").val())?jQuery("#date_from").val():0;
		var date_to =(jQuery("#date_to").val())?jQuery("#date_to").val():0;
		//var param = 'pilar_id='+pilar_id+'&date_from='+date_from+'&date_to='+date_to+'&domain_id='+domain_id;
		var param = 'organization_id='+organization_id+ '&organization_type_id='+organization_type_id+'&pilar_id='+pilar_id+'&date_from='+date_from+'&date_to='+date_to;
		return param;
	}
	
	function gridReload(){
		var param=getParam();
		jQuery("#list1").jqGrid('setGridParam',{
			url:"<?=site_url('msc/reportLoadDataGrid')?>/?"+param,
			page:1
		}).trigger("reloadGrid");
	}
</script>


<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url('Dashboard')?>">Home</a>
		</li>
		<li>
			<a href="#"><?=$title_page?></a>
		</li>
	</ul>
</div>

<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well">
				<h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title_page?></h2>
			</div>
			
			<div class="box-content">
				<?php echo form_open("msc/loadDataGrid",array('role'=>"form",'id'=>'input-form' ))?>
				
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class='control-label' for="pilar_id"><?=lang('lpilar');?></label>
							<div class="controls">
								
								<?php echo form_dropdown('pilar_id', $pilar_options,'0',$pilar_params);?>
							</div>
						</div>
						
						<div class="form-group">
                                <label class='control-label' for="organization_type_id"><?=lang('lorganization_type');?></label>
                                <div class="controls">
                                        <?php echo form_dropdown('organization_type_id', $organization_type_options,'0',$organization_type_params);?>
                                </div>                         
                            </div>

                               <div class="form-group">
                                <label class='control-label' for="organization_id"><?=lang('lorganization');?></label>
                                <div class="controls">
                                    <select id='organization_id' name='organization_id' class='form-control input-xxlarge' data-rel='chosen' data-placeholder='<?=lang('lplease_select_organization')?>'>
                                    <option value=''></option>
                                    </select>
                                    <img src="<?=base_url()?>assets/img/ajax-loaders/spinner-mini.gif" id="organization_spinner">
                                    </div>         
                                </div>                         
                            </div>

						<div class="form-group">
							<label for="start_date_from"><?php echo lang('ldate');?></label>
							<div class="controls">
								<input  class="input-small datepicker" type="text" name='date_from' id='date_from' value="">
								<img id="_date_from" border="0" src="<?=assets_url()?>img/b_del.gif">
								&nbsp;<?=lang('lto')?>&nbsp;
								<input  class=" input-small datepicker" type="text" name='date_to' id='date_to' value="">
								<img id="_date_to" border="0" src="<?=assets_url()?>img/b_del.gif">
							</div>
						</div>
					</div>
					<!-- <div class="col-md-6">
						<div class="form-group">
							<label class='control-label' for="domain_id"><?=lang('ldomain_perubahan');?></label>
							<div class="controls">
								
								<?php echo form_dropdown('domain_id', $domain_options,'0',$domain_params);?>
							</div>
						</div>
						
					</div>-->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id='show_message' style="display: none;"></div>
					</div>
					<div class="col-md-4">
						&nbsp;
					</div>
					<div class="col-md-8">
						<button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsubmit')?></button>
						<button type="button" class="btn btn-primary" id='export-btn'><?=lang('lexport')?></button>
					</div>
				</div>
				
				<?=form_close();?>
				<div class="row">
					<div class="col-lg-12">
						<br/>
						<table id="list1"></table> <!--Grid table-->
						<div id="pager1"></div>  <!--pagination div-->
					</div>
				</div>
			
			</div><!-- end box-content -->
		
		</div><!-- /end box-inner -->
	</div>
</div>

<!-- select or dropdown enhancer -->
<script src="<?php echo assets_url(); ?>bower_components/chosen/chosen.jquery.min.js"></script>