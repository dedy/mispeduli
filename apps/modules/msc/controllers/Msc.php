<?php
/**
 * Created by PhpStorm.
 * User: BDO-IT
 * Date: 20/02/2017
 * Time: 15:40
 */

require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Msc extends MY_Controller {
	var $columns = array(
		"msc_id",
		"organization_type_name",
		"pilar_name",
		"organization_name",
		"msc_title",
		"province_name",
		"domain_name",
		"related_goals",
		"date_fmt",
		"conversation"
		);
	
	var $domain_header = array();
	var $outcome_header = array();
	
	public function __construct() {
		parent::__construct();
		if(!isLoggedIn()){
			redirect('auth/logout','refresh');
		}
		$this->load->model(array('msc_model','outcome/outcome_model'));
	}
	
	public function index() {
		$this->lists();
	}
	
	public function lists() {
		$this->load->helper('form');
		$this->load->model('pilar/pilar_model');
		$this->load->model('organization_type/organization_type_model');
		$this->load->model('organization/organization_model');
		$this->load->model('domain/domain_model');
		$this->load->model('province/province_model');
		
		/* get pilar list */
		$pilars = $this->pilar_model->search(0,1,'name','asc');
		$pilar_options = array();
		$pilar_options[0] = '';
		foreach($pilars as $pilar){
			$pilar_options[$pilar->id] = $pilar->name;
		}
		$this->viewparams['pilar_options'] = $pilar_options;
		$this->viewparams['pilar_params'] = array(
			'id'   => 'pilar_id',
			'class'	=> "form-control input-xxlarge",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_pilar')
		);
		/* end pilar */
		
		/* get organization type list */
		$organization_types = $this->organization_type_model->search(0,1,'name','asc');
		$organization_type_options = array();
		$organization_type_options[0] = '';
		foreach($organization_types as $organization_type){
			$organization_type_options[$organization_type->id] = $organization_type->name;
		}

		$this->viewparams['organization_type_options'] = $organization_type_options;
		$this->viewparams['organization_type_params'] = array(
			'id'   => 'organization_type_id',
			'class'	=> "form-control input-medium",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_organization_type')
		);
		/* end organization type */

		/* get domain list */
		$domains = $this->domain_model->search(0,1,'name','asc');
		$domain_options = array();
		$domain_options[0] = '';
		foreach($domains as $domain) {
			$domain_options[$domain->id] = $domain->name;
		}

		$this->viewparams['domain_options'] = $domain_options;
		$this->viewparams['domain_params'] = array(
			'id'   => 'domain_id',
			'class'	=> "form-control input-xlarge",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_domain')
		);
		/* end domain  */
		
		/* year list */
		$year_lists = config_item("year_filter");
		$year_options = array();
		$year_options[0] = '';
		foreach($year_lists as $year_list) {
			$year_options[$year_list] = $year_list;
		}
		$this->viewparams['year_options'] = $year_options;
		$this->viewparams['year_params'] = array(
			'id'   => 'year',
			'class'	=> "form-control input-medium",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_year')
		);
		/* end year  */
		

		$title = array(
			'type'  => 'text',
			'name'  => 'title',
			'id'    => 'title',
			'value' => '',
			'placeholder'  => lang('lplease_input_title'),
			'class' => 'form-control input-xxlarge '
		);

		$this->viewparams['title'] = $title;
		
		/*$organization = array(
			'type'  => 'text',
			'name'  => 'organization',
			'id'    => 'organization',
			'value' => '',
			'placeholder'   => lang('lplease_input_organization_name'),
			'class' => 'form-control input-xxlarge '
		);
		
		$this->viewparams['organization'] = $organization;*/
		
		/* provinces */
		$provinces = $this->province_model->search(0,1,'name','asc');
		$province_options = array();
		$province_options[0] = '';
		foreach($provinces as $province){
			$province_options[$province->id] = $province->name;
		}
		$this->viewparams['province_options'] = $province_options;
		$this->viewparams['province_params'] = array(
			'id'   => 'province_id',
			'class'	=> "form-control input-xlarge",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_province')
		);
		/* end province */
		
		
		$this->viewparams['title_page']	= lang('lmsc');
		parent::viewpage("vlist");
	}
	
	public function loadDataGrid () {
		$page = isset($_POST['page']) ? $_POST['page']:1;
		$sidx = isset($_POST['sidx']) ? $_POST['sidx']:'date'; // get index row - i.e. user click to sort
		$sord = isset($_POST['sord']) ? $_POST['sord']:'desc'; // get the direction
		$limit = isset($_POST['rows'])? $_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
		
		$filter = array(
			'pilar_id' => $this->input->get('pilar_id'),
			'msc_title' => trim($this->input->get('title')),
			'organization_type_id' => trim($this->input->get('organization_type_id')),
			'organization_id' => trim($this->input->get('organization_id')),
			'domain_id' => trim($this->input->get('domain_id')),
			'province_id' => $this->input->get('province_id'),
			'date_from' => $this->input->get('date_from'),
			'date_to' => $this->input->get('date_to'),
			'year' => $this->input->get('year'),
		    'keyword'     => urldecode($this->input->get('keyword'))
  		);
		
		$query = $this->msc_model->search($limit,$page,$sidx,$sord,$filter);
		$this->firephp->log($this->db->last_query());
		$count = $this->msc_model->countSearch($filter);
		$this->firephp->log($this->db->last_query());
		$this->firephp->log("count : ".$count);
		$columns = $this->columns;
		
		  if(count($query)){
            for($i=0;$i<count($query);$i++){
                $query[$i]->related_goals = nl2br($query[$i]->related_goals);
                $query[$i]->msc_title = nl2br($query[$i]->msc_title);
            }
        }

		$this->DataJqGrid = array(
			"page"		=> $page,
			"sidx"		=> $sidx,
			"sord"		=> $sord,
			"query" 	=> $query,
			"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> $columns,
			"id"		=> "msc_id"
		);
		
		parent::loadDataJqGrid();
		
	}
	
	public function do_export() {
		$objPHPExcel = new PHPExcel();
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel,"Excel2007");
		$objPHPExcel->getProperties()
			->setTitle("MSC Report")
			->setDescription("MSC Report");
		$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$border = array( 'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )));
		$fill = array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'rotation'   => 0,
			'startcolor' => array (
				'rgb' => 'CCCCCC'
			),
			'endcolor'   => array (
				'argb' => 'CCCCCC'
			)
		);
		
	
		$n = 0;
		$column[$n]['title'] = lang('lorganization_type');
		$column[$n]['field'] = 'organization_type_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lpilar');
		$column[$n]['field'] = 'pilar_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$n++;
		$column[$n]['title'] = lang('lorganization');
		$column[$n]['field'] = 'organization_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;

		$n = $n + 1;
		$column[$n]['title'] = lang('ltitle');
		$column[$n]['field'] = 'msc_title';
		$column[$n]['auto_size'] = False;
		$column[$n]['width'] = 106;
		
		//$n = $n + 1;
		//$column[$n]['title'] = lang('lkabupaten_kota');
		//$column[$n]['field'] = 'kabupaten_kota_name';
		//$column[$n]['auto_size'] = True;
		//$column[$n]['width'] = 0;
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lprovince');
		$column[$n]['field'] = 'province_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		/*$n = $n + 1;
		$column[$n]['title'] = lang('lchange_level');
		$column[$n]['field'] = 'change_level';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;*/

		$n = $n + 1;
		$column[$n]['title'] = lang('ldomain_perubahan');
		$column[$n]['field'] = 'domain_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lrelated_goal');
		$column[$n]['field'] = 'related_goals';
		$column[$n]['auto_size'] = false;
		$column[$n]['width'] = 100;


		/*
		$change_level_records =  $this->config->item('change_level');
		$change_level_records_auto_size = False;
		$change_level_records_width = 8;
		for($li=1;$li<=$change_level_records;$li++) {
			$n = $n + $li;
			$column[$n]['title'] = $li;
			$column[$n]['field'] = 'change_level_'.$li;
			$column[$n]['auto_size'] = $change_level_records_auto_size;
			$column[$n]['width'] = $change_level_records_width;
		}
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lrelated_goal');
		$column[$n]['field'] = 'related_goals';
		$column[$n]['auto_size'] = False;
		$column[$n]['width'] = 40; 
		*/

		
		/**
		 * Source Table FROM OUTCOME
		 */
		$outcome_records =  $this->outcome_model->search();
		$outcome_records_auto_size = False;
		$outcome_records_width = 19;
		if(count($outcome_records) > 0) {
			$li=1;
			foreach($outcome_records as $key => $outcome) {
				$n = $n + $li;
				$column[$n]['title'] = $outcome->outcome_name;
				$column[$n]['field'] = False;
				$column[$n]['primary_key'] = 'id';
				$column[$n]['one2many'] = 'msc_outcomes';
				$column[$n]['one2many_pk'] = 'msc_id';
				$column[$n]['foreign_key'] = 'outcome_id';
				$column[$n]['foreign_key_value'] = $outcome->outcome_id;
				$column[$n]['value'] = '1';
				$column[$n]['auto_size'] = $outcome_records_auto_size;
				$column[$n]['width'] = $outcome_records_width;
				$li++;
			}
		}
		
		$n = $n + 1;
		$column[$n]['title'] = lang('ldate');
		$column[$n]['field'] = 'date';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lconversation');
		$column[$n]['field'] = 'conversation';
		$column[$n]['auto_size'] = False;
		$column[$n]['width'] = 50;
		
		$total_coloumn = count($column);
		
		$xcol = 0;
		$xrow = 1;
		
		$objWorksheet->setAutoFilterByColumnAndRow($xcol, $xrow,$xcol + ($total_coloumn - 1),$xrow);
		$objWorksheet->getRowDimension($xrow)->setRowHeight(90);
		
		foreach($column as $col){
			//set the header title , and set it to bold
			$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, $col['title'])->getStyleByColumnAndRow($xcol, $xrow)->getFont()->setBold(true);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getFill()->applyFromArray($fill);
			if($col['auto_size'] == True)
				$objWorksheet->getColumnDimensionByColumn($xcol)->setAutoSize(true);
			else
				$objWorksheet->getColumnDimensionByColumn($xcol)->setWidth($col['width']);
			
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setWrapText(true);
			$xcol = $xcol + 1;
		}
		
		$searchv = array(
			'pilar_id' => $this->input->get('pilar_id'),
			'msc_title' => trim($this->input->get('title')),
			'organization_type_id' => trim($this->input->get('organization_type_id')),
			'organization_id' => trim($this->input->get('organization_id')),
			//'organization_name' => trim($this->input->get('organization')),
			'domain_id' => trim($this->input->get('domain_id')),
			'province_id' => $this->input->get('province_id'),
			'date_from' => $this->input->get('date_from'),
			'date_to' => $this->input->get('date_to'),
			'year' => $this->input->get('year'),
			'keyword'     => urldecode($this->input->get('keyword'))
  		);
		
		$records = $this->msc_model->search(0,1,'date','asc',$searchv);
		$count = $this->msc_model->countSearch($searchv);
		
		if($count) {
			$xrow++;
			$xcol=0;
			
			for($i=0; $i<$count; $i++){
				$xcol = 0;
				$xrow = $i+2; //start row 2
				foreach($column as $key => $col){
					if(isset($col['one2many'])) {
						$primary_key = $col['primary_key'];
						$foreign_key_value = $col['foreign_key_value'];
						$this->db->where($col['one2many_pk'],$records[$i]->$primary_key);
						$this->db->where($col['foreign_key'],$foreign_key_value);
						$this->db->from($col['one2many']);
						$query = $this->db->get();
						$results = $query->num_rows();
						
						if($results)
							$value = $col['value'];
						else
							$value = "";
						
					} else {
						$fieldname = $col['field'];
						$value = $records[$i]->$fieldname;
					}
					
					$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, $value);
					$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
					if($col['auto_size'] == False) {
						if($col['width'] <= 20)
							$position = PHPExcel_Style_Alignment::VERTICAL_CENTER;
						else
							$position = PHPExcel_Style_Alignment::VERTICAL_TOP;
						
						$objWorksheet->getStyleByColumnAndRow($xcol, $xrow)->getAlignment()->setVertical($position);
						$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setWrapText(true);
					} else {
						$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					}
					
					$xcol++;
				}
			}
		}
		
		$filename = "MSC-REPORT.".date("Ymd").".xlsx";
		$objWriter->save(FCPATH."reports/".$filename);
		$file_url = base_url() . "reports/" . $filename;
		$message = "Done";
		$is_error = false;
			
		$result = array(
			"is_error"  =>  $is_error,
			"message"  => $message,
			"file_url" => $file_url
		);
		
		echo json_encode($result);
	}
	
	public function report() {
		$this->load->helper('form');
		$this->load->model('pilar/pilar_model');
		$this->load->model('domain/domain_model');
		$this->load->model('organization/organization_model');
		$this->load->model('organization_type/organization_type_model');
				
		/* get pilar list */
		$pilars = $this->pilar_model->search(0,1,'name','asc');
		$pilar_options = array();
		$pilar_options[0] = '';
		foreach($pilars as $pilar){
			$pilar_options[$pilar->id] = $pilar->name;
		}
		$this->viewparams['pilar_options'] = $pilar_options;
		$this->viewparams['pilar_params'] = array(
			'id'   => 'pilar_id',
			'class'	=> "form-control input-xxlarge",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_pilar')
		);
		/* end pilar */

		/* get organization type list */
		$organization_types = $this->organization_type_model->search(0,1,'name','asc');
		$organization_type_options = array();
		$organization_type_options[0] = '';
		foreach($organization_types as $organization_type){
			$organization_type_options[$organization_type->id] = $organization_type->name;
		}

		$this->viewparams['organization_type_options'] = $organization_type_options;
		$this->viewparams['organization_type_params'] = array(
			'id'   => 'organization_type_id',
			'class'	=> "form-control input-medium",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_organization_type')
		);
		/* end organization type */

		/* get domain list */
		/*$domains = $this->domain_model->search(0,1,'name','asc');
		$domain_options = array();
		$domain_options[0] = '';
		foreach($domains as $domain) {
			$domain_options[$domain->id] = $domain->name;
		}

		$this->viewparams['domain_options'] = $domain_options;
		$this->viewparams['domain_params'] = array(
			'id'   => 'domain_id',
			'class'	=> "form-control input-xlarge",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_domain')
		);*/
		/* end domain  */
		
		$this->viewparams['title_page']	= lang('lmsc_report');
		$header_col = $this->get_report_coloumn();
		$this->viewparams['column']	= $header_col['field']['column'];
		$this->viewparams['field']	= $header_col['field']['value'];
		$this->viewparams['width']	= $header_col['field']['width'];
		$this->viewparams['align']	= $header_col['field']['align'];
		$this->viewparams['domain']	= $this->domain_header;
		$this->viewparams['outcome'] = $this->outcome_header;
		parent::viewpage("report/vlist");
	}
	
	public function get_report_coloumn() {
		//ref http://www.trirand.com/jqgridwiki/doku.php?id=wiki%3agroupingheadar
		$columns = array();
		$n = 0;
		$columns['field']['column'][$n] = lang('ltotal_msc');
		$columns['field']['value'][$n] = 'total_msc';
		$columns['field']['align'][$n] = 'right';
		$columns['field']['width'][$n] = 100;
		
		$n = $n + 1;
		$columns['field']['column'][$n] = lang('lpilar');
		$columns['field']['value'][$n] = 'pilar_name';
		$columns['field']['align'][$n] = 'left';
		$columns['field']['width'][$n] = 315;

		/*$n = $n + 1;
		$columns['field']['column'][$n] = lang('ldomain_perubahan');
		$columns['field']['value'][$n] = 'domain_name';
		$columns['field']['align'][$n] = 'left';
		$columns['field']['width'][$n] = 80;*/
		
		$query = $this->db->query("SELECT id,name FROM domain_perubahan ORDER BY id asc");
		$domain_perubahan_records = $query->num_rows() > 0 ? $query->result() : False;
		if($domain_perubahan_records) {
			foreach($domain_perubahan_records as $key => $rec) {
				$n = $n + 1;
				$columns['field']['value'][$n] = url_title($rec->name,'underscore',true);
				$columns['field']['column'][$n] = $rec->name;
				$columns['field']['align'][$n] = 'right';
				$columns['field']['width'][$n] = 75;
				
				//header group
				if($key == 0)
					$this->domain_header['label'] =  url_title($rec->name,'underscore',true);
			}
		}
		$this->domain_header['total'] = count($domain_perubahan_records);
		

		$query = $this->db->query("SELECT id,code,name FROM outcomes ORDER BY id asc");
		$outcomes_records = $query->num_rows() > 0 ? $query->result() : False;
		if($outcomes_records) {
			foreach($outcomes_records as $key => $rec) {
				$n = $n + 1;
				$columns['field']['value'][$n] = url_title($rec->code,'underscore',true);
				$columns['field']['column'][$n] = trim($rec->name);
				$columns['field']['align'][$n] = 'right';
				$columns['field']['width'][$n] = 325;
				
				//header group
				if($key == 0)
					$this->outcome_header['label'] =  url_title($rec->code,'underscore',true);
			}
		}
		$this->outcome_header['total'] = count($outcomes_records);
		
		return $columns;
	}
	
	public function reportLoadDataGrid () {
		$page = isset($_POST['page']) ? $_POST['page']:1;
		$sidx = isset($_POST['sidx']) ? $_POST['sidx']:'date'; // get index row - i.e. user click to sort
		$sord = isset($_POST['sord']) ? $_POST['sord']:'asc'; // get the direction
		$limit = isset($_POST['rows'])? $_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
		
		$filter = array(
			'pilar_id' => $this->input->get('pilar_id'),
			'domain_id' => $this->input->get('domain_id'),
			'date_from' => $this->input->get('date_from'),
			'date_to' => $this->input->get('date_to'),
			'organization_type_id' => trim($this->input->get('organization_type_id')),
			'organization_id' => trim($this->input->get('organization_id'))
		);
		
		$query = $this->msc_model->search_group_by_pilar($limit,$page,$sidx,$sord,$filter);
		$this->firephp->log($this->db->last_query());
		$count = count($query);
		$this->firephp->log($this->db->last_query());
		$this->firephp->log("count : ".$count);
		$columns = $this->get_report_coloumn();
		$columns = $columns['field'];
		
		$this->DataJqGrid = array(
			"page"		=> $page,
			"sidx"		=> $sidx,
			"sord"		=> $sord,
			"query" 	=> $query,
			"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> $columns['value'],
			"id"		=> "msc_id"
		);
		
		parent::loadDataJqGrid();
	}
	
	public function do_msc_report_export() {
		$objPHPExcel = new PHPExcel();
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel,"Excel2007");
		$objPHPExcel->getProperties()
			->setTitle("MSC Report")
			->setDescription("MSC Report");
		$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$border = array( 'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )));
		$fill = array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'rotation'   => 0,
			'startcolor' => array (
				'rgb' => 'CCCCCC'
			),
			'endcolor'   => array (
				'argb' => 'CCCCCC'
			)
		);
		
		$n = 0;
		$column[$n]['title'] = lang('ltotal_msc');
		$column[$n]['field'] = 'total_msc';
		$column[$n]['sum'] = True;
		$column[$n]['merge']['row'] = True;
		$column[$n]['auto_size'] = False;
		$column[$n]['width'] = 13;
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lpilar');
		$column[$n]['field'] = 'pilar_name';
		$column[$n]['merge']['row'] = True;
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;

		/*$n = $n + 1;
		$column[$n]['title'] = lang('ldomain_perubahan');
		$column[$n]['field'] = 'domain_name';
		$column[$n]['merge']['row'] = True;
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$xcol_label = $n;
		*/

		
		$query = $this->db->query("SELECT id,name FROM domain_perubahan ORDER BY id asc");
		$domain_records = $query->num_rows() > 0 ? $query->result() : False;
		$domain_records_auto_size = False;
		$domain_records_width = 19;
		$nn = $n;
		$xcol_label = $nn;
		if($domain_records) {
			$li=1;
			foreach($domain_records as $key => $domain) {
				$nn++;
				$n = $n + $li;
				$column[$n]['title'] = $domain->name;
				$column[$n]['field'] = url_title($domain->name,'underscore',true);
				$column[$n]['auto_size'] = $domain_records_auto_size;
				$column[$n]['width'] = $domain_records_width;
				$li++;
			}
		}

		//$total_coloumn = count($column);
		$xcol = 0;
		$xrow = 1;
		
		$objWorksheet->getRowDimension($xrow)->setRowHeight(30);
		
		//domain perubahan
		$xcol_domain_merge_start =  $xcol + ($xcol_label + 1);
		$xcol_domain_merge_end = $xcol_domain_merge_start + count($domain_records) - 1;
			
		$objWorksheet->setCellValueByColumnAndRow($xcol_domain_merge_start, $xrow,lang('ldomain_change'))->getStyleByColumnAndRow($xcol_domain_merge_start, $xrow)->getFont()->setBold(true);
		$objWorksheet->mergeCellsByColumnAndRow($xcol_domain_merge_start,$xrow + 0,$xcol_domain_merge_end,$xrow + 0);
		$objWorksheet->getStyleByColumnAndRow($xcol_domain_merge_start,$xrow)->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		for($x=$xcol_domain_merge_start;$x<=$xcol_domain_merge_end;$x++) {
			$objWorksheet->getStyleByColumnAndRow($x,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($x,$xrow)->getFill()->applyFromArray($fill);
		}

		//outcomes
		$xcol_label = $nn;
		$query = $this->db->query("SELECT id,code,name FROM outcomes ORDER BY id asc");
		$outcomes_records = $query->num_rows() > 0 ? $query->result() : False;
		$outcome_records_auto_size = False;
		$outcome_records_width = 19;
		
		if($outcomes_records) {
			$li = 1;
			foreach ($outcomes_records as $key => $outcome) {
				$n = $n + $li;
				$column[$n]['title'] = $outcome->name;
				$column[$n]['field'] = url_title($outcome->code,'underscore',true);
				$column[$n]['auto_size'] = $outcome_records_auto_size;
				$column[$n]['width'] = $outcome_records_width;
				$li++;
			}
		}
		 
		//outcome merge cells
		$xcol_outcome_merge_start =  $xcol + ($xcol_label + 1);
		$xcol_outcome_merge_end = $xcol_outcome_merge_start + count($outcomes_records) - 1;
		
		$objWorksheet->setCellValueByColumnAndRow($xcol_outcome_merge_start, $xrow,lang('loutcome'))->getStyleByColumnAndRow($xcol_outcome_merge_start, $xrow)->getFont()->setBold(true);
		$objWorksheet->mergeCellsByColumnAndRow($xcol_outcome_merge_start,$xrow + 0,$xcol_outcome_merge_end,$xrow + 0);
		$objWorksheet->getStyleByColumnAndRow($xcol_outcome_merge_start,$xrow)->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		for($x=$xcol_outcome_merge_start;$x<=$xcol_outcome_merge_end;$x++) {
			$objWorksheet->getStyleByColumnAndRow($x,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($x,$xrow)->getFill()->applyFromArray($fill);
		}
		
		$xrow = $xrow + 1;
		$objWorksheet->getRowDimension($xrow)->setRowHeight(102);
		
		$merge = array();
		
		$xi = 0;
		foreach($column as $col){
			//set the header title , and set it to bold
			$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, $col['title'])->getStyleByColumnAndRow($xcol, $xrow)->getFont()->setBold(true);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getFill()->applyFromArray($fill);
			if($col['auto_size'] == True)
				$objWorksheet->getColumnDimensionByColumn($xcol)->setAutoSize(true);
			else
				$objWorksheet->getColumnDimensionByColumn($xcol)->setWidth($col['width']);
			
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setWrapText(true);
			
			if(isset($col['merge']['row']) && $col['merge']['row']== True) {
				$merge[$xi]['row']['title'] = $col['title'];
			}
			
			$xi++;
			$xcol = $xcol + 1;
		}
		
		//$xcol_label
		if(count($merge) >0 ) {
			for ($ilabel = 0; $ilabel < count($merge); $ilabel++) {
				$xrow_merge_start = $xrow - 1;
				$xrow_merge_end = $xrow_merge_start + 1;
				
				$objWorksheet->setCellValueByColumnAndRow($ilabel, $xrow_merge_start, $merge[$ilabel]['row']['title'])->getStyleByColumnAndRow($ilabel, $xrow_merge_end)->getFont()->setBold(true);
				$objWorksheet->mergeCellsByColumnAndRow($ilabel, $xrow_merge_start, $ilabel, $xrow_merge_end);
				$objWorksheet->getStyleByColumnAndRow($ilabel, $xrow_merge_start)->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
					->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				
				for($x=$xrow_merge_start;$x<=$xrow_merge_end;$x++) {
					$objWorksheet->getStyleByColumnAndRow($ilabel,$x)->applyFromArray($border);
					$objWorksheet->getStyleByColumnAndRow($ilabel,$x)->getFill()->applyFromArray($fill);
				}
			}
		}
		
		$xrow = $xrow + 1;
		
		$searchv = array (
			'pilar_id' => $this->input->get('pilar_id'),
			'date_from' => $this->input->get('date_from'),
			'date_to' => $this->input->get('date_to'),
			'organization_type_id' => trim($this->input->get('organization_type_id')),
			'organization_id' => trim($this->input->get('organization_id'))
	
		);
		
		$records = $this->msc_model->search_group_by_pilar(0,1,'date','asc',$searchv);
		$count =   count($records);
		
		if($count) {
			//$sum = array('total'=>0);
			for($i=0; $i<$count; $i++){
				$xcol = 0;
				$xrow = $i+3; //start row 3
				foreach($column as $key => $col){
					$fieldname = $col['field'];
					$value = $records[$i]->$fieldname;
					$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, $value);
					$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
					if($col['auto_size'] == False) {
						if($col['width'] <= 20)
							$position = PHPExcel_Style_Alignment::VERTICAL_CENTER;
						else
							$position = PHPExcel_Style_Alignment::VERTICAL_TOP;
						
						$objWorksheet->getStyleByColumnAndRow($xcol, $xrow)->getAlignment()->setVertical($position);
						$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setWrapText(true);
					} else {
						$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					}
					
					$xcol++;
					
				}
			}
			
			/*$xrow = $count + 3;
			$xcol = 0;
			$alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
			
			$count=$count+2;
			$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, "=SUM($alpha[0]3:$alpha[0]$count)");
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getFill()->applyFromArray($fill);
			
			$xcol=$xcol+1;
			$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow,lang('ltotal'));
			$objWorksheet->getStyleByColumnAndRow($xcol, $xrow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getFill()->applyFromArray($fill);
			$start_alpha = 2;
			$total_alpha = count($column);
			
			$xcol=$xcol+1;
			for($i=$start_alpha;$i<$total_alpha;$i++) {
				$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, "=SUM($alpha[$i]3:$alpha[$i]$count)");
				$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
				$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getFill()->applyFromArray($fill);
				$xcol = $xcol+1;
			}*/
			
		}
		
		$filename = "MSC-REPORT-REPORT.".date("Ymd").".xlsx";
		$objWriter->save(FCPATH."reports/".$filename);
		$file_url = base_url() . "reports/" . $filename;
		$message = "Done";
		$is_error = false;
		
		$result = array(
			"is_error"  =>  $is_error,
			"message"  => $message,
			"file_url" => $file_url
		);
		
		echo json_encode($result);
	}
}