<?php
class Kabupaten_kota_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    

    function get_kabupaten_id($kabupaten,$province_id){
        $total = count($this->datas);
              
        $kabupaten_id = 0;
        $is_found = false;
        //check in array
        if($total){
            foreach($this->datas as $value){
                //echo "key : ".$value['id'].";".$value['name'].";".$organization_province.PHP_EOL;
                if( (strtoupper(trim($value['name'])) == strtoupper(trim($kabupaten)) )
                    && (strtoupper(trim($value['province_id'])) == $province_id) )

                    {
                    echo $value['name']. "(".$value['id'].");";
                    $kabupaten_id = $value['id'];
                    $is_found = true;
                    break;
                }
            }
        }else{
            $is_found = false;
        }

        //if not exist in array, check in db
        if(!$is_found){
            $kabupaten_id = $this->data_exists_by_name($kabupaten,$province_id);
            
            //if not found in array or in db
            if(!$kabupaten_id){
                //if not exists, then insert
                $d['created_by'] = _UserId();
                $d['created_on'] = date("Y-m-d H:i:s");
                $d['updated_by'] = _UserId();
                $d['updated_on'] = date("Y-m-d H:i:s");
                $d['active'] = '1';                 
                $d['name'] = $kabupaten;
                $d['province_id'] = $province_id;
                $kabupaten_id = $this->insert($d);
                echo "insert kabupaten ".$kabupaten."(".$kabupaten_id.");";
            }else{
                 echo $kabupaten."(".$kabupaten_id.");";

            } 
            array_push($this->datas,array("id"=>$kabupaten_id,"name"=>$kabupaten,"province_id"=>$province_id));
        }
        return $kabupaten_id;
    }

    function set_kabupatens(){
        $kabupatens = $this->search(0,1,'kabupaten_kota_name','asc');

        if(count($kabupatens)){
            foreach($kabupatens as $kabupaten){
                array_push($this->datas,array("id"=>$kabupaten->kabupaten_kota_id,"name"=>$kabupaten->kabupaten_kota_name,"province_id"=>$kabupaten->province_id));
            }
        }   
    }

    function insert($data){
		$this->db->insert("kabupaten_kota", $data); 
		return $this->db->insert_id();
    }

    function data_exists_by_name($name,$province_id=0){
		$this->db->where('name',$name);

		if($province_id){
			$this->db->where('province_id',$province_id);
		}

	    $query = $this->db->get('kabupaten_kota');
	    if ($query->num_rows() > 0){
	    	//get the id 
	    	foreach ($query->result() as $row)
			{
			    return $row->id;
			}
	    }
	    else{
	        return false;
	    }
  	}

  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
  	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("kabupaten_kota.name as kabupaten_kota_name");
			$this->db->select("kabupaten_kota.id as kabupaten_kota_id");
			$this->db->select("provinces.name as province_name");
			$this->db->select("provinces.id as province_id");
			$this->db->join("provinces","provinces.id = kabupaten_kota.province_id");

		}

		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('kabupaten_kota');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}