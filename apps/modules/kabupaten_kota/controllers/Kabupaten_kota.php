<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kabupaten_kota extends MY_Controller {
	
	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
		$this->load->model("kabupaten_kota_model");
	}

	function index(){
		$this->lists();
	}
	
	function lists(){
   		$datas = $this->kabupaten_kota_model->search(0,1,'province_name, kabupaten_kota_name','asc');
   		$this->viewparams['datas'] = $datas;
   		$this->viewparams['title'] = lang('lkabupaten_kota');
		parent::viewpage("vlist");
	}

}
