 <link href='<?=assets_url()?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
 <link href='<?=assets_url()?>css/dataTables.bootstrap.css' rel='stylesheet'>
   <script>
	$(document).ready(function(){
		//datatable
	    $('.datatable').dataTable({
	        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12 center-block'p>>",
	        "sPaginationType": "bootstrap",
	        "oLanguage": {
	            "sLengthMenu": "_MENU_ records per page"
	        }
	         ,"iDisplayLength": 25
	        //,"pageLength": 25

	        ,"bLengthChange": false
	        //ini  ga jalan, scroll horiz ga muncul
	        ,"scroller":       true
	        ,"scrollCollapse": true
	        ,"scrollX": true
		});
	});
</script>

    <div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url('Dashboard')?>">Home</a>
        </li>
        <li>
            <a href="#"><?=lang('lintermediate_output')?></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=lang('lintermediate_output')?></h2>
            </div>
            <div class="box-content row">
	            <div class="col-lg-12">
		            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">

					    <thead>
					    	<tr>
					       		<th><?=lang('lobjective_code')?></th>
					       		<th><?=lang('lobjective_name')?></th>
					       		<th><?=lang('loutcome_code')?></th>
					       		<th><?=lang('loutcome_name')?></th>
					       		<th><?=lang('lintermediate_output_code')?></th>
					       		<th><?=lang('lintermediate_output_name')?></th>
								<th><?=lang('loutcome_code')?></th>
					       		<th><?=lang('loutcome_name')?></th>
					       		
						    </tr>

					    </thead>
					    <tbody>
					    
					    <?php
					    foreach($datas as $data) {
						?>
					    <tr>
					        <td><?=$data->objective_code?></td>
					        <td><?=$data->objective_name?></td>
					        <td><?=$data->outcome_code?></td>
					        <td><?=$data->outcome_name?></td>
					        <td><?=$data->intermediate_output_code?></td>
					        <td><?=$data->intermediate_output_name?></td>
					        <td><?=$data->output_code?></td>
					        <td><?=$data->output_name?></td>
					    </tr>
					    <?}?>


					    </tbody>
				    </table>
	            </div>
        	</div>
    	</div>
  	</div>
</div>

<!-- library for making tables responsive -->
<script src="<?=assets_url()?>bower_components/responsive-tables/responsive-tables.js"></script>

<!-- data table plugin -->
<script src='<?=assets_url()?>js/jquery.dataTables.min.js'></script>
<script src='<?=assets_url()?>js/dataTables.bootstrap.js'></script>
