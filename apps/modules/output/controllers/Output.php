<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Output extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
	}
	
	function index(){
    	$this->lists();
	}
	

   	
  function lists(){
   		$this->load->model('output_model');
   		$datas = $this->output_model->search(0,1,'objective_code,outcome_code,intermediate_output_code,output_code','asc');
   		$this->viewparams['datas'] = $datas;
		parent::viewpage("vlist");
	}
	

}

/* End of file dashboard.php */
/* Location: ./apps/modules/dashboard/controllers/dashboard.php */
