<?php
class Output_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function set_outputs(){
        $outputs = $this->search(0,1,'output_code','asc');

        if(count($outputs)){
            foreach($outputs as $output){
                array_push($this->datas,
                    array(
                        "id"     =>  $output->output_id
                        ,"name"  =>  $output->output_name
                        ,"output_code"  =>  $output->output_code
                        ,"outcome_code"  =>  $output->outcome_code
                        ,"outcome_id"  =>  $output->outcome_id
                        )
                );
            }
        }   
    }

    function get_output_id_by_code($output_code){
        $total = count($this->datas);
              
        $output_id = 0;
        $is_found = false;
        //check in array
        if($total){
            foreach($this->datas as $value){
                //echo "key : ".$value['id'].";".$value['name'].";".$organization_province.PHP_EOL;
                if(strtoupper(trim($value['output_code'])) == strtoupper(trim($output_code))){
                    echo $value['output_code']. "(".$value['id'].");";
                    $output_id = $value['id'];
                    $is_found = true;
                    break;
                }
            }
        }else{
            $is_found = false;
        }

        //if not exist in array, check in db
       /* if(!$is_found){
            $output_id = $this->output_model->data_exists_by_code($output_code);
            
            //if not found in array or in db
            if(!$output_id){
                //if not exists, then insert
                echo "output ".$output_code." not found;";
            }else{
                 echo "output ".$output_code." found in db(".$output_id.");";

            } 
            
            array_push($this->outputs,array("id"=>$output_id,"code"=>$output_code));
        }*/
        return $output_id;
    }


    function data_exists_by_code($code){
		$this->db->where('code',$code);
	    $query = $this->db->get('outputs');
	    if ($query->num_rows() > 0){
	    	//get the id 
	    	foreach ($query->result() as $row)
			{
			    return $row->id;
			}
	    }
	    else{
	        return false;
	    }
    }
  	
  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("outputs.id as output_id");
			$this->db->select("outputs.code as output_code");
			$this->db->select("outputs.name as output_name");
			$this->db->select("intermediate_outputs.id as intermediate_output_id");
			$this->db->select("intermediate_outputs.code as intermediate_output_code");
			$this->db->select("intermediate_outputs.name as intermediate_output_name");
			$this->db->select("outcomes.id as outcome_id");
			$this->db->select("outcomes.code as outcome_code");
			$this->db->select("outcomes.name as outcome_name");
			$this->db->select("objectives.id as objective_id");
			$this->db->select("objectives.code as objective_code");
			$this->db->select("objectives.name as objective_name");

			$this->db->join("intermediate_outputs","intermediate_outputs.id = outputs.intermediate_ouput_id");
			$this->db->join("outcomes","outcomes.id = intermediate_outputs.outcome_id");
			$this->db->join("objectives","objectives.id = outcomes.objective_id");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('outputs');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}