<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
	
	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
		$this->load->model("user_model");
	   	$this->lang->load('auth');
     
	}

	function index(){
		$this->lists();
	}
	
	function lists(){
   		$datas = $this->user_model->search(0,1,'name','asc');
   		$this->viewparams['datas'] = $datas;
		parent::viewpage("vlist");
	}

    function dochange_password(){
        $this->load->library(array('form_validation'));
		
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        $user = $this->ion_auth->user()->row();
        $is_error = false;
        $redirect = "";
        
		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$message = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
		}else{
            $identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
                $is_error = false;
                $message =  $this->ion_auth->messages();
                $redirect = site_url('auth/logout');
                
			}
			else
			{
				$is_error = true;
                $message =  $this->ion_auth->errors();
			}
        }
        $result = array("error"=>$is_error,"message" => $message, "redirect" => $redirect);
        echo json_encode($result);

    }
    // change password
	function change_password(){
		redirect('dashboard');
		exit;
        $this->load->helper('form');
        $user = $this->ion_auth->user()->row();
        
        $this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
		$this->viewparams['old_password'] = array(
            'name' => 'old',
            'id'   => 'old',
            'type' => 'password',
            'class'	=> "form-control input-xlarge"
        );
        $this->viewparams['new_password'] = array(
            'name'    => 'new',
            'id'      => 'new',
            'type'    => 'password',
            'class'	=> "form-control input-xlarge",

            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
        );
        $this->viewparams['new_password_confirm'] = array(
            'name'    => 'new_confirm',
            'id'      => 'new_confirm',
            'type'    => 'password',
            'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
            'class'	=> "form-control input-xlarge"

        );

        $this->viewparams['title_page']	= lang('lchange_password');
		parent::viewpage("vuser_changepass");

	}
    
	function test_sendmail(){
		/** sending email notification **/
		$this->load->library('email');
		
		$mail_config = $this->settings;
		
		$email = "dedy.adhiewirawan@gmail.com";
		$email_message= "testing email ";
		$this->email->set_newline("\r\n");
		$this->email->to($email);
		$this->email->from($mail_config['mail_activation_confirmation_sender_mail'], $mail_config['mail_activation_confirmation_sender_name']);
					 
	    $this->email->subject($mail_config['mail_activation_confirmation_subject']);
		$this->email->message($email_message);
    	$this->email->send();
    	
		echo $this->email->print_debugger();	
	}
	
	function generate_password($password,$salt){
		echo md5($password.$salt);
	}

}
