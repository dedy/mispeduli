<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>

<script>
	$(document).ready(function(){
	  	 $('#submit-btn').click(function(e){
			//$('form#login-form').submit();
			//return;
			e.preventDefault(); 
			dopost($(this));
	      	
		});
			
	
	});
	
	function dopost(obj){
		obj.attr('disabled','disabled');

		$.post('<?=site_url('user/dochange_password');?>', 
				$("#input-form").serialize(),
				function(returData) {
					obj.removeAttr('disabled');
     				
					$('#show_message').slideUp('normal',function(){
						
						if(returData.error){
							var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal');	
								
						}else{
							var rv = '<div class="alert alert-success">'+returData.message+'</div>';
							$('#show_message').html(rv);
							$('#show_message').slideDown('normal',function(){
								 setTimeout(function() {
								    $('#show_message').slideUp('normal',function(){
									    if(returData.redirect){
									    	window.location.replace(returData.redirect);
									    }
								    });	
								  }, 1500);
							});	
						}	
					});
					
				},'json');
	}
	</script>	


<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url()?>">Home</a>
        </li>
        <li>
            <a href="#"><?=lang('lchange_password')?></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-edit"></i> <?=lang('lchange_password')?></h2>
		 </div>
            <div class="box-content">
            <?php echo form_open("user/dochange_password",array('role'=>"form",'id'=>'input-form'))?>
					            
                    <div class="form-group">
                        <label for="old">* <?=lang('change_password_old_password_label');?></label>
	                            <?php echo form_input($old_password);?>
                       
                    </div>
                    <div class="form-group">
                        <label for="new"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
	                     <?php echo form_input($new_password);?>
                        
                    </div>
                  
                     <div class="form-group">
                        <label for="new"><?php echo lang('change_password_new_password_confirm_label');?></label>
	                     <?php echo form_input($new_password_confirm);?>
                        
                    </div>
            		
            		<div id='show_message' style="display: none;"></div> 
                  
                    <button type="submit" class="btn btn-primary" id='submit-btn'>Submit</button>
				<?=form_close();?>    <!--/form-->
       
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->