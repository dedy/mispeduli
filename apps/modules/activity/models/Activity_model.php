<?php
class Activity_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   	function insert_activity_output($data){
		$this->db->insert("activity_outputs", $data); 
		return $this->db->insert_id();
    }

  	
  	function insert($data){
		$this->db->insert("activities", $data); 
		return $this->db->insert_id();
    }
    
    function get_data_by_pilar_quarter($v){
    	$participants = config_item('participants');
        $beneficiaries = config_item('beneficiaries');

        //jumlah total peserta untuk per pilar, per output
     	foreach($beneficiaries as $beneficiary){
            foreach($participants as $participant){
                 $fieldParticipant  = $participant['field']."_".$beneficiary['field'];
    			$this->db->select("SUM(".$fieldParticipant.") AS ".$fieldParticipant);
          
             }
         }

    	$this->db->select("pilar_id");
    	$this->db->select("output_id");
  		$this->db->where("activities.pilar_id",$v['pilar_id']);
  		$this->db->where("activity_outputs.output_id",$v['output_id']);

  		if(isset($v['organization_id']) && $v['organization_id']){
	  		$this->db->where("activities.organization_id",$v['organization_id']);
  		}

  		if(isset($v['organization_type_id']) && $v['organization_type_id']){
	  		$this->db->where("activities.organization_type_id",$v['organization_type_id']);
  		}
  		
  		$this->db->where("activity_outputs.output_id",$v['output_id']);
    	$this->db->group_by("pilar_id");
    	$this->db->group_by("output_id");

    	$quarter = ($v['quarter'])?$v['quarter']:"";
    	$year = $v['year'];

    	if($quarter) {
			switch ($quarter) {
				case '1':
		    		# code...
		    		$this->db->where("activities.start_date >=",$year."-01-01");
		    		$this->db->where("activities.start_date <",$year."-04-01");
					break;
				case '2':
					# code...
		    		$this->db->where("activities.start_date >=",$year."-04-01");
		    		$this->db->where("activities.start_date <",$year."-07-01");
					break;
				case '3':
					# code...
		    		$this->db->where("activities.start_date >=",$year."-07-01");
		    		$this->db->where("activities.start_date <",$year."-10-01");
					break;
				case '4':
					# code...
		    		$this->db->where("activities.start_date >=",$year."-10-01");
		    		$this->db->where("activities.start_date <=",$year."-12-31");
					break;
			}
		}else{
			if($year){
		    		$this->db->where("activities.start_date >=",$year."-01-01");
		    		$this->db->where("activities.start_date <=",$year."-12-31");

			}
		}
		$this->db->join("activities","activities.id = activity_outputs.activity_id");
    	$this->db->from('activity_outputs');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
  
    function get_data_by_organization($limit=0,$page=0,$sidx='',$sord='',$v=array()){
    	$participants = config_item('participants');
        $beneficiaries = config_item('beneficiaries');
 		$quarter 	= ($v['quarter'])?$v['quarter']:"";
    	$year 		= $v['year'];
   		
   		   	$this->db->select("p.name as pilar_name");
	    	$this->db->select("op.name as parent_organization_name");
	    	$this->db->select("o.name as organization_name");
	    	$this->db->select("o.total_target_group as organization_total_target_group");
			$this->db->select("o.name as organization_name");
	    	
	    	$jumlah_peserta = "male_direct_beneficiaries+female_direct_beneficiaries+trans_direct_beneficiaries";

	    	//total peserta : m,f,t of direct , indirect , and gov
			$qq  = "(	SELECT sum(".$jumlah_peserta.") ";
			$qq .= " FROM activities as aa ";
			$qq .= "		WHERE aa.pilar_id = a.pilar_id  ";
			$qq .= "			AND aa.organization_id = a.organization_id  ";
			$qq .= "			AND aa.organization_type_id = a.organization_type_id  ";

			if($quarter) {
				switch ($quarter) {
					case '1':
			    		# code...
						$qq .= " AND aa.start_date >= '".$year."-01-01' ";
						$qq .= " AND aa.start_date < '".$year."-04-01' ";
						break;
					case '2':
						# code...
			    		$qq .= " AND aa.start_date >= '".$year."-04-01' ";
						$qq .= " AND aa.start_date < '".$year."-07-01' ";
						break;
					case '3':
						# code...
			    		$qq .= " AND aa.start_date >= '".$year."-07-01' ";
						$qq .= " AND aa.start_date < '".$year."-10-01' ";
						break;
					case '4':
						# code...
			    		$qq .= " AND aa.start_date >= '".$year."-10-01' ";
						$qq .= " AND aa.start_date <= '".$year."-12-31' ";
					break;
				}
			}else{
				if($year){
			    		$qq .= " AND aa.start_date >= '".$year."-01-01' ";
						$qq .= " AND aa.start_date < '".$year."-12-31' ";
				}
			}
			$qq .= "	) AS total_jumlah_peserta";

			$this->db->select($qq);


			//total activitas 
			$qq  = "(	SELECT count(aa.id) ";
			$qq .= " FROM activities as aa ";
			$qq .= "		WHERE aa.pilar_id = a.pilar_id  ";
			$qq .= "			AND aa.organization_id = a.organization_id  ";
			$qq .= "			AND aa.organization_type_id = a.organization_type_id  ";

			if($quarter) {
				switch ($quarter) {
					case '1':
			    		# code...
						$qq .= " AND aa.start_date >= '".$year."-01-01' ";
						$qq .= " AND aa.start_date < '".$year."-04-01' ";
						break;
					case '2':
						# code...
			    		$qq .= " AND aa.start_date >= '".$year."-04-01' ";
						$qq .= " AND aa.start_date < '".$year."-07-01' ";
						break;
					case '3':
						# code...
			    		$qq .= " AND aa.start_date >= '".$year."-07-01' ";
						$qq .= " AND aa.start_date < '".$year."-10-01' ";
						break;
					case '4':
						# code...
			    		$qq .= " AND aa.start_date >= '".$year."-10-01' ";
						$qq .= " AND aa.start_date <= '".$year."-12-31' ";
					break;
				}
			}else{
				if($year){
			    		$qq .= " AND aa.start_date >= '".$year."-01-01' ";
						$qq .= " AND aa.start_date < '".$year."-12-31' ";
				}
			}
			$qq .= "	) AS total_aktivitas";

			$this->db->select($qq);

			//activity types
		    $query = $this->db->query("SELECT id,name FROM activity_types ORDER BY id asc");
		    $results = $query->result();
	      	foreach($results as $res) {

				//total peserta : m,f,t of direct , indirect , and gov
				$qq  = "(	SELECT sum(".$jumlah_peserta.") ";
				$qq .= " FROM activities as aa ";
				$qq .= "		WHERE aa.pilar_id = a.pilar_id  ";
				$qq .= "			AND aa.organization_id = a.organization_id  ";
				$qq .= "			AND aa.organization_type_id = a.organization_type_id  ";
				$qq .= "			AND aa.activity_type_id = '".$res->id."' ";

				if($quarter) {
					switch ($quarter) {
						case '1':
				    		# code...
							$qq .= " AND aa.start_date >= '".$year."-01-01' ";
							$qq .= " AND aa.start_date < '".$year."-04-01' ";
							break;
						case '2':
							# code...
				    		$qq .= " AND aa.start_date >= '".$year."-04-01' ";
							$qq .= " AND aa.start_date < '".$year."-07-01' ";
							break;
						case '3':
							# code...
				    		$qq .= " AND aa.start_date >= '".$year."-07-01' ";
							$qq .= " AND aa.start_date < '".$year."-10-01' ";
							break;
						case '4':
							# code...
				    		$qq .= " AND aa.start_date >= '".$year."-10-01' ";
							$qq .= " AND aa.start_date <= '".$year."-12-31' ";
						break;
					}
				}else{
					if($year){
				    		$qq .= " AND aa.start_date >= '".$year."-01-01' ";
							$qq .= " AND aa.start_date < '".$year."-12-31' ";
					}
				}
				$qq .= "	) AS jumlah_peserta_".url_title($res->name,'underscore',true);

				$this->db->select($qq);


				//total activitas per activity id
				$qq  = "(	SELECT count(aa.id) ";
				$qq .= " FROM activities as aa ";
				$qq .= "		WHERE aa.pilar_id = a.pilar_id  ";
				$qq .= "			AND aa.organization_id = a.organization_id  ";
				$qq .= "			AND aa.organization_type_id = a.organization_type_id  ";
				$qq .= "			AND aa.activity_type_id = '".$res->id."' ";

				if($quarter) {
					switch ($quarter) {
						case '1':
				    		# code...
							$qq .= " AND aa.start_date >= '".$year."-01-01' ";
							$qq .= " AND aa.start_date < '".$year."-04-01' ";
							break;
						case '2':
							# code...
				    		$qq .= " AND aa.start_date >= '".$year."-04-01' ";
							$qq .= " AND aa.start_date < '".$year."-07-01' ";
							break;
						case '3':
							# code...
				    		$qq .= " AND aa.start_date >= '".$year."-07-01' ";
							$qq .= " AND aa.start_date < '".$year."-10-01' ";
							break;
						case '4':
							# code...
				    		$qq .= " AND aa.start_date >= '".$year."-10-01' ";
							$qq .= " AND aa.start_date <= '".$year."-12-31' ";
						break;
					}
				}else{
					if($year){
				    		$qq .= " AND aa.start_date >= '".$year."-01-01' ";
							$qq .= " AND aa.start_date < '".$year."-12-31' ";
					}
				}
				$qq .= "	) AS ".url_title($res->name,'underscore',true);
				$this->db->select($qq);
      		}
		
		

  		if(isset($v['organization_id']) && $v['organization_id']){
	  		$this->db->where("a.organization_id",$v['organization_id']);
  		}

  		if(isset($v['organization_type_id']) && $v['organization_type_id']){
	  		$this->db->where("a.organization_type_id",$v['organization_type_id']);
  		}

  		if(isset($v['pilar_id']) && $v['pilar_id']){
	  		$this->db->where("a.pilar_id",$v['pilar_id']);
  		}

  		$this->db->where('a.organization_type_id',2); //get only cso
  		
    	$this->db->group_by("pilar_name");
    	$this->db->group_by("parent_organization_name");
    	$this->db->group_by("organization_name");

   		if($quarter) {
			switch ($quarter) {
				case '1':
		    		# code...
		    		$this->db->where("a.start_date >=",$year."-01-01");
		    		$this->db->where("a.start_date <",$year."-04-01");
					break;
				case '2':
					# code...
		    		$this->db->where("a.start_date >=",$year."-04-01");
		    		$this->db->where("a.start_date <",$year."-07-01");
					break;
				case '3':
					# code...
		    		$this->db->where("a.start_date >=",$year."-07-01");
		    		$this->db->where("a.start_date <",$year."-10-01");
					break;
				case '4':
					# code...
		    		$this->db->where("a.start_date >=",$year."-10-01");
		    		$this->db->where("a.start_date <=",$year."-12-31");
					break;
			}
		}else{
			if($year){
		    		$this->db->where("a.start_date >=",$year."-01-01");
		    		$this->db->where("a.start_date <=",$year."-12-31");

			}
		}

		$this->db->join("organizations op","op.id = o.parent_id");
		$this->db->join("activities a","a.organization_id = o.id","LEFT OUTER");
		$this->db->join("pilar p","p.id = o.pilar_id");
		$this->db->join("organization_types t","t.id = o.organization_type_id");
    	$this->db->from('organizations o');

    	$this->db->order_by("parent_organization_name","asc");
    	$this->db->order_by("organization_name","asc");
		$this->db->order_by("pilar_name","asc");
    	
    	if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
		}

		$query = $this->db->get();
		
 		return $query->result();
    }

    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
    	if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
    		$this->db->where("activities.pilar_id",$searchval['pilar_id']);
    	}

    	if(isset($searchval['quarter']) && $searchval['quarter']){
    		$year = $searchval['year'];
    		$quarter = $searchval['quarter'];

    		switch ($quarter) {
				case '1':
		    		# code...
		    		$this->db->where("activities.start_date >=",$year."-01-01");
		    		$this->db->where("activities.start_date <",$year."-04-01");
					break;
				case '2':
					# code...
		    		$this->db->where("activities.start_date >=",$year."-04-01");
		    		$this->db->where("activities.start_date <",$year."-07-01");
					break;
				case '3':
					# code...
		    		$this->db->where("activities.start_date >=",$year."-07-01");
		    		$this->db->where("activities.start_date <",$year."-10-01");
					break;
				case '4':
					# code...
		    		$this->db->where("activities.start_date >=",$year."-10-01");
		    		$this->db->where("activities.start_date <=",$year."-12-31");
					break;
			}
    	}
    	if(isset($searchval['province_id']) && $searchval['province_id']){
    		$this->db->where("activities.province_id",$searchval['province_id']);
    	}
    	
    	if(isset($searchval['activity_type_id']) && $searchval['activity_type_id']){
    		$this->db->where("activities.activity_type_id",$searchval['activity_type_id']);
    	}

		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
    		$this->db->where("activities.organization_type_id",$searchval['organization_type_id']);
    	}

    	if(isset($searchval['organization_id']) && $searchval['organization_id']){
    		$this->db->where("activities.organization_id",$searchval['organization_id']);
    	}

    	if(isset($searchval['organization_name']) && $searchval['organization_name']){
    		$this->db->like("organizations.name",$searchval['organization_name']);
    		$this->db->or_like("organizations.name2",$searchval['organization_name']);
    	}

    	if(isset($searchval['start_date_from']) && $searchval['start_date_from']){
    		$this->db->where("start_date >=",$searchval['start_date_from']);
    	}

    	if(isset($searchval['start_date_to']) && $searchval['start_date_to']){
    		$this->db->where("start_date <=",$searchval['start_date_to']);
    	}

    	if(isset($searchval['end_date_from']) && $searchval['end_date_from']){
    		$this->db->where("end_date >=",$searchval['end_date_from']);
    	}

    	if(isset($searchval['end_date_to']) && $searchval['end_date_to']){
    		$this->db->where("end_date <=",$searchval['end_date_to']);
    	}

    	if(isset($searchval['keyword']) && $searchval['keyword']){
    		$qq = "(activity_name like '%".$searchval['keyword']."%'";
    		$qq .= " OR result_description like '%".$searchval['keyword']."%'";
    		$qq .= " OR conversation like '%".$searchval['keyword']."%') ";
    		$this->db->where($qq);
    	}

		if($this->is_count){
			$this->db->select("count(activities.id) as total");
		}{
			$this->db->select("activities.*");
			$this->db->select("activities.id as activity_id");
			$this->db->select("organization_types.name as organization_type_name");
			$this->db->select("pilar.name as pilar_name");
			$this->db->select("organizations.name as organization_name");
			$this->db->select("activity_types.name as activity_type_name");
			$this->db->select("provinces.name as province_name");
			$this->db->select("kabupaten_kota.name as kabupaten_kota_name");

            $this->db->select("DATE_FORMAT(start_date ,'%d/%m/%Y') as start_date_fmt",false);
            $this->db->select("DATE_FORMAT(end_date ,'%d/%m/%Y') as end_date_fmt",false);


			$this->db->join("organization_types","organization_types.id = activities.organization_type_id");
			$this->db->join("pilar","pilar.id = activities.pilar_id");
			$this->db->join("activity_types","activity_types.id = activities.activity_type_id");
			$this->db->join("provinces","provinces.id = activities.province_id");
			$this->db->join("kabupaten_kota","kabupaten_kota.id = activities.kabupaten_kota_id");
		}
		
		$this->db->join("organizations","organizations.id = activities.organization_id");
    
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('activities');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}

	 /* countSearchUser */
    function countSearchget_data_by_organization($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->get_data_by_organization(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}