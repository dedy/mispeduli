<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Report extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		  if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		  parent::__construct();
      $this->load->model('activity_model');
      $this->load->model('pilar/pilar_model');
      $this->load->model('organization_type/organization_type_model');
      $this->load->model('organization/organization_model');
      $this->load->model('output/output_model');
      $this->load->model('outcome/outcome_model');
	}
	
	function index(){
    	$this->Ouput();
	}
	   	
  function Ouput(){

  	 	$this->load->helper('form');
  	 	$this->load->model('pilar/pilar_model');
  	 	$this->load->model('activity_type/activity_type_model');
  	 	$this->load->model('province/province_model');

  	 	/* get pilar list */
  	 	$pilars = $this->pilar_model->search(0,1,'name','asc');
  	 	$pilar_options = array();
  	 	$pilar_options[0] = '';
  	 	foreach($pilars as $pilar){
  	 		$pilar_options[$pilar->id] = $pilar->name;
  	 	}
       	$this->viewparams['pilar_options'] = $pilar_options;
        $this->viewparams['pilar_params'] = array(
            'id'   => 'pilar_id',
            'class'	=> "form-control input-xxlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_pilar')
        );
        /* end pilar */
       
        /* quarter and year params */
        $quarters  = config_item('quarter');
        $quarter_options = array();
        $quarter_options[0] = '';
        foreach($quarters as $quarter){
          $quarter_options[$quarter['value']] = $quarter['title'];
        }
        $this->viewparams['quarter_options'] = $quarter_options;
        $this->viewparams['quarter_params'] = array(
            'id'   => 'quarter',
            'class' => "form-control input-medium"
            //,'data-rel'=> "chosen",
        );

        $year_options = array();
        for($i=config_item('year_start');$i<date("Y");$i++){
          $year_options[$i] = $i;
        }
        $this->viewparams['year_options'] = $year_options;
        $this->viewparams['year_params'] = array(
            'id'   => 'year',
            'class' => "form-control input-small"
            //,'data-rel'=> "chosen",
        ); 

          /* organization types */
        $organization_types = $this->organization_type_model->search(0,1,'id','asc');
        $organization_types_options = array();
        $organization_types_options[0] = '';
        foreach($organization_types as $organization_type){
          $organization_types_options[$organization_type->id] = $organization_type->name;
        }

        $this->viewparams['organization_type_id_options'] = $organization_types_options;
        $this->viewparams['organization_type_id_params'] = array(
            'id'   => 'organization_type_id',
            'class' => "form-control input-medium",
            'data-rel'=> "chosen"
          );

        $this->viewparams['title_page']	= lang('lreport_per_output');
		    parent::viewpage("vreport1");
	}
	 
  function doexport1(){
      //line 4 : pilar
    //line 5 : quartal 
    //line 6&7 : penerima manfaat


      $file_url = ""; 
      $pilar_id = $this->input->get('pilar_id');
      $year = $this->input->get('year');
      $quarter = $this->input->get('quarter');

      //check if data exists
      $searchv = array(
          'pilar_id'      => $pilar_id
          ,'year'         => $year
          ,'organization_id'    => $this->input->get('organization_id')
          ,'organization_type_id'       => $this->input->get('organization_type_id')
          ,'quarter'      => $quarter
        );
      

        //$query = $this->activity_model->search(0,1,'start_date','asc',$searchv);
        $count = $this->activity_model->countSearch($searchv);
        //$count = 1;*/

        $is_error = true;
        
        //write to excel
        if($count){
          
           $objPHPExcel  = new PHPExcel();
            $objPHPExcel->getProperties()
            ->setCreator('Farsight')
            ->setTitle('Activity Report');
   
            //write the sheet
            if($quarter){
                $this->writeSheet($objPHPExcel,1,$quarter,$searchv);
            }else{
              for($i=1;$i<=4;$i++){
                $this->writeSheet($objPHPExcel,$i,$i,$searchv);
              }
            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $filename = "activity_report_peroutput.".date("Ymd").".xlsx";
            $objWriter->save(FCPATH."reports/".$filename);

            
            $file_url = base_url()."reports/".$filename;
            $message = "Done";

            $is_error = false;
        }else{
            $message = "Data tidak ditemukan";
        }

        $result = array(
          "is_error"  =>  $is_error
          ,"message"  => $message
          ,"file_url" => $file_url
        );

        echo json_encode($result);

  }

  function cellsToMergeByColsRow($start = NULL, $end = NULL, $row = NULL){
    $merge = 'A1:A1';
    if($start && $end && $row){
        $start = PHPExcel_Cell::stringFromColumnIndex($start);
        $end = PHPExcel_Cell::stringFromColumnIndex($end);
        $merge = "$start{$row}:$end{$row}";

    }

    return $merge;
  }

  function writeSheet($objPHPExcel,$sheet, $quarter,$searchv){
        $title = lang('llaporan_output'); //line 3 , size 24 bold
 
        $row = 1;
        $iSheet = $sheet-1;
        $year = $searchv['year'];
        $pilar_id = $searchv['pilar_id'];

        //more than 1 sheet , create new sheet 
        if($iSheet > 0){
          $objPHPExcel->createSheet($iSheet);
        }

        $worksheet = $objPHPExcel->getSheet($iSheet);
        $worksheet->setTitle('Quarter '.$quarter." ".$searchv['year']);

        $quarters = config_item('quarter');
        $quarter_title = "";
        foreach($quarters as $q){
            if($q['value'] == $quarter){
                $quarter_title = $q['title']." ".$year;
            }
        }
        
 
        //title 
        $row++;
        $worksheet->setCellValueByColumnAndRow(0, $row, $title)->getStyle("A2")->getFont()->setSize(24);
        
        //auto height , but not working in libre
       // $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(26);
        $worksheet->getRowDimension($row)->setRowHeight(26);


        //filter info
        $row++;
        $row++;

        //get pilar name
        $pilar_name="";
        if($pilar_id){
            $data_pilar = $this->pilar_model->get_data_by_id($pilar_id);
            if(isset($data_pilar[0]->name)){
                $pilar_name = $data_pilar[0]->name;
            }
        }

        //get organization type name
        $organization_type_name = "";
        if($searchv['organization_type_id']){
           $data_org_type = $this->organization_type_model->get_data_by_id($searchv['organization_type_id']);
            if(isset($data_org_type[0]->name)){
                $organization_type_name = $data_org_type[0]->name;
            } 
        }
       
        //get organization type name
        $organization_name = "";
        if($searchv['organization_id']){
           $data_organization = $this->organization_model->get_data_by_id($searchv['organization_id']);
            if(isset($data_organization[0]->organization_name)){
                $organization_name = $data_organization[0]->organization_name;
            } 
        }
     
        $period_filter = "";
        if($searchv['quarter']){
          $period_filter = lang('lquarter')." ".$searchv['quarter'];
          $period_filter .= " ".$searchv['year'];
        }else{
          if($searchv['year']) {
            $period_filter = $searchv['year'];
          }
        }

        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lpilar')." : ".$pilar_name);
        $row++;
        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lorganization_type')." : ".$organization_type_name);
        $row--;
        $worksheet->setCellValueByColumnAndRow(1, $row, lang('lorganization')." : ".$organization_name);
         $row++;
        $worksheet->setCellValueByColumnAndRow(1, $row, lang('lperiode')." : ".$period_filter);
       

        $row++;

        //Pilar , width : 9 cols
        $pilars = $this->pilar_model->search(0,1,'name','asc',array("pilar_id"=>$pilar_id));
        $row++;
       
        //merge A and B
         $worksheet->mergeCells('A'.$row.':B'.$row);

        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lpilar'))->getStyle("A7")->getFont()->setBold(true);
        $worksheet->getStyle("A7")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cols = 2;
        $col = $cols;
        foreach($pilars as $pilar){
            $worksheet->setCellValueByColumnAndRow($cols, $row,$pilar->name);
            $cols += 9;
            //$worksheet->mergeCells($this->mergeCells($col,$cols,$row));
            $worksheet->mergeCells($this->cellsToMergeByColsRow($col,$cols-1,$row));
            $columnName = PHPExcel_Cell::stringFromColumnIndex($col);
            $columnRowName = "$columnName{$row}";
            $worksheet->getStyle($columnRowName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $col = $cols;
        }

        //quartal 
        $row++;
        $worksheet->mergeCells('A'.$row.':B'.$row);
        $worksheet->getStyle("A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lkuartal'))->getStyle("A8")->getFont()->setBold(true);

        $cols = 2;
        $col = $cols;

        foreach($pilars as $pilar){
            $worksheet->setCellValueByColumnAndRow($cols, $row,$quarter_title);
            $cols += 9;

            //$worksheet->mergeCells($this->mergeCells($col,$cols,$row));
            $worksheet->mergeCells($this->cellsToMergeByColsRow($col,$cols-1,$row));
            $columnName = PHPExcel_Cell::stringFromColumnIndex($col);
            $columnRowName = "$columnName{$row}";
            $worksheet->getStyle($columnRowName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $col = $cols;
        }

        //beneficiaries
        $row++;
        $worksheet->mergeCells('A'.$row.':B'.$row);
        $worksheet->getStyle("A".$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cols = 2;
        $col = $cols;
        $beneficiaries = config_item('beneficiaries');
        foreach($pilars as $pilar){
           foreach($beneficiaries as $beneficiary){
              $worksheet->setCellValueByColumnAndRow($cols, $row,$beneficiary['title']);
              $cols += 3;

              //$worksheet->mergeCells($this->mergeCells($col,$cols,$row));
              $worksheet->mergeCells($this->cellsToMergeByColsRow($col,$cols-1,$row));
              $columnName = PHPExcel_Cell::stringFromColumnIndex($col);
              $columnRowName = "$columnName{$row}";
              $worksheet->getStyle($columnRowName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              $col = $cols;

           }
        }


        //output
        $worksheet->setCellValueByColumnAndRow(0, $row, lang('louput'))->getStyle("A9")->getFont()->setBold(true);

        //L P W
        $row++;
        $cols = 2;
        $participants = config_item('participants');
        foreach($pilars as $pilar){
            foreach($beneficiaries as $beneficiary){
                foreach($participants as $participant){
                    $worksheet->setCellValueByColumnAndRow($cols, $row,$participant['code']);
                    $columnName = PHPExcel_Cell::stringFromColumnIndex($cols);
                    $cols += 1;
                    $columnRowName = "$columnName{$row}";
                    //echo $columnRowName.";";
                    $worksheet->getStyle($columnRowName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
           }
        }

        //outputs
        $row++;
        $outputs = $this->output_model->search(0,1,'outcome_id,output_id','asc');
        $outcome = "";
        foreach($outputs as $output){
            $cols = 2;
            if($outcome <> $output->outcome_code){
                $outcome = $output->outcome_code;
                $worksheet->setCellValueByColumnAndRow(0, $row,$output->outcome_name)->getStyle("A".$row)->getFont()->setBold(true);
                $worksheet->mergeCells('A'.$row.':B'.$row);
                //$worksheet->getStyle("A".$row)->getAlignment()->setWrapText(true); 

                $row++;
             }
              $worksheet->setCellValueByColumnAndRow(0, $row,$output->output_name);
              $worksheet->mergeCells('A'.$row.':B'.$row);
              
             //get the data per row
              foreach($pilars as $pilar){
                  $v = array(
                    "pilar_id"    => $pilar->id
                    ,"output_id"  => $output->output_id
                    ,"quarter"    => $quarter
                    ,"year"       => $year
                    ,'organization_id'            => $searchv['organization_id']
                    ,'organization_type_id'       => $searchv['organization_type_id']
                  );
                  
                  //get data by pilar, by quarter 
                  $datas = $this->activity_model->get_data_by_pilar_quarter($v);
                  //echo $this->db->last_query();
                  //echo "<BR>";
                  //$this->firephp->log("sql : ".$this->db->last_query());

                  foreach($beneficiaries as $beneficiary){
                      foreach($participants as $participant){
                          $fieldParticipant  = $participant['field']."_".$beneficiary['field'];
                          $fieldValue = 0;
                          if(isset($datas[0])){
                             $fieldValue = $datas[0]->$fieldParticipant;
                          }
                          //echo "val:".$fieldValue."<BR>";
                          //  $this->firephp->log("data : ".$fieldValue);

                          $worksheet->setCellValueByColumnAndRow($cols, $row,$fieldValue);
                          $columnName = PHPExcel_Cell::stringFromColumnIndex($cols);
                          $cols += 1;
                          $columnRowName = "$columnName{$row}";
                          //echo $columnRowName.";";
                          $worksheet->getStyle($columnRowName)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                      }
                 }
              }
         

            $row++;
        }
        //exit;

        $worksheet->getColumnDimension('A')->setWidth(50);
  }
}

/* End of file Report.php */
/* Location: ./apps/modules/activity/controllers/Report.php */
