<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Beneficiary extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		  if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		  parent::__construct();
      $this->load->model('activity_model');
      $this->load->model('pilar/pilar_model');
      $this->load->model('organization_type/organization_type_model');
      $this->load->model('organization/organization_model');
      $this->load->model('output/output_model');
      $this->load->model('outcome/outcome_model');
	}
	
	function index(){
    	$this->Output();
	}
	   	
  function Output(){

  	 	$this->load->helper('form');
  	 	$this->load->model('pilar/pilar_model');
  	 	$this->load->model('activity_type/activity_type_model');
  	 	$this->load->model('province/province_model');

  	 	/* get pilar list */
  	 	$pilars = $this->pilar_model->search(0,1,'name','asc');
  	 	$pilar_options = array();
  	 	$pilar_options[0] = '';
  	 	foreach($pilars as $pilar){
  	 		$pilar_options[$pilar->id] = $pilar->name;
  	 	}
       	$this->viewparams['pilar_options'] = $pilar_options;
        $this->viewparams['pilar_params'] = array(
            'id'   => 'pilar_id',
            'class'	=> "form-control input-xxlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_pilar')
        );
        /* end pilar */
       
        /* quarter and year params */
        $quarters  = config_item('quarter');
        $quarter_options = array();
        $quarter_options[0] = '';
        foreach($quarters as $quarter){
          $quarter_options[$quarter['value']] = $quarter['title'];
        }
        $this->viewparams['quarter_options'] = $quarter_options;
        $this->viewparams['quarter_params'] = array(
            'id'   => 'quarter',
            'class' => "form-control input-medium"
            //,'data-rel'=> "chosen",
        );

        $year_options = array();
        for($i=config_item('year_start');$i<date("Y");$i++){
          $year_options[$i] = $i;
        }
        $this->viewparams['year_options'] = $year_options;
        $this->viewparams['year_params'] = array(
            'id'   => 'year',
            'class' => "form-control input-small"
            //,'data-rel'=> "chosen",
        ); 

        $this->viewparams['title_page']	= lang('lreport_per_beneficiary');
        $header_col = $this->get_report_columns();
        $this->viewparams['columns'] = $header_col;
  
		    parent::viewpage("vbeneficiary");
	}
	 
  function doexport(){
      //line 4 : pilar
    //line 5 : quartal 
    //line 6&7 : penerima manfaat


      $file_url = ""; 
      $pilar_id = $this->input->get('pilar_id');
      $year = $this->input->get('year');
      $quarter = $this->input->get('quarter');

      //check if data exists
      $searchv = array(
          'pilar_id'      => $pilar_id
          ,'year'         => $year
          ,'organization_id'    => $this->input->get('organization_id')
          ,'organization_type_id'       => $this->input->get('organization_type_id')
          ,'quarter'      => $quarter
        );
      

        //$query = $this->activity_model->search(0,1,'start_date','asc',$searchv);
        //check if there is any records
        $count = $this->activity_model->countSearch($searchv);
        //$count = 1;*/

        $is_error = true;
        
        //write to excel
        if($count){
          
           $objPHPExcel  = new PHPExcel();
            $objPHPExcel->getProperties()
            ->setCreator('Farsight')
            ->setTitle('Activity Report Per Beneficiary');
   
            //write the sheet
            if($quarter){
                $this->writeSheet($objPHPExcel,1,$quarter,$searchv);
            }else{
              for($i=1;$i<=4;$i++){
                $this->writeSheet($objPHPExcel,$i,$i,$searchv);
              }
            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $filename = "activity_report_perbeneficiary.".date("Ymd").".xlsx";
            $objWriter->save(FCPATH."reports/".$filename);

            
            $file_url = base_url()."reports/".$filename;
            $message = "Done";

            $is_error = false;
        }else{
            $message = "Data tidak ditemukan";
        }

        $result = array(
          "is_error"  =>  $is_error
          ,"message"  => $message
          ,"file_url" => $file_url
        );

        echo json_encode($result);

  }

  function writeSheet($objPHPExcel,$sheet, $quarter,$searchv){
        $title = lang('lreport_per_beneficiary'); //line 3 , size 24 bold
 
        $row = 1;
        $iSheet = $sheet-1;
        $year = $searchv['year'];
        $pilar_id = $searchv['pilar_id'];

        //more than 1 sheet , create new sheet 
        if($iSheet > 0){
          $objPHPExcel->createSheet($iSheet);
        }

        $worksheet = $objPHPExcel->getSheet($iSheet);
        $worksheet->setTitle('Beneficiaries Report Q'.$quarter." ".$searchv['year']);

        $quarters = config_item('quarter');
        $quarter_title = "";
        foreach($quarters as $q){
            if($q['value'] == $quarter){
                $quarter_title = $q['title']." ".$year;
            }
        }
        

        //title 
        $row++;
        $worksheet->setCellValueByColumnAndRow(0, $row, $title)->getStyle("A2")->getFont()->setSize(20);
        
        //auto height , but not working in libre
        //$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(-1);
        $worksheet->getRowDimension($row)->setRowHeight(26);


        //filter info
        $row++;
        $row++;

        //get pilar name
        $pilar_name="";
        if($pilar_id){
            $data_pilar = $this->pilar_model->get_data_by_id($pilar_id);
            if(isset($data_pilar[0]->name)){
                $pilar_name = $data_pilar[0]->name;
            }
        }

         
        //get organization type name
        $organization_name = "";
        if($searchv['organization_id']){
           $data_organization = $this->organization_model->get_data_by_id($searchv['organization_id']);
            if(isset($data_organization[0]->organization_name)){
                $organization_name = $data_organization[0]->organization_name;
            } 
        }
     
        $period_filter = "";
        if($searchv['quarter']){
          $period_filter = lang('lquarter')." ".$searchv['quarter'];
          $period_filter .= " ".$searchv['year'];
        }else{
          if($searchv['year']) {
            $period_filter = $searchv['year'];
          }
        }

        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lpilar')." : ".$pilar_name);
        $row++;
        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lorganization')." : ".$organization_name);
         $row++;
        $worksheet->setCellValueByColumnAndRow(0, $row, lang('lperiode')." : ".$period_filter);
       

        $row++;
        /* end filter info */


        //set header
        $headers = $this->get_report_columns();
        $row++;
        $cols = 0;
        $worksheet->getRowDimension($row)->setRowHeight(40);

        foreach($headers as $header) {
              //set to bold
              $worksheet->setCellValueByColumnAndRow($cols, $row,$header['title'])->getStyleByColumnAndRow($cols, $row)->getFont()->setBold(true);
              
              //set vertical center and horizontal center
              $worksheet->getStyleByColumnAndRow($cols,$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
              
              $worksheet->getStyleByColumnAndRow($cols,$row)->getAlignment()->setWrapText(true);

              //set the column width
              if($header['auto_size'] == True)
                $worksheet->getColumnDimensionByColumn($cols)->setAutoSize(true);
              else
                $worksheet->getColumnDimensionByColumn($cols)->setWidth($header['width']);
              
            $cols++;
        }
        
        $filter = array(
          "pilar_id"                    => $searchv['pilar_id']
          ,'organization_id'            => $searchv['organization_id']
          ,'organization_type_id'       => $searchv['organization_type_id']
          ,"quarter"                    => $quarter
          ,"year"                       => $searchv['year']
        );
   
        //get data by organization, by activity type 
        $datas = $this->activity_model->get_data_by_organization(0,1,'','',$filter);
        $this->firephp->log($this->db->last_query());
        
        
        //set the values 
        if(count($datas)){
            foreach($datas as $data){
                $row++;
                $cols = 0;
                foreach($headers as $header){
                     $fieldValue = "";
                     if(isset($data->$header['field']) && $data->$header['field']){
                       $fieldValue = $data->$header['field'];
                      }

                      $worksheet->setCellValueByColumnAndRow($cols, $row,$fieldValue);

                      //set alignment
                      if($header['align'] == "right"){
                        $worksheet->getStyleByColumnAndRow($cols,$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                      }else
                      if($header['align'] == "left"){
                          $worksheet->getStyleByColumnAndRow($cols,$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                      }else{
                          $worksheet->getStyleByColumnAndRow($cols,$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                      }   
                      $cols++;                      
                }
            }
        }
  }

  
  private function get_report_columns() {
    //ref http://www.trirand.com/jqgridwiki/doku.php?id=wiki%3agroupingheadar
    $columns = array();

    $n = 0;
    $columns[$n]['title'] = lang('lpilar');
    $columns[$n]['field'] = 'pilar_name';
    $columns[$n]['align'] = 'left';
    $columns[$n]['auto_size'] = false;
    $columns[$n]['width'] = 30;

    $n++;
    $columns[$n]['title'] = lang('leo');
    $columns[$n]['field'] = 'parent_organization_name';
    $columns[$n]['align'] = 'left';
    $columns[$n]['auto_size'] = false;
    $columns[$n]['width'] = 25;

    $n++;
    $columns[$n]['title'] = lang('lorganization');
    $columns[$n]['field'] = 'organization_name';
    $columns[$n]['align'] = 'left';
    $columns[$n]['auto_size'] = false;
    $columns[$n]['width'] = 35;

    $n++;
    $columns[$n]['title'] = lang('ltotal_target_group');
    $columns[$n]['field'] = 'organization_total_target_group';
    $columns[$n]['align'] = 'right';
    $columns[$n]['auto_size'] = false;
    $columns[$n]['width'] = 12;

    $n++;
    $columns[$n]['title'] = lang('ltotal_peserta');
    $columns[$n]['field'] = 'total_jumlah_peserta';
    $columns[$n]['align'] = 'right';
    $columns[$n]['width'] = 12;
    $columns[$n]['auto_size'] = false;

    $n++;
    $columns[$n]['title'] = lang('ljumlah_aktivitas');
    $columns[$n]['field'] = 'total_aktivitas';
    $columns[$n]['align'] = 'right';
    $columns[$n]['width'] = 12;
    $columns[$n]['auto_size'] = false;

    //activity types
    $query = $this->db->query("SELECT id,name FROM activity_types ORDER BY id asc");
    $results = $query->result();
      foreach($results as $res) {
        $n++;
        $columns[$n]['title'] = $res->name;
        $columns[$n]['field'] = url_title($res->name,'underscore',true);
        $columns[$n]['align'] = 'right';
        $columns[$n]['width'] = 12;
        $columns[$n]['auto_size'] = false;

        $n++;
        $columns[$n]['title'] = lang('ljumlah_peserta');
        $columns[$n]['field'] = "jumlah_peserta_".url_title($res->name,'underscore',true);
        $columns[$n]['align'] = 'right';
        $columns[$n]['width'] = 12;
        $columns[$n]['auto_size'] = false;

      }
    return $columns;
  }
  
}

/* End of file Report.php */
/* Location: ./apps/modules/activity/controllers/Report.php */
