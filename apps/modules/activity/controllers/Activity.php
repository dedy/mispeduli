<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Activity extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
   var  $columns = array(
                "activity_id"
                ,"organization_type_name"
                ,"pilar_name"
                ,"start_date_fmt"
                ,"end_date_fmt"
                ,"activity_name"
                ,"organization_name"
                ,"activity_type_name"
                ,"province_name"
                ,"kabupaten_kota_name"
                ,"result_description"
                ,"conversation"
                ,"male_direct_beneficiaries"
                ,"female_direct_beneficiaries"
                ,"trans_direct_beneficiaries"
                ,"male_indirect_beneficiaries"
                ,"female_indirect_beneficiaries"
                ,"trans_indirect_beneficiaries"
                ,"male_government_official_beneficiaries"
                ,"female_government_official_beneficiaries"
                ,"trans_government_official_beneficiaries"

      );

	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
		$this->load->model('activity_model');
	}
	
	function index(){
    	$this->lists();
	}
	   	
  function lists(){

  	 	$this->load->helper('form');
      $this->load->model('pilar/pilar_model');
  	 	$this->load->model('organization/organization_model');
  	 	$this->load->model('activity_type/activity_type_model');
      $this->load->model('province/province_model');
  	 	$this->load->model('organization_type/organization_type_model');

  	 	/* get pilar list */
  	 	$pilars = $this->pilar_model->search(0,1,'name','asc');
  	 	$pilar_options = array();
  	 	$pilar_options[0] = '';
  	 	foreach($pilars as $pilar){
  	 		$pilar_options[$pilar->id] = $pilar->name;
  	 	}
       	$this->viewparams['pilar_options'] = $pilar_options;
        $this->viewparams['pilar_params'] = array(
            'id'   => 'pilar_id',
            'class'	=> "form-control input-xxlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_pilar')
        );
        /* end pilar */

        /* get organization list */
      /*$organizations = $this->organization_model->search(0,1,'organization_type_id,organization_name','asc');
      $organization_options = array();
      $organization_options[0] = '';
      foreach($organizations as $organization){
        $organization_options[$organization->organization_id] = $organization->organization_name;
      }
        $this->viewparams['organization_options'] = $organization_options;
        $this->viewparams['organization_params'] = array(
            'id'   => 'organization_id',
            'class' => "form-control input-xxlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_organization')
        );*/
        /* end organization */  

       	/* activity type */
       	$activity_types = $this->activity_type_model->search(0,1,'name','asc');
  	 	$activity_type_options = array();
  	 	$activity_type_options[0] = '';
  	 	foreach($activity_types as $activity_type){
  	 		$activity_type_options[$activity_type->id] = $activity_type->name;
  	 	}
       	$this->viewparams['activity_type_options'] = $activity_type_options;
        $this->viewparams['activity_type_params'] = array(
            'id'   => 'activity_type_id',
            'class'	=> "form-control input-xlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_activity_type')
        );
       	/* end activity type */

       	/* provinces */
       	$provinces = $this->province_model->search(0,1,'name','asc');
  	 	$province_options = array();
  	 	$province_options[0] = '';
  	 	foreach($provinces as $province){
  	 		$province_options[$province->id] = $province->name;
  	 	}
       	$this->viewparams['province_options'] = $province_options;
        $this->viewparams['province_params'] = array(
            'id'   => 'province_id',
            'class'	=> "form-control input-xlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_province')
        );
       	/* end province */

        /* organization types */
        $organization_types = $this->organization_type_model->search(0,1,'id','asc');
        $organization_types_options = array();
        $organization_types_options[0] = '';
        foreach($organization_types as $organization_type){
          $organization_types_options[$organization_type->id] = $organization_type->name;
        }

        $this->viewparams['organization_type_id_options'] = $organization_types_options;
        $this->viewparams['organization_type_id_params'] = array(
            'id'   => 'organization_type_id',
            'class' => "form-control input-medium",
            'data-rel'=> "chosen"
          );


  	    $this->viewparams['title_page']	= lang('lactivity');
		parent::viewpage("vlist");
	}
	 
   function dodo(){
       $objPHPExcel  = new PHPExcel();
       
       $objPHPExcel->getProperties()
           ->setCreator('Farsight')
           ->setTitle('Activity Report');

      $objPHPExcel->setActiveSheetIndex(0);

      $worksheet = $objPHPExcel->getSheet(0);
      $worksheet->setTitle('Data');
      //$worksheet->setCellValue('A1', 'This is just some text value');
      
      for($row=1; $row<10; $row++){
        for($col=0;$col<20;$col++){
          $worksheet->setCellValueByColumnAndRow($col, $row, "row($row) col($col)");
        }
      }
      
      //$objPHPExcel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
      //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
      //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
      $objWriter->save("/home/dedy/www/mispeduli/test.xlsx");


/*      //activate worksheet number 1
      $objExcel->setActiveSheetIndex(0);
      //name the worksheet
      $objExcel->getActiveSheet()->setTitle('test worksheet');
      //set cell A1 content with some text
      $objExcel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
      //change the font size
      $objExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
      //make the font become bold
      $objExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
      //merge cell A1 until D1
      $objExcel>getActiveSheet()->mergeCells('A1:D1');
      //set aligment to center for that merged cell (A1 to D1)
      $objExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

      $filename='just_some_random_name.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache
                  
      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
      //See more at: https://arjunphp.com/how-to-use-phpexcel-with-codeigniter/#sthash.U7oAZY4m.dpuf
*/
   }


  function doexport(){

      //fields
      $columns = array(
        array("title"   => lang('lorganization_type') , "field"  => "organization_type_name")
        ,array("title"  => lang('lpilar')             , "field"   => "pilar_name")
        ,array("title"  => lang("lstart_date")        , "field"   => "start_date_fmt")
        ,array("title"  => lang('lend_date')          , "field"   => "end_date_fmt")
        ,array("title"  => lang('lactivity_name')     , "field" => "activity_name")
        ,array("title"  => lang('lorganization')      , "field" => "organization_name")
        ,array("title"  => lang('lactivity_type')     , "field" => "activity_type_name")


        ,array("title"   => "MD" , "field"   => "male_direct_beneficiaries")
        ,array("title"  => "FD" , "field" => "female_direct_beneficiaries")
        ,array("title"  => "TD" , "field" => "trans_direct_beneficiaries")
        ,array("title"  => "MI" , "field" => "male_indirect_beneficiaries")
        ,array("title"  => "FI" , "field" => "female_indirect_beneficiaries")
        ,array("title"  => "TI" , "field" => "trans_indirect_beneficiaries")
        ,array("title"  => "MG" , "field" => "male_government_official_beneficiaries")
        ,array("title"  => "FG" , "field" => "female_government_official_beneficiaries")
        ,array("title"  => "TG" , "field" => "trans_government_official_beneficiaries")
      
        ,array("title"  => lang('lprovince')  ,   "field" => "province_name")
        ,array("title"  => lang('lkabupaten_kota'), "field" => "kabupaten_kota_name")
        ,array("title"  => lang('laddress') ,  "field" => "activity_address")
        ,array("title"  => lang('lresult_description') ,  "field" => "result_description")
        ,array("title"  => lang('lconversation'),  "field" => "conversation")
        ,array("title"  => lang('loutcome') ,  "field" => "outcome_codes")
        ,array("title"  => lang('loutput') ,  "field" => "output_codes")
      );

       $column1_headers = array(
          array("title"   => lang('lorganization_type') )
          ,array("title"  => lang('lpilar')             )
          ,array("title"  => lang("lstart_date")        )
          ,array("title"  => lang('lend_date')          )
          ,array("title"  => lang('lactivity_name')     )
          ,array("title"  => lang('lorganization')      )
          ,array("title"  => lang('lactivity_type')     )

          
          ,array("title"   => "Direct Beneficiaries" )
          ,array("title"  => "" , "field" )
          ,array("title"  => "" , "field" )

          ,array("title"  => "Indirect Beneficiaries" )
          ,array("title"  => "" , "field" )
          ,array("title"  => "" , "field" )
          
          ,array("title"  => "Government Official" )
          ,array("title"  => "" , "field" )
          ,array("title"  => "" , "field" )
        
          ,array("title"  => lang('lprovince')  )
          ,array("title"  => lang('lkabupaten_kota'))
          ,array("title"  => lang('laddress') )
          ,array("title"  => lang('lresult_description'))
          ,array("title"  => lang('lconversation'))
          ,array("title"  => lang('loutcome'))
          ,array("title"  => lang('loutput') )
      );

      $column2_headers = array(
        array("title"   => "" ) //org type
        ,array("title"  => "" ) //pilar
        ,array("title"  => "" ) //start date
        ,array("title"  => "" ) //end date
        ,array("title"  => "" ) //activity name
        ,array("title"  => "" ) //org name
        ,array("title"  => "" ) //activity type name

        
        ,array("title"   => "Male"    )
        ,array("title"  => "Female"   )
        ,array("title"  => "Transgender" )

        ,array("title"  => "Male" )
        ,array("title"  => "Female" )
        ,array("title"  => "Transgender")
        
        ,array("title"  => "Male" )
        ,array("title"  => "Female" )
        ,array("title"  => "Transgender" )
      
        ,array("title"  => "" ) //province
        ,array("title"  => "") //kabupaten kota
        ,array("title"  => "") //address
        ,array("title"  => "")  //rsult description
        ,array("title"  => "") //conversation
        ,array("title"  => "") //outcome
        ,array("title"  => "") //output
      );



      $file_url = "";
       $searchv = array(
          'pilar_id'          => $this->input->get('pilar_id')
          ,'organization_id'    => $this->input->get('organization_id')
          ,'province_id'        => $this->input->get('province_id')
          ,'organization_type_id'       => $this->input->get('organization_type_id')
          ,'activity_type_id'     => $this->input->get('activity_type_id')
          ,'start_date_from'     => $this->input->get('start_date_from')
          ,'start_date_to'     => $this->input->get('start_date_to')
          ,'end_date_from'     => $this->input->get('end_date_from')
          ,'end_date_to'     => $this->input->get('end_date_to')
          ,'keyword'     => urldecode($this->input->get('keyword'))
    
        );
      

        $query = $this->activity_model->search(0,1,'start_date','asc',$searchv);
        $count = $this->activity_model->countSearch($searchv);


        $is_error = true;
        
        //write to excel
        if($count){
            $objPHPExcel  = new PHPExcel();
            $objPHPExcel->getProperties()
             ->setCreator('Farsight')
             ->setTitle('Activity Report');
          
          //$objPHPExcel->setActiveSheetIndex(0);

            $worksheet = $objPHPExcel->getSheet(0);
            $worksheet->setTitle('Data');


            //$worksheet->setCellValue('A1', 'This is just some text value');
            //set header
            $col = 0;
            foreach($column1_headers as $column1_header){
                //set the header title , and set it to bold
                $worksheet->setCellValueByColumnAndRow($col, 1, $column1_header['title'])->getStyle("A1:W1")->getFont()->setBold(true);
                $col++;
            }

            //header 2
            $col = 0;
            foreach($column2_headers as $column2_headers){
                //set the header title , and set it to bold
                $worksheet->setCellValueByColumnAndRow($col, 2, $column2_headers['title'])->getStyle("A2:W2")->getFont()->setBold(true);
                $col++;
            }

            //merge the header
            $worksheet->mergeCells('A1:A2');
            $worksheet->mergeCells('B1:B2');
            $worksheet->mergeCells('C1:C2');
            $worksheet->mergeCells('D1:D2');
            $worksheet->mergeCells('E1:E2');
            $worksheet->mergeCells('F1:F2');
            $worksheet->mergeCells('G1:G2');

            //set vertical align top
            $worksheet->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("B1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("C1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("D1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("E1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("F1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("G1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            
            //participants header
            $worksheet->mergeCells('H1:J1');
            $worksheet->mergeCells('K1:M1');
            $worksheet->mergeCells('N1:P1');

            //set horizontal center
            $worksheet->getStyle("H1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $worksheet->getStyle("K1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $worksheet->getStyle("N1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            //merge header 
            $worksheet->mergeCells('Q1:Q2');
            $worksheet->mergeCells('R1:R2');
            $worksheet->mergeCells('S1:S2');
            $worksheet->mergeCells('T1:T2');
            $worksheet->mergeCells('U1:U2');
            $worksheet->mergeCells('V1:V2');
            $worksheet->mergeCells('W1:W2');

            //set vertical align top 
            $worksheet->getStyle("Q1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("R1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("S1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("T1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("U1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("V1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $worksheet->getStyle("W1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);


            for($i=0; $i<$count; $i++){
              $col = 0;
              $row = $i+3; //start row 3
              foreach($columns as $column){
                $fieldname = $column['field'];
                $worksheet->setCellValueByColumnAndRow($col, $row, $query[$i]->$fieldname);
                $col++;
              }
            }
            
            //$objPHPExcel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
            //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
            //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $filename = "activity_report.".date("Ymd").".xlsx";
            $objWriter->save(FCPATH."reports/".$filename);
            $file_url = base_url()."reports/".$filename;
            $message = "Done";

            $is_error = false;
        }else{
            $message = "Data tidak ditemukan";
        }

        $result = array(
          "is_error"  =>  $is_error
          ,"message"  => $message
          ,"file_url" => $file_url
        );

        echo json_encode($result);

  }

	function loadDataGrid(){
		
		    $page = isset($_POST['page'])?$_POST['page']:1;
        $sidx = isset($_POST['sidx'])?$_POST['sidx']:'start_date'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		  $limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       	
        $searchv = array(
        	'pilar_id' 					=> $this->input->get('pilar_id')
        	,'organization_id'		=> $this->input->get('organization_id')
          ,'province_id'        => $this->input->get('province_id')
        	,'organization_type_id'				=> $this->input->get('organization_type_id')
        	,'activity_type_id'			=> $this->input->get('activity_type_id')
           ,'start_date_from'     => $this->input->get('start_date_from')
          ,'start_date_to'     => $this->input->get('start_date_to')
          ,'end_date_from'     => $this->input->get('end_date_from')
          ,'end_date_to'     => $this->input->get('end_date_to')
          ,'keyword'     => urldecode($this->input->get('keyword'))
  		  );
  		

      	$query = $this->activity_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		    $count = $this->activity_model->countSearch($searchv);
		    $this->firephp->log($this->db->last_query());
		    $this->firephp->log("count : ".$count);

        if(count($query)){
            for($i=0;$i<count($query);$i++){
                $query[$i]->result_description = nl2br($query[$i]->result_description);
            }
        }
        $columns = $this->columns;

		    $this->DataJqGrid = array(
        	 "page"		=> $page,
        	 "sidx"		=> $sidx,
        	 "sord"		=> $sord,
        	 "query" 	=> $query,
        	 "limit"		=> $limit,
			     "count"		=> $count,
			     "column"	=> $columns,
          "id"		=> "activity_id"
 		     );
            
        parent::loadDataJqGrid();
		
	}
}

/* End of file dashboard.php */
/* Location: ./apps/modules/dashboard/controllers/dashboard.php */
