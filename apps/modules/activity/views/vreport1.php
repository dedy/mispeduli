 <script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>

<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo assets_url(); ?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>

    
<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>

<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>

<script type="text/javascript">
	 jQuery().ready(function (){

        $("#organization_spinner").hide();

        //chosen - improves select
    	$('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect:true});
     	

        $("select#pilar_id").change(function(){
           var organization_type_id = $("#organization_type_id").val();
           populate_organization($(this).val(),organization_type_id);
        });

        $("select#organization_type_id").change(function(){
           var pilar_id = $("#pilar_id").val();
            populate_organization(pilar_id,$(this).val());
        });
    
        populate_organization(0,0);
       
        $('#export-btn').click(function(e){
            //$('form#search-frm').submit();
            //return;
            e.preventDefault(); 
                var param = getParam();

              $.confirm({
                title:"Export",
                text: "<?=lang('lexport_confirm')?>",
                confirmButton: "Ok",
                cancelButton: "Cancel",
                confirm: function(button) {
                   $.post("<?=base_url()?>activity/report/doexport1/?"+param, 
                    function(data) {
                        if(!data.is_error){
                            window.location.href= data.file_url;
                        }else{
                            var rv = '<div class="alert alert-danger">'+data.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal');
                        }
                    },"json"
                );
                },
                cancel: function(button) {
                   // alert("You cancelled.");
                }
            });

            
        });

    });
    
    function getParam(){
        var organization_type_id = (jQuery("#organization_type_id option:selected").val())?jQuery("#organization_type_id option:selected").val():'0';
        var organization_id = (jQuery("#organization_id option:selected").val())?jQuery("#organization_id option:selected").val():'0';
        var pilar_id = (jQuery("#pilar_id option:selected").val())?jQuery("#pilar_id option:selected").val():'0';
        var quarter = (jQuery("#quarter option:selected").val())?jQuery("#quarter option:selected").val():'0';
        var year = (jQuery("#year option:selected").val())?jQuery("#year option:selected").val():'0';
        
        var param = 'organization_type_id='+organization_type_id+'&organization_id='+organization_id+'&pilar_id='+pilar_id+'&quarter='+quarter+'&year='+year;
        return param;
    }

    function populate_organization(pilar_id,organization_type_id){
        $("#organization_spinner").show();
        $.getJSON("<?=site_url('organization/get_by_pilar')?>/"+pilar_id+'/'+organization_type_id,
            function(j){
              var options = '<option value=""></option>';
      
              for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].organization_id + '">' + j[i].organization_name+ '</option>';
              }
              $("select#organization_id").html(options).trigger('chosen:updated');
              $("#organization_spinner").hide();
            }
        );
    }
   
    
</script>


<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url('Dashboard')?>">Home</a>
        </li>
        <li>
            <a href="#"><?=$title_page?></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title_page?></h2>
            </div>

              <div class="box-content">
                <?php echo form_open("activity/loadDataGrid",array('role'=>"form",'id'=>'input-form' ))?>


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class='control-label' for="pilar_id"><?=lang('lpilar');?></label>
                                <div class="controls">

                                        <?php echo form_dropdown('pilar_id', $pilar_options,'0',$pilar_params);?>
                                </div>                         
                            </div>

                            <div class="form-group">
                                <label class='control-label' for="organization_type_id"><?=lang('lorganization_type');?></label>
                                <div class="controls">

                                        <?php echo form_dropdown('organization_type_id', $organization_type_id_options,'0',$organization_type_id_params);?>
                                </div>                         
                            </div>

                             
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="organization"><?php echo lang('lorganization');?></label>
                                <div class="controls">
                                           <select id='organization_id' name='organization_id' class='form-control input-xxlarge' data-rel='chosen' data-placeholder='<?=lang('lplease_select_organization')?>'>
                                    <option value=''></option>
                                    </select>
                                    <img src="<?=base_url()?>assets/img/ajax-loaders/spinner-mini.gif" id="organization_spinner">
                       
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="period"><?php echo lang('lperiode');?></label>
                                <div class="controls">
                                    <div class="col-md-4">
                                    <?php echo form_dropdown('quarter', $quarter_options,0,$quarter_params);?>
                                    </div>
                                    <div class="col-md-3">
                                    <?php echo form_dropdown('year', $year_options,0,$year_params);?>
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id='show_message' style="display: none;"></div> 
                        </div>
                        <div class="col-md-4">
                        &nbsp;  
                        </div>
                        <div class="col-md-8">
                             <button type="button" class="btn btn-primary" id='export-btn'><?=lang('lexport')?></button>
                        </div>
                    </div>
                    <div class="row">
                        &nbsp;
                    </div>
                <?=form_close();?> 
                </div><!-- end box-content -->

        	</div><!-- /end box-inner -->
    	</div>
</div>

<!-- select or dropdown enhancer -->
<script src="<?php echo assets_url(); ?>bower_components/chosen/chosen.jquery.min.js"></script>