<script src="<?php echo assets_url(); ?>/js/jquery.form.js" type="text/javascript"></script>
<link href='<?php echo assets_url(); ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>

<!-- jqgrid -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo assets_url(); ?>js/jqueryui/themes/smoothness/jquery-ui-1.10.2.custom.css" />
<style type="text/css" media="screen">@import url(<?php echo assets_url(); ?>js/jqgrid/css/ui.jqgrid.css?v1);</style>
<script src="<?php echo assets_url(); ?>js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="<?php echo assets_url(); ?>js/jqgrid/plugins/jQuery.jqGrid.dynamicLink.js"></script>

    
<!-- jQuery UI -->
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.core.min.js"></script>
<script src="<?=assets_url()?>js/jqueryui/ui/minified/jquery.ui.datepicker.min.js"></script>

<!--jqconfirm -->
<script src="<?php echo assets_url(); ?>js/jquery.confirm.min.js"></script>
<style type="text/css">
.ui-jqgrid tr.jqgrow td {vertical-align:top !important}
</style>

<script type="text/javascript">
	 jQuery().ready(function (){

        $("#organization_spinner").hide();

        $("#_start_date_from").click(function(){
            $("#start_date_from").val("");
        }); 
        
          $("#_start_date_to").click(function(){
            $("#start_date_to").val("");
        }); 

        $("#_end_date_from").click(function(){
            $("#end_date_from").val("");
        }); 
        
          $("#_end_date_to").click(function(){
            $("#end_date_to").val("");
        }); 

        $('.datepicker').datepicker(
                {
                     changeMonth: true
                    ,changeYear: true
                    ,yearRange: '<?=config_item('year_start')?>:' + new Date().getFullYear()
                    ,beforeShow: function (textbox, instance) {
                        var txtBoxOffset = $(this).offset();
                        var top = txtBoxOffset.top;
                        var left = txtBoxOffset.left;
                        var textBoxHeight = $(this).outerHeight();
                        setTimeout(function () {
                            instance.dpDiv.css({
                               top: top-$("#ui-datepicker-div").outerHeight(),
                               left: left
                            });
                        }, 0);
                    }
                    ,dateFormat: "yy-mm-dd"
                }
        );
    	
        $("select#pilar_id").change(function(){
           var organization_type_id = $("#organization_type_id").val();
           populate_organization($(this).val(),organization_type_id);
        });

        $("select#organization_type_id").change(function(){
           var pilar_id = $("#pilar_id").val();
            populate_organization(pilar_id,$(this).val());
        });

	     populate_organization(0,0);
       //chosen - improves select
    	$('[data-rel="chosen"],[rel="chosen"]').chosen({allow_single_deselect:true});

        
     	$("#submit-btn").click(function(e){
            e.preventDefault(); 
            $("#submit_spinner").show();
			gridReload();
			return false;
		});	

        $('#export-btn').click(function(e){
            //$('form#search-frm').submit();
            //return;
            e.preventDefault(); 
                var param = getParam();

              $.confirm({
                title:"Export Activity",
                text: "<?=lang('lexport_confirm')?>",
                confirmButton: "Ok",
                cancelButton: "Cancel",
                confirm: function(button) {
                   $.post("<?=base_url()?>activity/doexport/?"+param, 
                    function(data) {
                        if(!data.is_error){
                            window.location.href= data.file_url;
                        }else{
                            var rv = '<div class="alert alert-danger">'+data.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal');
                        }
                        
                        /*if(!data.is_error){
                            var rv = '<div class="alert alert-success">'+data.message+'</div>';
                        }else{
                            //error
                            var rv = '<div class="alert alert-danger">'+data.message+'</div>';
                        }
                        $('#show_message').html(rv);
                        $('#show_message').slideDown('normal');*/
                                      
                    },"json"
                );
                },
                cancel: function(button) {
                   // alert("You cancelled.");
                }
            });

            
        });

        
    	jQuery("#list1").jqGrid({
            url:'<?=site_url('activity/loadDataGrid')?>',      //another controller function for generating data
            mtype : "post",             //Ajax request type. It also could be GET
            datatype: "json",            //supported formats XML, JSON or Arrray
            colNames:['No','id',
            	'<?=lang('lorganization_type')?>'
            	,'<?=lang('lpilar')?>'
            	,'<?=lang('lstart_date')?>'
            	,'<?=lang('lend_date')?>'
            	,'<?=lang('lactivity')?>'
            	,'<?=lang('lorganization')?>'
            	,'<?=lang('lactivity_type')?>'
            	,'<?=lang('lprovince')?>'
                ,'<?=lang('lkabupaten_kota')?>'
                ,'<?=lang('ldescription')?>'
                ,'<?=lang('lconversation')?>'
                ,'<?=lang('lmale')?>'
                ,'<?=lang('lfemale')?>'
                ,'<?=lang('ltransgender')?>'
                ,'<?=lang('lmale')?>'
                ,'<?=lang('lfemale')?>'
                ,'<?=lang('ltransgender')?>'
                ,'<?=lang('lmale')?>'
                ,'<?=lang('lfemale')?>'
                ,'<?=lang('ltransgender')?>'
            ],       //Grid column headings
            colModel:[
                {name:'no',index:'no', width:25, align:"right",sortable:false},
                {name:'id',index:'activity_id', hidden: true},
                {name:'organization_type_name',index:'organization_type_name', align:'left',stype:'text',width:50},
                {name:'pilar_name',index:'pilar_name', align:'left',stype:'text',width:250},
                {name:'start_date',index:'start_date', align:'left',stype:'text',width:100},
                {name:'end_date',index:'end_date', align:'left',stype:'text',width:100},
                {name:'activity_name',index:'activity_name', align:'left',stype:'text',width:350,sortable:false},
                {name:'organization_name',index:'organization_name', align:'left',stype:'text',width:150},
                {name:'activity_type_name',index:'activity_type_name', align:'left',stype:'text',width:150},
                {name:'province',index:'province_name', align:'left',stype:'text',width:150},
                {name:'kabupaten_kota',index:'kabupaten_kota_name', align:'left',stype:'text',width:150},
                {name:'result_description',index:'result_description', align:'left',stype:'text',width:400,sortable:false},
                {name:'conversation',index:'output_codes', align:'left',stype:'text',width:250,sortable:false},
                {name:'directm',index:'directm', align:'right',stype:'integer',width:50,sortable:false},
                {name:'directf',index:'directf', align:'right',stype:'integer',width:50,sortable:false},
                {name:'directt',index:'directl', align:'right',stype:'integer',width:50,sortable:false},
                {name:'indirectm',index:'indirectm', align:'right',stype:'integer',width:50,sortable:false},
                {name:'indirectf',index:'indirect', align:'right',stype:'integer',width:50,sortable:false},
                {name:'indirectt',index:'indirectl', align:'right',stype:'integer',width:50,sortable:false},
                {name:'govm',index:'govm', align:'right',stype:'integer',width:50,sortable:false},
                {name:'govf',index:'govf', align:'right',stype:'integer',width:50,sortable:false},
                {name:'govt',index:'govt', align:'right',stype:'integer',width:50,sortable:false}
            ],
            rowNum:<?=$rowNum?>,
            <?php if(isset($rowList) && $rowList){?>
            	rowList:[<?=$rowList?>],
        	<?php }?>
            width: <?=$rowWidth?>,
            height: <?=$rowHeight?>,
           	pager: '#pager1',
            viewrecords: true,
            caption:"<?=$title_page?>",
            sortname: 'start_date',
            toppager: true, 
            shrinkToFit:false,
            hoverrows:false,
			loadComplete: function(data) {
				//$("#total_records").html("("+data.records+")");
		   }
        }).navGrid('#pager1',{search:false,edit:false,add:false,del:false},{cloneToTop:true});
		 
         jQuery("#list1").jqGrid('setGroupHeaders', {
            useColSpanStyle: true,
            groupHeaders:[
                {startColumnName: 'directm', numberOfColumns: 3, titleText: '<?=lang('ldirect_beneficiaries')?>'},
                {startColumnName: 'indirectm', numberOfColumns: 3, titleText: '<?=lang('lindirect_beneficiaries')?>'},
                {startColumnName: 'govm', numberOfColumns: 3, titleText: '<?=lang('lgovernment_official')?>'}
            ]
        });

    });
    
    function getParam(){
        var organization_id = (jQuery("#organization_id option:selected").val())?jQuery("#organization_id option:selected").val():'0';
         var province_id = (jQuery("#province_id option:selected").val())?jQuery("#province_id option:selected").val():'0';
        var organization_type_id = (jQuery("#organization_type_id option:selected").val())?jQuery("#organization_type_id option:selected").val():'0';
        var pilar_id = (jQuery("#pilar_id option:selected").val())?jQuery("#pilar_id option:selected").val():'0';
        var activity_type_id = (jQuery("#activity_type_id option:selected").val())?jQuery("#activity_type_id option:selected").val():'0';
        var start_date_from =(jQuery("#start_date_from").val())?jQuery("#start_date_from").val():0;
        var start_date_to =(jQuery("#start_date_to").val())?jQuery("#start_date_to").val():0;
        var end_date_from =(jQuery("#end_date_from").val())?jQuery("#end_date_from").val():0;
        var end_date_to =(jQuery("#end_date_to").val())?jQuery("#end_date_to").val():0;
        var keyword =(jQuery("#keyword").val())?encodeURIComponent(jQuery("#keyword").val()):0;

        var param = "organization_id="+organization_id+'&province_id='+province_id+'&pilar_id='+pilar_id+'&activity_type_id='+activity_type_id+'&start_date_from='+start_date_from+'&start_date_to='+start_date_to+'&end_date_from='+end_date_from+'&end_date_to='+end_date_to+'&organization_type_id='+organization_type_id+'&keyword='+keyword;
        return param;
    }

    function populate_organization(pilar_id,organization_type_id){
        $("#organization_spinner").show();
        $.getJSON("<?=site_url('organization/get_by_pilar')?>/"+pilar_id+'/'+organization_type_id,
            function(j){
              var options = '<option value=""></option>';
      
              for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].organization_id + '">' + j[i].organization_name+ '</option>';
              }
              $("select#organization_id").html(options).trigger('chosen:updated');
              $("#organization_spinner").hide();
            }
        );
    }

    function gridReload(){
    	   var param=getParam();
			jQuery("#list1").jqGrid('setGridParam',{
				url:"<?=site_url('activity/loadDataGrid')?>/?"+param,
				page:1
			}).trigger("reloadGrid");
	}

    
</script>
<style type="text/css">
.ui-jqgrid tr.jqgrow td
{ 
    vertical-align:top !important;          
    word-wrap: break-word; /* IE 5.5+ and CSS3 */
    white-space: pre-wrap; /* CSS3 */
    white-space: -pre-wrap; /* Opera 4-6 */
    white-space: -o-pre-wrap; /* Opera 7 */
    white-space: normal !important;
    height: auto;
    vertical-align: text-top;
    padding-top: 2px;
    padding-bottom: 3px;
}


</style>

<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url('Dashboard')?>">Home</a>
        </li>
        <li>
            <a href="#"><?=$title_page?></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=$title_page?></h2>
            </div>

              <div class="box-content">
                <?php echo form_open("activity/loadDataGrid",array('role'=>"form",'id'=>'input-form' ))?>


                    <div class="row">
                        <div class="col-md-6">
                                   
                             <div class="form-group">
                                <label class='control-label' for="pilar_id"><?=lang('lpilar');?></label>
                                <div class="controls">

                                        <?php echo form_dropdown('pilar_id', $pilar_options,'0',$pilar_params);?>
                                </div>                         
                            </div>

                             <div class="form-group">
                                <label class='control-label' for="organization_type_id"><?=lang('lorganization_type');?></label>
                                <div class="controls">

                                        <?php echo form_dropdown('organization_type_id', $organization_type_id_options,'0',$organization_type_id_params);?>
                                </div>                         
                            </div>

                             <div class="form-group">
                                <label for="organization"><?php echo lang('lorganization');?></label>
                                <div class="controls">
                                    <select id='organization_id' name='organization_id' class='form-control input-xxlarge' data-rel='chosen' data-placeholder='<?=lang('lplease_select_organization')?>'>
                                    <option value=''></option>
                                    </select>
                                    <img src="<?=base_url()?>assets/img/ajax-loaders/spinner-mini.gif" id="organization_spinner">
                       

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="start_date_from"><?php echo lang('lkeyword');?></label>
                                <div class="controls">
                                     <input  class="input-xlarge" type="text" name='keyword' id='keyword' value="">
                                 </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                           
                           <div class="form-group">
                                <label for="start_date_from"><?php echo lang('lstart_date');?></label>
                                <div class="controls">
                                     <input  class="input-small datepicker" type="text" name='start_date_from' id='start_date_from' value="">
                                  <img id="_start_date_from" border="0" src="<?=assets_url()?>img/b_del.gif">
                                  &nbsp;<?=lang('lto')?>&nbsp;
                                   <input  class=" input-small datepicker" type="text" name='start_date_to' id='start_date_to' value="">
                                  <img id="_start_date_to" border="0" src="<?=assets_url()?>img/b_del.gif">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="end_date_from"><?php echo lang('lend_date');?></label>
                                <div class="controls">
                                     <input  class="input-small datepicker" type="text" name='end_date_from' id='end_date_from' value="">
                                  <img id="_end_date_from" border="0" src="<?=assets_url()?>img/b_del.gif">
                                  &nbsp;<?=lang('lto')?>&nbsp;
                                   <input  class=" input-small datepicker" type="text" name='end_date_to' id='end_date_to' value="">
                                  <img id="_end_date_to" border="0" src="<?=assets_url()?>img/b_del.gif">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="activity_type_id"><?=lang('lactivity_type');?></label>
                                <div class="controls">
                                        <?php echo form_dropdown('activity_type_id', $activity_type_options,'0',$activity_type_params);?>
                               </div>
                            </div>
                            <div class="form-group">
                                <label for="province"><?php echo lang('lprovince');?></label>
                                <div class="controls">
                                        <?php echo form_dropdown('activity_type_id', $province_options,'0',$province_params);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id='show_message' style="display: none;"></div> 
                        </div>
                        <div class="col-md-4">
                        &nbsp;  
                        </div>
                        <div class="col-md-8">
                             <button type="submit" class="btn btn-primary" id='submit-btn'><?=lang('lsubmit')?></button>
                            <button type="button" class="btn btn-primary" id='export-btn'><?=lang('lexport')?></button>
                        </div>
                    </div>

                <?=form_close();?> 
                    <div class="row">
                        <div class="col-lg-12">
                            <br/>
                            <table id="list1"></table> <!--Grid table-->
                            <div id="pager1"></div>  <!--pagination div-->
                        </div>
                    </div>

                </div><!-- end box-content -->

        	</div><!-- /end box-inner -->
    	</div>
</div>

<!-- select or dropdown enhancer -->
<script src="<?php echo assets_url(); ?>bower_components/chosen/chosen.jquery.min.js"></script>