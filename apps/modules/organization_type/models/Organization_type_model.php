<?php
class Organization_type_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
  
  function get_data_by_id($id){
    	$data = $this->search(0,1,'id','asc',array("id"=>$id));
    	return $data;
    }

  function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	if(isset($searchval['id']) && $searchval['id'] ){
    		$this->db->where("id",$searchval['id']);
    	}

		if($this->is_count){
			$this->db->select("count(id) as total");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('organization_types');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}