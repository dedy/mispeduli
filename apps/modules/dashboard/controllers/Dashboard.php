<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
	}
	
	function index(){
    	$this->dashboard();
	}
	

   	
  function dashboard(){
   		$this->rmReport();
		parent::viewpage("vdashboard");
	}
	
	function rmReport(){
		//hapus isi folder temp report
  		$dir = FCPATH."reports";
  		if ($handle = opendir($dir)) {
		    /* This is the correct way to loop over the directory. */
		    while (false !== ($entry = readdir($handle))) {
		    	if($entry != "." && $entry != ".."){
			    	//delete file old than 24 hours
			    	$ftime = filemtime($dir."/".$entry);
			    	$ctime = time();
			    	$dtime = $ctime - $ftime;
			    	$dtime_in_days = round(abs($dtime) / (3600*24),2);

			    	//delete files older than 48 hours(2 days)
			    	if($dtime_in_days > 2){
			    		if($entry){
			    			unlink($dir."/".$entry);
			    		}
			    	}

			    	//echo date ("F d Y H:i:s.", $ftime)." ===> ".$ctime." - ".$ftime." = ".$dtime." (".round(abs($dtime) / 3600,2). " hours)<BR>";
			        //echo "$entry ". date ("F d Y H:i:s.", filemtime($dir."/".$entry))."<BR>";
			    }
		    }

		    closedir($handle);
		}
	}
}

/* End of file dashboard.php */
/* Location: ./apps/modules/dashboard/controllers/dashboard.php */
