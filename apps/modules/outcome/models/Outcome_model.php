<?php
class Outcome_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
  
   function set_outcomes($datas){
   		$this->datas = $datas;
   }
  	
  	function get_outcome_id_by_outcome_code($outcome_code){
  		$total = count($this->datas);
              
        $output_code = "";
        $is_found = false;
        //check in array
        if($total){
            foreach($this->datas as $value){
                if($value['outcome_code'] == $outcome_code){
                    $outcome_id = $value['outcome_id'];
                    $is_found = true;
                    break;
                }
            }
        }else{
            $is_found = false;
        }
        return $outcome_id;
  	}

   	function get_outcome_code_by_output_id($output_id){
        $total = count($this->datas);
              
        $output_code = "";
        $is_found = false;
        //check in array
        if($total){
            foreach($this->datas as $value){
                if($value['id'] == $output_id){
                    $outcome_code = $value['outcome_code'];
                    $is_found = true;
                    break;
                }
            }
        }else{
            $is_found = false;
        }
        return $outcome_code;
    }
 
  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("outcomes.id as outcome_id");
			$this->db->select("outcomes.name as outcome_name");
			$this->db->select("outcomes.code as outcome_code");
			$this->db->select("objectives.code as objective_code");
			$this->db->select("objectives.name as objective_name");

			$this->db->join("objectives","objectives.id = outcomes.objective_id");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('outcomes');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}