 <link href='<?=assets_url()?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
  <link href='<?=assets_url()?>css/dataTables.bootstrap.css' rel='stylesheet'>

   <script>
	$(document).ready(function(){
		//datatable
	    $('.datatable').dataTable({
	        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12 center-block'p>>",
	        "sPaginationType": "bootstrap",
	        "oLanguage": {
	            "sLengthMenu": "_MENU_ records per page"
	        }
	        ,"bFilter": false 
	        ,"bPaginate": false 
	        ,"bLengthChange": false
	        ,"sDom":"it"
	        
	    });
	});
</script>

    <div>
    <ul class="breadcrumb">
        <li>
            <a href="<?=site_url('Dashboard')?>">Home</a>
        </li>
        <li>
            <a href="#"><?=lang('lpilar')?></a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> <?=lang('lpilar')?></h2>
            </div>
            <div class="box-content row">
	            <div class="col-lg-6">
		            <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
					    <thead>
					    	<tr>
					       		<th><?=lang('lpilar')?></th>
						    </tr>
					    </thead>
					    <tbody>
					    
					    <?php
					    foreach($datas as $data) {
						?>
					    <tr>
					        <td><?=$data->name?></td>
					    </tr>
					    <?}?>


					    </tbody>
				    </table>
	            </div>
        	</div>
    	</div>
  	</div>
</div>

<!-- library for making tables responsive -->
<script src="<?=assets_url()?>bower_components/responsive-tables/responsive-tables.js"></script>

<!-- data table plugin -->
<script src='<?=assets_url()?>js/jquery.dataTables.min.js'></script>
<script src='<?=assets_url()?>js/dataTables.bootstrap.js'></script>