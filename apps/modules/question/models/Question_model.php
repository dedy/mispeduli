<?php
class Question_model extends CI_Model {
	var $is_count = false;
	var $datas = array();
    var $answers = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function set_answers(){
        $this->db->from('answer_choices');  
        $query = $this->db->get();
        
        foreach($query->result() as $result){
             array_push($this->answers,array("id"=>$result->id,"description"=>$result->description));
        }
    }

    function get_question_answer_id($var){
        $var = strtolower($var);

        foreach($this->answers as $answer){
            if(strtolower($answer['description']) == $var){
                return $answer['id'];
            }
        }

        return NULL;
    }

    function get_question_id_by_code($col){
        switch ($col) {
            case 'q1':
                # code...
                return 1;
                break;
            case 'q2':
                # code...
                return 2;
                break;
            case 'q3a':
                # code...
                return 3;
                break;
            case 'q3b':
                # code...
                return 4;
                break;
            case 'q3c':
                # code...
                return 5;
                break;
            case 'q4':
                # code...
                return 6;
                break;
            }
    }

   
  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
  	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
            $this->db->select("a.id as question_id");
            $this->db->select("a.code as question_code");
			$this->db->select("a.label as question_label");
			$this->db->select("a.description as question_description");
		}

		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('qa_questions a');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}