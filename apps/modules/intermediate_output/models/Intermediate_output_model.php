<?php
class Intermediate_output_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function data_exists_by_code($code){
		$this->db->where('code',$code);
	    $query = $this->db->get('intermediate_outputs');
	    if ($query->num_rows() > 0){
	    	//get the id 
	    	foreach ($query->result() as $row)
			{
			    return $row->id;
			}
	    }
	    else{
	        return false;
	    }
    }
  	
  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("intermediate_outputs.id as intermediate_output_id");
			$this->db->select("intermediate_outputs.code as intermediate_output_code");
			$this->db->select("intermediate_outputs.name as intermediate_output_name");
			$this->db->select("outcomes.id as outcome_id");
			$this->db->select("outcomes.code as outcome_code");
			$this->db->select("outcomes.name as outcome_name");
			$this->db->select("objectives.id as objective_id");
			$this->db->select("objectives.code as objective_code");
			$this->db->select("objectives.name as objective_name");

			$this->db->join("outcomes","outcomes.id = intermediate_outputs.outcome_id");
			$this->db->join("objectives","objectives.id = outcomes.objective_id");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('intermediate_outputs');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}