<?php include "header.php" ?>

<script>
    $(document).ready(function(){
        $('#identity').focus();
        $('#submit-btn').click(function(e){
            //$('form#login-form').submit();
            //return;
            e.preventDefault(); 
       
            $.post('<?=site_url('auth/dologin');?>', 
                $("#login-form").serialize(),
                function(returData) {
                    $('#show_message').slideUp('normal',function(){
                      //  alert(returData.message);
                        if(returData.error){

                            var rv = ' <div class="alert alert-danger">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal'); 
                        }else{
                            var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal',function(){
                                 setTimeout(function() {
                                    $('#show_message').slideUp('normal',function(){
                                        if(returData.redirect){
                                            window.location.replace(returData.redirect);
                                        }
                                    }); 
                                  }, '<?=config_item('message_delay')?>');
                            }); 
                        }   
                    });
                    
                },'json');
        });
        
    });
</script>   
<div class="ch-container">
         <header class="row">
            <div class="col-md-12 center">
                <img src='<?=base_url()?>/assets/img/logo-header.png' border='0' height='100px'>
            </div>
        </header>

        <div class="row">&nbsp;
        </div><!--/row-->

        <div class="row">
            <div class="well col-md-5 center login-box">
                <div class="alert alert-info">
                    Please login with your Username and Password.
                </div>
                  <?php echo form_open("auth/dologin",array('id'=>'login-form','class'=>'form-horizontal'));?>    <fieldset>
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                            <?php echo form_input($identity);?>
              

                        </div>
                        <div class="clearfix"></div><br>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                            <?php echo form_input($password);?>
                        </div>
                        <div class="clearfix"></div>

                       <div class="clearfix"></div>

                        <p class="center col-md-5">
                                <button id='submit-btn' type="submit" class="btn btn-primary">Login</button>
                        </p>
                        <p class="center col-md-8">
                             <div id='show_message' style="margin:5px 0 0 0"></div>
                        </p>
                            <?php echo form_close();?>

                          <div class="input-prepend">
                            <label class="remember" for="remember"><a href='<?=site_url('forgot_password')?>'>Forgot Your Password ?</a></label>
                        </div>
                       
                    </fieldset>
            </div>
            <!--/span-->
        </div><!--/row-->

          <footer class="row">
            <div class="col-md-12 center">
                <img src='<?=base_url()?>/assets/img/logo-footer.png' border='0' height='100px'>
            </div>

           <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://programpeduli.org" target="_blank">Program Peduli</a> 2017</p>
        </footer>

</div><!--/.fluid-container-->


</body>
</html>
