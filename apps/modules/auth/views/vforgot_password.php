<?php include "header.php" ?>

<script>
    $(document).ready(function(){
        $('#identity').focus();
        $('#submit-btn').click(function(e){
            //$('form#login-form').submit();
            //return;
            e.preventDefault(); 
            
             //disable the button to prevent double submit
             $(this).attr('disabled', 'disabled');
             
             $.post('<?=site_url('auth/doforgot');?>', 
                $("#login-form").serialize(),
                function(returData) {
                    $('#show_message').slideUp('normal',function(){
                      //  alert(returData.message);
                        if(returData.error){
                            var rv = '<div class="alert alert-danger">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal'); 
                             $('#submit-btn').removeAttr('disabled');
                            $('#identity').focus();
                            
                        }else{
                            var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                            $('#show_message').html(rv);
                            $('#show_message').slideDown('normal',function(){
                                 setTimeout(function() {
                                    $('#show_message').slideUp('normal',function(){
                                        if(returData.redirect){
                                            window.location.replace(returData.redirect);
                                        }
                                    }); 
                                  }, '<?=config_item('message_delay')?>');
                            }); 
                        }   
                    });
                    
                },'json');
        });
        
    });
</script>   

<div class="ch-container">
        
    <div class="row">
        <div class="col-md-12 center login-header">
                
            <h2>Forgot Password Page.</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
                <?=lang('lplease_enter_your_username')?>
            </div>
            <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

            <?php echo form_open("auth/doforgot",array('id'=>'login-form','class'=>'form-horizontal'));?>

                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        
                                       <?php echo form_input($identity);?>


                    </div>
                    <div class="clearfix"></div><br>

                   <div class="clearfix"></div>

                    <p class="center col-md-5">
                     <p><?php echo form_submit(array("id"=>"submit-btn","name"=>"submit" , "class"=>"btn btn-primary","value"=>lang('forgot_password_submit_btn')));?></p>
                     </p>
                     <div id='show_message' style="margin:5px 0 0 0"></div>

                      <div class="input-prepend">
                        <label class="remember" for="remember"><a href='<?=site_url('/')?>'>Login Page</a></label>
                    </div>
                   
                </fieldset>
            
           <?php echo form_close();?>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->


</body>
</html>
