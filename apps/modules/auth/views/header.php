<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>MIS Peduli</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Dedy Adhiewirawan">

    <!-- The styles -->
    <link id="bs-css" href="<?=assets_url()?>css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?=assets_url()?>css/charisma-app.css" rel="stylesheet">
    <link href="<?=assets_url()?>css/custom-app.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?=assets_url()?>bower_components/jquery/jquery.min.js"></script>

    <!-- library for cookie management -->
    <script src="<?=assets_url()?>js/jquery.cookie.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?=assets_url()?>img/favicon.ico">


<script>
    $(document).ready(function(){

        $(document).ajaxError(function() {
            alert('There is something wrong with the system. \r\n If the problem persist, please contact administrator');
            //window.location.reload(true);
        });

         $(document).ajaxStart(function() {
             $("#ajaxStatusDisplay_userMessage").show();

        });

        $( document ).ajaxStop(function() {
             $("#ajaxStatusDisplay_userMessage").hide();
        });


    });
    </script>   
<style type="text/css">
.ajaxStatusDisplay_userStyle {
    z-index: : 999;
    display: none;
    position:fixed;
    left:45%;top:0px;height:20;
    background-color:#F2F760;
    color:black;
    padding-left: 5px;
    padding-right: 5px;
    width:75px;
    font-weight: normal;
    font-family:Arial, Helvetica, sans-serif;
  
}

</style>
</head>
<body>
<div id="ajaxStatusDisplay_userMessage" class='ajaxStatusDisplay_userStyle'>
Loading...
</div>