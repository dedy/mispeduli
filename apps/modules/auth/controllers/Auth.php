<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	function __construct()
	{
		parent::__construct();

        $this->load->library(array('ion_auth','form_validation'));
        $this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->load->helper('security');
        
	}
    
    function index()
	{
            $this->login();
	}
     

    // login page
    function login()
    {
        //if logged in, redirect to dashboard
        if(isLoggedIn()){
            redirect('dashboard','refresh');
        }
        
        // the user is not logging in so display the login page
        // set the flash data error message if there is one
        $this->viewparams['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->viewparams['identity'] = array(
            'name' => 'identity',
            'id'    => 'identity',
            'type'  => 'text',
            'placeholder'   => lang('login_username_label'),
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('identity'),
        );
                   
        $this->viewparams['password'] = array(
            'placeholder'   => lang('lpassword'),
            'class'   => "form-control",
            'name' => 'password',
            'id'   => 'password',
            'type' => 'password',
        );

        $this->viewparams['enable_recaptcha'] =  config_item('enable_recaptcha') ; 
        
        //view login page
        $this->load->view("vlogin", $this->viewparams);
     
    }

    function checkLogin(){
        echo json_encode(array("isLoggedIn" => $this->ion_auth->logged_in()));
        
    }

    function dologin(){
        $this->load->model('user/user_model');

        //validate form input
        $this->form_validation->set_rules('identity', 'lang:login_username_label','required');
        $this->form_validation->set_rules('password', 'lang:lpassword', 'required');
        $is_error = false;
        $messages = $redirect = "";
                
        if(!$is_error) {
            $identity = $this->input->post('identity');
            if ($this->ion_auth->is_max_login_attempts_exceeded($identity))
            {
                $messages = lang('ltoo_many_login_attempts');
                $is_error = true;

                //set user to locked
                $this->user_model->set_locked($identity);

            }else {

                //if ($this->form_validation->run() == true)
                /*
                form validation call_back in hmvc
                http://stackoverflow.com/questions/32665715/codeigniter-unable-to-access-an-error-message-corresponding-to-your-field-name

                */
                  if ($this->form_validation->run($this) == true)
                  {
                    // check to see if the user is logging in
                    // check for "remember me"
                    $remember = (bool) $this->input->post('remember');

                    if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
                    {
                        //if the login is successful
                        //redirect them back to the home page
                        $messages = $this->ion_auth->messages();                 //lang("llogin_successful")

                        $this->ion_auth->clear_login_attempts($identity);

                        //add some session
                        $user = $this->ion_auth->user()->row();

                        //insert into login history
                        $values['ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $values['user_agent'] =  $_SERVER['HTTP_USER_AGENT'];
                        $values['login'] = $identity;
                        $values['time'] = time();
                        $this->db->insert("login_histories", $values); 
                        $redirect = site_url('dashboard');

                    }else
                    {
                        // if the login was un-successful
                        // redirect them back to the login page
                        $messages = $this->ion_auth->errors();
                        $is_error = true;
                    }

                }
                else
                {
                    // the user is not logging in so display the login page
                    // set the flash data error message if there is one
                    $messages  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                    $is_error = true;
                }
            }
        }
        
        echo json_encode(
            array(  "error"     => $is_error,
                    "message"   => $messages,
                    "redirect"  => $redirect,
                )
            );
    }
    
    // log the user out
    function logout()
    {
        // log the user out
        $logout = $this->ion_auth->logout();

        redirect('/', 'refresh');
    }

	// forgot password
	function forgot_password()
	{
        
        $this->viewparams['type'] = $this->config->item('identity','ion_auth');
        // setup the input
        $this->viewparams['identity'] = array('name' => 'identity',
            'id' => 'identity',
            'placeholder' => "Username"
            ,"class"    => "form-control"
        );
                    
        if ( $this->config->item('identity', 'ion_auth') != 'email' ){
            $this->viewparams['identity_label'] = $this->lang->line('forgot_password_username_label');
        }
        else
        {
            $this->viewparams['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
        }
        
        $this->viewparams['flash_message']  = $this->session->flashdata('message');	

        //view forgot passpage
		$this->load->view("auth/vforgot_password", $this->viewparams);
	}
    
    function doforgot(){
        $redirect = $messages = "";
        $is_error = false;
        
        // setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_username_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}
        
        //validate the input
        if ($this->form_validation->run() == true){
            //search username
            $identity_column = $this->config->item('identity','ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            //if not found , then error
            if(empty($identity)) {

                if($this->config->item('identity', 'ion_auth') != 'email')
                {
                    $this->ion_auth->set_error('forgot_password_username_label');
                }
                else
                {
                   $this->ion_auth->set_error('forgot_password_email_not_found');
                }

                $redirect = "";//we should display a confirmation page here instead of the login page
                //$messages = lang('lforgot_password_message');
                $messages = lang('lusername_not_found');
                //$messages = $this->ion_auth->errors();
                $is_error = true;
            }else{
                
                // run the forgotten password method to email an activation code to the user
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

                if ($forgotten)
                {
                  
                    $this->load->model('email/email_model');

                    //if username is found, then send the email 
                    $username = $forgotten['identity'];
                    $passcode = $forgotten['forgotten_password_code'];
                    
                    $this->load->model('email/email_model');
                    $id_email = 1;
                    $to_email = $identity->email;
      

                    $original = array("{full_name}","{reset_url}");
                    $to_replace = array($identity->full_name,"<a href='".site_url('auth/reset_password')."/".$passcode."'>This link</a>");
                    
                    $is_send = $this->email_model->send_mail($id_email,$to_email,array(),array(),$original,$to_replace);
                                                
                    // if there were no errors
                    // $messages = $this->ion_auth->messages();
                     if(!$is_send){
                            $messages = lang('lforgot_pass_email_failed');
                            $is_error = true;
                            $redirect = "";//we should display a confirmation page here instead of 
                     }else{
                            $messages = lang('lforgot_password_message').".&nbsp(".$this->email_model->obfuscate_email($to_email).")";
                            $redirect = site_url('/');//we should display a confirmation page here instead of the 
                    }
                
                }
                else
                {
                    $redirect = site_url('/');//we should display a confirmation page here instead of the login page
                    $messages = lang('lforgot_password_message');
                     
                    //$messages = $this->ion_auth->errors();
                    //$is_error = true;
                
                }
            }
        }else{
            //form validation error
            $messages  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
        }
        
         echo json_encode(
            array(	"error"		=> $is_error,
                    "message"	=> $messages,
                    "redirect"	=> $redirect
                )
            );
    }//end function doforgot_password
    
    function doreset_password(){
        $code = $this->input->post('code');
        $user = $this->ion_auth->forgotten_password_check($code);

        // if the code is valid then display the password reset form
        $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

        $messages = $redirect = "";
        $is_error = false;
        if ($this->form_validation->run() == false){
             //form validation error
            $messages  = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $is_error = true;
        }else  {

            // do we have a valid request?
           /* if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
            {

                // something fishy might be up
                $this->ion_auth->clear_forgotten_password_code($code);
                $messages  = $this->lang->line('error_csrf');
                $is_error = true;
            }
            else
            {*/
                // finally change the password
                $identity = $user->{$this->config->item('identity', 'ion_auth')};

                $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                if ($change)
                {
                    // if the password was successfully changed
                    $messages  = $this->ion_auth->messages();
                    $redirect = site_url('/');
                }
                else
                {
                     $messages  = $this->ion_auth->errors();
                     $is_error = true;
                     $redirect = site_url('auth/reset_password/')."/". $code;
                }
          //  }
        }
        
          echo json_encode(
            array(	"error"		=> $is_error,
                    "message"	=> $messages,
                    "redirect"	=> $redirect
                )
            );
    }
    
    
	// reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
				// display the form

				// set the flash data error message if there is one
				$this->viewparams['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->viewparams['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                //'placeholder'=> sprintf(lang('reset_password_new_password_label'), $this->viewparams['min_password_length']),
				$this->viewparams['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
                    'placeholder'=> lang('reset_password_validation_new_password_label'),
                    'class' => "form-control",

					'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
				);
				$this->viewparams['new_password_confirm'] = array(
					'name'         => 'new_confirm',
					'id'           => 'new_confirm',
                    'class'        => "form-control",
					'type'         => 'password',
                    'placeholder'   => lang('reset_password_new_password_confirm_label'), 
					'pattern' => '^.{'.$this->viewparams['min_password_length'].'}.*$',
				);
				$this->viewparams['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				//$this->viewparams['csrf'] = $this->_get_csrf_nonce();
				$this->viewparams['code'] = $code;

                $this->viewparams['flash_message']  = $this->session->flashdata('message');	

				// render
                $this->load->view("auth/reset_password", $this->viewparams);
		}
		else
		{
            // if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
            //$this->session->set_flashdata('message', lang('linvalid_forgot_password_code'));
			redirect("forgot_password", 'refresh');
		}
	}


}
