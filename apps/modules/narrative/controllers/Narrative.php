<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Narrative extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
   	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
    $this->load->model('narrative_model');
		$this->load->model('organization/organization_model');
	}
	
	function index(){
    	$this->lists();
	}
	   	
  function lists(){

  	 	$this->load->helper('form');
  	 	$this->load->model('pilar/pilar_model');
  	 	$this->load->model('organization_type/organization_type_model');

  	 	/* get pilar list */
  	 	$pilars = $this->pilar_model->search(0,1,'name','asc');
  	 	$pilar_options = array();
  	 	$pilar_options[0] = '';
  	 	foreach($pilars as $pilar){
  	 		$pilar_options[$pilar->id] = $pilar->name;
  	 	}
       	$this->viewparams['pilar_options'] = $pilar_options;
        $this->viewparams['pilar_params'] = array(
            'id'   => 'pilar_id',
            'class'	=> "form-control input-xxlarge",
            'data-rel'=> "chosen",
            'data-placeholder'=> lang('lplease_select_pilar')
        );
        /* end pilar */

        //org type
        $organization_types = $this->organization_type_model->search(0,1,'id','asc');
        $organization_types_options = array();
        $organization_types_options[0] = '';
        foreach($organization_types as $organization_type){
          $organization_types_options[$organization_type->id] = $organization_type->name;
        }

        
        
        $this->viewparams['organization_type_id_options'] = $organization_types_options;
        $this->viewparams['organization_type_id_params'] = array(
            'id'   => 'organization_type_id',
            'class' => "form-control input-medium",
            'data-rel'=> "chosen"
          );

        $organization = array(
	        'type'  => 'text',
	        'name'  => 'organization',
	        'id'    => 'organization',
	        'value' => '',
	 		    'placeholder'   => lang('lplease_input_organization_name'),
	        'class' => 'form-control input-xxlarge '
		  );
       	$this->viewparams['organization'] = $organization;

        $narrative_title = array(
          'type'  => 'text',
          'name'  => 'narrative_title',
          'id'    => 'narrative_title',
          'value' => '',
          'placeholder'   => lang('lplease_input_title'),
          'class' => 'form-control input-xxlarge '
      );
        $this->viewparams['narrative_title'] = $narrative_title;

            /* year list */
        $year_lists = config_item("year_filter");
        $year_options = array();
        $year_options[0] = '';
        foreach($year_lists as $year_list) {
          $year_options[$year_list] = $year_list;
        }
        $this->viewparams['year_options'] = $year_options;
        $this->viewparams['year_params'] = array(
          'id'   => 'year',
          'class' => "form-control input-medium",
          'data-rel'=> "chosen",
          'data-placeholder'=> lang('lplease_select_year')
        );
        /* end year  */
   	  $this->viewparams['title_page']	= lang('lnarrative');
		  parent::viewpage("vlist");
	}
	 

  function doexport(){

      //fields
      $columns = array(
        array("title"   => lang('lorganization_type') , "field"  => "organization_type_name")
        ,array("title"  => lang('lpilar')             , "field"   => "pilar_name")
        ,array("title"  => lang('lorganization')      , "field" => "organization_name")
        ,array("title"  => lang("ldate")        , "field"   => "date_fmt")
        ,array("title"  => lang("lnarrative_title")        , "field"   => "narrative_title")
        ,array("title"  => lang("lpencapaian_kegiatan")        , "field"   => "activity_goal")
        ,array("title"  => lang("ltantangan")        , "field"   => "challenge")
        ,array("title"  => lang("lpembelajaran")        , "field"   => "learning")
        ,array("title"  => lang("lconversation")        , "field"   => "conversation")
      );

        $file_url = "";
           $searchv = array(
           'pilar_id'           => $this->input->get('pilar_id')
           ,'organization_type_id'   => $this->input->get('organization_type_id')
           ,'organization_id'   => $this->input->get('organization_id')
          ,'narrative_title'   => trim($this->input->get('title'))
           ,'date_from'     => $this->input->get('start_date_from')
          ,'year' => $this->input->get('year')
            ,'date_to'     => $this->input->get('start_date_to')
         ,'keyword'     => urldecode($this->input->get('keyword'))
        );
      

        $query = $this->narrative_model->search(0,1,'date','asc',$searchv);
        $count = $this->narrative_model->countSearch($searchv);


        $is_error = true;
        
        //write to excel
        if($count){
            $objPHPExcel  = new PHPExcel();
            $objPHPExcel->getProperties()
             ->setCreator('Farsight')
             ->setTitle('Narrative Report');
          
          //$objPHPExcel->setActiveSheetIndex(0);

            $worksheet = $objPHPExcel->getSheet(0);
            $worksheet->setTitle('Data');


            //$worksheet->setCellValue('A1', 'This is just some text value');
            //set header
            $col = 0;
            foreach($columns as $column){
                //set the header title , and set it to bold
                $worksheet->setCellValueByColumnAndRow($col, 1, $column['title'])->getStyle("A1:I1")->getFont()->setBold(true);
                $col++;
            }

            //set the width
            for($cols=0;$cols<5;$cols++){
              $worksheet->getColumnDimensionByColumn($cols)->setAutoSize(true);
            }
              $worksheet->getColumnDimension('F')->setWidth(100);
            $worksheet->getColumnDimension('G')->setWidth(100);
            $worksheet->getColumnDimension('H')->setWidth(100);
            $worksheet->getColumnDimension('I')->setWidth(100);


            //set vertical align top
            $worksheet->getStyle("A1:I1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

            for($i=0; $i<$count; $i++){
              $col = 0;
              $row = $i+2; //start row 2
              foreach($columns as $column){
                $fieldname = $column['field'];
                $worksheet->setCellValueByColumnAndRow($col, $row, $query[$i]->$fieldname);
                $worksheet->getStyle("A".$row.":I".$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $worksheet->getStyleByColumnAndRow($col,$row)->getAlignment()->setWrapText(true);

                $col++;
              }
            }
            
          
            //$objPHPExcel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
            //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
            //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $filename = "narrative_report.".date("Ymd.His").".xlsx";
            $objWriter->save(FCPATH."reports/".$filename);
            $file_url = base_url()."reports/".$filename;
            $message = "Done";

            $is_error = false;
        }else{
            $message = "Data tidak ditemukan";
        }

        $result = array(
          "is_error"  =>  $is_error
          ,"message"  => $message
          ,"file_url" => $file_url
        );

        echo json_encode($result);

  }

	function loadDataGrid(){
		
		    $page = isset($_POST['page'])?$_POST['page']:1;
        $sidx = isset($_POST['sidx'])?$_POST['sidx']:'date'; // get index row - i.e. user click to sort
        $sord = isset($_POST['sord'])?$_POST['sord']:'asc'; // get the direction
   		  $limit = isset($_POST['rows'])?$_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
       	
        $searchv = array(
        	 'pilar_id' 					=> $this->input->get('pilar_id')
           ,'organization_type_id'   => $this->input->get('organization_type_id')
           ,'organization_id'   => $this->input->get('organization_id')
        	 ,'narrative_title'		=> trim($this->input->get('title'))
           ,'date_from'     => $this->input->get('start_date_from')
            ,'date_to'     => $this->input->get('start_date_to')
          ,'year' => $this->input->get('year')
  		   ,'keyword'     => urldecode($this->input->get('keyword'))
        );
  		

      	$query = $this->narrative_model->search($limit,$page,$sidx,$sord,$searchv);
      	$this->firephp->log($this->db->last_query());
		    $count = $this->narrative_model->countSearch($searchv);
		    $this->firephp->log($this->db->last_query());
		    $this->firephp->log("count : ".$count);

        $columns = array(
          "narrative_id"
          ,"organization_type_name"
          ,"pilar_name"
          ,"organization_name"
          ,"date_fmt"
          ,"narrative_title"
          ,"activity_goal"
          ,"challenge"
          ,"learning"
          ,"conversation"
          );

         if(count($query)){
            for($i=0;$i<count($query);$i++){
                $query[$i]->activity_goal = nl2br($query[$i]->activity_goal);
           }
        }

		    $this->DataJqGrid = array(
        	 "page"		=> $page,
        	 "sidx"		=> $sidx,
        	 "sord"		=> $sord,
        	 "query" 	=> $query,
        	 "limit"		=> $limit,
			     "count"		=> $count,
			     "column"	=> $columns,
          "id"		=> "narrative_id"
 		     );
            
        parent::loadDataJqGrid();
		
	}
}

/* End of file dashboard.php */
/* Location: ./apps/modules/dashboard/controllers/dashboard.php */
