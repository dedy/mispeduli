<?php
class Narrative_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
   	function insert($data){
		$this->db->insert("narrative", $data); 
		return $this->db->insert_id();
    }
    
 
    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
    	if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
    		$this->db->where("narrative.pilar_id",$searchval['pilar_id']);
    	}

      	if(isset($searchval['organization_name']) && $searchval['organization_name']){
    		$this->db->like("organizations.name",$searchval['organization_name']);
    	}

    	
        if(isset($searchval['organization_id']) && $searchval['organization_id']){
    		$this->db->where("organization_id",$searchval['organization_id']);
    	}

    	if(isset($searchval['narrative_title']) && $searchval['narrative_title']){
    		$this->db->like("narrative.narrative_title",$searchval['narrative_title']);
    	}
		
		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
    		$this->db->where("organizations.organization_type_id",$searchval['organization_type_id']);
    	}

    	if(isset($searchval['date_from']) && $searchval['date_from']){
    		$this->db->where("date >=",$searchval['date_from']);
    	}

        if(isset($searchval['year']) && $searchval['year']){
            $this->db->where("EXTRACT(YEAR FROM date) = '".$searchval['year']."' ");
        }

    	if(isset($searchval['date_to']) && $searchval['date_to']){
    		$this->db->where("date <=",$searchval['date_to']);
    	}

    	if(isset($searchval['keyword']) && $searchval['keyword']){
    		$qq = "(narrative_title like '%".$searchval['keyword']."%'";
    		$qq .= " OR activity_goal like '%".$searchval['keyword']."%'";
    		$qq .= " OR challenge like '%".$searchval['keyword']."%'";
    		$qq .= " OR conversation like '%".$searchval['keyword']."%'";
    		$qq .= " OR learning like '%".$searchval['keyword']."%') ";
    		$this->db->where($qq);
    	}

		if($this->is_count){
			$this->db->select("count(narrative.id) as total");
		}{
			$this->db->select("narrative.*");
			$this->db->select("narrative.id as narrative_id");
			$this->db->select("organization_types.name as organization_type_name");

			$this->db->select("pilar.name as pilar_name");
			$this->db->select("organizations.name as organization_name");
		    $this->db->select("DATE_FORMAT(date ,'%d/%m/%Y') as date_fmt",false);

			$this->db->join("pilar","pilar.id = narrative.pilar_id");
		}
		
		$this->db->join("organizations","organizations.id = narrative.organization_id");
		$this->db->join("organization_types","organization_types.id = organizations.organization_type_id");
    
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('narrative');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}