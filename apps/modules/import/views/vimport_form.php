<script src="<?php echo base_url(); ?>assets/js/jquery/form/jquery.form.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery/jquery.confirm.min.js"></script>

<script>
	$(document).ready(function(){
        <?
          //disable button import , if no files found
          if(!$total_import_file) { ?>
                $("#import-btn").attr("disabled", "disabled");
          <? } else { ?>      
                $("#import-btn").click(function(e){
                    e.preventDefault(); 
                    var obj = $(this);
                    
                    
                    $.confirm({
                        title:"<?=lang('limport')?>",
                        text: "<?=lang('limport_confirmation')?>",
                        confirmButton: "<?=lang('lok')?>",
                        cancelButton: "<?=lang('lcancel')?>",
                        confirm: function(button) {
                           obj.attr('disabled','disabled');
                            $('#loadingmessage').show();

                            $('form#input-form').ajaxSubmit({
                                URL:'<?=site_url('admin/import/doimport');?>',
                                dataType: 'json', 
                                success: function(returData){
                                    //alert('hi');
                                    $('#loadingmessage').hide();
                                    obj.removeAttr('disabled');

                                    $('#show_message').slideUp('normal',function(){

                                        if(returData.error){
                                            var rv = '<div class="alert alert-error">'+returData.message+'</div>';
                                            $('#show_message').html(rv);
                                            $('#show_message').slideDown('normal');	

                                        }else{
                                            var rv = '<div class="alert alert-success">'+returData.message+'</div>';
                                            $('#show_message').html(rv);
                                            $('#show_message').slideDown('normal');
                                            
                                            };	
                                        });	
                                  }
                              });

                        },
                        cancel: function(button) {
                           // alert("You cancelled.");
                        }
                    });
                });    
        
        <?}?>
        
            
	});
	
	</script>	

<div>
	<ul class="breadcrumb">
		<li>
			<a href="<?=site_url("admin")?>"><?=lang('lhome')?></a> <span class="divider">/</span>
		</li>
        <li><a href="#"><?=$title_page?></a></li>
	</ul>
</div>



<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-edit"></i> <?=$title_page?>&nbsp;<span id='total_records'></span></h2>
			<div class="box-icon">
				<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
			</div>
		</div>

		<div class="box-content">
              <div class="row-fluid">
                  <div class="span6">
                   <?
                    if($total_import_file) {?>
                        <?=lang('lfiles_found')?> : 
                        <? foreach ($import_files as $file) { ?>
                        <br>&nbsp;&nbsp; - &nbsp; <?=$file['filename']?> (<?=$file['filesize']?>)
                        <?}?>
                        
                    <?}?>
                  </div>
              </div>
             <?php 
              echo form_open("admin/import/doimport",array('id'=>'input-form','class'=>'form-horizontal'));
              ?>
					<div id='show_message' style="display: none;"></div> 
                    
                   
					<div class="form-actions">
					   <div id='loadingmessage' style="display: none;float:left;padding-right:5px">
						  <img src='<?=base_url()?>/assets/admin/img/spinner-mini.gif'/>
					  </div>
					  
					  	<button type="submit" class="btn btn-primary" id='import-btn' ><?=lang('limport')?></button>
					</div><!--/form-actions-->
					
				</form>   <!--/form-->
		</div><!--/box-content-->
	</div><!--/span-->

</div><!--/row-->

