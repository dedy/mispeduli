<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Msc extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    var $organizations = array();
    var $provinces = array();
    var $kabupatens = array();
    var $outcomes = array();

	public function __construct()
	{
   
		parent::__construct();

        //If it's a cli request , no need to check session 
        if(!$this->input->is_cli_request()) {
            if(!isLoggedIn()){
                redirect('auth/logout','refresh');
            }
        }
        $this->load->model('msc/msc_model');
        $this->load->model('province/province_model');
        $this->load->model('kabupaten_kota/kabupaten_kota_model');
        $this->load->model('outcome/outcome_model');
        $this->load->model('output/output_model');
        $this->load->helper('date');
        $this->load->helper('file');
        $this->load->model('organization/organization_model');
		
    }
    
    function test(){
        echo "halo".PHP_EOL;
    }

	
    function index(){
        $this->run();
    }

	function run(){

        $file = FCPATH."files/msc.xlsx";
        $log_file  = './apps/logs/'.date("Ymd.His")."_msc_import_log.txt";
         

        $this->province_model->set_provinces();
        $this->kabupaten_kota_model->set_kabupatens();
        $this->output_model->set_outputs();
        $this->outcome_model->set_outcomes($this->output_model->datas);
        $this->organization_model->set_organizations();

        $cols = array(
            "org_type"
            ,"pilar"
            ,"organization_name"
            ,"msc_title"
            ,'kabupaten'
            ,'province_name'
            ,'domain_id'
            ,'msc_domain1'
            ,'msc_domain2'
            ,'msc_domain3'
            ,'msc_domain4'
            ,'msc_domain5'
            ,'related_goals'
            ,'oc.lbl2' 
            ,'oc.lbl3'
            ,'oc.lbl4'
            ,'oc.lbl5'
            ,'oc.lbl6'
            ,'oc.lbl7'
            ,'oc.lbl8'
            ,'oc.lbl9'
            ,'oc.lbl10'
            ,'oc.lbl11' 
            ,'date'
            ,'status'
            ,'conversation'
        );

        $Reader = new SpreadsheetReader($file);
        $sheets = $Reader->Sheets();
        $Reader -> ChangeSheet(0);

        $iRow = 0;
        $tmp_organisasi = $tmp_province = $tmp_kabupaten = "";
        foreach ($Reader as $Row){
            $row_domain =  $data = $row_outcome = array();
            $log_data = "";
            //read start line 2
            if($iRow < 1){
                echo "continue : ".$iRow.PHP_EOL;
                $iRow++;
                continue;
            }
            
            echo "row ".$iRow.";";

            //looping kolom
            $iCol = 0;
            $pilar_id = 0;
            $organization_name = "";
            $data = array();
            foreach($cols as $col){
                $Row[$iCol] = isset($Row[$iCol])?trim($Row[$iCol]):"";

                switch($col){
                   case "province_name" : 
                        if($Row[$iCol] == "Nangroe Aceh Darussalam")
                            $Row[$iCol] = "Nanggroe Aceh Darussalam";
                        
                        $provinces_name = explode("dan", $Row[$iCol]);
                        $province_id = NULL;
                        $province_id2 = NULL;


                        if(isset($provinces_name[0]) && isset($provinces_name[1]) ){
                            $provinces_name[1] = trim($provinces_name[1]);
                            $provinces_name[0] = trim($provinces_name[0]);

                            if($provinces_name[0]){
                                $province_id = $this->province_model->get_province_id($provinces_name[0]);
                            }

                            if($provinces_name[1]){
                                $province_id2 = $this->province_model->get_province_id($provinces_name[1]);
                            }

                        }else
                        if(isset($provinces_name[0])){
                            $provinces_name[0] = trim($provinces_name[0]);

                            if($provinces_name[0] <> $tmp_province){
                                $tmp_province = $provinces_name[0];
                                if($provinces_name[0] <> "N.A" && $provinces_name[0]) {
                                    $province_id = $this->province_model->get_province_id($provinces_name[0]);
                                }else{
                                    $province_id = NULL;
                                }
                            }
                        }

                        $organization_province = $provinces_name[0];
                        $data['province_id'] = $province_id;
                        $data['province_id2'] = $province_id2;
                       
                    break;
                    case "org_type" :
                        $organization_type_id  = $this->organization_model->get_org_type_id($Row[$iCol]);
                    break;

                    /*case "msc_domain1":
                        if($Row[$iCol]){
                            array_push($row_domain,1);
                            echo "domain1;";

                        }
                    break;  
                    case "msc_domain2":
                        if($Row[$iCol]){
                            echo "domain2;";

                            array_push($row_domain,2);
                        }
                    break;  
                    case "msc_domain3":
                        if($Row[$iCol]){
                            echo "domain3;";

                            array_push($row_domain,3);
                        }
                    break;  
                    case "msc_domain4":
                        if($Row[$iCol]){
                            echo "domain4;";

                            array_push($row_domain,4);
                        }
                    break;  
                    case "msc_domain5":
                        if($Row[$iCol]){
                            echo "domain5;";
                            array_push($row_domain,5);
                        }
                    break;  
                    */
                    case "oc.lbl2"   : 
                    case "oc.lbl3"   :      
                    case "oc.lbl4"   :
                    case "oc.lbl5"   :
                    case "oc.lbl6"   :
                    case "oc.lbl7"   :
                    case "oc.lbl8"   :
                    case "oc.lbl9"   :
                    case "oc.lbl10"  :
                    case "oc.lbl11"  :
                         //get outcome
                        if($Row[$iCol]){
                            $outcome_code = $col;
                            $outcome_id = $this->outcome_model->get_outcome_id_by_outcome_code($outcome_code);
                            echo $outcome_code.";";
                            array_push($row_outcome,$outcome_id);
                        }
                    break;
                    case "pilar" :
                        $data['pilar_id'] = $this->get_pilar_id($Row[$iCol]);
                     break;
                    case "organization_name":
                        $organization_name = $Row[$iCol];
                    break;
                    case "date" :
                    case "msc_title" :
                    case "conversation" :
                    case "related_goals" :
                    case "domain_id" :
                        $data[$col] = (strtoupper($Row[$iCol]) == "NULL")?NULL:$Row[$iCol];
                    break;
                } //end switch 

                $iCol++;
            }//end foreach
            
           
             //check in array first
            if($organization_name <> $tmp_organisasi){
                $organization_id = $this->organization_model->get_organization_id($organization_name,$organization_type_id);
                if(!$organization_id){
                    //log to file
                    $log_data = $organization_name." not exists \r\n";
                }
            }else{
                //echo "organization \"". $organization_name."\" same as prev (".$organization_id."); ";

            }
            
            //u/ skr, kl ga ada organizati , jgn di insert dulu
            if($organization_id){
                $data['organization_id'] = $organization_id;
                echo " insert data msc; ";
                $data['created_by'] = 1;
                $data['created_on'] = date("Y-m-d H:i:s");
                $data['updated_by'] = 1;
                $data['updated_on'] = date("Y-m-d H:i:s");

                $msc_id = $this->msc_model->insert($data);
                
                /* 
                2017 02 24 , 1 msc - 1 domain 

                if(count($row_domain)){
                    unset($data);
                    foreach($row_domain as $d){
                        $data['msc_id'] = $msc_id;
                        $data['domain_id'] = $d;
                        $this->msc_model->insert_domain($data);
                    }
                }
                */
                if(count($row_outcome)){
                    unset($data);
                    foreach($row_outcome as $o){
                        $data['msc_id'] = $msc_id;
                        $data['outcome_id'] = $o;
                        $this->msc_model->insert_outcome($data);
                    }
                }

               
            }else{
                echo "organization \"". $organization_name."\" not exists; ";
            }

            $tmp_organisasi = $organization_name;
            $tmp_province = $organization_province;
            //$tmp_kabupaten = $kabupaten;
            if($log_data )
                write_file($log_file, $log_data, 'a');

            $iRow++;
            echo PHP_EOL.PHP_EOL;

           // if($iRow > 2500){
           //     break;
           // }
        }


	}
	
    function get_pilar_id($pilar_name){
        $pilar_name = trim($pilar_name);
         $pilars = array(
        array("value"   => "MASYARAKAT ADAT DAN LOKAL TERPENCIL DAN TERGANTUNG PADA SUMBER DAYA ALAM","id" => 1)
        ,array("value"  => "KELOMPOK WARIA","id"  => 2)
        ,array("value"  => "PEDULI DESA","id"=>7)
        ,array("value"  => "DISABILITAS","id"=>3)
        ,array("value"  => "KORBAN DISKRIMINASI, INTOLERANSI, DAN KEKERASAN BERBASIS AGAMA","id"=>4)
        ,array("value"  => "TRANSGENDER GROUP","id"=>2)
        ,array("value"  => "KORBAN PELANGGARAN HAM BERAT","id"=>5)
        ,array("value"  => "ANAK DAN REMAJA RENTAN","id"=>6)

        );

        foreach($pilars as $pilar){
            if(strtoupper($pilar_name) == $pilar['value']){
                return $pilar['id'];
            }
        }
    }    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
