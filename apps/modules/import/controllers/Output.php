<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';

//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Ouput extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    

	public function __construct()
	{
   
		parent::__construct();

        //If it's a cli request , no need to check session 
        if(!$this->input->is_cli_request()) {
            if(!isLoggedIn()){
                redirect('auth/logout','refresh');
            }
        }
		
    }
    
    function test(){
        echo "halo".PHP_EOL;
    }

	
    function index(){
        $this->run();
    }

    function run(){
        $this->load->model('intermediate_output/intermediate_output_model','ioutput_model');
        $this->load->model('output/output_model');
        $file = FCPATH."files/outputs.xlsx";

        $cols = array(
            ""
         );

        $Reader = new SpreadsheetReader($file);
        $sheets = $Reader->Sheets();
        $Reader -> ChangeSheet(0);

        $iRow = 0;

        //looping kolom
        $intermediate_code = "";
        foreach ($Reader as $Row){
        
            //read start line 2
            if($iRow < 1){
                echo "continue : ".$iRow.PHP_EOL;
                $iRow++;
                continue;
            }
         //col 4,5 = intermediate output code/name
            //col 6,7 = output code/name

            //if intermediate_output code exists
            if(isset($Row[4]) && $Row[4]){
                $intermediate_code = $Row[4];
            }

            if(isset($Row[6]) && $Row[6]){
                //check if ouput exists
                $id = $this->output_model->data_exists_by_code($Row[6]);
                if($id){
                    echo "row ".$iRow. ";".$Row[6].";exists;".PHP_EOL;
                    continue;
                }else{

                    if($intermediate_code){
                        //get the intermediate output id                                 
                        $intermediate_id = $this->ioutput_model->data_exists_by_code($intermediate_code);
                        if($intermediate_id){
                            //insert output
                            $data = array(
                               'intermediate_ouput_id' => $intermediate_id,
                               'code' => $Row[6] ,
                               'name' => $Row[7]
                            );

                            $this->db->insert('outputs',$data);
                            echo "row ".$iRow. ";".$Row[6].";insert;".PHP_EOL;

                        }
                    }   
                }
                
            }
            echo PHP_EOL;
            $iRow++;
        }

    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
