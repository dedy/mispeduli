<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Qa extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    var $organizations = array();
 
	public function __construct()
	{
   
		parent::__construct();

        //If it's a cli request , no need to check session 
        if(!$this->input->is_cli_request()) {
            if(!isLoggedIn()){
                redirect('auth/logout','refresh');
            }
        }

        $this->load->helper('date');
        $this->load->helper('file');
        $this->load->model('qa/qa_model');
        $this->load->model('organization/organization_model');
        $this->load->model('question/question_model');
		
    }
    
    function test(){
        echo "halo".PHP_EOL;
    }

	
    function index(){
        $this->run();
    }

	function run(){

        $file = FCPATH."files/qa.xls";
        $log_file  = './apps/logs/'.date("Ymd.His")."_qa_import_log.txt";
        $this->organization_model->set_organizations();

        $cols = array(
            "date"
            ,"pilar_name"
            ,"organization_name"
            ,"organization_type_name"
            ,"visited_partner"
            ,'q1' 
            ,'q2'
            ,'q3a'
            ,'q3b'
            ,'q3c'
            ,'q4'
        );

        $Reader = new SpreadsheetReader($file);
        $sheets = $Reader->Sheets();
        $Reader -> ChangeSheet(0);

        $iRow = 0;
        $tmp_organisasi ="";
        $this->question_model->set_answers();
        foreach ($Reader as $Row){
            $row_domain =  $data = $row_outcome = array();
            $log_data = "";
            //read start line 2
            if($iRow < 1){
                echo "continue : ".$iRow.PHP_EOL;
                $iRow++;
                continue;
            }
            
            echo "row ".$iRow.";";

            //looping kolom
            $iCol = 0;
            $pilar_id = $organization_id = $organization_type_id = NULL;

            $organization_name = "";
            $data = $row_answers = $answer = array();
            foreach($cols as $col){
                $answer_choice_id = $wording_answer = NULL;
                $Row[$iCol] = isset($Row[$iCol])?trim($Row[$iCol]):"";

                switch($col){
                    case "visited_partner" :   
                        $data['visited_partner'] = $Row[$iCol];
                    break; 
                    case "date" :   
                        $data['date'] = dbdate($Row[$iCol]);
                    break;
                    case "organization_type_name" :
                       $organization_type_id = $this->organization_model->get_org_type_id($Row[$iCol]);
                        $data['organization_type_id'] = $organization_type_id;
                    break;
                    case "pilar_name" :
                        $data['pilar_id'] = $this->get_pilar_id($Row[$iCol]);
                     break;
                   
                    case "q1"   : 
                    case "q2"   :      
                    case "q3a"  :
                    case "q3b"  :
                    case "q3c"  :
                    case "q4"   :
                        //get question id
                        $question_id = $this->question_model->get_question_id_by_code($col);

                        //if question 4, answer in wording
                        if($col == "q4"){
                            $wording_answer = $Row[$iCol];
                        }else{
                            $answer_choice_id = $this->question_model->get_question_answer_id($Row[$iCol]);

                        };
                        $answer['qa_question_id'] = $question_id;
                        $answer['wording_answer'] = $wording_answer;
                        $answer['answer_choice_id'] = $answer_choice_id;

                        array_push($row_answers,$answer);
                    break;
                    case "organization_name":
                        $organization_name = $Row[$iCol];
                        switch ($organization_name) {
                            case 'PPSW':
                                # code...
                                $organization_name = "PPSW Jakarta";
                                break;
                            case 'PKBI':
                                # code...
                                $organization_name = "PKBI Pusat";
                                break;
                        }
                    break;
                    case "date" :

                    break;
                } //end switch 

                $iCol++;
            }//end foreach
            

            $organization_id = $this->organization_model->get_organization_id($organization_name,$organization_type_id);
               
             //check in array first
            /*if($organization_name <> $tmp_organisasi){
                $organization_id = $this->organization_model->get_organization_id($organization_name,$organization_type_id);
                if(!$organization_id){
                     $log_data = $organization_name." (".$organization_type_id."/".$organization_type_name.") not exists \r\n";

                     echo "organization \"". $organization_name."\" not exists;".PHP_EOL;
                     echo $this->db->last_query().PHP_EOL;

                }
            }*/
               
            //u/ skr, kl ga ada organizati , jgn di insert dulu
            if($organization_id){
                $data['organization_id'] = $organization_id;
                echo " insert data qa; ";
                $data['updated_by'] = 1;
                $data['updated_on'] = date("Y-m-d H:i:s");

                $qa_id = $this->qa_model->insert($data);
                
                if(count($row_answers)){
                    unset($data);
                    foreach($row_answers as $row_answer){
                        $data['qa_id'] = $qa_id;
                        $data['qa_question_id']     = $row_answer['qa_question_id'];
                        $data['wording_answer']     = $row_answer['wording_answer'];
                        $data['answer_choice_id']   = $row_answer['answer_choice_id'];
                        $this->qa_model->insert_answer($data);
                    }
                }

               
            }else{
                echo "org ".$organization_name." not exists;";
            }

            $tmp_organisasi = $organization_name;
            //$tmp_kabupaten = $kabupaten;
            if($log_data ){
                write_file($log_file, $log_data, 'a');
            }

            $iRow++;
            echo PHP_EOL;

           // if($iRow > 2500){
           //     break;
           // }
        }


	}
	
    function get_pilar_id($pilar_name){
        $pilar_name = trim($pilar_name);
         $pilars = array(
            array("value"   => "MASYARAKAT ADAT DAN LOKAL TERPENCIL YANG TERGANTUNG PADA SUMBER DAYA ALAM","id" => 1)
            ,array("value"   => "ADAT","id" => 1)
            ,array("value"  => "KELOMPOK WARIA","id"  => 2)
            ,array("value"  => "WARIA","id"  => 2)
            ,array("value"  => "PEDULI DESA","id"=>7)
            ,array("value"  => "DISABILITAS","id"=>3)
            ,array("value"  => "KORBAN DISKRIMINASI, INTOLERANSI DAN KEKERASAN BERBASIS AGAMA","id"=>4)
            ,array("value"  => "AGAMA","id"=>4)
            ,array("value"  => "TRANSGENDER GROUP","id"=>2)
            ,array("value"  => "KORBAN PELANGGARAN HAM BERAT","id"=>5)
            ,array("value"  => "HAM","id"=>5)
            ,array("value"  => "ANAK DAN REMAJA RENTAN","id"=>6)

        );

        foreach($pilars as $pilar){
            if(strtoupper($pilar_name) == $pilar['value']){
                return $pilar['id'];
            }
        }
    }    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
