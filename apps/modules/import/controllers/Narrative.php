<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Narrative extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    var $organizations = array();
    var $provinces = array();
    var $kabupatens = array();
    var $outcomes = array();

	public function __construct()
	{
   
		parent::__construct();

        //If it's a cli request , no need to check session 
        if(!$this->input->is_cli_request()) {
            if(!isLoggedIn()){
                redirect('auth/logout','refresh');
            }
        }
        $this->load->model('narrative/narrative_model');
        $this->load->helper('date');
        $this->load->helper('file');
        $this->load->model('organization/organization_model');
    }
    
    function test(){
        echo "halo".PHP_EOL;
    }

	
    function index(){
        $this->run();
    }

	function run(){

        $file = FCPATH."files/narratives.xlsx";
       
        $this->organization_model->set_organizations();
        $Reader = new SpreadsheetReader($file);
        //$sheets = $Reader->Sheets();
        $this->readSheet($Reader,0);
        $this->readSheet($Reader,1);

	}

    function readSheet($Reader,$sheet){

        $cols = array(
            "pilar"
            ,"organization_name"
            ,"date"
            ,"narrative_title"
            ,'status'
            ,'activity_goal'
            ,'challenge'
            ,'learning'
            ,'conversation'
         );

        $log_file  = './apps/logs/'.date("Ymd.His")."_narrative_import_log.txt";
        if($sheet == 0){ //cso 
            $organization_type_id = 2;
        }else
        if($sheet == 1){//eo
            $organization_type_id = 1;
        }

        $Reader -> ChangeSheet($sheet);

        $iRow = 0;
        $tmp_organisasi = "";
        foreach ($Reader as $Row){
            $data = array();
            $log_data = "";
            //read start line 2
            if($iRow < 1){
                echo "continue : ".$iRow.PHP_EOL;
                $iRow++;
                continue;
            }
            
            echo "row ".$iRow.";";

            //looping kolom
            $iCol = 0;
            $pilar_id = 0;
            $organization_name = "";
            $data = array();
            foreach($cols as $col){
                $Row[$iCol] = isset($Row[$iCol])?trim($Row[$iCol]):"";

                switch($col){
                  
                    case "pilar" :
                        $data['pilar_id'] = $this->get_pilar_id($Row[$iCol]);
                    break;
                    case "organization_name":
                        $organization_name = $Row[$iCol];
                    break;
                    case "date" :
                    case "narrative_title" :
                    case "activity_goal" :
                    case "challenge" :
                    case "learning" :
                    case "conversation" :
                        $data[$col] = $Row[$iCol];
                    break;
                } //end switch 

                $iCol++;
            }//end foreach
            
           
             //check in array first
            if($organization_name <> $tmp_organisasi){
                $organization_id = $this->organization_model->get_organization_id($organization_name,$organization_type_id);
                if(!$organization_id){
                    //log to file
                    $log_data = $organization_name." not exists \r\n";
                }
            }else{
                //echo "organization \"". $organization_name."\" same as prev (".$organization_id."); ";

            }
            
            //u/ skr, kl ga ada organizati , jgn di insert dulu
            if($organization_id){

                $data['organization_id'] = $organization_id;
                echo " insert data narrative; ";
                $data['created_by'] = 1;
                $data['created_on'] = date("Y-m-d H:i:s");
                $data['updated_by'] = 1;
                $data['updated_on'] = date("Y-m-d H:i:s");

                $narrative_id = $this->narrative_model->insert($data);
            }

            $tmp_organisasi = $organization_name;
             if($log_data )
                write_file($log_file, $log_data, 'a');

            $iRow++;
            echo PHP_EOL.PHP_EOL;

           // if($iRow > 2500){
           //     break;
           // }
        }
    }
	
    function get_pilar_id($pilar_name){
        $pilar_name = trim($pilar_name);
        $pilars = array(
        array("value"   => "MASYARAKAT ADAT DAN LOKAL TERPENCIL DAN TERGANTUNG PADA SUMBER DAYA ALAM","id" => 1)
        ,array("value"  => "KELOMPOK WARIA","id"  => 2)
        ,array("value"  => "PEDULI DESA","id"=>7)
        ,array("value"  => "DISABILITAS","id"=>3)
        ,array("value"  => "KORBAN DISKRIMINASI, INTOLERANSI, DAN KEKERASAN BERBASIS AGAMA","id"=>4)
        ,array("value"  => "TRANSGENDER GROUP","id"=>2)
        ,array("value"  => "KORBAN PELANGGARAN HAM BERAT","id"=>5)
        ,array("value"  => "ANAK DAN REMAJA RENTAN","id"=>6)

        );

        foreach($pilars as $pilar){
            if(strtoupper($pilar_name) == $pilar['value']){
                return $pilar['id'];
            }
        }
    }    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
