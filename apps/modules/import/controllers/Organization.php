<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';

//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Organization extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    

	public function __construct()
	{
   
		parent::__construct();

        //If it's a cli request , no need to check session 
        if(!$this->input->is_cli_request()) {
            if(!isLoggedIn()){
                redirect('auth/logout','refresh');
            }
        }
		
    }
    
    function test(){
        echo "halo".PHP_EOL;
    }

	
    function index(){
        $this->run();
    }

    function run(){
        $this->load->model('organization/organization_model');
        $this->load->model('province/province_model');
        $file = FCPATH."files/organizations.xlsx";

        $cols = array(
            ""
         );

        $Reader = new SpreadsheetReader($file);
        $sheets = $Reader->Sheets();
        $Reader -> ChangeSheet(2);

        $iRow = 0;

        //looping kolom
        $intermediate_code = "";
        foreach ($Reader as $Row){
        
            //read start line 2
            if($iRow < 1){
                echo "continue : ".$iRow.PHP_EOL;
                $iRow++;
                continue;
            }

           // if($iRow > 69){
           //     break;
           // }

            //if cso name exists in xls 
            if(isset($Row[3]) && $Row[3]){
                $eo_name    = trim($Row[2]);
                $cso_name   = trim($Row[3]);
                $cso_name_2   = trim($Row[4]);
                $address    = isset($Row[5])?trim($Row[5]):"";
                $phone      = isset($Row[9])?($Row[9]):"";
                $person_in_charge = ($Row[10])?trim($Row[10]):"";
                $target_group = ($Row[11])?trim($Row[11]):NULL;
                $active = ($Row[12])?trim($Row[12]):"";

                $total_target_group = ($Row[15])?trim($Row[15]):0;
                $pilar_id = $this->get_pilar_id(trim($Row[16]));
 
                $province_name = ($Row[6])?trim($Row[6]):"";

                if(strpos($province_name,",")){
                    list($province_name1,$province_name2)  = explode(",",$province_name);
                    $province_name1 = trim($province_name1);
                    $province_name2 = trim($province_name2);
                }else{
                    $province_name1 = trim($province_name);
                    $province_name2 = "";
                }

                //echo $iRow.";".$province_name.";".$province_name1.";".$province_name2.PHP_EOL;
                //$iRow++;
                //continue;

                //check if eo exists in table
                $id = $this->organization_model->data_exists_by_name($eo_name,1);
                if(!$id){
                    unset($data);

                  
                    //insert eo 
                    $data['name'] = $eo_name;
                    $data['name2'] = $eo_name;
                    $data['organization_type_id'] = '1';
                    $data['pilar_id'] = $this->get_pilar_id(trim($Row[16]));

                    $data['active'] = '1';
                    $this->db->insert('organizations',$data);
                    $id = $this->db->insert_id();
                    echo "insert eo : ".$iRow.";".$eo_name.PHP_EOL;

                }

                //check if cso exists in table
                $cso_id = $this->organization_model->data_exists_by_name($cso_name,2,$address);

                //get province id
                $d['created_by'] = _UserId();
                $d['created_on'] = date("Y-m-d H:i:s");
                $d['updated_by'] = _UserId();
                $d['updated_on'] = date("Y-m-d H:i:s");
                $d['active'] = '1';
                
                if($province_name1){
                    switch ($province_name1) {
                        case 'NTB':
                            # code...
                            $province_name1 = "Nusa Tenggara Barat";

                            break;
                         case 'NTT':
                            # code...
                            $province_name1 = "Nusa Tenggara Timur";

                            break;
                        case 'Sumatra Selatan':
                            # code...
                            $province_name1 = "Sumatera Utara";

                            break;
                        case 'Sumatra Barat':
                            # code...
                            $province_name1 = "Sumatera Utara";

                            break;
                        case 'Sumatra Utara':
                            # code...
                            $province_name1 = "Sumatera Utara";
                            break;

                    }

                    //if not exist, insert
                    $pid = $this->province_model->data_exists_by_name($province_name1);
                    //not exists
                    if(!$pid) {
                        $d['name'] = $province_name1;
                        $province_id = $this->province_model->insert($d);
                    }else{
                        $province_id = $pid;
                    }
                }

                if($province_name2){
                    if($province_name2 == "NTB"){
                        $province_name2 = "Nusa Tenggara Barat";
                    }

                    if($province_name2 == "NTT"){
                        $province_name2 = "Nusa Tenggara Timur";
                    }


                    //if not exist, insert
                    $pid2= $this->province_model->data_exists_by_name($province_name2);
                    //not exists
                    if(!$pid2){
                        $d['name'] = $province_name2;
                        $province_id_2 = $this->province_model->insert($d);
                    }else{
                        $province_id_2 = $pid2;
                    }
                }

                unset($data);
                //insert cso 
                $data['name'] = $cso_name;
                $data['name2'] = $cso_name_2;
                $data['organization_type_id'] = 2;
                $data['parent_id'] = $id;
                $data['address'] = $address;
                $data['phone'] = $phone;
                $data['person_in_charge'] = $person_in_charge;
                $data['province_id'] = $province_id;
                $data['province_id_2'] = ($province_name2)?$province_id_2:NULL;
               
                $data['target_group'] = ($target_group)?$target_group:NULL;
                $data['total_target_group'] = ($total_target_group)?$total_target_group:NULL;
                $data['active'] = (strtoupper($active) == "YA")?'1':'0';
                $data['pilar_id'] = $pilar_id;
               
                if(!$cso_id){
                    $this->db->insert('organizations',$data);
                    echo "insert cso : ".$iRow.";".$cso_name.";".$Row[15].";".$Row[16].";".PHP_EOL;

                }else{
                     $this->db->where('id', $cso_id);
                    $this->db->update('organizations', $data); 

                    echo "update : ".$iRow.";".$cso_name.PHP_EOL;
                }
            }
            echo PHP_EOL;
            $iRow++;
        }

    }

    function get_pilar_id($pilar_name){
        $pilar_name = trim($pilar_name);
        $pilars = array(
            array("value"   => "INDIGENOUS PEOPLE AND REMOTE GROUP","id" => 1)
            ,array("value"  => "KELOMPOK  WARIA","id"  => 2)
            ,array("value"  => "PEDULI DESA","id"=>7)
            ,array("value"  => "PEOPLE WITH DISABILITY","id"=>3)
            ,array("value"  => "RELIGIOUS MINORITY AND LOCAL BELIEF","id"=>4)
            ,array("value"  => "TRANSGENDER GROUP","id"=>2)
            ,array("value"  => "VICTIM OF GROSS HUMAN RIGHT VIOLATION","id"=>5)
            ,array("value"  => "VULNERABLE CHILDREN AND YOUTH","id"=>6)

        );

        foreach($pilars as $pilar){
            if(strtoupper($pilar_name) == $pilar['value']){
                return $pilar['id'];
            }
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
