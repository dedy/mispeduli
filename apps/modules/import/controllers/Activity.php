<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include autoloader
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/php-excel-reader/excel_reader2.php';
require_once FCPATH.'vendor/nuovo/spreadsheet-reader/SpreadsheetReader.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
//require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Activity extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
    var $organizations = array();
    var $provinces = array();
    var $kabupatens = array();
    var $outputs = array();

	public function __construct()
	{
   
		parent::__construct();

        //If it's a cli request , no need to check session 
        if(!$this->input->is_cli_request()) {
            if(!isLoggedIn()){
                redirect('auth/logout','refresh');
            }
        }
        $this->load->model('activity/activity_model');
        $this->load->model('activity_type/activity_type_model');
        $this->load->model('province/province_model');
        $this->load->model('kabupaten_kota/kabupaten_kota_model');
        $this->load->model('output/output_model');
        $this->load->model('outcome/outcome_model');
        $this->load->model('organization/organization_model');
        $this->load->helper('date');
        $this->load->helper('file');
		
    }
    
    function test(){
        echo "halo".PHP_EOL;
    }

	
    function index(){
        $this->run();
    }

	function run(){
        $this->load->model('organization/organization_model');

        $file = FCPATH."files/activities.xlsx";
        $log_file  = './apps/logs/'.date("Ymd.His")."_activity_import_log.txt";

        $cols = array(
            "org_type"
            ,"pilar"
            ,"start_date"
            ,"end_date"
            ,"activity_name"
            ,"organization_name"
            ,"activity_type"
            ,'male_direct_beneficiaries'
            ,'female_direct_beneficiaries'
            ,'trans_direct_beneficiaries'
            ,'male_indirect_beneficiaries'
            ,'female_indirect_beneficiaries'
            ,'trans_indirect_beneficiaries'
            ,'male_government_official_beneficiaries'
            ,'female_government_official_beneficiaries'
            ,'trans_government_official_beneficiaries'
            ,'province_name'
            ,'kabupaten'
            ,'activity_address'
            ,'status'
            ,'result_description'
            ,'conversation'
            ,'outcome'
            ,'outcome1'
            ,'outcome2'
            ,'outcome3'
            ,'outcome4'
            ,'outcome5'
            ,'outcome6'
            ,'outcome7'
            ,'outcome8'
            ,'outcome9'
            ,'outcome10'
            ,'output'
        );

        /*    ,'op.lbl1a'
            ,'op.lbl1b'
            ,'op.lbl1c'
            ,'op.lbl2a'
            ,'op.lbl2b'
            ,   'op.lbl2c    
            op.lbl2d    
            op.lbl3a    
            op.lbl3b    
            op.lbl4a    
            op.lbl4b    
            op.lbl4c    
            op.lbl5a    
            ,'op.lbl6a    
            op.lbl7a    
            op.lbl8a    
            op.lbl9a    
            op.lbl10a   
            op.lbl10b   
            op.lbl11a   
            op.lbl12a   
            ,'op.lbl13a   
            op.lbl13b   
            op.lbl13c   
            op.lbl14a   
            op.lbl14b   
            op.lbl14c   
            op.lbl14d   
            op.lbl14e   
            ,'op.lbl14f   
            op.lbl15a   
            op.lbl15b   
            op.lbl15c   
            op.lbl15d   
            op.lbl15e   
            op.lbl15f   
            op.lbl16a   
            ,'op.lbl17a   
            op.lbl18a   
            op.lbl19a   
            op.lbl19b   
            op.lbl20a   
            op.lbl21a   
            op.lbl21b   
            op.lbl22a   
            ,'op.lbl23a   
            op.lbl24a   
            op.lbl24b   
            op.lbl25a   
            op.lbl26a   
            op.lbl27a   
            'op.lbl28a   
            'op.lbl29a   
            ,'op.lbl30a   
            'op.lbl30b
         );*/

        $Reader = new SpreadsheetReader($file);
        $sheets = $Reader->Sheets();
        $Reader -> ChangeSheet(0);

        //load data
        $this->province_model->set_provinces();
        $this->kabupaten_kota_model->set_kabupatens();
        $this->output_model->set_outputs();
        $this->outcome_model->set_outcomes($this->output_model->datas);
        $this->organization_model->set_organizations();

        $iRow = 0;
        $tmp_organisasi = $tmp_province = $tmp_kabupaten = "";
        foreach ($Reader as $Row){
            $log_data = "";
            //read start line 3
            if($iRow < 2 ){
            //if($iRow < 2200){
                echo "continue : ".$iRow.PHP_EOL;
                $iRow++;
                continue;
            }

            echo "Row ".($iRow+1). ";";

            //looping kolom
            $iCol = 0;
            $organization_type_id = $pilar_id = $activity_type_id = 0;
            $organization_name = "";
            $outputs = $outcomes = array();
            $data = array();
            foreach($cols as $col){
                $Row[$iCol] = trim($Row[$iCol]);

                switch($col){
                    case 'male_direct_beneficiaries' :
                    case 'female_direct_beneficiaries' : 
                    case 'trans_direct_beneficiaries' :
                    case 'male_indirect_beneficiaries' :
                    case 'female_indirect_beneficiaries' :
                    case 'trans_indirect_beneficiaries' :
                    case 'male_government_official_beneficiaries' :
                    case 'female_government_official_beneficiaries' :
                    case 'trans_government_official_beneficiaries' :
                        $data[$col] = ($Row[$iCol])?$Row[$iCol]:0;
                        break;
                    case "activity_name" :
                    case "conversation" :
                    case "result_description" :
                    case "activity_address" :
                        $data[$col] = $Row[$iCol];
                    break;

                    case "org_type" :
                        $data['organization_type_id'] = $organization_type_id  = $this->organization_model->get_org_type_id($Row[$iCol]);
                    break;
                    case "pilar" :
                        $data['pilar_id'] = $this->get_pilar_id($Row[$iCol]);
                    break;
                    case "start_date" :
                    case "end_date" :
                        $data[$col] = dbdate($Row[$iCol]);
                    break;
                    case "organization_name":
                        switch ($Row[$iCol]) {
                            case 'LPA Tulung Agung':
                                $Row[$iCol] = "LPA Tulungagung";
                                # code...
                                break;
                            case 'P3M Jakarta'  :
                                $Row[$iCol] = "P3M";
                                # code...
                                break;
                        }

                        $organization_name = $Row[$iCol];
                    break;

                    case "kabupaten" : 
                        $kabupaten = $Row[$iCol];

                        //get kabupaten id
                        //check in array first
                        if($kabupaten <> $tmp_kabupaten){
                            $kabupaten_id = $this->kabupaten_kota_model->get_kabupaten_id($kabupaten,$province_id);
                        }else{
                            //echo "same as prev kabupaten ".$kabupaten."(".$kabupaten_id.");";
                        }

                        $data['kabupaten_kota_id'] = $kabupaten_id;
                       
                    break;

                    case "province_name" : 
                        if($Row[$iCol] == "Aceh")
                            $Row[$iCol] = "Nanggroe Aceh Darussalam";

                        $organization_province = $Row[$iCol];

                        //get province id
                        //check in array first
                        if($organization_province <> $tmp_province){
                            $province_id = $this->province_model->get_province_id($organization_province);
                        }else{
                           // echo "same as prev province ".$organization_province."(".$province_id.");";
                        }
                        $data['province_id'] = $province_id;
                       
                    break;
                    
                     case "activity_type" :
                        $activity_type_id = $this->activity_type_model->get_activity_type_id($Row[$iCol]);
                        $data['activity_type_id'] = $activity_type_id;

                    break;
                    case "output";
                        $output = $Row[$iCol];
                        if($Row[$iCol]){
                            $output_codes = explode("\n", $Row[$iCol]);
                            $data['output_codes'] = implode(', ', $output_codes);
                            $outcome_code = "";
                            foreach($output_codes as $output_code){
                                //get output id by code
                                $output_id = $this->output_model->get_output_id_by_code($output_code);
                                $outputs[] = $output_id;

                                //get outcome
                                $outcome_code = $this->outcome_model->get_outcome_code_by_output_id($output_id);
                                if(!in_array($outcome_code, $outcomes)) {
                                    $outcomes[] =  $outcome_code;
                                }
                            }
                            array_unique($outcomes);
                            $data['outcome_codes'] = implode(', ',$outcomes);
                        }
                   break;
                    
                } //end switch 
                $iCol++;
            }//end foreach
        
             //check in array first
            if($organization_name <> $tmp_organisasi){
                $organization_id = $this->organization_model->get_organization_id($organization_name,$organization_type_id);
                //$organization_id = $this->organization_model->get_organization_id($organization_name,$organization_type_id,$province_id);
                
                if(!$organization_id){
                    //log to file
                    $log_data = $organization_name." (".$organization_type_id."/".$province_id.") not exists \r\n";
                }
            }else{
                //echo "organization \"". $organization_name."\" same as prev (".$organization_id."); ";

            }
            
            //u/ skr, kl ga ada organizati , jgn di insert dulu
            if($organization_id){
                $data['organization_id'] = $organization_id;
                echo " insert data activity; ";
                $data['created_by'] = 1;
                $data['created_on'] = date("Y-m-d H:i:s");
                $data['updated_by'] = 1;
                $data['updated_on'] = date("Y-m-d H:i:s");

                $activity_id = $this->activity_model->insert($data);
                //$activity_id = 0;
                //if exist , input output
                if(count($outputs) && $activity_id){
                    foreach($outputs as $v){
                        $this->activity_model->insert_activity_output(array("output_id"=>$v,"activity_id"=>$activity_id)); 
                    }
                }
            }else{
                echo "organization \"". $organization_name."\" not exists; ";
            }

            $tmp_organisasi = $organization_name;
            $tmp_province = $organization_province;
            $tmp_kabupaten = $kabupaten;

            if($log_data)
                write_file($log_file, $log_data, 'a');
      
            $iRow++;
            echo PHP_EOL.PHP_EOL;

            //if($iRow > 5){
            //    break;
            //}
        }


	}
	
    function get_pilar_id($pilar_name){
        $pilar_name = trim($pilar_name);
        $pilars = array(
            array("value"   => "INDIGENOUS PEOPLE AND REMOTE GROUP","id" => 1)
            ,array("value"  => "KELOMPOK  WARIA","id"  => 2)
            ,array("value"  => "PEDULI DESA","id"=>7)
            ,array("value"  => "PEOPLE WITH DISABILITY","id"=>3)
            ,array("value"  => "RELIGIOUS MINORITY AND LOCAL BELIEF","id"=>4)
            ,array("value"  => "TRANSGENDER GROUP","id"=>2)
            ,array("value"  => "VICTIM OF GROSS HUMAN RIGHT VIOLATION","id"=>5)
            ,array("value"  => "VULNERABLE CHILDREN AND YOUTH","id"=>6)

        );

        foreach($pilars as $pilar){
            if(strtoupper($pilar_name) == $pilar['value']){
                return $pilar['id'];
            }
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
