<?php
class Import_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function insert($values){
        $values["created_on"]   = date("Y-m-d H:i_s");
        $values["created_by"]   = _userid();
                 
		$this->db->insert("import_logs", $values); 
		$id = $this->db->insert_id();
       
        return $id;
    }
    
    function insert_detail($values){
		$values["created_on"]   = date("Y-m-d H:i_s");
        $this->db->insert("import_logs_detail", $values); 
		$id = $this->db->insert_id();
       
        return $id;
    }

}