<?php
class Domain_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
  function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('domain_perubahan');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}