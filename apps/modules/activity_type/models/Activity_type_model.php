<?php
class Activity_type_model extends CI_Model {
	var $is_count = false;
	
	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
    	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}
		
		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('activity_types');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
	function get_activity_type_id($activity_name){
        $activity_name = trim($activity_name);
        $activities = array(
            array("value"=>"DISKUSI"                ,"id"=>1)
            ,array("value"=>"KAMPANYE DAN ADVOKASI" ,"id"=>2)
            ,array("value"=>"KOLABORASI EKSTERNAL"  ,"id"=>3) 
            ,array("value"=>"KOLABORASI INTERNAL"   ,"id"=>4)
            ,array("value"=>"LOKAKARYA (WORKSHOP)"  ,"id"=>5)
            ,array("value"=>"PELATIHAN (TRAINING)"  ,"id"=>6)
            ,array("value"=>"PERAYAAN"              ,"id"=>7)
            ,array("value"=>"RAPAT"                 ,"id"=>8)
            ,array("value"=>"SEMINAR"               ,"id"=>9)
        );

        foreach($activities as $activity){
            if(strtoupper($activity_name) == $activity['value']){
                return $activity['id'];
            }
        }
    }

}