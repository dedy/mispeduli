<?php
class Qa_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function insert($data){
        $this->db->insert("qa", $data); 
        return $this->db->insert_id();
    }

    function insert_answer($data){
        $this->db->insert("qa_answers", $data); 
        return $this->db->insert_id();
    }
    /*

select qa.id as qa_id, qa.date as qa_date , visited_partner
, (SELECT c.description
FROM
	qa_answers a
INNER JOIN
	qa_questions q ON q.id = a.qa_question_id
INNER JOIN
	answer_choices c ON c.id = a.answer_choice_id
WHERE
	q.id = '1'
and 
	a.qa_id = qa.id)
as q1

From qa
limit 5;*/


    function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
  		if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
			$this->db->where("qa.pilar_id",$searchval['pilar_id']);
		}
		
		if(isset($searchval['date_from']) && $searchval['date_from']){
			$this->db->where("qa.date >=",$searchval['date_from']);
		}
		
		if(isset($searchval['date_to']) && $searchval['date_to']){
			$this->db->where("qa.date <=",$searchval['date_to']);
		}
		
		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
			$this->db->where("qa.organization_type_id",$searchval['organization_type_id']);
		}
		
		if(isset($searchval['organization_id']) && $searchval['organization_id']){
			$this->db->where("qa.organization_id",$searchval['organization_id']);
		}

		if(isset($searchval['keyword']) && $searchval['keyword']){
    		$qq = "(a.wording_answer like '%".$searchval['keyword']."%')";
    		$this->db->where($qq);
    	}

		if($this->is_count){
			$this->db->select("count(id) as total");
		}else{
			$this->db->select("qa.id as qa_id, qa.date as qa_date , visited_partner");
			$this->db->select("p.name as pilar_name");
			$this->db->select("o.name as organization_name");
			$this->db->select("ot.name as organization_type_name");
            $this->db->select("DATE_FORMAT(date ,'%d/%m/%Y') as date_fmt",false);

			$query = $this->db->query("SELECT id,code,question_type_id FROM qa_questions ORDER BY id asc");
			$result = $query->result();
			if($result) {
				foreach($result as $key => $res) {

					$qq = "(SELECT ";

					if($res->code <> "Q4") {
						$qq .= " c.description ";
					}else {
						$qq .= "a.wording_answer ";
					}

					$qq .="FROM
							qa_answers a
						INNER JOIN
							qa_questions q ON q.id = a.qa_question_id ";

					//joinwith choices if code  <> q4
					if($res->code <> "Q4") {
						$qq .= "INNER JOIN
								answer_choices c ON c.id = a.answer_choice_id ";
					}
						
					$qq .= "WHERE
							q.id = '".$res->id."'
						and 
							a.qa_id = qa.id ";

					$qq .= "	) AS ".url_title($res->code,'dash',true);

					$this->db->select($qq);
				}
			}

			$this->db->join("pilar p","p.id = qa.pilar_id");
			$this->db->join("organizations o","o.id = qa.organization_id");
			$this->db->join("organization_types ot","ot.id = qa.organization_type_id");
		}

		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('qa');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}

	 function countSearchView($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search_view(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}

	 function search_view($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
  		if(isset($searchval['pilar_id']) && $searchval['pilar_id']){
			$this->db->where("pilar_id",$searchval['pilar_id']);
		}
		
		if(isset($searchval['date_from']) && $searchval['date_from']){
			$this->db->where("qa_date >=",$searchval['date_from']);
		}
		
		if(isset($searchval['date_to']) && $searchval['date_to']){
			$this->db->where("qa_date <=",$searchval['date_to']);
		}
		
		if(isset($searchval['organization_type_id']) && $searchval['organization_type_id']){
			$this->db->where("organization_type_id",$searchval['organization_type_id']);
		}
		
		if(isset($searchval['organization_id']) && $searchval['organization_id']){
			$this->db->where("organization_id",$searchval['organization_id']);
		}

		if(isset($searchval['keyword']) && $searchval['keyword']){
    		$qq = "(q4 like '%".$searchval['keyword']."%' or visited_partner like '%".$searchval['keyword']."%')";
    		$this->db->where($qq);
    	}

		if($this->is_count){
			$this->db->select("count(qa_id) as total");
		}

		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('qa_view');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
	
}