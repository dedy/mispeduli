<?php
/**
 * Created by PhpStorm.
 * User: BDO-IT
 * Date: 20/02/2017
 * Time: 15:40
 */

require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once FCPATH.'vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

class Qa extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if(!isLoggedIn()){
			redirect('auth/logout','refresh');
		}
		$this->load->model(array('qa/qa_model'));
		$this->load->model('question/question_model');

	}
	
	public function index() {
		$this->lists();
	}
	
	public function lists() {
		$this->load->helper('form');
		$this->load->model('pilar/pilar_model');
		$this->load->model('organization_type/organization_type_model');
		$this->load->model('organization/organization_model');
		
		/* get pilar list */
		$pilars = $this->pilar_model->search(0,1,'name','asc');
		$pilar_options = array();
		$pilar_options[0] = '';
		foreach($pilars as $pilar){
			$pilar_options[$pilar->id] = $pilar->name;
		}
		$this->viewparams['pilar_options'] = $pilar_options;
		$this->viewparams['pilar_params'] = array(
			'id'   => 'pilar_id',
			'class'	=> "form-control input-xxlarge",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_pilar')
		);
		/* end pilar */
		
		/* get organization type list */
		$organization_types = $this->organization_type_model->search(0,1,'name','asc');
		$organization_type_options = array();
		$organization_type_options[0] = '';
		foreach($organization_types as $organization_type){
			$organization_type_options[$organization_type->id] = $organization_type->name;
		}

		$this->viewparams['organization_type_options'] = $organization_type_options;
		$this->viewparams['organization_type_params'] = array(
			'id'   => 'organization_type_id',
			'class'	=> "form-control input-medium",
			'data-rel'=> "chosen",
			'data-placeholder'=> lang('lplease_select_organization_type')
		);
		/* end organization type */

		$header_col = $this->get_report_coloumn();
		$this->viewparams['column']	= $header_col['field']['column'];
		$this->viewparams['field']	= $header_col['field']['value'];
		$this->viewparams['width']	= $header_col['field']['width'];
		$this->viewparams['xindex']	= $header_col['field']['index'];
	
		$this->viewparams['title_page']	= lang('lqa');
		parent::viewpage("vlist");
	}
	
	public function loadDataGrid () {
		$page = isset($_POST['page']) ? $_POST['page']:1;
		$sidx = isset($_POST['sidx']) ? $_POST['sidx']:'qa_date'; // get index row - i.e. user click to sort
		$sord = isset($_POST['sord']) ? $_POST['sord']:'desc'; // get the direction
		$limit = isset($_POST['rows'])? $_POST['rows']: config_item('rowNum'); // get how many rows we want to have into the grid
		
		$filter = array(
			'pilar_id' => $this->input->get('pilar_id'),
			'organization_type_id' => trim($this->input->get('organization_type_id')),
			'organization_id' => trim($this->input->get('organization_id')),
			'date_from' => $this->input->get('date_from'),
			'date_to' => $this->input->get('date_to'),
			'keyword'     => urldecode($this->input->get('keyword'))
       
		);
		
		$query = $this->qa_model->search_view($limit,$page,$sidx,$sord,$filter);
		$this->firephp->log($this->db->last_query());
		$count = $this->qa_model->countSearchView($filter);
		$this->firephp->log($this->db->last_query());
		$this->firephp->log("count : ".$count);
		$columns = $this->get_report_coloumn();

		 if(count($query)){
            for($i=0;$i<count($query);$i++){
                $query[$i]->q4 = nl2br($query[$i]->q4);
            }
        }
		$columns = $columns['field'];
		$this->DataJqGrid = array(
			"page"		=> $page,
			"sidx"		=> $sidx,
			"sord"		=> $sord,
			"query" 	=> $query,
			"limit"		=> $limit,
			"count"		=> $count,
			"column"	=> $columns['value'],
			"id"		=> "qa_id"
		);
		
		parent::loadDataJqGrid();
		
	}
	
	public function do_export() {
		$objPHPExcel = new PHPExcel();
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel,"Excel2007");
		$objPHPExcel->getProperties()
			->setTitle("QA Report")
			->setDescription("QA Report");
		$objPHPExcel->setActiveSheetIndex(0);
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$border = array( 'borders' => array( 'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )));
		$fill = array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'rotation'   => 0,
			'startcolor' => array (
				'rgb' => 'CCCCCC'
			),
			'endcolor'   => array (
				'argb' => 'CCCCCC'
			)
		);
	
		$n = 0;
		$column[$n]['title'] = lang('ldate');
		$column[$n]['field'] = 'date_fmt';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$n = $n + 1;
		$column[$n]['title'] = lang('lpilar');
		$column[$n]['field'] = 'pilar_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;
		
		$n++;
		$column[$n]['title'] = lang('lorganization');
		$column[$n]['field'] = 'organization_name';
		$column[$n]['auto_size'] = false;
		$column[$n]['width'] = 55;

		$n++;
		$column[$n]['title'] = lang('lorganization_type');
		$column[$n]['field'] = 'organization_type_name';
		$column[$n]['auto_size'] = True;
		$column[$n]['width'] = 0;

		$n = $n + 1;
		$column[$n]['title'] = lang('lvisited_partner');
		$column[$n]['field'] = 'visited_partner';
		$column[$n]['auto_size'] = False;
		$column[$n]['width'] = 55;
		
		
		/**
		 * Source Table FROM qa_question
		 */
		$query = $this->db->query("SELECT id,code,question_type_id FROM qa_questions ORDER BY id asc");
		$questions = $query->result();
		$question_records_auto_size = False;
		$question_records_width = 19;
		if(count($questions)) {
			$li=1;
			foreach($questions as $question) {
				if($question->code == "Q4"){
					$question_records_width = 75;
				}

				$n = $n + $li;
				$column[$n]['title'] = $question->code;
				$column[$n]['field'] = url_title($question->code,'dash',true);
				$column[$n]['auto_size'] = $question_records_auto_size;
				$column[$n]['width'] = $question_records_width;
				$li++;

			}	
		}
		
		$total_coloumn = count($column);
		
		$xcol = 0;
		$xrow = 1;
		
		$objWorksheet->setAutoFilterByColumnAndRow($xcol, $xrow,$xcol + ($total_coloumn - 1),$xrow);
		$objWorksheet->getRowDimension($xrow)->setRowHeight(90);
		
		foreach($column as $col){
			//set the header title , and set it to bold
			$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, $col['title'])->getStyleByColumnAndRow($xcol, $xrow)->getFont()->setBold(true);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getFill()->applyFromArray($fill);
			if($col['auto_size'] == True)
				$objWorksheet->getColumnDimensionByColumn($xcol)->setAutoSize(true);
			else
				$objWorksheet->getColumnDimensionByColumn($xcol)->setWidth($col['width']);
			
			$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setWrapText(true);
			$xcol = $xcol + 1;
		}
		

	
		$filter = array(
			'pilar_id' => $this->input->get('pilar_id'),
			'organization_type_id' => trim($this->input->get('organization_type_id')),
			'organization_id' => trim($this->input->get('organization_id')),
			'date_from' => $this->input->get('date_from'),
			'date_to' => $this->input->get('date_to'),
			'keyword'     => urldecode($this->input->get('keyword'))

		);
		
		$records = $this->qa_model->search_view(0,1,'qa_date','asc',$filter);
		$count = $this->qa_model->countSearchView($filter);
		
		if($count) {
			$xrow++;
			$xcol=0;
			
			for($i=0; $i<$count; $i++){
				$xcol = 0;
				$xrow = $i+2; //start row 2
				foreach($column as $key => $col){
					$fieldname = $col['field'];
					$value = $records[$i]->$fieldname;
					
					$objWorksheet->setCellValueByColumnAndRow($xcol, $xrow, $value);
					$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->applyFromArray($border);
					if($col['auto_size'] == False) {
						if($col['width'] <= 20)
							$position = PHPExcel_Style_Alignment::VERTICAL_CENTER;
						else
							$position = PHPExcel_Style_Alignment::VERTICAL_TOP;
						
						$objWorksheet->getStyleByColumnAndRow($xcol, $xrow)->getAlignment()->setVertical($position);
						$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setWrapText(true);
					} else {
						$objWorksheet->getStyleByColumnAndRow($xcol,$xrow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					}
					
					$xcol++;
				}
			}
		}
		
		$filename = "QA-REPORT.".date("Ymd.His").".xlsx";
		$objWriter->save(FCPATH."reports/".$filename);
		$file_url = base_url() . "reports/" . $filename;
		$message = "Done";
		$is_error = false;
			
		$result = array(
			"is_error"  =>  $is_error,
			"message"  => $message,
			"file_url" => $file_url
		);
		
		echo json_encode($result);
	}
	
	public function get_report_coloumn() {
		//ref http://www.trirand.com/jqgridwiki/doku.php?id=wiki%3agroupingheadar
		$columns = array();

		$n = 0;
		$columns['field']['column'][$n] = lang('ldate');
		$columns['field']['index'][$n] = 'qa_date';
		$columns['field']['value'][$n] = 'date_fmt';
		$columns['field']['align'][$n] = 'center';
		$columns['field']['width'][$n] = 100;
		
		$n++;
		$columns['field']['column'][$n] = lang('lpilar');
		$columns['field']['index'][$n] = 'pilar_name';
		$columns['field']['value'][$n] = 'pilar_name';
		$columns['field']['align'][$n] = 'left';
		$columns['field']['width'][$n] = 200;

			$n++;
		$columns['field']['column'][$n] = lang('lorganization');
		$columns['field']['index'][$n] = 'organization_name';
		$columns['field']['value'][$n] = 'organization_name';
		$columns['field']['align'][$n] = 'left';
		$columns['field']['width'][$n] = 150;

			$n++;
		$columns['field']['column'][$n] = lang('lorganization_type');
		$columns['field']['index'][$n] = 'organization_type_name';
		$columns['field']['value'][$n] = 'organization_type_name';
		$columns['field']['align'][$n] = 'left';
		$columns['field']['width'][$n] = 50;

		$n++;
		$columns['field']['column'][$n] = lang('lvisited_partner');
		$columns['field']['index'][$n] = 'visited_partner';
		$columns['field']['value'][$n] = 'visited_partner';
		$columns['field']['align'][$n] = 'left';
		$columns['field']['width'][$n] = 150;

		//get question lists
		$questions = $this->question_model->search(0,1,'question_id','asc');
		if(count($questions)) {
			foreach($questions as $question) {
				$n++;
				if($question->question_code == "Q4"){
					$columns['field']['width'][$n] = 350;
				}else{
					$columns['field']['width'][$n] = 100;
				}
				$columns['field']['value'][$n] = url_title($question->question_code,'dash',true);
				$columns['field']['column'][$n] = $question->question_label;
				$columns['field']['index'][$n] = $question->question_code;
				$columns['field']['align'][$n] = 'center';
				
			}
		}
		
		return $columns;
	}
	
}