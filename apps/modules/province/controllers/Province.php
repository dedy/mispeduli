<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Province extends MY_Controller {
	
	public function __construct()
	{
		if(!isLoggedIn()){
            redirect('auth/logout','refresh');
        }
        
		parent::__construct();
		$this->load->model("province_model");
	}

	function index(){
		$this->lists();
	}
	
	function lists(){
   		$datas = $this->province_model->search(0,1,'name','asc');
   		$this->viewparams['datas'] = $datas;
   		$this->viewparams['title'] = lang('lprovince');
		parent::viewpage("vlist");
	}

}
