<?php
class Province_model extends CI_Model {
	var $is_count = false;
	var $datas = array();

	function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function set_provinces(){
 		$provinces = $this->search(0,1,'name','asc');

        if(count($provinces)){
            foreach($provinces as $province){
                array_push($this->datas,array("id"=>$province->id,"name"=>$province->name));
            }
        }   
    }

	function get_province_id($organization_province){
        $total = count($this->datas);
              
        $province_id = 0;
        $is_found = false;
    
        //check in array
        if($total){
            foreach($this->datas as $value){
                if(strtoupper(trim($value['name'])) == strtoupper(trim($organization_province))){
                    echo $value['name']. "(".$value['id'].");";
                    $province_id = $value['id'];
                    $is_found = true;
                    break;
                }
            }
        }else{
            $is_found = false;
        }

        //if not exist in array, check in db
        if(!$is_found){
            $province_id = $this->data_exists_by_name($organization_province);
            
            //if not found in array or in db
            if(!$province_id){
                //if not exists, then insert
                $d['created_by'] = _UserId();
                $d['created_on'] = date("Y-m-d H:i:s");
                $d['updated_by'] = _UserId();
                $d['updated_on'] = date("Y-m-d H:i:s");
                $d['active'] = '1';                 
                $d['name'] = $organization_province;
                $province_id = $this->insert($d);
                echo "insert province ".$organization_province."(".$province_id.")";
            }else{
                 echo $organization_province."(".$province_id.");";

            } 
            array_push($this->datas,array("id"=>$province_id,"name"=>$organization_province));
        }
        return $province_id;
    }

    function insert($data){
		$this->db->insert("provinces", $data); 
		return $this->db->insert_id();
    }

    function data_exists_by_name($name){
		$this->db->where('name',$name);

	    $query = $this->db->get('provinces');
	    if ($query->num_rows() > 0){
	    	//get the id 
	    	foreach ($query->result() as $row)
			{
			    return $row->id;
			}
	    }
	    else{
	        return false;
	    }
  	}

  	function search($limit=0,$page=0,$sidx='',$sord='',$searchval=array()){
  	
		if($this->is_count){
			$this->db->select("count(id) as total");
		}

		if(!$this->is_count){
			if($limit&&$page) {
				$offset = ($page-1)*$limit;
				if(!$offset)
					$offset = 0;
				$this->db->limit($limit,$offset);
			}
	  			
		if($sidx)
				$this->db->order_by($sidx,$sord);
		}
		
		$this->db->from('provinces');
		
		$query = $this->db->get();
		
 		return $query->result();
    }
    
    /* countSearchUser */
    function countSearch($searchval=array()){
    	$this->is_count = true;
		
    	$res = $this->search(0,0,'','',$searchval);
    	
    	return $res[0]->total;
	}
	
}