<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['website_title']			= "Website Title";
$config['website_description']		= "Website Description";

/* grid option */
$config['rowNum']		= 25; //number of row per page
$config['rowList']		= "25,50,75"; //option for row per page
$config['rowHeight']	= 480;
$config['rowWidth']	= 1024;

//ckeditor config
$config['ck_height']    = 500;
$config['ck_width']     = 1024;
$config['message_delay']     = 2500;
$config['change_level'] = 5;

/*
$config['quarter'] = array(
	"1"		=>array(1,3)
	,"2"	=>array(4,6)
	,"3"	=>array(7,9)
	,"4"	=>array(10,12)
);
*/
$config['quarter'] = array(
	array(
		"value"		=> 1
		,"title"	=> "Quarter 1"
		,"months"	=> array(1,3)
	)
	,array(
		"value"		=> 2
		,"title"	=> "Quarter 2"
		,"months"	=> array(4,6)
	)
	,	array(
		"value"		=> 3
		,"title"	=> "Quarter 3"
		,"months"	=> array(7,9)
	)
	,	array(
		"value"		=> 4
		,"title"	=> "Quarter 4"
		,"months"	=> array(10,12)
	)
);

$config['year_start'] = 2014;

$config['participants'] = array(
	array("code" => "L","field"=>"male")
	,array("code" => "P","field"=>"female")
	,array("code" => "W","field"=>"trans")
);

$config['beneficiaries'] = array(
	array("title" => "Direct Beneficiaries"		,"field"=>"direct_beneficiaries")
	,array("title" => "Indirect Beneficiaries"	,"field"=>"indirect_beneficiaries")
	,array("title" => "Government Official"		,"field"=>"government_official_beneficiaries")
);

$config['year_filter']	= array("2014","2015","2016");