<?php
/* for offline setting*/
$config['protocol'] 	= 'mail';
$config['smtp_host'] 	= "localhost";
$config['mailtype'] 	= 'html';
$config['charset'] 		= 'iso-8859-1';
$config['wordwrap'] 	= TRUE;

$config['crlf'] 		= "\r\n";
$config['newline'] 		= "\r\n";
$config['_smtp_auth']   = TRUE; 

$config['admin_email_address']          = "dedy.adhiewirawan@gmail.com";
$config['admin_email_error_subject']    = "An Error has been occured";

$config['sender_mail']     = 'noreply@programpeduli.org';
$config['sender_name']        = "No Reply";
