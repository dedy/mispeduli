<div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav ">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                       
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="<?=site_url('dashboard')?>"><i class="glyphicon glyphicon-home"></i><span>&nbsp;<?=lang('ldashboard')?></span></a>
                        </li>

                        <li class="nav-header"><?=lang('lactivity')?></li>

                          <li><a class="ajax-link" href="<?=site_url('activity')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lactivity')?> </span></a>
						  
                          <li><a class="ajax-link" href="<?=site_url('activity/report')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lreport_per_output')?></span></a>
                        </li>
                         <li><a class="ajax-link" href="<?=site_url('activity/beneficiary')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lreport_per_beneficiary')?></span></a>
                        </li>


                        <li class="nav-header"><?=lang('lmsc')?></li>

                          <li><a class="ajax-link" href="<?=site_url('msc')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lmsc')?> </span></a>
                          <li><a class="ajax-link" href="<?=site_url('msc/report')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lmsc_report')?> </span></a>
                          
                        <li class="nav-header"><?=lang('lnarrative')?></li>

                          <li><a class="ajax-link" href="<?=site_url('narrative')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lnarrative_report')?> </span></a>
                        
                         <li class="nav-header"><?=lang('lqa')?></li>

                          <li><a class="ajax-link" href="<?=site_url('qa')?>"><i class="glyphicon glyphicon-list-alt"></i><span>&nbsp;<?=lang('lqa')?> </span></a>

                        <li class="nav-header hidden-md">Master Data</li>
                         <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Master Data</span></a>
                            <ul class="nav nav-pills nav-stacked">

                                 <li>
                                    <a  href="<?=site_url('organization/cso')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lcso')?>
                                   </a>
                                </li>

                                <li>
                                    <a  href="<?=site_url('domain')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('ldomain')?>
                                   </a>
                                </li>
                                
                                 <li>
                                    <a  href="<?=site_url('organization/eo')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('leo')?>
                                   </a>
                                </li>
                                
                                <li>
                                    <a  href="<?=site_url('intermediate_output')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lintermediate_output')?>
                                   </a>
                                </li>

                                 <li>
                                    <a  href="<?=site_url('kabupaten_kota')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('ltown_kabupaten')?>
                                   </a>
                                </li>

                                <li>
                                    <a  href="<?=site_url('objective')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lobjective')?>
                                   </a>
                                </li>
                                <li>
                                    <a  href="<?=site_url('outcome')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('loutcome')?>
                                   </a>
                                </li>
                                
                                <li>
                                    <a  href="<?=site_url('output')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('loutput')?>
                                   </a>
                                </li>
                                <li>
                                    <a  href="<?=site_url('question')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lqa_question')?>
                                   </a>
                                </li>
                                <li>
                                    <a  href="<?=site_url('pilar')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lpilar')?>
                                   </a>
                                </li>

                                 <li>
                                    <a  href="<?=site_url('province')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lprovince')?>
                                   </a>
                                </li>
                                <li>
                                    <a  href="<?=site_url('activity_type')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('lactivity_type')?>
                                   </a>
                                </li>
                                 <li>
                                    <a  href="<?=site_url('user')?>">
                                        <i class="glyphicon glyphicon-list-alt"></i>
                                        <?=lang('luser')?>
                                   </a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                    <div align='center'>
                      <a href='http://seefar.org/' target='_blank'><img src='<?=base_url()?>assets/img/seefar-logo.png' width='150px' border='0'></a>
                      </div>
                </div>
            </div>
        </div>
        <!--/span-->