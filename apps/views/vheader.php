<!-- jquery confirm -->
<script src="<?=assets_url()?>js/jquery.confirm.min.js"></script>

<script>

     jQuery().ready(function (){
            $("#logoutid").confirm({
                title:"Logout confirmation",
                text:"<?=lang('lconfirm_logout')?>",
                confirm: function(button) {
                    //alert("You just confirmed.");
                    window.location.href = '<?=site_url('auth/logout')?>';
                },
                cancel: function(button) {
                   // alert("You aborted the operation.");
                },
                confirmButton: "OK",
                cancelButton: "Cancel"
            });
        
    
    });
        
        

</script>
<div class="navbar-inner">
    <button type="button" class="navbar-toggle pull-left animated flip">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?=site_url('/')?>"> <img alt="MIS Peduli" src="<?=assets_url()?>img/logo.png" class="hidden-xs"/>
        <span>MIS Program Peduli 2014 - 2016</span></a>

    <!-- user dropdown starts -->
    <div class="btn-group pull-right">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?=_UserName()?></span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <!-- <li><a href="<?=base_url()?>user/change_password/"><?=lang('lchange_password')?></a></li>
            <li class="divider"></li>-->
            <li><a id="logoutid" href="#"><?=lang('llogout')?></a></li>
        </ul>
    </div>
    <!-- user dropdown ends -->

</div>