<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>MIS Program Peduli 2015-2016</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Dedy Adhiewirawan">

    <!-- The styles -->
    <link id="bs-css" href="<?=assets_url()?>css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?=assets_url()?>css/charisma-app.css?v1.1" rel="stylesheet">
    <link href="<?=assets_url()?>css/custom-app.css?v1" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?=assets_url()?>bower_components/jquery/jquery.min.js"></script>

    <script src="<?=assets_url()?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?=assets_url()?>img/favicon.ico">
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            data: {
                csrfpedulitoken : $.cookie('csrfpedulicookie')
            }
        });

        $(document).ajaxError(function() {
            alert('There is something wrong with the system. \r\n If the problem persist, please contact administrator');
            //window.location.reload(true);
        });

         $(document).ajaxStart(function() {
             $("#ajaxStatusDisplay_userMessage").show();
             
        });

        $( document ).ajaxStop(function() {
             $("#ajaxStatusDisplay_userMessage").hide();
        });
        
    });
    </script>   
    <style type="text/css">
    .ajaxStatusDisplay_userStyle {
        display: none; 
        z-index: 999;
        position:fixed;
        left:45%;top:0px;height:20;
        background-color:#F2F760;
        color:black;
        padding-left: 5px;
        padding-right: 5px;
        width:75px;
        font-weight: normal;
        font-family:Arial, Helvetica, sans-serif;
      
    }

 /*body {
    padding-top: 75px;
  }*/
    </style>
</head>

<body>
    <div id="ajaxStatusDisplay_userMessage" class='ajaxStatusDisplay_userStyle'>
    Loading...
    </div>
    <!-- topbar starts -->
    <!-- <div class="navbar navbar-default navbar-fixed-top" role="navigation">-->
    <div class="navbar navbar-default" role="navigation">
    	<?=$header?>
    </div>
    <!-- topbar ends -->
	<div class="ch-container">
	    <div class="row">
	        
	        <!-- left menu starts -->
	        <?=$menunavigation?>
	        
	        <!-- left menu ends -->

	        <noscript>
	            <div class="alert alert-block col-md-12">
	                <h4 class="alert-heading">Warning!</h4>

	                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
	                    enabled to use this site.</p>
	            </div>
	        </noscript>

	        <div id="content" class="col-lg-10 col-sm-10">
	            <!-- content starts -->
	            <?=$content?>
				<!-- content ends -->
		    </div><!--/#content.col-md-0-->
		</div><!--/fluid-row-->

	    <hr>


	    <footer class="row">
	        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://programpeduli.org" target="_blank">Program Peduli</a> 2017</p>
	    </footer>

	</div><!--/.fluid-container-->

<!-- external javascript -->

<!-- library for cookie management -->
<script src="<?=assets_url()?>js/jquery.cookie.js"></script>

<!-- history.js for cross-browser state change on ajax -->
<script src="<?=assets_url()?>js/jquery.history.js"></script>

<!-- application script for Charisma demo -->
<script src="<?=assets_url()?>js/app.js?v0.1"></script>


</body>
</html>
