<?php

class MY_Controller extends MX_Controller{

 	var $viewparams;
	
	function __construct(){
		
		parent::__construct();
		
		header("Cache-Control: no-cache, no-store, must-revalidate");
    	header("Pragma: no-cache"); 
    	date_default_timezone_set('Asia/Jakarta');

        //-- for debugging
		$this->firephp->setEnabled(config_item('debug'));
        
		$this->viewparams['website_title']   		= config_item('website_title');
        $this->viewparams['website_description']   	= config_item('website_description');
        
        //get from config file
		//default total data per page
		$this->viewparams['rowNum']	= config_item('rowNum');
		
		//option for total data per page 
		$this->viewparams['rowList'] =  config_item('rowList');
		
		//option for grid height
		$this->viewparams['rowHeight'] =  config_item('rowHeight');
		
	
    	//option for grid width
		$this->viewparams['rowWidth'] =  config_item('rowWidth');
	}
	
    
    /* backend view */
    function viewpage($page){

        $this->viewparams['header'] 		= $this->load->view("vheader", $this->viewparams, true);		
		$this->viewparams['menunavigation'] = $this->load->view("vmenu", $this->viewparams, true);		
		$this->viewparams['content'] 		= $this->load->view($page, $this->viewparams, true);
		$this->load->view("vtemplate", $this->viewparams);	
	
	}
	
	/* backend ajax */
	function viewajax($page){
    	$themes = config_item('themes');
		if($themes){
			$this->load->view($themes."/".$page, $this->viewparams);	
		}else{
			$this->load->view($page, $this->viewparams);	
		}
	}
	
	
	
	/* backend popup view */
	function viewpopup($page){
		$themes = config_item('themes');
		if($themes){
			$this->viewparams['content'] 	= $this->load->view($themes."/".$page, $this->viewparams);	
		}else{
			$this->viewparams['content'] 	= $this->load->view($page, $this->viewparams);
		}
		$this->load->view("admin/vtemplate_popup", $this->viewparams);	
	}
	
	function loadDataJqGrid(){
		
		$page = isset($this->DataJqGrid['page'])?$this->DataJqGrid['page']:1; // get the requested page
        $limit = isset($this->DataJqGrid['limit'])?$this->DataJqGrid['limit']:config_item('rowNum'); // get how many rows we want to have into the grid
        $sidx = isset($this->DataJqGrid['sidx'])?$this->DataJqGrid['page']:''; // get index row - i.e. user click to sort
        $sord = isset($this->DataJqGrid['sord'])?$this->DataJqGrid['sord']:''; // get the direction
 
        if(!$sidx) $sidx =1;
        
       	$query = $this->DataJqGrid['query'];
       	$count = $this->DataJqGrid['count'];
       	
       	if($limit > 0){
	        if( $count > 0 ) {
	            $total_pages = ceil($count/$limit);    //calculating total number of pages
	        } else {
	            $total_pages = 0;
	        }
	    }else{
	    	$total_pages = 1;
	    }
	    
        if ($page > $total_pages) $page=$total_pages;
 
        $start = $limit*$page - $limit; // do not put $limit*($page - 1)
        $start = ($start<0)?0:$start;  // make sure that $start is not a negative value
 		
 		$responce = new stdClass();
        $responce->page = $page;
        $responce->total = $total_pages;
        $responce->records = $count;
        $i=0;
        $offset = ($page-1)*$limit;
        
        $id = $this->DataJqGrid['id'];
        
        foreach($query as $row) {
            $responce->rows[$i]['id']=$row->$id;
            
            $responce->rows[$i]['cell'][0] = $offset+$i+1;
            $j=1;
            foreach($this->DataJqGrid['column'] as $rowcols){
            	$responce->rows[$i]['cell'][$j] = $row->$rowcols;
            	$j++;
            }
            $i++;
        }
        echo json_encode($responce);
    }
    
	
}

?>
