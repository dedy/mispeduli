###################
MIS Peduli
###################

MIS Peduli is an Application using Codeigniter 3.1.3 , with Charisma Admin Template
This Application provides reports for programpeduli.org activities

Default login :  admin / password.

*******************
Server Requirements
*******************

PHP version 5.6 or newer is recommended.
MYSQL version 5.x

*****************
Command line
*****************
To import output 	:  /usr/bin/php index.php import output

To import eo/cso 	:  /usr/bin/php index.php import organization
To import msc 		:  /usr/bin/php index.php import msc
To import narrative 	:  /usr/bin/php index.php import narrative

To import activity 	:  /usr/bin/php index.php import activity


Report security issues to `Dedy <mailto:dedy@geraiweb.com>`_ , thank you.
