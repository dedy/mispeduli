drop table msc_domain;
ALTER TABLE  `msc` CHANGE  `change_level`  `domain_id` TINYINT( 1 ) UNSIGNED NULL ;
UPDATE `msc` SET  `domain_id` = NULL WHERE  `msc`.`domain_id` = 0;
ALTER TABLE `msc`
  ADD CONSTRAINT `fk_msc_domain_id` FOREIGN KEY (`domain_id`) REFERENCES `domain_perubahan` (`id`);

