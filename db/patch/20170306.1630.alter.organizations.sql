ALTER TABLE  `organizations` ADD  `target_group` VARCHAR( 255 ) NULL AFTER  `person_in_charge` ,
ADD  `total_target_group` SMALLINT( 5 ) UNSIGNED NULL AFTER  `target_group` ;
ALTER TABLE  `organizations` ADD  `pilar_id` INT( 11 ) UNSIGNED NOT NULL AFTER  `total_target_group` ;
ALTER TABLE  `organizations` CHANGE  `pilar_id`  `pilar_id` INT( 11 ) UNSIGNED NULL ;
UPDATE organizations SET pilar_id = NULL ;
ALTER TABLE `organizations`
  ADD CONSTRAINT `fk_organization_pilar_id` FOREIGN KEY (`pilar_id`) REFERENCES `pilar` (`id`);

ALTER TABLE `msc`
  ADD CONSTRAINT `fk_msc_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`);